        <footer>
		<section class="ftr-area ftr--blind" id="footer">
			<div class="container">
                   
                <div class="footer-box footer-list clrlist nav-cntr">
				     <div class="footer-img text-center"><img src="images/footer-logo.png" alt=""/></div> 
				     <ul>
						<?php 
                                                $menus = getMenus();
                                                while ($row = fetch_object($menus)) 
                                                {
                                                ?>
                                                    <li><a href="<?php echo ($row->code!="")?$row->code:$row->url; ?>"><?php echo $row->menu_name; ?></a></li>
                                                <?php
                                                }
                                                ?>
					 </ul>
                </div>
          
                <div class="footer-box col-sm-3 address-list clrlist listview">
                         <ul>
                         <li>
                         <i class="fa fa-map-marker" aria-hidden="true"></i> 
                         <a href="#"><?php  $result = getFooter('address'); 
                                        $row = fetch_object($result);
                                        
                                        echo $row->body;
                                 ?></a>
                         </li>
                         </ul> 
                </div>	
                <div class="footer-box col-sm-3 col-sm-offset-2 address-list clrlist listview">
				     <ul>
					     <li>
						 <i class="fa fa-envelope-o" aria-hidden="true"></i> 
						 <?php  
                                                        $result = getFooter('email'); 
                                                        $row = fetch_object($result);

                                                        echo $row->body;
                                                 ?>
						 </li>
					 </ul> 
                </div>
                <div class="footer-box col-sm-2 col-sm-offset-2 address-list  clrlist listview">
				     <ul>
					     <li>
						 <i class="fa fa-phone" aria-hidden="true"></i> 
						 <?php  
                                                        $result = getFooter('phone'); 
                                                        $row = fetch_object($result);

                                                        echo $row->body;
                                                 ?>
                                                
						 </li>
						 
					 </ul> 
                </div>				
			
			</div>
		</section>
		
		<section class="copyright-area">
		<div class="container">
		     <div class="copyright-box col-sm-4  pull-left">
			     <p>&copy; <?php echo date("Y"); ?> RitzyNails All Rights Reserved.</p>
				  
			 </div>
		     <div class="copyright-box col-sm-4 social-list  clrlist text-center">
			     <ul>
				     <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
				     <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
				     <li><a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
				     <li><a href="#"><i class="fa fa-rss" aria-hidden="true"></i></a></li>					 
				 </ul>
				  
			</div>		
                    
                    <div class="copyright-box col-sm-3  pull-right clrlist text-right">
                        <p>Design & Developed by <a href="http://www.golpik.com/" target="_blank">Golpik</a></p>
                    </div>    
		</div>
		</section>
		

		
	</footer>
	
			<a href="" class="scrollToTop"><i class="fa fa-arrow-up"></i></a>
	
</main>
    <!--Bootstrap-->
    <script src="script.js" type="text/javascript"></script>  
	<!--Bootstrap-->
    <script src="js/bootstrap.min.js"></script>
	<!--./Bootstrap-->
	
	<!--Major Scripts-->
	<script src="js/viewportchecker.js"></script>
    <script src="js/kodeized.js"></script>
	<!--./Major Scripts-->
	        

	
		</body>
</html>