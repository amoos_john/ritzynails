<?php 
include_once('config/connection.php');
include_once('config/model.php');
include_once('config/functions.php');

$code = charEsc($_GET["code"]);
$result= getContent($code);
if(count($result)>0)
{
    $content = fetch_array($result);

    $pagetitle=$content["title"];
    $body=$content["body"];
    $banner=$content["image"];
    $description=$content["metaDescription"];
    $keywords=$content["keywords"];
}
else {
    
    $pagetitle="404";
    $body="<p>Page Not Found!</p>";
    $banner = "images/slider.jpg";
    
}
include("header.php");
?>

<section class="inr-bnr-area ster-bnr clrlist" style="background-image: url(<?php echo $banner;?>);">
	<div class="container">
		<div class="inr-bnr-cont">
			
		</div>
	</div>
</section>

<section class="ster-cont-area">
	<div class="container">
		<div class="ster__cont__title col-sm-12">
			<h1><?php echo $pagetitle; ?></h1>
		</div>
		<div class="ster__cont__lft col-sm-12">
                    <?php echo $body; ?>
		</div>
			
	</div>
</section>


<?php //include("subscribe.php"); ?>

	
<?php include("footer.php"); ?>