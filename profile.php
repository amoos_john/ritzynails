<?php 
include_once('config/connection.php');
include_once('config/session.php');
include_once('config/model.php');
include_once('config/functions.php');

$userid=$_SESSION["user"];

$result=getProfile($userid);
if(!$result)
{
     echo "<script type='text/javascript'>window.location='index.php';</script>";
     exit();
}
 else {
     
    $user= fetch_array($result);
    $name=isset($user['name']) ? $user['name'] : '' ;
    $last_name=isset($user['last_name']) ? $user['last_name'] : '' ;
    $email=isset($user['email']) ? $user['email'] : '' ;
    $postal_code=isset($user['postal_code']) ? $user['postal_code'] : '' ;  
    $phone=isset($user['phone']) ? $user['phone'] : '' ; 
    $address=isset($user['address']) ? $user['address'] : '' ;
    
}

if(isset($_POST["update_profile"]))
{
        $userid=charEsc($_SESSION["user"]);
        $name = charEsc($_POST["full_name"]);
        $last_name = charEsc($_POST["last_name"]);
        $email = charEsc($_POST["email"]);
	$pass = charEsc($_POST["password"]);
        $confirm_pass = charEsc($_POST["confirm_password"]);
        $postal_code = charEsc($_POST["postal_code"]);
        $phone= charEsc($_POST["phone"]);
        $address = charEsc($_POST["address"]);
       
       
        /* Form Validation */
        $checkname=ValidateName($name);
        $checklastname=ValidateName($last_name);
        $checkemail=ValidateEmail($email);
        $checkpass=ValidatePassword($pass);
        $checkconfimpass=ValidatePassword($confirm_pass);
        $checkpostal=ValidatePostal($postal_code);
        $checkphone=ValidatePhone($phone);
        
       
        if(!empty($checkname))
        {
            $_SESSION["regerror"] = $checkname;	
           
        }
        else if(!empty($checklastname))
        {
            $_SESSION["regerror"] = $checklastname;	
              
        }
        else if(!empty($checkemail))    
	{
            $_SESSION["regerror"] = $checkemail;	
	}
        else if(!empty($checkpostal))
	{
            $_SESSION["regerror"] = $checkpostal;    
            
	}
        else if(!empty($checkphone))
	{
            $_SESSION["regerror"] = $checkphone;
            
	}
        else if(!empty($checkpass))
        {
            
            if(!empty($checkpass))
            {
                $_SESSION["regerror"] = $checkpass;   

            }
            else if(!empty($checkconfimpass))
            {
                $_SESSION["regerror"] = $checkconfimpass;    
                
            }
            else if($pass!=$confirm_pass)
            {
                 $_SESSION["regerror"] = "Your password and confirm password didn't matched!"; 
                 
            }
           
        }
	
        
        else
        {
          
            $mDate=date("Y-m-d H:i:s");
           
            if(!empty($pass))
            {
                $password= md5($pass);
                $passquery=query("UPDATE `users` SET `password`='{$password}',
                `modified_date`='{$mDate}' WHERE `id`='{$userid}'");
            }
            
            $sql= "select * from `users` where email='{$email}'";
		
	    $result = query($sql);
            if(num_rows($result)>0)
            {
                $row= fetch_assoc($result);
                if($userid==$row["id"])
                {
                    $query=query("UPDATE `users` SET `name`='{$name}',`last_name`='{$last_name}',`email`='{$email}',
                    `phone`='{$phone}',`address`='{$address}',`postal_code`='{$postal_code}',`modified_date`='{$mDate}' WHERE `id`='{$userid}'");
                    
                    if($query)
                    {
                        $_SESSION['success']="Profile updated successfully!";
                    }
                    else
                    {
                        $_SESSION['regerror']="Profile cannot update!";
                    }
                }    
                else
                {
                    $_SESSION['regerror']="Email is already exist!";
                }
                
            }
            else
            {
                    $query=query("UPDATE `users` SET `name`='{$name}',`last_name`='{$last_name}',`email`='{$email}',
                    `phone`='{$phone}',`address`='{$address}',`postal_code`='{$postal_code}',`modified_date`='{$mDate}' WHERE `id`='{$userid}'");
                    
                    if($query)
                    {
                        $_SESSION['success']="Profile updated successfully!";
                    }
                    else
                    {
                        $_SESSION['regerror']="Profile cannot update!";
                    }
            }
            
            
            
        }
        
    
}

$pagetitle="Profile";
include("header.php");
?>

<section class="edit-profile-area">

	<div class="container">
		<div class="edit__title col-sm-12">
			<div class="hed">
				  <h2>Profile <span></span></h2>
			 </div>
		</div>
		<div class="edit__profile__inr clrlist text-center col-sm-8">
                       <?php 
                            if(isset($_SESSION['regerror']))
                            {
                                ?>
                            <div class="alert alert-danger fade in alert-dismissable" style="margin-top:18px;">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
                            <i class="fa fa-times-circle"></i> <?php echo $_SESSION['regerror']; ?> </div>
                                <?php
                               unset($_SESSION['regerror']);      
                                
                            }
                             if(isset($_SESSION['success']))
                            {
                                ?>
                            <div class="alert alert-success fade in alert-dismissable" style="margin-top:18px;">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
                            <i class="fa fa-check-circle"></i> <?php echo $_SESSION['success']; ?> </div>
                                <?php
                               unset($_SESSION['success']);      
                                
                            }
                         ?>
                    
                    <form method="post" action="profile.php"  class="form-horizontal" name="register-form" id="register-form">

				<div class="form-group">
				  <label class="control-label col-sm-2" for="email">Name:</label>
				  <div class="col-sm-10">
                                        <input type="text" name="full_name" id="full_name" class="form-control" placeholder="Name"  maxlength="60" required value="<?php echo $name; ?>"/>
				  </div>
				</div>
                                <div class="form-group">
				  <label class="control-label col-sm-2" for="email">Last Name:</label>
				  <div class="col-sm-10">
                                        <input type="text" name="last_name"  id="last_name" class="form-control" placeholder="Last Name"  maxlength="60" required value="<?php echo $last_name; ?>"/>
				  </div>
				</div>
				<div class="form-group">
				  <label class="control-label col-sm-2" for="email">Email:</label>
				  <div class="col-sm-10">
                                      <input type="email" class="form-control" name="email" id="email" value="<?php echo $email; ?>" maxlength="60" required/>
				  </div>
				</div>
				<div class="form-group">
				  <label class="control-label col-sm-2" for="pwd">Password:</label>
				  <div class="col-sm-10">          
					<input type="password" name="password"  id="password" class="form-control"  placeholder="Password" maxlength="60" />
				  </div>
				</div>
				<div class="form-group">
				  <label class="control-label col-sm-2" for="pwd">Confirm Password:</label>
				  <div class="col-sm-10">          
					<input type="password" name="confirm_password"  id="confirm_password" class="form-control"  placeholder="Confirm Password" maxlength="60" />
				  </div>
				</div>
				
				
				<div class="form-group">
				  <label class="control-label col-sm-2" for="email">Postal code:</label>
				  <div class="col-sm-10">
					<input type="text" name="postal_code"  id="postal_code" class="form-control" placeholder="Postal code" maxlength="10" value="<?php echo $postal_code; ?>" required/>
				  </div>
				</div>
                            <div class="form-group">
				  <label class="control-label col-sm-2" for="email">Phone:</label>
				  <div class="col-sm-10">
					<input type="text" name="phone"  id="phone" class="form-control" maxlength="15" placeholder="(XXX) XXX-XX-XX"  value="<?php echo $phone; ?>" required/>
				  </div>
				</div>
                            <div class="form-group">
				  <label class="control-label col-sm-2" for="email">Address:</label>
				  <div class="col-sm-10">
					<textarea type="text"  name="address"  id="address" class="form-control" placeholder="Address" rows="4" maxlength="60" required ><?php echo $address; ?></textarea>
				  </div>
				</div>
                              
				<div class="form-group">        
				  <div class="col-sm-offset-2 edit-update-btn edit-btn-lft col-sm-1">
                                      
					<button type="submit" name="update_profile" class="btn btn-default">Update</button>
				  </div>
				  <div class="col-sm-offset-1 edit-btn-lft col-sm-1">
                                      <a href="dashboard.php"  class="btn btn-warning">Cancel</a>
				  </div>
				</div>
			</form>
		</div>
	</div>

</section>
<script>
    $('#phone', '#register-form').keydown(function (e) {
		var key = e.charCode || e.keyCode || 0;
		$phone = $(this);

		// Auto-format- do not expose the mask as the user begins to type
		if (key !== 8 && key !== 9) {
			if ($phone.val().length === 4) {
				$phone.val($phone.val() + ')');
			}
			if ($phone.val().length === 5) {
				$phone.val($phone.val() + ' ');
			}			
			if ($phone.val().length === 9) {
				$phone.val($phone.val() + '-');
			}
      if ($phone.val().length === 12) {
				$phone.val($phone.val() + '-');
			}
		}

		// Allow numeric (and tab, backspace, delete) keys only
		return (key == 8 || 
				key == 9 ||
				key == 46 ||
				(key >= 48 && key <= 57) ||
				(key >= 96 && key <= 105));	
	})
	
	.bind('focus click', function () {
		$phone = $(this);
		
		if ($phone.val().length === 0) {
			$phone.val('(');
		}
		else {
			var val = $phone.val();
			$phone.val('').val(val); // Ensure cursor remains at the end
		}
	})
	
	.blur(function () {
		$phone = $(this);
		
		if ($phone.val() === '(') {
			$phone.val('');
		}
	});
        


        
   
  
</script>

<?php include("footer.php"); ?>