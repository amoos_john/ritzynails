<?php 
include_once('config/connection.php');
include_once('config/session.php');
include_once('config/model.php');
include_once('config/functions.php');


$user_id=$_SESSION["user"];

$orders=getOrders($user_id);

$pagetitle="Orders";
include("header.php");
?>

<section class="order-area">

	<div class="container">
		<div class="edit__title text-center col-sm-12">
			<div class="hed">
				  <h2>Orders<span></span></h2>
			 </div>
		</div>
		<div class="order__table clrlist text-center col-sm-12">
                    <?php 
                    if(num_rows($orders)>0)
                    {
                        ?>
                    
			<table class="table table-striped table-bordered order__table__inr">
				<thead>
					<tr>
						
						<th>Order ID</th>
						<th>Payment ID</th>
                                                <th>Order Status</th>
						<th>Order Date</th>						<th>Order Total</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
                                    <?php 
                                    while($order= fetch_object($orders))
                                    {
                                       
                                    ?>        
					<tr class="" data-toggle="collapse" data-target="#accordion">
						<td><?php echo $order->id; ?></td>
						<td><?php echo $order->payment_id; ?></td>
                                                <td><?php echo $order->order_status; ?></td>
						<td><?php echo DateFormat($order->created_date); ?></td>
                                                <td><?php echo PriceFormat($order->grand_total); ?></td>

						<td class="order__actions">
				
							<a href="order-detail.php?id=<?php echo $order->id; ?>" class="btn btn-book">View</a>
							<!--<a href="#" class="btn btn-danger">Cancel</a>-->	
						</td>
                                                
					</tr>
					
                                    <?php
                                    }
                                    ?>
				</tbody>
			</table>
                     <?php
                      }
                      else
                      {
                    ?>
                        <div class="alert alert-success fade in alert-dismissable">
                        <h4 ><i class="fa fa-cart"></i> Order not found!</h4></div>
                    
                    <?php
                      }
                     
                    ?>
                    
		</div>
	</div>

</section>


<?php include("footer.php"); ?>