<?php
include_once('config/connection.php');
include_once('config/model.php');
include_once('config/functions.php');
include_once('config/constants.php');

// Check if paypal request or response
if (isset($_POST["checkout"])){
    
  if(isset($_SESSION["cart"]) && isset($_SESSION["user"]))
  {
    $sess_id=$_SESSION["cart"];
    $checkouts=ServicesCart($sess_id);
    
    $querystring = '';
 
    // Firstly Append paypal account to querystring
    $querystring .= "?business=".urlencode($paypalID)."&";
 
 
    //loop for posted values and append to querystring
    $count=1;
    while($checkout = fetch_object($checkouts))
    {
        $value1 = urlencode(stripslashes($checkout->service_name));
        $key1   = "item_name_".$count;  
        $value2 = urlencode(stripslashes($checkout->service_id));
        $key2   = "item_number_".$count;
        $value3 = urlencode(stripslashes($checkout->total_price));
        $key3   = "amount_".$count;
        $value4 = '1';//urlencode(stripslashes($checkout->persons))
        $key4   = "quantity_".$count;
        
        $querystring .= "$key1=$value1&";
        $querystring .= "$key2=$value2&";
        $querystring .= "$key3=$value3&";
        $querystring .= "$key4=$value4&";
        $count++;
    }
    if(isset($_POST["park_fee"]))
       {
         
           $_SESSION["park_fee"]=$_POST["park_fee"];
           $key1   = "item_name_".$count; 
           $value1 = urlencode(stripslashes("Parking Fee"));
           
           $value3 = urlencode(stripslashes($_POST["park_fee"]));
           $key3   = "amount_".$count;
          
           $querystring .= "$key1=$value1&";
           $querystring .= "$key3=$value3&";
       }
    if(isset($_POST["grand_total"]))
    {
        $_SESSION["grand_total"]=$_POST["grand_total"];
        $_SESSION["address"]=$_POST["address"];
    }
   foreach($_POST as $key => $value) {
          
           $value = urlencode(stripslashes($value));
           $querystring .= "$key=$value&";  
    }
 
    // Append paypal return addresses      
    //$notify_url=$siteUrl."/checkout.php";        
    $querystring .= "return=".$siteUrl."/".$paypalSuccess."&";
    $querystring .= "cancel_return=".$siteUrl."/".$paypalCancel;
    //$querystring .= "notify_url=".$notify_url;
 
    // Redirect to paypal IPN
    header('location:'.$paypalURL.$querystring);
    exit();
    }
} else {
  
    print_R("PAK");DIE();
    
}
?>