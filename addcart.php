<?php
extract($_POST);
include_once('config/connection.php');
include_once('config/model.php');

if(isset($additem))
{
    $ser_id= charEsc($ser_id);
    $persons= charEsc($person);
    $discount= charEsc($discount);
    $totalprice= charEsc($totalprice);
    $totaltime= charEsc($totaltime);
    //print_R($persons);die();
    
    if(!empty($ser_id) && !empty($persons) && !empty($totalprice))
    {
      $exist=getServicesId($ser_id); 
      if(count($exist)>0)
      {
        if(!isset($_SESSION["cart"]))
        {
            $session_id=session_id() ;
            //uniqid().time()
        }
        else
        {
            $session_id=$_SESSION["cart"];      
        }
        
        if($persons>=5)
        {
             echo '<div class="alert alert-danger fade in alert-dismissable">
                <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
                 <i class="fa fa-times-circle"></i>  Service Cannot added in Cart!</div>';
             exit;
        }
        
        $result= CheckPerson($session_id,$ser_id,$persons);
        
        if(count($result)>0)
        {      
            /*$mDate=date("Y-m-d H:i:s");
            $query=query("UPDATE `cart` SET `persons`='{$persons}',`discount`='{$discount}',`total_price`='{$totalprice}',
            `total_time`='{$totaltime}',`modified_date`='{$mDate}'
            WHERE `service_id`='{$ser_id}' and `session_id`='{$session_id}'");
            if($query)
            {
                echo '<div class="alert alert-success fade in alert-dismissable">
    <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
     <i class="fa fa-check-circle"></i> Service updated in cart. <strong><a href="appointment.php"> schedule an appointment!</a></strong></div>';

            }
            else
            {
                echo '<div class="alert alert-danger fade in alert-dismissable" style="margin-top:18px;">
    <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
     Service Cart Cannot updated</div>';
            } */
            
          echo '<div class="alert alert-success fade in alert-dismissable">
    <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
     <i class="fa fa-info-circle"></i> Service already exist!</div>';
            
        } 
        else 
        {
            $cDate=date("Y-m-d H:i:s");
            $addcart=array("session_id"=>$session_id,"service_id"=>$ser_id,"persons"=>$persons,"discount"=>$discount,"total_price"=>$totalprice,"total_time"=>$totaltime,"created_date"=>$cDate);
            $query= insert('cart',$addcart);
            if($query)
            {
                if(isset($_SESSION["review"]))
                {
                    DeleteBooking($session_id);
                }
                if(!isset($_SESSION["cart"]))
                {
                   $_SESSION["cart"]=$session_id;
                }
                
                 echo '<div class="alert alert-success fade in alert-dismissable">
                <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
                <i class="fa fa-check-circle"></i> Service added in cart.</div>';

                
            }
            else
            {
                echo '<div class="alert alert-danger fade in alert-dismissable">
                <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
                 <i class="fa fa-times-circle"></i>  Service Cannot added in Cart!</div>';
              
            }

        }
      }
      else
      {
            echo '<div class="alert alert-danger fade in alert-dismissable">
                <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
                 <i class="fa fa-times-circle"></i>  Service Cannot added in Cart!</div>';
              
       }
        
       
    }
    else
    {
          echo '<div class="alert alert-danger fade in alert-dismissable" >
                <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
                 <i class="fa fa-times-circle"></i> Service person or price is empty</div>';
    }
    
    
    exit(); 
        
}
if(isset($addproduct))
{
    $item_id= charEsc($item_id);
    $quan= charEsc($quan);
    $totalprice= charEsc($totalprice);
  
    if(!empty($item_id) && !empty($quan) && !empty($totalprice))
    {
        if(!isset($_SESSION["cart"]))
        {
            $session_id= uniqid().time();
            
        }
        else
        {
            $session_id=$_SESSION["cart"];      
        }
        
        $result= CheckService($item_id,$session_id);
       
        
        if(num_rows($result)>0)
        {      
            $mDate=date("Y-m-d H:i:s");
            $query=query("UPDATE `cart` SET `persons`='{$quan}',`total_price`='{$totalprice}',`modified_date`='{$mDate}'
            WHERE `service_id`='{$item_id}' and `session_id`='{$session_id}'");
            if($query)
            {
                echo '<div class="alert alert-success fade in alert-dismissable">
    <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
     <i class="fa fa-check-circle"></i> Product updated in cart. <strong><a href="cart.php"> view shopping cart!</a></strong></div>';

            }
            else
            {
                echo '<div class="alert alert-danger fade in alert-dismissable" style="margin-top:18px;">
    <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
     Product Cart Cannot updated</div>';
            } 
            
        } 
        else 
        {
            $cDate=date("Y-m-d H:i:s");
            $addcart=array("session_id"=>$session_id,"service_id"=>$item_id,"persons"=>$quan,"total_price"=>$totalprice,"created_date"=>$cDate);
            $query= insert('cart',$addcart);
            if($query)
            {
                if(!isset($_SESSION["cart"]))
                {
                   $_SESSION["cart"]=$session_id;
                }
                
                 echo '<div class="alert alert-success fade in alert-dismissable">
                <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
                <i class="fa fa-check-circle"></i> Product added in cart. <strong><a href="cart.php"> view shopping cart!</a></strong></div>';

                
            }
            else
            {
                echo '<div class="alert alert-danger fade in alert-dismissable">
                <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
                 <i class="fa fa-times-circle"></i>  Product Cannot added in Cart!</div>';
              
            }

        }
        
       
    }
    else
    {
          echo '<div class="alert alert-danger fade in alert-dismissable" >
                <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
                 <i class="fa fa-times-circle"></i> Product quantity or price is empty</div>';
    }
    
    
    exit(); 
        
}

if(isset($total_cart_items))
{
	$session_id=$_SESSION['cart'];
        
        if(!empty($session_id))
        {
            $query= query("SELECT COUNT(*) AS total_services FROM cart where session_id='{$session_id}'");
            if(num_rows($query)>0)
            {
                $row=fetch_assoc($query);
                echo $row["total_services"];
            }
            else
            {
                echo "0";
            }
            
        }
        else
        {
             echo "0";
             
        }
     	 exit();
}

    
?>