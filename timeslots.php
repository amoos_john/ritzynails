<?php
extract($_POST);
include_once('config/connection.php');
include_once('config/model.php');

if(isset($showtime))
{
    $date= charEsc($date);
   
    if(!empty($date))
    {
        $result=CheckTime($date);
        $timearray=array();
        $endarray=array();
       
        if($result)
        {
            while($row=fetch_object($result))
            {
                
                $start    = new DateTime($row->start_time);
                $end      = new DateTime($row->end_time);
                $interval = new DateInterval('PT1H');
                $period   = new DatePeriod($start, $interval, $end);
               foreach ($period as $time) {
                    $timearray[]= $time->format('g:i a');
                }
                
            }
        }
       
        
        $time = '09:00'; // start
        $change='';
        ?>
         
         <?php
         for ($i = 0; $i <= 11; $i++)
         {
             $next = strtotime('+1hour', strtotime($time)); // add 30 mins
             $time = date('g:i a', $next); // format the next time
             $Hour = date('G', strtotime($time));
             
             if($Hour>=10 && $Hour<=11 && $change!=1)
             {
                 ?>
             <div class="timeslots__box text-center col-sm-4">
			<div class="timeslots__inr">
			<div class="timeslots__hdr">
				<h4>Morning</h4>
			</div>
		<div class="timeslots__content">
			<ul>
                 <?php
                $change=1;
             }
             else if($Hour>=12 && $Hour<=18 && $change!=2)
             {
                 ?>   
                             </ul>
		</div>
		</div>
		</div>
                      <div class="timeslots__box text-center col-sm-4">
			<div class="timeslots__inr">
			<div class="timeslots__hdr">
                        <h4>Afternoon</h4>  
			</div>
                        <div class="timeslots__content">
			<ul>
                 <?php
                  $change=2;
             }
             else if($Hour>=19 && $Hour<=21 && $change!=3)
             {
                 ?>
                             </ul>
		</div>
		</div>
		</div>
                        <div class="timeslots__box text-center col-sm-4">
			<div class="timeslots__inr">
			<div class="timeslots__hdr">
                        <h4>Evening</h4>  
			</div>
                        <div class="timeslots__content">
			<ul>
                 <?php
                  $change=3;
             }  
            
            $getdt=$date." ".$time;
            $currdt=date('Y-m-d g:i a'); 
           
            if(in_array($time, $timearray))
            {
                echo '<li class="booked">Booked</li>';
            }
            else if($currdt>$getdt)
            {
                echo '<li>No Available Hours</li>';
            }
            else
            {
                echo '<li class="btn-time">'.$time.'</li>';
            }
             
         }
         
     
    ?>
                    
     <script>
     $(".btn-time").click(function(){
   
    var time=$(this).text();
    $('li.current').removeClass('current');
    $(this).addClass('current');
    $("#time").text(time);
    $("#booktime").val(time);
    $("#btn-next").attr("disabled", false);
    $("#btn-next").css("opacity", "1");
  
});</script>
     <?php
    }
}
    
?>