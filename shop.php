<?php 
include_once('config/connection.php');
include_once('config/model.php');
include_once('config/functions.php');

$products=getProducts();

$pagetitle="Shop";
include("header.php");

?>

<section class="shop-area">
	<div class="container">
	    <div class="hed text-center">
		    <h2>Shop</h2>
			<span><img src="images/flower-icon.png" alt=""/></span>
		</div>
            <div id="message"></div>
            <?php 
                $count=0;
                while($product = fetch_object($products))
                { 
                ?>
		 
		<div class="shop__box col-sm-4  imgzoom--hover">
			<div class="shop__inr">
				<div class="shop__img">
                                    <a href="product-page.php?id=<?php echo $product->id; ?>"><img src="<?php echo $product->image; ?>" alt="<?php echo $product->service_name; ?>"></a>
				</div>
				<div class="shop__cont">
					<a href="product-page.php?id=<?php echo $product->id; ?>"><h4><?php echo $product->service_name; ?></h4></a>
					<div class="pul-lft"><h5><span><?php echo ($product->old_price!=0)?'C'.PriceFormat($product->old_price):''; ?></span>C<?php echo PriceFormat($product->price); ?></h5></div>
					
                                        <div class="shop__cart__btn pul-rgt">
                                            <input type="hidden" id="item_id<?php echo $count; ?>" name="item_id" value="<?php echo $product->id; ?>"/>
                                            <input type="hidden" id="quan<?php echo $count; ?>" name="quan" value="1"/>
                                            <input type="hidden" name="total_price"  id="gettotalprice<?php echo $count; ?>" value="<?php echo $product->price; ?>"/>
                                            <button class="btn btn-book" id="btn-add-to-cart"onclick="productcart(<?php echo $count; ?>)">Add to Cart</button>
                                        </div>
                                        
				</div>
			</div>
		</div>
            <?php
                  $count++;
                  }
             ?>
		
		
		
	</div>
	</section>

<?php include("footer.php"); ?>