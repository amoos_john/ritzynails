<?php 
include_once('config/connection.php');
include_once('config/model.php');
if(isset($_SESSION["cart"]))
{
   unset($_SESSION['error']);
   $sess_id=$_SESSION["cart"];
   $person=$_SESSION["person"];
   $checkbook=Checkbooking($sess_id);
   $services=ServicesCartBooking($sess_id,''); 
   $cart=ServicesCart($sess_id,'');
   if(!$services)
   {
       echo "<script type='text/javascript'>window.location='index.php';</script>";
   }
}
else
{
    echo "<script type='text/javascript'>window.location='index.php';</script>"; 
    exit;
}

$pagetitle="Colors";
include("header.php");

?>
<style>
.btn-danger {
   margin-bottom:0px !important;
}
</style>
<section class="colors-area clrlist">
	<div class="container">
           <div class="hed">
		<h2><?php echo $pagetitle;  ?><span></span></h2>
           </div> 
		<div class="accordion-area accordion-arr col-sm-10 col-sm-offset-1">
       
                    <?php
            
              
                     if(count($checkbook)>0)
                     {
              ?>            
				<div class="row">
                                                            
                                 <div id="message"></div> 
                                 
                                <form method="post" id="save-color">
                                <div class="colors__service col-sm-6 p0">
                                <div class="col-sm-4 text-right">    
                                <label>Select Person</label>    
                                </div>
                                 <div class="col-sm-8">
                                     <select class="form-control" name="persons" id="persons" required="">
					<option value="">Select Person</option>
                                         <?php
                                        for($i=1;$i<=$person;$i++)
                                        {
                                        ?>
                                            <option value="<?php echo $i;?>"><?php echo $i;?></option>

                                        <?php
                                        }
                                        ?>
				</select>					
				 </div>				
                                </div>								
                                <div class="colors__person col-sm-6 p0">
                                     <div class="col-sm-4 text-right">   
                                     <label>Select Service</label>
                                      </div>
                                     <div class="col-sm-8">
                                    <select class="form-control" name="service" id="service" required="">
					<option value="">Select Service</option>
					
                                    </select>
                                   </div>	      
				</div>
                                
                               </form> 
                              <?php   
                                 if(count($cart)>0)
                                {
                               ?>

                               <div class="booktable-area table-responsive col-sm-9 col-sm-offset-2" >
                                   <div id="load"></div>  
                                   <div id="color-cart-summary"></div>
                               </div> 	         
                               <?php
                                }
                               ?>
                                
                                                            
                            </div>    
					
							
                    <p>Due to screen limitations, colors seen here may not accurately reflect nail colors.</p>
                
                                
					<div class="panel-group " id="accordion2">
					  
					  <div class="panel panel-default">
						<div class="panel-heading">
						  <h4 class="panel-title">
							<a data-toggle="collapse" data-parent="#accordion2" href="#collapseArr1">
							   1. Select the skin tone that closest resemble yours
							</a>
						  </h4>
						</div>
						<div id="collapseArr1" class="panel-collapse collapse">
						  <div class="panel-body">
							 <!-- Content -->
                
                <div id="skin-tone">
                    <div id="selected-tone"><img src="images/skin-color-3.png" alt=""></div>
                    <ul>
                        <li id="skin-color-1">&nbsp;</li>
                        <li id="skin-color-2">&nbsp;</li>
                        <li id="skin-color-3" class="on">&nbsp;</li>
                        <li id="skin-color-4">&nbsp;</li>
                        <li id="skin-color-5">&nbsp;</li>
                        <li id="skin-color-6">&nbsp;</li>
                    </ul>
                    
                <script>
                $(function(){
                    
                    var $tone = $('#skin-tone li');
                    $tone.on('click', function(){
                        var $this = $(this);
                        var $src = $this.attr('id');  
                        $tone.removeClass('on');	
                        $this.addClass('on');
                        $source = 'images/' + $src + '.png';
                        $('#selected-tone img').attr('src',$source);
                        var $src2 = $src.replace('skin-color-','hand-skin-');
                        $source2 = 'images/' + $src2 + '.png';
                        $('.selected-tone img').attr('src',$source2);
                    });
                    
                });
                </script>    
                </div>
                
                <!-- END: Content -->
						  </div>
						</div>
					  </div>
					  
					  <div class="panel panel-default">
						<div class="panel-heading">
						  <h4 class="panel-title">
							<a data-toggle="collapse" data-parent="#accordion2" href="#collapseArr2" class="collapsed">
							   2. Select your type of nails
							</a>
						  </h4>
						</div>
						<div id="collapseArr2" class="panel-collapse collapse">
						  <div class="panel-body">
 <!-- Content -->
                
                <div id="nail-shape">
                <ul>
                    <li id="shape-almond" class="on"><img src="images/shape-almond.png" alt="Almond"><span>Almond</span></li>
                    <li id="shape-ballerina"><img src="images/shape-ballerina.png" alt="Ballerina"><span>Ballerina</span></li>
                    <li id="shape-edge"><img src="images/shape-edge.png" alt="Edge"><span>Edge</span></li>
                    <li id="shape-lipstick"><img src="images/shape-lipstick.png" alt="Lipstick"><span>Lipstick</span></li>
                    <li id="shape-mountain"><img src="images/shape-mountain.png" alt="Mountain Peek"><span>Mountain Peek</span></li>
                    <li id="shape-oval"><img src="images/shape-oval.png" alt="Oval"><span>Oval</span></li>
                    <li id="shape-rounded"><img src="images/shape-rounded.png" alt="Rounded"><span>Rounded</span></li>
                    <li id="shape-square-oval"><img src="images/shape-square-oval.png" alt="Square Oval"><span>Square Oval</span></li>
                    <li id="shape-square"><img src="images/shape-square.png" alt="Square"><span>Square</span></li>
                    <li id="shape-stiletto"><img src="images/shape-stiletto.png" alt="Stiletto"><span>Stiletto</span></li>
                </ul>
				<script>
                $(function(){
                    
                    var $shape = $('#nail-shape li');
                    $shape.on('click', function(){
                        var $this = $(this);
                        var $src = $this.attr('id').replace('shape-','nail-');;		
                        $shape.removeClass('on');	
                        $this.addClass('on');
                        $source = 'images/' + $src + '.png';
                        $('.french img').attr('src',$source);
			$('.selected-nail img').attr('src',$source);
                    });
                    
                });
                </script>  
                </div>
                
                <!-- END: Content -->						  </div>
						</div>
					  </div>
					  <div class="panel panel-default">
						<div class="panel-heading">
						  <h4 class="panel-title">
							<a data-toggle="collapse" data-parent="#accordion2" href="#collapseArr3" class="collapsed">
							  3. Select your nail colors
							</a>
						  </h4>
						</div>
						<div id="collapseArr3" class="panel-collapse collapse">
						  <div class="panel-body">
							  <!-- Content -->
                    
                    <div id="nail-color">
                    <p id="error">Sorry, XML can't be read</p>
                    
                    <div id="selectedColors"><p><strong>Selected colors:</strong></p>
                        <div id="deleteMe"></div>
                    </div>
                    
                     <!--<<div>
                       label for="french"><input type="checkbox" id="french" name="french"> French manicure</label>
                    </div>-->

 
                    <div id="color-selector">
                        <div class="selected-tone"><img src="images/hand-skin-3.png" alt=""></div>
                        <div class="french"><img src="images/nail-almond.png" alt=""></div>
                        <div class="selected-nail"><img src="images/nail-almond.png" alt=""></div>
                        <div class="base-bkg"><img src="images/base-nail-color.png" alt=""></div>
                    </div>
                    <div class="wrapper">
                     <span id="chosenManicure"></span>
                    <input type="text" class="input-text form-control" id="search" placeholder="Search"/>
                    <p id="filter-count" class="help-block"></p> 
                    <br clear="all">
                    <div class="loader"></div>
                    <ul id="nail-polish"></ul>
                    </div>
		<script>                          
                    $(function(){
                        
                         /* $.ajax({
                            type: 'GET',
                            url: 'nail-polish-colour.php',
                            dataType: 'xml',
                            success: parseXml,
                            error : function(){
                                $('#error').show();
                            }
                        });*/
                   
           $('#persons').change(function(){
		
		var val = $(this).val();
		
		var datasend='person='+val;
                
                $('#deleteMe').html('');
                $('#chosenManicure').html('');
                $('#nail-polish').html('');
		
		$.ajax({
			
			type:"POST",
			data:datasend,
			url:'getperson.php',
			cache:false,
			success:function(msg){
				
				$('#service').html(msg);
                             
			}
			
		});
                
               
              
                
                
	});             
                     $('#service').change(function(){
                         var val = $(this).val();
		
                         var datasend='cartid='+val;
                         
                        $('#deleteMe').html('');
                        $('#chosenManicure').html('');
                       
                         
                        $("#nail-polish").empty();
                        $.ajax({
                            type: 'GET',
                            url: 'nail-polish-colour.php',
                            data:datasend,
                            dataType: 'xml',
                            success: parseXml,
                            error : function(){
                                $('#error').show();
                            }
                        });
                  
                    
                    }); 
                        
                        //Parsing the nail polish xml
                        function parseXml(xml)
                        {
                            $(xml).find('colour').each(function(){
                                var $colour = $(this);
                                var id = $colour.attr('id');
                                var img = $colour.attr('image');
				//var collection = $colour.attr('collection');
				var vinylux = $colour.attr('vinylux');
                                var color = $colour.attr('color');
                                
                                var title = $colour.text();
                                var thumb = img;
                                var image = img;//'images/'+ id +'.png';
								if(vinylux){
                                	thumbArray = '<li id="'+ id +'"><span>'+ title +'</span><img data-id="'+ id +'" src="'+ thumb +'" alt="'+ title +'" data-type="vinylux"></li>';
								} else {
									thumbArray = '<li id="'+ id +'"><span>'+ title +'</span><img data-id="'+ id +'" src="'+ thumb +'" alt="'+ title +'"></li>';
									}
                                $('#nail-polish').append(thumbArray);
                               
                                   //$('#nail-polish li:lt(6)').addClass('winter');
                                   //$('#nail-polish li').slice(6,12).addClass('fall');
                               
                               
				
                            });
                             
        var $img = $('#nail-polish img');
        
        $img.on('click', function(){
            var $this = $(this);
            var $title = $this.attr('alt');
            var $thumb = $this.attr('src');
            var $titleId = $title.replace(/ /g,'');
            var $thisId = $this.data('id');
            var $vinylux = $this.data('type');
            var $imgOn = $this.hasClass('on');
            var $imagesOn = $('#nail-polish img.on');

            //console.log('$imagesOn.length :' + $imagesOn.length);


            $('#nail-polish img').removeClass('on');
            $this.addClass('on');
            var background = 'url(' + $thumb + ')';
            $('.selected-nail').css('background-image',background);
            if($vinylux) { 
                    $('#deleteMe').html('');
                    $('#deleteMe').append('<a href="javascript:void(0)" class="deleteMe"><span id="' + $thisId + '" class="vinylux">' + $title + '</span></a>');
                    $('#chosenManicure').html('<span id="' + $thisId + '" class="vinylux">' + $title + '</span>');
                   
            } else { 
                    //$('#selectedColors').html('');
                   $('#deleteMe').html('');

                    $('#deleteMe').append('<a href="javascript:void(0)" class="deleteMe"><span id="' + $thisId + '" class="vinylux">' + $title + '</span></a>');
                    $('#chosenManicure').html('<span id="' + $thisId + '">' + $title + '</span>');
                     
            }

            var $color = $('.selection');	

            $color.on('click', function(){
                    var $this = $(this);
                    var $thisSelection = $this.data('select');
                    var $thisName = $this.data('name');
                    var $vinylux = $this.data('vinylux');
                    var $color = $this.data('id');
                    var $colorid = $this.data('color');

                    var $threeSelected = $('#selectedColors span');

                    $this.parent('span').html('');

                   
            if($threeSelected.length < 1) 
            {			
                    if($vinylux) 
                    {
                        $('#selectedColors').append('<a href="javascript:void(0)" class="deleteMe"><span id="' + $thisSelection + '" class="vinylux">' + $thisName + '</span></a>');

                    } 
                    else 
                    {
                            $('#selectedColors').append('<a href="javascript:void(0)" class="deleteMe"><span id="' + $thisSelection + '">' + $thisName + '</span></a>');	

                    }
            }


            $('#selectedColors .deleteMe').on('click', function(){
                    var $this = $(this);

                    //console.log($this);
                    $this.empty();
                    });
            });
								
         });
	$img.on({												
        dblclick: function(){
            var $this = $(this);
            var $title = $this.attr('alt');
            var $titleId = $title.replace(/ /g,'');
            var $thisId = $this.data('id');
            var $vinylux = $this.data('type');
            var $imgOn = $this.hasClass('on');
            var $imagesOn = $('#nail-polish img.on');

            //console.log('$imagesOn.length :' + $imagesOn.length);


            $('#nail-polish img').removeClass('on');
            $this.addClass('on');
            var background = 'url(images/' + $thisId + '-thumb.png)';
            $('.selected-nail').css('background-image',background);
            if($vinylux) { 
                    $('#deleteMe').html('');
                    $('#deleteMe').append('<a href="javascript:void(0)" class="deleteMe"><span id="' + $thisId + '" class="vinylux">' + $title + '</span></a>');
                    $('#chosenManicure').html('<span id="' + $thisId + '" class="vinylux">' + $title + '</span>');
                    var persons=$("#persons").val();
                    var cartid=$("#service").val();

                    addColor(persons,cartid,$thisId);    
                    $("html, body").animate({ scrollTop: 0 }, "slow");
                    return false;
            } else { 
                    //$('#selectedColors').html('');
                   $('#deleteMe').html('');

                    $('#deleteMe').append('<a href="javascript:void(0)" class="deleteMe"><span id="' + $thisId + '" class="vinylux">' + $title + '</span></a>');
                    $('#chosenManicure').html('<span id="' + $thisId + '">' + $title + '</span>');
                    var persons=$("#persons").val();
                    var cartid=$("#service").val();

                    addColor(persons,cartid,$thisId);    
                    $("html, body").animate({ scrollTop: 0 }, "slow");
                    return false;

            }

            var $color = $('.selection');	

            $color.on('click', function(){
                    var $this = $(this);
                    var $thisSelection = $this.data('select');
                    var $thisName = $this.data('name');
                    var $vinylux = $this.data('vinylux');
                    var $color = $this.data('id');
                    var $colorid = $this.data('color');

                    var $threeSelected = $('#selectedColors span');

                    $this.parent('span').html('');

                   
            if($threeSelected.length < 1) 
            {			
                    if($vinylux) 
                    {
                        $('#selectedColors').append('<a href="javascript:void(0)" class="deleteMe"><span id="' + $thisSelection + '" class="vinylux">' + $thisName + '</span></a>');

                    } 
                    else 
                    {
                            $('#selectedColors').append('<a href="javascript:void(0)" class="deleteMe"><span id="' + $thisSelection + '">' + $thisName + '</span></a>');	

                    }
            }


            $('#selectedColors .deleteMe').on('click', function(){
                    var $this = $(this);

                    //console.log($this);
                    $this.empty();
                    });
            });
								
                }, touch: function() {
                
                                var $this = $(this);
            var $title = $this.attr('alt');
            var $titleId = $title.replace(/ /g,'');
            var $thisId = $this.data('id');
            var $vinylux = $this.data('type');
            var $imgOn = $this.hasClass('on');
            var $imagesOn = $('#nail-polish img.on');

            //console.log('$imagesOn.length :' + $imagesOn.length);


            $('#nail-polish img').removeClass('on');
            $this.addClass('on');
            var background = 'url(images/' + $thisId + '-thumb.png)';
            $('.selected-nail').css('background-image',background);
            if($vinylux) { 
                    $('#deleteMe').html('');
                    $('#deleteMe').append('<a href="javascript:void(0)" class="deleteMe"><span id="' + $thisId + '" class="vinylux">' + $title + '</span></a>');
                    $('#chosenManicure').html('<span id="' + $thisId + '" class="vinylux">' + $title + '</span>');
                    var persons=$("#persons").val();
                    var cartid=$("#service").val();

                    addColor(persons,cartid,$thisId);    
                    $("html, body").animate({ scrollTop: 0 }, "slow");
                    return false;
            } else { 
                    //$('#selectedColors').html('');
                   $('#deleteMe').html('');

                    $('#deleteMe').append('<a href="javascript:void(0)" class="deleteMe"><span id="' + $thisId + '" class="vinylux">' + $title + '</span></a>');
                    $('#chosenManicure').html('<span id="' + $thisId + '">' + $title + '</span>');
                    var persons=$("#persons").val();
                    var cartid=$("#service").val();

                    addColor(persons,cartid,$thisId);    
                    $("html, body").animate({ scrollTop: 0 }, "slow");
                    return false;

            }

            var $color = $('.selection');	

            $color.on('click', function(){
                    var $this = $(this);
                    var $thisSelection = $this.data('select');
                    var $thisName = $this.data('name');
                    var $vinylux = $this.data('vinylux');
                    var $color = $this.data('id');
                    var $colorid = $this.data('color');

                    var $threeSelected = $('#selectedColors span');

                    $this.parent('span').html('');

                   
            if($threeSelected.length < 1) 
            {			
                    if($vinylux) 
                    {
                        $('#selectedColors').append('<a href="javascript:void(0)" class="deleteMe"><span id="' + $thisSelection + '" class="vinylux">' + $thisName + '</span></a>');

                    } 
                    else 
                    {
                            $('#selectedColors').append('<a href="javascript:void(0)" class="deleteMe"><span id="' + $thisSelection + '">' + $thisName + '</span></a>');	

                    }
            }


            $('#selectedColors .deleteMe').on('click', function(){
                    var $this = $(this);

                    //console.log($this);
                    $this.empty();
                    });
            });
    
                 }
	
        
        });
                            
        };
						
        $('#french').on('change', function(){
            $('.french img').toggleClass('on','off');
         });
   
                        
        });
                    </script>
                    <script>
					$(function () {
						$("#search").keyup(function () {
							// Retrieve the input field text and reset the count to zero
							var filter = $(this).val(),
								count = 0;
							// Loop through the menu list
							$("#nail-polish li").each(function () {
								// If the list item does not contain the text phrase fade it out
								if ($(this).attr("id").search(new RegExp(filter, "i")) < 0) {
									$(this).fadeOut();
									// Show the list item if the phrase matches and increase the count by 1
								} else {
									$(this).show();
									count++;
								}
							});
							// Update the count
							if (count > 0) {
								$("#filter-count").text("Top " + count + " results for: " + filter);
							} else {
								$("#filter-count").text("No results for: " + filter);
							}
							if (filter == "") {
								$("#filter-count").text("")
							}
						});
					});
			</script>
                    </div>
                    
                    <!-- END: Content -->
						  </div>
						</div>
					  </div>
				</div>
				<p><strong>Note:</strong> Double click to select the color.  Click on the linked heading text to expand or collapse accordion panels.</p>
				<div class="row">
                                  
					<div class=" btn-group colors__nxt__btn mb30 pul-rgt">
                                                
                                                <?php 
                                                if(isset($_SESSION['user']))
                                                {
                                                    ?>
                                                      <a href="cart.php" class="btn btn-book btn-lg pul-rgt">Next</a>

                                                    <?php
                                                }
                                                else
                                                {
                                                     ?>
                                                      <a href="login.php" class="btn btn-book btn-lg pul-rgt">Next</a>

                                                    <?php
                                                }
                                                 ?>
					</div>
				</div>
			</div>
                     <?php
                    }
                else
                {
                ?>
                     <div class="alert alert-success fade in alert-dismissable">
    <h4 class="text-center"><i class="fa fa-calendar"></i> Kindly first <strong><a href="appointment.php">schedule an appointment!</a></strong></h4></div>

		<?php
                }?>
                     
		</div>
	
</section>
<script>    
      $('#save-color').submit(function(e){
          e.preventDefault();
           
          
     });
      
</script>
		
<?php include("footer.php"); ?>