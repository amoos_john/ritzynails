<?php include("header.php"); ?>


	<section class="services-page">
		<div class="container">
			 <div class="hed">
				  <h2>Sign-up<span></span></h2>
			 </div> 
			 
		
			<div class="fom fom-login  fom-shad col-sm-6 col-sm-offset-3 mt30 mb70">
			
				<div class="form-group">
					<label>Name<em>*</em></label>
					<input type="text" class="form-control" placeholder="Name" />
				</div>
				
				<div class="form-group">
					<label>Email<em>*</em></label>
					<input type="text" class="form-control" placeholder="Email" />
				</div>
				
				<div class="form-group">
					<label>Phone No<em>*</em></label>
					<input type="text" class="form-control" placeholder="Phone No" />
				</div>
				
				<div class="form-group">
					<label>Address<em>*</em></label>
					<textarea type="text" class="form-control" placeholder="Address" rows=4 ></textarea>
				</div>
				
				<div class="form-group">
					<label>Appartment No/Floor<em>*</em></label>
					<input type="text" class="form-control" placeholder="Phone No" />
				</div>
				
				<div class="form-group">
					<label>City<em>*</em></label>
					<input type="text" class="form-control" placeholder="City" />
				</div>
				
				<div class="form-group">
					<label>Comment<em>*</em></label>
					<textarea type="text" class="form-control" placeholder="Comment" rows=4 ></textarea>
				</div>
				
				
				<div class="form-group text-right">
					<button class="btn btn-book btn-lg ">Submit</button>
				</div>
			
			</div>
			 
			 
			 
		</div>
	</section>
	 
	
	
<?php include("footer.php"); ?>