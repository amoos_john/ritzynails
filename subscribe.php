<section class="mail-area">
	<div class="container">
		<div class="mail__main col-sm-12">
			<div class="mail__lft col-sm-8">
				<p>Joignez-vous à notre liste de diffusion ci-dessous et nous vous tiendrons au courant à des événements spéciaux, des mises à jour, les remises et offres promotionnelles.</p>
			</div>
			<div class="mail__rgt col-sm-4">
				<div class="mail__sub">
					<div class="mail__sub__hed">
						<h4>Abonnez-vous à notre infolettre</h4>
						<p>Ne manquez aucune actualité</p>
					</div>
					<div class="mail__inputs">
						<form>
						  <div class="form-group">
							<input type="email" class="form-control" id="email">
						  </div>
						  <button type="submit" class="btn btn-default">Abonnez-vous maintenant</button>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>