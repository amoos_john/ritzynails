<?php 
include_once('config/connection.php');
include_once('config/model.php');
include_once('config/functions.php');


include("header.php"); 
   
?>

	<section class="slider-area hover-ctrl " >
		<div id="carousel-example-generic" class="carousel slide slider--pauseplay" data-ride="carousel" data---interval="false">
		
		  <ol class="carousel-indicators thumbs hidden">
                      <?php
                        $count=0;
                        $result = getSlider();
                        
                        foreach($result as $row)
                        {
                        ?>  
			<li data-target="#carousel-example-generic" data-slide-to="<?php echo $count;?>" class="<?php ($count==0)?'active':'';?>"></li>
 
                        
                        <?php  
                         $count++;
                        }
                        unset($result);
                        ?>
			
		  </ol>

		  <!-- Wrapper for slides -->
		  <div class="carousel-inner">
                        <?php
                        $counter=0;
                        $sliders = getSlider();
                        
                        while($slider = fetch_object($sliders)) 
                        {
                        ?>  
                         <div class="item <?php echo ($counter==0)?'active':'';?>">
			  <img src="<?php echo $slider->image; ?>" alt="...">
			  <div class="container">
				  <div class="caro-caps col-sm-6">
                       <h1><?php echo $slider->title; ?></h1> 
                       <p><?php echo $slider->body; ?></p>
			<div class="let-btn"><a href="booknow">let the pampering begin</a></div>
				  </div>
			  </div>
			</div>   
                        
                        <?php 
                        $counter++;
                        }     
                        ?>
                       
		  </div>

		  <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
			<span class="glyphicon glyphicon-chevron-left"></span>
		  </a>
		  <a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
			<span class="glyphicon glyphicon-chevron-right"></span>
		  </a>
		  
		  
		</div>
		
	
		
	</section>
	
	<section class="service-area">
	<div class="container">
	     <div class="hed text-center">
		      <h2>our amazing services</h2>
			  <span><img src="images/flower-icon.png" alt=""/></span>
		 </div>
                 <?php
		 $result = getCategories();
                 while($row = fetch_object($result)) 
                 {     
                 ?>
		 <div class="service-box col-sm-4 text-center">
		      <div class="service__inr">
			      <div class="service__img"><img src="<?php echo $row->image; ?>" alt=""/></div>
				  <div class="service__cont service__hover">
				       <img src="images/hover-logo.png" alt=""/>
				       <h3><?php echo $row->category_name; ?></h3>
					<p><?php echo $row->description; ?></p>
                                        <div class="book-btn"><a href="booknow">Book Now</a></div>
				  </div>
			  </div>
		 </div>
                <?php
                }
                ?>
                
		 <div class="service-box col-sm-4 text-center">
		      <div class="service__inr">
			      <div class="service__img"><img src="images/product6.jpg" alt=""/></div>
				  <div class="service__cont service__hover">
				       <img src="images/hover-logo.png" alt=""/>
				       <h3>Events</h3>
					   <p>Shine from Hand to Toe</p>
					   <div class="book-btn"><a href="booknow">Book Now</a></div>
				  </div>

			  </div>
		 </div>		 
		 
	</div>
	</section>
	
	<section class="best-area">
	<div class="container">
	     <div class="best-cont text-center">
                <?php $result = getBlock('block1');
                      $block = fetch_object($result); 
                      
                      echo $block->body;
                ?>
            </div>		 
	</div>
	</section>
	
    <section class="photo-area">
	<div class="container0">
	     <div class="hed text-center">
		      <h2>our photo galleries</h2>
			  <span><img src="images/flower-icon.png" alt=""/></span>
		 </div>
            <?php 
            $result = getPhotos('photo');
            while ($row = fetch_object($result)) 
            {
                     
             ?>
            
	    <div class="photo-box cols-5 inline-block text-center ">
		 <div class="photo__inr">
			     <div class="photo__img">
				      <img src="<?php echo $row->image;?>" alt=""/>  
                 </div>	
                 <div class="photo__hover">
                  <h3><?php echo $row->title;?></h3>
                  <p><?php echo $row->body;?></p>
		 </div>				 
            </div>			
        </div>	
            <?php
            }
            ?>
           <!--
	    <div class="photo-box cols-5 inline-block text-center">
		    <div class="photo__inr">
			     <div class="photo__img">
				      <img src="images/gallery2.jpg" alt=""/>  
                 </div>	
                 <div class="photo__hover">
				      <h3>fancy title</h3>
					  <p>Donec justo odio, lobortis eget congue sed, rutrum sit amet mauris. Curabitur sed lectus nulla.</p>
				 </div>				 
            </div>			
        </div>
		
	    <div class="photo-box cols-5 inline-block text-center">
		    <div class="photo__inr">
			     <div class="photo__img">
				      <img src="images/gallery3.jpg" alt=""/>  
                 </div>	
                 <div class="photo__hover">
				      <h3>fancy title</h3>
					  <p>Donec justo odio, lobortis eget congue sed, rutrum sit amet mauris. Curabitur sed lectus nulla.</p>
				 </div>				 
            </div>			
        </div>		

	    <div class="photo-box cols-5 inline-block text-center">
		    <div class="photo__inr">
			     <div class="photo__img">
				      <img src="images/gallery4.jpg" alt=""/>  
                 </div>	
                 <div class="photo__hover">
				      <h3>fancy title</h3>
					  <p>Donec justo odio, lobortis eget congue sed, rutrum sit amet mauris. Curabitur sed lectus nulla.</p>
				 </div>				 
            </div>			
        </div>		
		
	    <div class="photo-box cols-5 inline-block text-center">
		    <div class="photo__inr">
			     <div class="photo__img">
				      <img src="images/gallery5.jpg" alt=""/>  
                 </div>	
                 <div class="photo__hover">
				      <h3>fancy title</h3>
					  <p>Donec justo odio, lobortis eget congue sed, rutrum sit amet mauris. Curabitur sed lectus nulla.</p>
				 </div>				 
            </div>			
        </div>	
         
	    <div class="photo-box cols-5 inline-block text-center">
		    <div class="photo__inr">
			     <div class="photo__img">
				      <img src="images/gallery6.jpg" alt=""/>  
                 </div>	
                 <div class="photo__hover">
				      <h3>fancy title</h3>
					  <p>Donec justo odio, lobortis eget congue sed, rutrum sit amet mauris. Curabitur sed lectus nulla.</p>
				 </div>				 
            </div>			
        </div>	

	    <div class="photo-box cols-5 inline-block text-center">
		    <div class="photo__inr">
			     <div class="photo__img">
				      <img src="images/gallery7.jpg" alt=""/>  
                 </div>	
                 <div class="photo__hover">
				      <h3>fancy title</h3>
					  <p>Donec justo odio, lobortis eget congue sed, rutrum sit amet mauris. Curabitur sed lectus nulla.</p>
				 </div>				 
            </div>			
        </div>	

	    <div class="photo-box cols-5 inline-block text-center">
		    <div class="photo__inr">
			     <div class="photo__img">
				      <img src="images/gallery8.jpg" alt=""/>  
                 </div>	
                 <div class="photo__hover">
				      <h3>fancy title</h3>
					  <p>Donec justo odio, lobortis eget congue sed, rutrum sit amet mauris. Curabitur sed lectus nulla.</p>
				 </div>				 
            </div>			
        </div>			
       	
	    <div class="photo-box cols-5 inline-block text-center">
		    <div class="photo__inr">
			     <div class="photo__img">
				      <img src="images/gallery9.jpg" alt=""/>  
                 </div>	
                 <div class="photo__hover">
				      <h3>fancy title</h3>
					  <p>Donec justo odio, lobortis eget congue sed, rutrum sit amet mauris. Curabitur sed lectus nulla.</p>
				 </div>				 
            </div>			
        </div>			
		
	    <div class="photo-box cols-5 inline-block text-center">
		    <div class="photo__inr">
			     <div class="photo__img">
				      <img src="images/gallery10.jpg" alt=""/>  
                 </div>	
                 <div class="photo__hover">
				      <h3>fancy title</h3>
					  <p>Donec justo odio, lobortis eget congue sed, rutrum sit amet mauris. Curabitur sed lectus nulla.</p>
				 </div>				 
            </div>			
        </div>			
		-->
	</div>
	</section>
	
	<section class="client-area">
	<div class="container">
	     <div class="hed text-center">
		      <h2>what our clients say</h2>
			  <span><img src="images/flower-icon.png" alt=""/></span>
		 </div>	
		<div id="carousel-example-generic2" class="carousel slide slider--pauseplay" data-ride="carousel" data---interval="false">
		
		  <ol class="carousel-indicators thumbs">
                      <?php
                        $count=0;
                        $result = getTestimonials();
                        
                        foreach($result as $row)
                        {
                        ?>  
			<li data-target="#carousel-example-generic2" data-slide-to="<?php echo $count;?>" class="<?php ($count==0)?'active':'';?>"></li>
 
                        
                        <?php  
                         $count++;
                        }
                        unset($result);
                        ?>
			
		  </ol>

		  <!-- Wrapper for slides -->
		  <div class="carousel-inner">

                      <?php
                        $counter=0;
                        $testimonials = getTestimonials();
                        
                        while($test = fetch_object($testimonials)) 
                        {
                        ?>  
                         <div class="item <?php echo ($counter==0)?'active':'';?>">
		  
			  <div class="container0">
				    <div class="client-box text-center">
					    <div class="client__inr">
						     <div class="client__img"><img src="<?php echo $test->image; ?>" alt="<?php echo $test->title; ?>"></div>
                             <div class="client__cont">
                                 <?php echo $test->body; ?>
                             </div> <hr>
							<div class="author-area clrlist">
								<h3><?php echo $test->title; ?></h3>
									<ul>
									    <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                                                            <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                                                            <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                                                            <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                                                            <li><i class="fa fa-star" aria-hidden="true"></i></li>
									</ul>
							</div>							 
                        </div>
					</div>   
					   
				 
			  </div>
			
			</div>   
                        
                        <?php 
                        $counter++;
                        }     
                        ?>
                      
		
		  </div>

		  <a class="left carousel-control" href="#carousel-example-generic2" data-slide="prev">
			<span class="glyphicon glyphicon-chevron-left"></span>
		  </a>
		  <a class="right carousel-control" href="#carousel-example-generic2" data-slide="next">
			<span class="glyphicon glyphicon-chevron-right"></span>
		  </a>
		  
		  
		</div>	    
	</div>
	</section>
	
<?php include("footer.php"); ?>