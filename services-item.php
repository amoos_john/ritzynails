<?php 
include_once('config/connection.php');
include_once('config/model.php');
include_once('config/functions.php');

if(isset($_GET["id"]))
{
     $cat_id= charEsc($_GET["id"]);
     if(!empty($cat_id))
     {
        $services = getServicesCat($cat_id); 
     }
     
}
else
{
    $services= getServices(); 
}
?>
 <!-- Nav tabs -->
				<ul class="nav nav-tabs">
				  <li class="active"><a href="#client1" data-toggle="tab">Client1</a></li>
				  <li><a href="#client2" data-toggle="tab">Client2</a></li>
				  <li><a href="#client3" data-toggle="tab">Client3</a></li>
				  <li><a href="#client4" data-toggle="tab">Client4</a></li>
				</ul>

				<!-- Tab panes -->
				<div class="tab-content">
				  <div class="tab-pane active" id="client1">
				        <div class="services services-item">
							<ul>
							  <li class="img"><span class="services__img"><img src="images/services/service_58773e99cd453.png" alt=""></span></li>
							  <li class="title">

								<div class="panel-group " id="accordion1">

								  <div class="panel panel-default">
									<div class="panel-heading">
									  <h4 class="panel-title">
											<a data-toggle="collapse" data-parent="#accordion1" href="#collapseArr1">
												Pédicure Française Shellac	
											</a>
										</h4>
									</div>
									<div id="collapseArr1" class="panel-collapse collapse">
									  <div class="panel-body">
										<p>Le limage des ongles, le soin des cuticules, le retrait des callosités, un massage avec une lotion aux extraits de chèvrefeuille et de pamplemousse rose, le soin à l’huile de jojoba, la pose du vernis à ongles Shellac de la couleur de votre
										  choix et une manucure à la française.</p>

										<p><strong>SHELLAC FRENCH PEDICURE </strong></p>

										<p>The nail shaping, cuticle care, removal of calluses, a massage with lotion with extracts of honeysuckle and pink grapefruit, care with jojoba oil, laying the Shellac polish color of your choice and a French manicure.</p>
									  </div>
									</div>
								  </div>

								</div>


							  </li>
							  <li class="price">
								<table class="table">
								  <tbody>
									<tr>
									  <td>Time</td>
									  <td>Price</td>
									</tr>
									<tr>
									  <td><span class="services__time"><span class="hour">1 hr</span> <span class="mins">20 mins</span></span>
									  </td>
									  <td><span class="services__price" id="">$60.00</span></td>
									</tr>
								  </tbody>
								</table>

							  </li>
							  <li class="cta">


                                  
								<input type="hidden" id="item_id0" name="item_id" value="4">
								<input type="hidden" id="person0" name="person" value="1">
								<input type="hidden" id="discount0" name="discount" value="">
								<input type="hidden" id="totaltime0" name="totaltime" value="1:20">
								<input type="hidden" id="gettotalprice0" name="total_price" value="79.95">
								<button class="btn btn-book" id="btn-add-to-cart" onclick="cart(0)">Add to Cart</button>

							  </li>

							</ul>						
                        </div>	
				        <div class="services services-item">
							<ul>
							  <li class="img"><span class="services__img"><img src="images/services/service_58773e99cd453.png" alt=""></span></li>
							  <li class="title">

								<div class="panel-group " id="accordion2">

								  <div class="panel panel-default">
									<div class="panel-heading">
									  <h4 class="panel-title">
											<a data-toggle="collapse" data-parent="#accordion2" href="#collapseArr2">
												Pédicure Française Shellac	
											</a>
										</h4>
									</div>
									<div id="collapseArr2" class="panel-collapse collapse">
									  <div class="panel-body">
										<p>Le limage des ongles, le soin des cuticules, le retrait des callosités, un massage avec une lotion aux extraits de chèvrefeuille et de pamplemousse rose, le soin à l’huile de jojoba, la pose du vernis à ongles Shellac de la couleur de votre
										  choix et une manucure à la française.</p>

										<p><strong>SHELLAC FRENCH PEDICURE </strong></p>

										<p>The nail shaping, cuticle care, removal of calluses, a massage with lotion with extracts of honeysuckle and pink grapefruit, care with jojoba oil, laying the Shellac polish color of your choice and a French manicure.</p>
									  </div>
									</div>
								  </div>

								</div>


							  </li>
							  <li class="price">
								<table class="table">
								  <tbody>
									<tr>
									  <td>Time</td>
									  <td>Price</td>
									</tr>
									<tr>
									  <td><span class="services__time"><span class="hour">1 hr</span> <span class="mins">20 mins</span></span>
									  </td>
									  <td><span class="services__price" id="">$60.00</span></td>
									</tr>
								  </tbody>
								</table>

							  </li>
							  <li class="cta">


                                  
								<input type="hidden" id="item_id0" name="item_id" value="4">
								<input type="hidden" id="person0" name="person" value="1">
								<input type="hidden" id="discount0" name="discount" value="">
								<input type="hidden" id="totaltime0" name="totaltime" value="1:20">
								<input type="hidden" id="gettotalprice0" name="total_price" value="79.95">
								<button class="btn btn-book" id="btn-add-to-cart" onclick="cart(0)">Add to Cart</button>

							  </li>

							</ul>						
                        </div>							
				  </div>
				  <div class="tab-pane" id="client2">
				        <div class="services services-item">
							<ul>
							  <li class="img"><span class="services__img"><img src="images/services/service_58774113baacc.png" alt=""></span></li>
							  <li class="title">

								<div class="panel-group " id="accordion5">

								  <div class="panel panel-default">
									<div class="panel-heading">
									  <h4 class="panel-title">
											<a data-toggle="collapse" data-parent="#accordion5" href="#collapseArr5">
												Pédicure Française Shellac	
											</a>
										</h4>
									</div>
									<div id="collapseArr5" class="panel-collapse collapse">
									  <div class="panel-body">
										<p>Le limage des ongles, le soin des cuticules, le retrait des callosités, un massage avec une lotion aux extraits de chèvrefeuille et de pamplemousse rose, le soin à l’huile de jojoba, la pose du vernis à ongles Shellac de la couleur de votre
										  choix et une manucure à la française.</p>

										<p><strong>SHELLAC FRENCH PEDICURE </strong></p>

										<p>The nail shaping, cuticle care, removal of calluses, a massage with lotion with extracts of honeysuckle and pink grapefruit, care with jojoba oil, laying the Shellac polish color of your choice and a French manicure.</p>
									  </div>
									</div>
								  </div>

								</div>


							  </li>
							  <li class="price">
								<table class="table">
								  <tbody>
									<tr>
									  <td>Time</td>
									  <td>Price</td>
									</tr>
									<tr>
									  <td><span class="services__time"><span class="hour">1 hr</span> <span class="mins">20 mins</span></span>
									  </td>
									  <td><span class="services__price" id="">$60.00</span></td>
									</tr>
								  </tbody>
								</table>

							  </li>
							  <li class="cta">


                                  
								<input type="hidden" id="item_id0" name="item_id" value="4">
								<input type="hidden" id="person0" name="person" value="1">
								<input type="hidden" id="discount0" name="discount" value="">
								<input type="hidden" id="totaltime0" name="totaltime" value="1:20">
								<input type="hidden" id="gettotalprice0" name="total_price" value="79.95">
								<button class="btn btn-book" id="btn-add-to-cart" onclick="cart(0)">Add to Cart</button>

							  </li>

							</ul>						
                        </div>					  
				  
				  </div>
				  <div class="tab-pane" id="client3">
				        <div class="services services-item">
							<ul>
							  <li class="img"><span class="services__img"><img src="images/services/services_58787ed88dcac.jpg" alt=""></span></li>
							  <li class="title">

								<div class="panel-group " id="accordion3">

								  <div class="panel panel-default">
									<div class="panel-heading">
									  <h4 class="panel-title">
											<a data-toggle="collapse" data-parent="#accordion3" href="#collapseArr3">
												Pédicure Française Shellac	
											</a>
										</h4>
									</div>
									<div id="collapseArr3" class="panel-collapse collapse">
									  <div class="panel-body">
										<p>Le limage des ongles, le soin des cuticules, le retrait des callosités, un massage avec une lotion aux extraits de chèvrefeuille et de pamplemousse rose, le soin à l’huile de jojoba, la pose du vernis à ongles Shellac de la couleur de votre
										  choix et une manucure à la française.</p>

										<p><strong>SHELLAC FRENCH PEDICURE </strong></p>

										<p>The nail shaping, cuticle care, removal of calluses, a massage with lotion with extracts of honeysuckle and pink grapefruit, care with jojoba oil, laying the Shellac polish color of your choice and a French manicure.</p>
									  </div>
									</div>
								  </div>

								</div>


							  </li>
							  <li class="price">
								<table class="table">
								  <tbody>
									<tr>
									  <td>Time</td>
									  <td>Price</td>
									</tr>
									<tr>
									  <td><span class="services__time"><span class="hour">1 hr</span> <span class="mins">20 mins</span></span>
									  </td>
									  <td><span class="services__price" id="">$60.00</span></td>
									</tr>
								  </tbody>
								</table>

							  </li>
							  <li class="cta">


                                  
								<input type="hidden" id="item_id0" name="item_id" value="4">
								<input type="hidden" id="person0" name="person" value="1">
								<input type="hidden" id="discount0" name="discount" value="">
								<input type="hidden" id="totaltime0" name="totaltime" value="1:20">
								<input type="hidden" id="gettotalprice0" name="total_price" value="79.95">
								<button class="btn btn-book" id="btn-add-to-cart" onclick="cart(0)">Add to Cart</button>

							  </li>

							</ul>						
                        </div>					  
				  </div>
				  <div class="tab-pane" id="client4">
				        <div class="services services-item">
							<ul>
							  <li class="img"><span class="services__img"><img src="images/services/service_587744acc0f6a.jpg" alt=""></span></li>
							  <li class="title">

								<div class="panel-group " id="accordion4">

								  <div class="panel panel-default">
									<div class="panel-heading">
									  <h4 class="panel-title">
											<a data-toggle="collapse" data-parent="#accordion4" href="#collapseArr4">
												Pédicure Française Shellac	
											</a>
										</h4>
									</div>
									<div id="collapseArr4" class="panel-collapse collapse">
									  <div class="panel-body">
										<p>Le limage des ongles, le soin des cuticules, le retrait des callosités, un massage avec une lotion aux extraits de chèvrefeuille et de pamplemousse rose, le soin à l’huile de jojoba, la pose du vernis à ongles Shellac de la couleur de votre
										  choix et une manucure à la française.</p>

										<p><strong>SHELLAC FRENCH PEDICURE </strong></p>

										<p>The nail shaping, cuticle care, removal of calluses, a massage with lotion with extracts of honeysuckle and pink grapefruit, care with jojoba oil, laying the Shellac polish color of your choice and a French manicure.</p>
									  </div>
									</div>
								  </div>

								</div>


							  </li>
							  <li class="price">
								<table class="table">
								  <tbody>
									<tr>
									  <td>Time</td>
									  <td>Price</td>
									</tr>
									<tr>
									  <td><span class="services__time"><span class="hour">1 hr</span> <span class="mins">20 mins</span></span>
									  </td>
									  <td><span class="services__price" id="">$60.00</span></td>
									</tr>
								  </tbody>
								</table>

							  </li>
							  <li class="cta">


                                  
								<input type="hidden" id="item_id0" name="item_id" value="4">
								<input type="hidden" id="person0" name="person" value="1">
								<input type="hidden" id="discount0" name="discount" value="">
								<input type="hidden" id="totaltime0" name="totaltime" value="1:20">
								<input type="hidden" id="gettotalprice0" name="total_price" value="79.95">
								<button class="btn btn-book" id="btn-add-to-cart" onclick="cart(0)">Add to Cart</button>

							  </li>

							</ul>						
                        </div>					  
				  </div>
				</div>	
				
            </div>		
                                                             
				    <div class="clearfix"></div>
			  
					<div class="btn-group mb30 pul-rgt">
						<a href="appointment.php" class="btn btn-book btn-lg pul-rgt">Continue</a>
					</div>			
		</div>	 
        


