<?php 
if(!isset($_GET["id"]) || empty($_GET["id"]))
{
     echo "<script type='text/javascript'>window.location='index.php';</script>";
     exit();
}

include_once('config/connection.php');
include_once('config/model.php');
include_once('config/functions.php');

$id=charEsc($_GET["id"]);
$result=getProductbyid($id);
if($result)
{
    $product=fetch_array($result);
}
else
{
    echo "<script type='text/javascript'>window.location='index.php';</script>";
    exit();
}

$pagetitle="Ritzy Nails | ".$product["service_name"];
include("header.php");

?>

<section class="prod-dtl-area clrlist">
	<div class="container">
		<div class="hed text-center">
		    <h2>Product Detail</h2>
			<span><img src="images/flower-icon.png" alt=""/></span>
		</div>
		<div class="prod__dtl__cont col-sm-12">
                    <div id="message"></div>
			<div class="prod__dtl__lft col-sm-6">
				<div class="prod__dtl__img">
					<img src="<?php echo $product["image"]; ?>" alt="product detail">
				</div>
                             <?php echo $product["short_desc"]; ?>
			</div>
			<div class="prod__dtl__rgt col-sm-6">
				<div class="prod__dtl__summary">
					<h3 class="prod__dtl__title"><?php echo $product["service_name"]; ?></h3>
					<h4 class="prod__dtl__price"><span><?php echo ($product["old_price"]!=0)?'C'.PriceFormat($product["old_price"]):''; ?></span> C<?php echo PriceFormat($product["price"]); ?></h4>
					<div class="prod__dtl__quant">
					  <label class="qty">Quantity</label>
                                          <input type="hidden" name="item_id"  id="item_id0" value="<?php echo $product["id"]; ?>"/>
                                          <input type="hidden" id="price" value="<?php echo $product["price"]; ?>"/>
                                          <input type="hidden" name="total_price"  id="gettotalprice0" value="<?php echo $product["price"]; ?>"/>
                                          <select class="form-control" name="quan" id="quan0">
                                              <?php
                                              for($i=1;$i<=10;$i++)
                                              {
                                                ?>  
                                               <option value="<?php echo $i;?>"><?php echo $i;?></option>
                                              <?php } ?>
                                             
                                          </select>
					</div>
					<div class="clearfix"></div>
				</div>
				<div class="clearfix"></div>
				<div class="prod__dtl__cart">
					<button id="btn-add-to-cart"onclick="productcart(0)">Add to Cart</button>
				</div>
				<div class="prod__dtl__info">
                                    <h4>Description</h4>   
                                <?php echo $product["description"]; ?>
					<!--<p>Le soin quotidien Rescue Rxx à la kératine de CND est un nouveau traitement quotidien composé de kératine dont l’efficacité à améliorer l’apparence des ongles en une semaine a été démontrée cliniquement. Réduction des taches blanches, du fendillement et de l'écaillage des ongles.
</p>
				<p>Amélioration de l'apparence générale des ongles. Fortifie, traite et hydrate.<br>
15ml 0.5 fl oz</p>
				<p>Daily care Rxx Rescue Keratin CND is a new daily treatment of Keratin, the effectiveness to improve the appearance of nails in a week has been demonstrated clinically. Reduction of white spots, the cracking and peeling nails.</p>
				<p>Improving the overall appearance of the nails. Strengthens, treats and hydrates.
<br>15ml 0.5 fl oz</p>-->

				</div>
				<div class="prod__dtl__icons clrlist">
					<ul>
						<li><a href="#"><i class="fa fa-facebook"></i></a></li>
						<li><a href="#"><i class="fa fa-twitter"></i></a></li>
						<li><a href="#"><i class="fa fa-google-plus"></i></a></li>
						<li><a href="#"><i class="fa fa-pinterest"></i></a></li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</section>


<?php include("footer.php"); ?>