function deleteItem(id) {
            $.ajax({
             type:'post',
             url:'showcart.php',
             data:{
               itemid:id
             },
             success:function(response) {
                  window.location="booknow.php"; 
                  //$('#message').html(response);
                  
                 //show_cart();
                // totalitem();
                // changebutton();
                // show_summary();
             }

            });
           
     }       
              
     function totalitem()
      {
           $.ajax({
            type:'post',
            url:'addcart.php',
            data:{
              total_cart_items:"totalitems"
            },
            success:function(response) {
               $('#total_items').html(response);
            }
          });
          
      }
    totalitem();
  
      
    function cart(id)
    {
	  var item_id=$('#item_id'+id).val();  
          var person=$('#person'+id).val();
          var discount=$('#discount'+id).val();  
          var totaltime=$('#totaltime'+id).val(); 
          var totalprice=$('#gettotalprice'+id).val();  
          
	  $.ajax({
            type:'post',
            url:'addcart.php',
            data:{
              ser_id:item_id,
              person:person,
              discount:discount,
              totalprice:totalprice,
              totaltime:totaltime,
              additem:'additem'
            },
          success:function(html){
                    $('#message').html(html);
                    $('#btn-add-to-cart'+id).addClass("btn-booked");
                    $('#btn-add-to-cart'+id).html('<i class="fa fa-check"></i> Added');
                    totalitem();
                    show_cart();
                    changebutton();
                    show_summary();
                    $("html, body").animate({ scrollTop: 0 }, "slow");
                    return false;
                },
                error: function(errormessage) {
                      //you would not show the real error to the user - this is just to see if everything is working
                      $('#message').html("Cart Not Add");
                    //alert('Cart Not Add: ' + errormessage);
                }
      });
	
    }
    function productcart(id)
    {
	  var item_id=$('#item_id'+id).val();  
          var person=$('#quan'+id).val();
          var totalprice=$('#gettotalprice'+id).val();  
          
	  $.ajax({
            type:'post',
            url:'addcart.php',
            data:{
              item_id:item_id,
              quan:person,
              totalprice:totalprice,
              addproduct:'addproduct'
            },
          success:function(html){
                    $('#message').html(html);
                    totalitem();
                    show_cart();
                },
                error: function(errormessage) {
                      //you would not show the real error to the user - this is just to see if everything is working
                      $('#message').html("Cart Not Add");
                    //alert('Cart Not Add: ' + errormessage);
                }
      });
	
    }
    function show_cart()
    {
        $.ajax({
        type:'post',
        url:'showcart.php',
        data:{
          showcart:"cart"
        },
        success:function(response) {
             $('#mycart').html(response);
        }
       
       });

    }
    function show_summary()
    {
        $('#load').fadeIn(400).html("<i class='fa fa-spinner fa-spin fa-2x'></i>");
        $.ajax({
        type:'post',
        url:'showcart.php',
        data:{
          showsumm:"cart"
        },
        success:function(response) {
     
           $('#load').delay(350).fadeOut("slow");
           $('#cart-summary').fadeIn("slow").html(response);   
        }
       
       });

    }
    show_summary();
    
   function color_summary()
    {
        $('#load').fadeIn(400).html("<i class='fa fa-spinner fa-spin fa-2x'></i>");
        $.ajax({
        type:'post',
        url:'showcart.php',
        data:{
          showsumm:"cart",
          colors:"getcolor"
        },
        success:function(response) {
     
           $('#load').delay(350).fadeOut("slow");
           $('#color-cart-summary').fadeIn("slow").html(response);   
        }
       
       });

    }
    color_summary();
    
     function cartItem(id) {
            $.ajax({
             type:'post',
             url:'showcart.php',
             data:{
               itemid:id
             },
             success:function(response) {
                 $('#message').html(response);
                  window.location="cart.php"; 
                  show_cart();
                  totalitem();
             }

            });
           
     }
     
     function addColor(persons,cartid,colorid)
     {
            
            $.ajax({
                type:'post',
                url:'addcolor.php',
                  data:{
                    addcolor:'addcolor', 
                    persons:persons,
                    cartid:cartid,
                    colorid:colorid
                  },
                success:function(html){
                    $('#message').html(html);
                    color_summary();
                }
            });
     }
     function deleteColor(id)
     {
         $.ajax({
                type:'post',
                url:'addcolor.php',
                  data:{
                    delcolor:'delcolor',  
                    id:id   
                  },
                success:function(html){
                    $('#message').html(html);
                    color_summary();
                }
            });
     }
     function colorItem(id) 
     {
             $.ajax({
                type:'post',
                url:'addcolor.php',
                  data:{
                    delcolor:'delcolor',  
                    id:id   
             },
             success:function(response) {
                 $('#message').html(response);
                  window.location="cart.php"; 
                  show_cart();
                  totalitem();
             }

            });
           
     }

        
            
    
       
$(document).ready(function() {
    
  $('.addr').click(function () {

        var addr=$(this).val();
        if(addr=='addit')
        {
            $('#additional').html('<div class="form-group"><label>Postal code<em>*</em></label><input type="text" name="postal_code" pattern="[A-Za-z0-9]+" title="Must contain at least one number and one uppercase" id="postal_code" class="form-control" placeholder="Postal code" maxlength="10"  required/></div><div class="form-group"><label>Address<em>*(please include both street and apartment number also entrance code of the apartment if applicable)</em></label><textarea  name="address" id="address" class="form-control" placeholder="Address" rows="4" maxlength="60" required ></textarea></div>');
                    
        }
        else
        {
            $('#additional').html('');
        }

    });
     $('#btn-cont').attr("disabled",'true'); 
     $('#terms').change(function () {

        var term= $(this).is(':checked');
        if(term=='1')
        {
            $('#btn-cont').removeAttr("disabled");
        }
        else
        {
           $('#btn-cont').attr("disabled",'true');
        }

    });
    
    $(".termsdet[data-toggle=modal]").click(function()  {

                $('#termsModal').show();
                
 });  
  $("#quan0").change(function () {
      var quan = parseFloat($(this).val());
      var price=parseFloat($("#price").val());
      var total = price*quan;
      $("#gettotalprice0").val(total.toFixed(2));
    });   
    
$('#park').change(function(){
    
    var subtotal=$("#subtotal").text();
    var fee=parseFloat(6.00);
    localStorage.input = $(this).is(':checked');
     
    if(this.checked)
    {
        var total= parseFloat(subtotal) - fee;
        var grandtotal=total.toFixed(2);
        $('#parkfee').text('$0.00');
        $('#park_fee').val('');
        $('#total').text(grandtotal);
        $('#grand_total').val(grandtotal);     
    }
    else
    {
        $('#park_fee').val('6.00');
        $('#parkfee').text('$6.00');
        $('#total').text(subtotal);
        $('#grand_total').val(subtotal);

    }
 
});    
    
$('#parking').change(function(){
    
    var subtotal=$("#subtotal").text().split("$");
    var fee=parseFloat(6.00);
    var totalprice= parseFloat(subtotal[1]) + fee;
    
    if(this.checked)
    {
       var total= totalprice - fee;
        var grandtotal=total.toFixed(2);
        $('#park_fee').val('');
        $('#grand_total').val(grandtotal);
        $('#total').text(grandtotal);
        $('#addfee').hide();
    }
    else
    {
       $('#addfee').show();
        var grandtotal=totalprice.toFixed(2);
        $('#park_fee').val(fee);
        $('#grand_total').val(grandtotal);
        $('#total').text(grandtotal);
    }
 
});

$('#login-form :input').keyup(function() {

        var empty = false;
     
            if ($("#user_email").val() == '') {
                empty = true;
            }
            else if($("#user_password").val() == '')
            {
                empty = true;
            }
        if (empty) {
          $("#login").attr('disabled', 'disabled');

        } else {
             $('#login').removeAttr('disabled');

        }
    });
    
     $('#register-form :input').keyup(function() {

        var empty = false;
     
            if ($("#full_name").val() == '') {
                empty = true;
            }
            else if($("#last_name").val() == '')
            {
                empty = true;
            }
            else if($("#email").val() == '')
            {
                empty = true;
            }
            else if($("#password").val() == '')
            {
                empty = true;
            }
            else if($("#confirm_password").val() == '')
            {
                empty = true;
            }
            else if($("#postal_code").val() == '')
            {
                empty = true;
            }
            else if($("#phone").val() == '')
            {
                empty = true;
            }
             else if($("#address").val() == '')
            {
                empty = true;
            }
             
        if (empty) {
          $("#register").attr('disabled', 'disabled');

        } else {
             $('#register').removeAttr('disabled');

        }
    });

});
