<?php 
include_once('config/connection.php');
include_once('config/model.php');
include_once('config/functions.php');

$code="about-us";
$result= getContent($code);
$content = fetch_array($result);

$pagetitle=$content["title"];
$body=$content["body"];
$banner=$content["image"];
$description=$content["metaDescription"];
$keywords=$content["keywords"];

$bodyarr = explode("<hr />",$body);
if(count($bodyarr)>0)
{
    $body1=$bodyarr[0];
    $timing=$bodyarr[1];
}

include("header.php");
?>

<section class="inr-bnr-area about-bnr clrlist" style="background-image: url(<?php echo $banner;?>);">
	<div class="container">
		<div class="inr-bnr-cont">
			<div class="abt__bnr__cont col-sm-6">
				<ul>
					<li>style</li>
					<li>qualité</li>
					<li>engagement</li>
				</ul>
			</div>
		</div>
	</div>
</section>

<section class="ster-cont-area">
	<div class="container">
		<div class="ster__cont__title abt__cont__title col-sm-12">
			<h2><?php echo $pagetitle; ?></h2>
		</div>
		<div class="ster__cont__lft abt__cont__lft col-sm-7">
			<div class="abt__cont__inr p0 col-sm-12">
                        <?php echo $body1; ?>
			      
                    </div>
			<div class="abt__cont__img text-center col-sm-7 col-sm-offset-2">
				<div class="abt__img__inr abt__img__inr--1">
					<img src="images/abt-cont1.jpg" alt=" " />
				</div>
				<div class="abt__img__inr abt__img__inr--2">
					<img src="images/abt-cont2.jpg" alt=" " />
				</div>
				<div class="abt__img__inr abt__img__inr--2 abt__img__inr--3">
					<img src="images/abt-cont3.jpg" alt=" " />
				</div>
				<div class="abt__img__inr abt__img__inr--2 abt__img__inr--3">
					<img src="images/abt-cont4.jpg" alt=" " />
				</div>
				<div class="abt__img__inr abt__img__inr--2 abt__img__inr--4">
					<img src="images/abt-cont5.jpg" alt=" " />
				</div>
				<div class="abt__img__inr abt__img__inr--2">
					<img src="images/abt-cont6.jpg" alt=" " />
				</div>
				<div class="abt__img__inr abt__img__inr--2">
					<img src="images/abt-cont7.jpg" alt=" " />
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
		
		<div class="abt__rgt col-sm-5">
			<div class="abt__rgt__cont">
				<?php echo $timing; ?>
			</div>
			<div class="mail__inputs abt__inputs">
				<form name="contact_us" id="contact_us" method="post" enctype="multipart/form-data">
				  <div class="form-group">
					<input type="text" class="form-control" name="full_name" id="name" placeholder="Name" required/>
				  </div>
				  <div class="form-group">
					<input type="email" class="form-control" name="email" id="email" placeholder="Email" required/>
				  </div>
				  <div class="form-group">
					<input type="text" class="form-control" name="subject" id="subject" placeholder="Subject" required/>
				  </div>
				  <div class="form-group">
					<textarea class="form-control" rows="5" name="comment" id="message" placeholder="Message" required></textarea>
				  </div>
				  <button type="submit" class="btn btn-default">Send</button>
                                 <div class="clearfix"></div>
                                </form>
                            <div class="clearfix"></div>
                            <div id="view"></div>
			</div>
		</div>	
		<div class="clearfix"></div>
	</div>
</section>



<script type="text/javascript">
	
$(document).ready(function () {
    $("#contact_us").submit(function (e) {			
    e.preventDefault();

$.ajax({ 
     type: "POST",
     url: "sendemail.php",
     data: $( this ).serialize(),
     success: function(data){
         $('#view').delay(500).fadeIn(); 
         $('#view').html(data);
         $('#view').delay(8000).fadeOut(); 
	     
},
     error: function(errormessage) {
           //you would not show the real error to the user - this is just to see if everything is working
          $('#view').delay(500).fadeIn(); 
	  $('#view').html(errormessage);
          $('#view').delay(8000).fadeOut(); 
     }
});
        
    });
});
</script>	
<?php include("footer.php"); ?>