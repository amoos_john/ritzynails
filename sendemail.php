<?php
extract($_POST);

$err=false;
$message="";

if (empty($full_name)) {
    $message .= "\nName is required<br/>";
    $err=true;
  } else {
    $full_name= strip_tags($full_name);
    // check if name only contains letters and whitespace
    if (!preg_match("/^[a-zA-Z ]*$/",$full_name)) {
      $message .= "\nOnly letters and white space allowed<br/>";
      $err=true;
    }
  }

  if(empty($email)) {
    $message .= "\nEmail is required<br/>";
    $err=true;
  } else {
    $email = strip_tags($email);
    // check if e-mail address is well-formed
    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
      $message .= "\nInvalid email format<br/>"; 
      $err=true;
    }
  }
if (empty($subject)) {
    $message .= "\nSubject  is required<br/>";
    $err=true;
  } 
if (empty($comment)) {
    $message .= "\nMessage is required<br/>";
    $err=true;
  } 

$to  = 'amoos.golpik@gmail.com'; // note the comma 

// message
$body = '
<html>
<head>
  <title>Ritzy Nails Contact Form</title>
</head>
<body>
  <p>Ritzy Nails Contact Form</p>
  <table>
    <tr><td>Full Name:</td><td>'.$full_name.'</td></tr>
    <tr><td>Email:</td><td>'.$email.'</td></tr>
    <tr><td>Subject:</td><td>'. $subject. '</td></tr>
    <tr><td>Message:</td><td>'.$comment.'</td></tr>
  </table>
</body>
</html>
';


// To send HTML mail, the Content-type header must be set
$headers  = 'MIME-Version: 1.0' . "\r\n";
$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";

$headers .= 'From: '.$to.' <'.$email.'>' . "\r\n";

	// Mail it
	if($err===false)
	{
		if(mail($to, $subject, $body, $headers)){
                    
                    $message = "Message has been send successfully!";
                    echo '<div class="alert alert-success">'.$message.'</div>';
		
		}
	}
	else if($err===true)
	{
		echo '<div class="alert alert-danger">'.$message.'</div>';
	}


?>
