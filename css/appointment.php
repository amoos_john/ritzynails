<?php 
include("header.php"); 
include_once('config/connection.php');
include_once('config/model.php');

if(isset($_SESSION["cart"]))
{
   $sess_id=$_SESSION["cart"];
   $cartservices=ServicesCart($sess_id); 
}
else
{
    header("Location: index.php");
}

?>
 <style>
     td.fc-past {
      opacity: 0.3;
}
     td.fc-other-month {
      opacity: 1 !important;
}
.btn-book {
    opacity: 0.4;
}

</style>
	<section class="services-page">
		<div class="container">
			 <div class="hed">
				  <h2>Schedule Online <span></span></h2>
			 </div> 
			 
			 
			 <div class="services-list col-sm-9 girdbox-area ">
			
				
				 <div class="calendar-area">
				 <div id='calendar'></div>
				 </div>
				 
				 <div class="timeslot-area">
					 <div id="timeslots">
						<div class="timeslots__box text-center col-sm-4">
							<div class="timeslots__inr">
								<div class="timeslots__hdr">
									<h4>Morning</h4>
								</div>
								<div class="timeslots__content">
									<ul>
										<li>10:00 am</li>
										<li>10:30 am</li>
										<li>11:00 am</li>
										<li>10:30 am</li>
									</ul>
								</div>
							</div>
						</div>
						<div class="timeslots__box text-center col-sm-4">
							<div class="timeslots__inr">
								<div class="timeslots__hdr">
									<h4>Afternoon</h4>
								</div>
								<div class="timeslots__content">
									<ul>
										<li>10:00 am</li>
										<li>10:30 am</li>
										<li>11:00 am</li>
										<li>10:30 am</li>
									</ul>
								</div>
							</div>
						</div>
						<div class="timeslots__box text-center col-sm-4">
							<div class="timeslots__inr">
								<div class="timeslots__hdr">
									<h4>Evening</h4>
								</div>
								<div class="timeslots__content">
									<ul>
										<li>10:00 am</li>
										<li>10:30 am</li>
										<li>11:00 am</li>
										<li>10:30 am</li>
									</ul>
								</div>
							</div>
						</div>
					 </div>
				 </div>
				
			 </div>
                   
			 
			 <div class="services-list col-sm-3">
                        <div class="selected-item-detail panel panel-default">
				  
                        <div class="panel-heading"><h4>Selected Date/Time</h4></div>
                        <form action="colourselector.php" method="post">
                        <input type="hidden" id="bookdate"/>
                        <input type="hidden" id="booktime"/>
				  <table class="table">
					<tbody>
						<tr>
                                                    <td>Date:</td><td id="date"></td>
						</tr>
						<tr>
							<td>Time:</td><td id="time"></td>
						</tr>
                                                <tr>
						<td><div class="btn-group mb30 text-right">
                                                        <button type="submit" id="btn-next" class="btn btn-book btn-lg pul-rgt" disabled>Suivant / Next</button>
                                                </div></td>
						</tr>
						
					</tbody>
				  </table>
                        </form>
                    </div>
			 
				
				<?php 
                                if(count($cartservices)>0)
                                {
                                    
                                while($cartservice = fetch_object($cartservices))
                                {
                                ?>
                                
				<div class="selected-item-detail panel panel-default">
				  
				  <div class="panel-heading"><h4><?php echo $cartservice->service_name; ?></h4></div>

				  <table class="table">
					<tbody>
						<tr>
							<td>Service Time:</td><td><?php echo ($cartservice->hours!="")?$cartservice->hours." hr":''; ?> <?php echo ($cartservice->minutes!="")?$cartservice->minutes." mins":''; ?></td>
						</tr>
						<tr>
							<td>Persons:</td><td><?php echo $cartservice->persons; ?></td>
						</tr>
                                                <tr>
							<td>Price:</td><td>$ <?php echo $cartservice->price; ?></td>
						</tr>
						<tr>
							<td>Total Price:</td><td>$ <?php echo $cartservice->total_price; ?></td>
						</tr>
					</tbody>
				  </table>
				</div>
                             <?php
                                }
                             }
                                ?>
				
				
				<!--<div class="selected-item-detail panel panel-default">
				  
				  <div class="panel-heading"><h4>Selected Item Two</h4></div>

				  <table class="table">
					<tbody>
						<tr>
							<td>Service Name:</td><td>1</td>
						</tr>
						<tr>
							<td>Service Time:</td><td>8:30AM</td>
						</tr>
						<tr>
							<td>Total Persons:</td><td>4</td>
						</tr>
						<tr>
							<td>Total Time/Price:</td><td>4/457.22</td>
						</tr>
					</tbody>
				  </table>
				</div>-->
				
				
				
				
				
			 
			 </div>
			 
			 
			 
		</div>
	</section>
	 
	
<script src='js/moment.min.js'></script>
<script src='js/fullcalendar.min.js'></script>	
<script>
    $(document).ready(function() {
    var date = new Date();
    var d = date.getDate();
    var m = date.getMonth();
    var y = date.getFullYear();

$('#calendar').fullCalendar({
    
  header: {
      left: 'prev,next today',
      center: 'title',
      right: 'month,agendaWeek,agendaDay'
    },
    editable: false,
    selectable: true,
    selectHelper: false,
     dayClick: function(date, jsEvent, view) {
         
            var selectionStart = moment(date).format('MM-DD-YYYY'); 
            var today = moment().format('MM-DD-YYYY'); // passing moment nothing defaults to today
            
            //console.log(selectionStart);
            if (selectionStart < today) { 
             //console.log(today);

                $('#calendar').fullCalendar('unselect');
            }
            else {
                var ddate=date.format('YYYY-MM-DD');
                $("#date").text(date.format('LL'));
                $("#bookdate").val(ddate);
                $(this).css('border', '2px solid #FFCC00');
                $(this).css('color', '#FFCC00');
                $("#btn-next").attr("disabled", false);
                $("#btn-next").css("opacity", "1");
                gettimeslot(ddate);
                 
            } 
        

        //alert('Coordinates: ' + jsEvent.pageX + ',' + jsEvent.pageY);

        //alert('Current view: ' + view.name);

        // change the day's background color just for fun
       

    },
  select: function(start, end, allDay) {
            var selectionStart = moment(start); 
            var today = moment(); // passing moment nothing defaults to today
            if (selectionStart < today) { 
                $('#calendar').fullCalendar('unselect');
               
            }
            else {
             // do stuff
            }
        }

  
    
});


});


function gettimeslot(date)
{ 
    $.ajax({
            type:'post',
            url:'timeslots.php',
            data:{
              date:date,  
              showtime:'showtime'
            },
            success:function(response) {
               $('#timeslots').html(response);
            }
          });
}


</script>
    <?php include("footer.php"); ?>