<?php 
include_once('config/connection.php');
include_once('config/model.php');
include_once('config/functions.php');

if(isset($_GET["id"]))
{
     $sess_id=$_SESSION['cart'];
     $cat_id= charEsc($_GET["id"]);
     if(isset($_GET["person"]))
     {
         if($_SESSION["person"]<$_GET["person"])
         {
             DeleteCartPerson($sess_id,$_SESSION["person"]);
             unset($_SESSION["person"]);
         }
         if(empty($_GET["person"]) || $_GET["person"]>=5)
         {
             unset($_SESSION["person"]);
         }
         if(isset($_SESSION["review"]))
         {
             $sess_id=$_SESSION['cart'];
             DeleteBooking($sess_id);
         }
        $_SESSION["person"]=charEsc($_GET["person"]);

        
     }
     
     $person=$_SESSION["person"];
     if(!empty($cat_id))
     {
        $services = getServicesCat($cat_id); 
     }
     
}
else
{
     if(isset($_GET["person"]))
     {
         $sess_id=$_SESSION['cart'];
         
         if($_GET["person"]<$_SESSION["person"])
         {
             DeleteCartPerson($sess_id,$_SESSION["person"]);
             unset($_SESSION["person"]);
         }
         if(empty($_GET["person"]) || $_GET["person"]>=5)
         {
             unset($_SESSION["person"]);
         }
         if(isset($_SESSION["review"]))
         {
             DeleteBooking($sess_id);
         }
         
         $_SESSION["person"]=charEsc($_GET["person"]);
        
     }
    $person=$_SESSION["person"];
    
   
}

$pagetitle="Book Now";
include("header.php");
?>


<section class="services-page booknow-page">
		<div class="container">
			 <div class="hed">
				  <h2>Book Now<span></span></h2>
			 </div> 
                    <div id="message">
                        <?php if(isset($_SESSION["success"]))
                        {
                            echo $_SESSION["success"];
                            unset($_SESSION["success"]);
                        }
                        ?>
                    </div>
		 		<?php include("services-nav.php") ?>	 

			 
                    <div class="services-list col-sm-9 girdbox-area ">
			 
		    <div class="booktab-area">
                        
                        
		
                    <div class="form-group col-sm-6 col-sm-offset-3 p0">
					<div class="col-sm-4"><label>Select Persons</label></div>
					<div class="col-sm-6">
					<select class="form-control" id="sel-persons">
                                                        <option value="">Select Person</option>
							<option value="1" <?php echo ($person==1)?'selected':''; ?> >1</option>
							<option value="2" <?php echo ($person==2)?'selected':''; ?> >2</option>
							<option value="3" <?php echo ($person==3)?'selected':''; ?> >3</option>
							<option value="4" <?php echo ($person==4)?'selected':''; ?> >4</option>
					</select>
					</div>
		</div>
                        
                <?php
           if(isset($person) && !empty($person) && $person<5) 
           {    
            
                 $sess_id=$_SESSION['cart'];
                 $cart=ServicesCart($sess_id,'');
                 if(count($cart)>0)
                 {
                ?>
            
<div class="booktable-area table-responsive col-sm-8 col-sm-offset-2" >
<div id="load"></div>  
<div id="cart-summary"></div>
</div> 	         
                <?php
                 }
                ?>
				

	
			        
        <div class="booktab-nav col-sm-12 p0"  id="services-item">  
          
		<div class="btn-group mb30">
                    <a href="appointment.php" id="btn-cont" class="btn btn-book btn-lg pul-rgt" disabled>Continue</a>
		</div>
            <ul class="nav nav-tabs" id="myTabs">
                        <?php
                        for($i=1;$i<=$person;$i++)
                        {
                        ?>
				  <li class="<?php echo ($i==1)?'active':''; ?>"><a href="#client<?php echo $i; ?>" data-toggle="tab">Person <?php echo $i; ?></a></li>
				  
                        <?php
                        }
                        ?>
                        
            </ul>
            <!-- Tab panes -->
	    <div class="tab-content">
                 <?php
                $counter=0; 
                for($j=1;$j<=$person;$j++)
                {
                    
                ?>
		<div class="tab-pane <?php echo ($j==1)?'active':''; ?>" id="client<?php echo $j; ?>">
		<?php 
              
                            unset($services);
                            if(isset($_GET["id"]))
                            {
                                 $cat_id= charEsc($_GET["id"]);
                               
                                 if(!empty($cat_id))
                                 {
                                    $services = getServicesCat($cat_id); 
                                 }

                            }
                            else
                            { 
                                if(isset($_GET["type"]))
                                {
                                    $services=getProducts();

                                }
                                else
                                {
                                    $services = getServicesCat(1); 
                                } 
                            }
                            if(count($services)>0)
                            {
                             $count=($counter!=0)?$counter:0;
                             
                             while($service = fetch_object($services))
                             { 
                                 
                             ?>
                             
                                <div class="services services-item clrlist" data-price="<?php echo $service->price; ?>">
					<ul>
						<li class="img"><span class="services__img"><img src="<?php echo $service->image; ?>" alt="" /></span></li>
						<li class="title">
						
						<div class="panel-group " id="accordion<?php echo $count; ?>">
					  
							  <div class="panel panel-default">
								<div class="panel-heading">
								  <h4 class="panel-title">
									<a data-toggle="collapse" data-parent="#accordion<?php echo $count; ?>" href="#collapseArr<?php echo $count; ?>">
									  <?php echo $service->service_name; ?>
									</a>
								  </h4>
								</div>
								<div id="collapseArr<?php echo $count; ?>" class="panel-collapse collapse">
								  <div class="panel-body">
									<?php echo $service->description;  ?>
								  </div>
								</div>
							  </div>
					  
						</div>
						
						
                                                </li>
						<li class="price">
							<table class="table">
								<tr><td><?php echo ($service->type=='services')?'Time':''?></td><td>Price</td></tr>
                                                                <tr><td><span class="services__time"><span class="hour"><?php echo ($service->hours!="")?$service->hours." hr":''; ?></span> <span class="mins"><?php echo ($service->minutes!="")?$service->minutes." mins":''; ?></span></span></td>
                                                                    <td><span class="services__price" id=""><?php echo ($service->old_price!=0)?'<span>'.PriceFormat($service->old_price).'</span>':''; ?><?php echo PriceFormat($service->price); ?></span></td></tr>
							</table>
                                                        
						</li>
						<li class="cta">
                                                    <?php
                                                        $result= CheckPerson($sess_id,$service->id,$j);
                                                        $booked='';
                                                        $btn_text='Add to Cart';
                                                        if(count($result)>0)
                                                        {
                                                            ?>
                                                        <button class="btn btn-book btn-booked" ><i class="fa fa-check"></i> Added</button>
                                                        <?php
                                                        }   
							else
                                                        {
                                                        ?>
                                                        <input type="hidden" id="item_id<?php echo $count; ?>" name="item_id" value="<?php echo $service->id; ?>"/>
                                                        <input type="hidden" class="persons" id="person<?php echo $count; ?>" name="person" value="<?php echo $j; ?>"/>
                                                        <input type="hidden" id="totaltime<?php echo $count; ?>" name="totaltime" value="<?php echo ($service->type=='services')?$service->hours.":".$service->minutes:''; ?>"/>
                                                        <input type="hidden" id="gettotalprice<?php echo $count; ?>" name="total_price" value="<?php echo $service->price; ?>"/>
                                                        <button class="btn btn-book<?php echo $booked; ?>" id="btn-add-to-cart<?php echo $count; ?>" onclick="cart(<?php echo $count; ?>)">Add to Cart</button>
                                                        <?php
                                                        }
                                                        ?>
						</li>
                                                
					</ul>
				</div>
                             
                             <?php
                             $count++;
                             }
                             $counter=$count;
                             }
                             else
                             {
                                 ?>
                            <div class="col-sm-12">         
                            <h3 class="text-left">This service will arrive soon</h3>
                            </div>
                                 <?php
                             }
                             ?>							
				  </div>
                <?php 
              
                }
                
             
              ?>          
			
				</div>	
           
	</div>
			 
		</div>
                             
              <div class="clearfix"></div>
			  
		<div class="btn-group mb30">
                    <a href="appointment.php" class="btn btn-book btn-lg pul-rgt" id="btn-cont1" disabled>Continue</a>
		</div>
	  <?php
            }
            ?>		                
                             
            </div>
                    
                    
             </div>       
	</section>
		


	<script>
    
    function changebutton()
    {
        var person='<?php echo ($person)?$person:''; ?>';
         $.ajax({
            type:'post',
            url:'getdiscount.php',
            data:{
              person:person,
              getperson:1
            },
          success:function(html){
                    if(html!=0)
                    {
                        $('#btn-cont').removeAttr("disabled");
                        $('#btn-cont1').removeAttr("disabled");
                    }
                    else
                    {
                        $('#btn-cont').attr("disabled","disabled");
                        $('#btn-cont1').attr("disabled","disabled");
                      
                    }
                },
                error: function(errormessage) {
                      //you would not show the real error to the user - this is just to see if everything is working
                    console.log(errormessage);
                    //alert("Error ");
                }
      });
    }
   changebutton();
     
    $("#sel-persons").change(function() {
          var person = $(this).val();
          if(person>=5)
          {
               window.location="booknow.php";
          }
          else
          {
               window.location="booknow.php?person="+person;
          }
    
    });        
$(document).ready(function(){
   $('a[data-toggle="tab"]').click(function (e) {
    e.preventDefault();
    $(this).tab('show');
});

$('a[data-toggle="tab"]').on("shown.bs.tab", function (e) {
    var id = $(e.target).attr("href");
    localStorage.setItem('selectedTab', id)
});

var selectedTab = localStorage.getItem('selectedTab');
if (selectedTab != null) {
    $('a[data-toggle="tab"][href="' + selectedTab + '"]').tab('show');
}
});
 </script>
	
	
<?php include("footer.php"); ?>