<?php 
include_once('config/connection.php');
include_once('config/model.php');
include_once('config/functions.php');

$code="gallery";
$result= getContent($code);
$content = fetch_array($result);

$pagetitle=$content["title"];
$body=$content["body"];
$banner=$content["image"];
$description=$content["metaDescription"];
$keywords=$content["keywords"];
$link=$content["subject"];


include("header.php");
?>

<section class="inr-bnr-area gall-bnr clrlist" style="background-image: url(<?php echo $banner;?>);">
	<div class="container">
		<div class="inr-bnr-cont">
			<p>« Prendre du temps pour soi n’est plus un luxe, <br />
				c’est un mode de vie! »
			</p>

		</div>
	</div>
</section>

<section class="gall-video-area">
	<div class="container">
		<div class="video-box col-sm-6 col-sm-offset-3">
			<div class="video__inr">
				<div class="video__iframe">
					<iframe width="100%" height="100%" src="<?php echo $link; ?>" frameborder="0" allowfullscreen></iframe>
				</div>
				<?php echo $body; ?>
			</div>
		</div>
	</div>
</section>

<section class="gall-slider-area">
	<div class="container">
		<div class="gall__slider__box col-sm-10 col-sm-offset-1">
			<div id="carousel-example-generic" class="carousel slide slider--pauseplay" data-ride="carousel" data---interval="false">
			  <!-- Indicators -->
                          <ol class="carousel-indicators thumbs">
			  <?php
                            $count=0;
                            $result = getGalleryImages();

                            foreach($result as $row)
                            {
                            ?>  
                            <li data-target="#carousel-example-generic" data-slide-to="<?php echo $count;?>" class="<?php ($count==0)?'active':'';?>"></li>


                            <?php  
                             $count++;
                            }
                            unset($result);
                        ?>
			  
				
                        </ol>

			  <!-- Wrapper for slides -->
			  <div class="carousel-inner">
                               <?php
                        $counter=0;
                        $sliders = getGalleryImages();
                        
                        while($slider = fetch_object($sliders)) 
                        {
                        ?>   
                         
			 <div class="item <?php echo ($counter==0)?'active':'';?>">
				  <img src="<?php echo $slider->image; ?>" alt="<?php echo $slider->title; ?>">
			 </div>
                        
                        <?php 
                        $counter++;
                        }     
                        ?>   
			  </div>

			  <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
				<span class="fa fa-angle-left"></span>
			  </a>
			  <a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
				<span class="fa fa-angle-right"></span>
			  </a>
			  
			  
			</div>
		</div>	
	</div>
</section>

	
<?php include("footer.php"); ?>