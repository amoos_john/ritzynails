<?php 
include_once('config/connection.php');
include_once('config/model.php');
include_once('config/functions.php');
//include_once('config/session.php');

$name=isset($_POST['full_name']) ? $_POST['full_name'] : '' ;
$last_name=isset($_POST['last_name']) ? $_POST['last_name'] : '' ;
$email=isset($_POST['email']) ? $_POST['email'] : '' ;
$postal_code=isset($_POST['postal_code']) ? $_POST['postal_code'] : '' ;  
$phone=isset($_POST['phone']) ? $_POST['phone'] : '' ; 
$address=isset($_POST['address']) ? $_POST['address'] : '' ;

$pagetitle="Login/Register";
include("header.php");
?>
<style>
    .img-captcha
    {
        width: 145px;
        height:45px;
        margin-bottom:5px;
    }
    .reload{
        width: 35px;
        margin-left:10px;
    }
</style>
<script src="https://www.google.com/recaptcha/api.js" async defer></script>

	<section class="services-page">
		<div class="container">
			 <div class="hed">
				  <h2>Login / Signup <span></span></h2>
			 </div> 
			 
		
			<div class="fom fom-login col-sm-5 col-sm-offset-1 bdr-next">
                             <?php 
                            if(isset($_SESSION['error']))
                            {
                                ?>
                            <div class="alert alert-danger fade in alert-dismissable" style="margin-top:18px;">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
                            <i class="fa fa-times-circle"></i> <?php echo $_SESSION['error']; ?> </div>
                                <?php
                               unset($_SESSION['error']);      
                                
                            }
                         ?>
                            <form method="post" action="logincheck.php" name="login-form" id="login-form">
				<div class="form-group">
					<label>Email<em>*</em></label>
                                        <input type="email" name="email"  id="user_email" class="form-control" placeholder="Email" maxlength="60" required/>
				</div>
				
				<div class="form-group">
					<label>Password<em>*</em></label>
					<input type="password" name="password" id="user_password" class="form-control"  placeholder="Password" maxlength="60" required/>
				</div>
			
			
				<div class="form-group text-right">
					<a class="pul-lft" href="forgot.php">Forgot Password</a>
                                        <button type="submit" class="btn btn-book btn-lg " name="login" id="login" disabled>Login</button>
				</div>
                            </form>    
			</div>
			
			
		
			<div class="fom fom-login  col-sm-5  mb50">
                         <?php 
                            if(isset($_SESSION['regerror']))
                            {
                                ?>
                            <div class="alert alert-danger fade in alert-dismissable" style="margin-top:18px;">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
                            <i class="fa fa-times-circle"></i> <?php echo $_SESSION['regerror']; ?> </div>
                                <?php
                               unset($_SESSION['regerror']);      
                                
                            }
                         ?>
			<form method="post" action="register.php" name="register-form" id="register-form">
				<div class="form-group">
					<label>Name<em>*</em></label>
                                        <input type="text" name="full_name" id="full_name" class="form-control" placeholder="Name"  maxlength="60" required value="<?php echo $name; ?>"/>
				</div>
                                <div class="form-group">
					<label>Last Name<em>*</em></label>
                                        <input type="text" name="last_name"  id="last_name" class="form-control" placeholder="Last Name"  maxlength="60" required value="<?php echo $last_name; ?>"/>
				</div>
				
				<div class="form-group">
					<label>Email<em>*</em></label>
					<input type="email"  name="email"  id="email" class="form-control" placeholder="Email"  maxlength="60" value="<?php echo $email; ?>" required/>
				</div>
				<div class="form-group">
					<label>Password<em>*</em></label>
					<input type="password" name="password"  id="password" class="form-control"  placeholder="Password" maxlength="60" required/>
				</div>
				<div class="form-group">
					<label>Confirm Password<em>*</em></label>
					<input type="password" name="confirm_password"  id="confirm_password" class="form-control"  placeholder="Confirm Password" maxlength="60" required/>
				</div>
                                <div class="form-group">
					<label>Postal code<em>*</em></label>
					<input type="text" name="postal_code"  id="postal_code" class="form-control" placeholder="Postal code" maxlength="10" value="<?php echo $postal_code; ?>" required/>
				</div>
				
				<div class="form-group">
					<label>Phone<em>*</em></label>
					<input type="text" name="phone"  id="phone" class="form-control" maxlength="15" placeholder="(XXX) XXX-XX-XX"  value="<?php echo $phone; ?>" required/>
				</div>
				
				<div class="form-group">
					<label>Address<em>*</em></label>
					<textarea name="address"  id="address" class="form-control" placeholder="Address" rows="4" maxlength="60" required ><?php echo $address; ?></textarea>
				</div>
				
                                <div class="form-group">
					<label>Captcha Code<em>*</em></label>
                                     
                                <div class="g-recaptcha" data-sitekey="6Le_ixQUAAAAAAKVGCDkDpy6s6mLvB26ElytNMJk"></div>
                                        </div>
				<div class="form-group text-right">
                                    <button type="submit" name="register" id="register" class="btn btn-book btn-lg" disabled>Register</button>
				</div>
                        </form>
			
			</div>
			 
			 
			 
			 
			 
		</div>
	</section>
	 
<script>
    $('#phone', '#register-form').keydown(function (e) {
		var key = e.charCode || e.keyCode || 0;
		$phone = $(this);

		// Auto-format- do not expose the mask as the user begins to type
		if (key !== 8 && key !== 9) {
			if ($phone.val().length === 4) {
				$phone.val($phone.val() + ')');
			}
			if ($phone.val().length === 5) {
				$phone.val($phone.val() + ' ');
			}			
			if ($phone.val().length === 9) {
				$phone.val($phone.val() + '-');
			}
      if ($phone.val().length === 12) {
				$phone.val($phone.val() + '-');
			}
		}

		// Allow numeric (and tab, backspace, delete) keys only
		return (key == 8 || 
				key == 9 ||
				key == 46 ||
				(key >= 48 && key <= 57) ||
				(key >= 96 && key <= 105));	
	})
	
	.bind('focus click', function () {
		$phone = $(this);
		
		if ($phone.val().length === 0) {
			$phone.val('(');
		}
		else {
			var val = $phone.val();
			$phone.val('').val(val); // Ensure cursor remains at the end
		}
	})
	
	.blur(function () {
		$phone = $(this);
		
		if ($phone.val() === '(') {
			$phone.val('');
		}
	});
        

</script>	
	
<?php include("footer.php"); ?>