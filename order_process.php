<?php
include_once('config/connection.php');
include_once('config/model.php');
include_once('config/functions.php');
include_once('config/constants.php');


include_once __DIR__ . "/paypal/vendor/autoload.php"; //include PayPal SDK
include_once __DIR__ . "/functions.inc.php"; //our PayPal functions

#### Prepare for Payment ####
if(isset($_POST["checkout"])){
    
    if(!isset($_POST["terms"]))
    {
         $_SESSION["error"] = 'Check the terms and condition!';    
          header("Location: cart.php");
    }
    if(isset($_POST["addr"]))
    {
        $check= charEsc($_POST["addr"]);
        if($check=='addit')
        {
            $postal_code = charEsc($_POST["postal_code"]);
            $address = charEsc($_POST["address"]);
            
            $checkpostal=ValidatePostal($postal_code);
            if(!empty($checkpostal))
            {
                $_SESSION["error"] = $checkpostal;    
                 header("Location: cart.php");
            }
            elseif ($address=='') 
            {
                $_SESSION["error"] = 'Address is required!';    
                header("Location: cart.php");
            }
            else 
            {
                $_SESSION["address"]= $address;
                $_SESSION["postal_code"]=$postal_code;
            }    
        }
         
     }
     
 
    $sess_id=$_SESSION["cart"];
    $checkouts=ServicesCart($sess_id,'');
	//set array of items you are selling, single or multiple
       
    $item_total = 0;
    while($checkout = fetch_object($checkouts))
    {
            $item_quantity=1;
            //add item array to $items variable, it will be used in order_process.php
            $items[] = array(
                            'name' => $checkout->service_name, 
                            'quantity' => $item_quantity, 
                            'price' => $checkout->total_price, 
                            'sku' => $checkout->service_id, 
                            'currency' => PP_CURRENCY
                            );
            //calculate total price of all items        
            $item_total= $item_total + $checkout->total_price;
       
    }
       if(!empty($_POST["mobile_fee"]))
       {
         
            $mobile_fee=charEsc($_POST["mobile_fee"]);
            $items[] = array(
                            'name' => 'Mobile Fee', 
                            'quantity' => $item_quantity, 
                            'price' => $mobile_fee, 
                            'sku' => '1', 
                            'currency' => PP_CURRENCY
                            );
            $_SESSION["mobile_fee"]=$mobile_fee;
            
            $item_total= $item_total + $mobile_fee;
           
       }
	
       if(!empty($_POST["park_fee"]))
       {
         
            $park_fee=charEsc($_POST["park_fee"]);
            $items[] = array(
                            'name' => 'Parking Fee', 
                            'quantity' => $item_quantity, 
                            'price' => $park_fee, 
                            'sku' => '1', 
                            'currency' => PP_CURRENCY
                            );
            $_SESSION["park_fee"]=$park_fee;
            
            $item_total= $item_total + $park_fee;
           
       }
        
       
	//calculate total amount of all quantity. 
        $_SESSION["items"]=$items;
        $total_amount =  $item_total;
	$_SESSION["items_total"] = $total_amount;
	
	try{ // try a payment request
		//if payment method is paypal
		 $result = create_paypal_payment($total_amount, PP_CURRENCY, '', $items, RETURN_URL, CANCEL_URL);
                 //print_r($result);die();
                 if($result->state == "created" && $result->payer->payment_method == "paypal"){
                    $_SESSION["payment_id"] = $result->id; //set payment id for later use, we need this to execute payment
                    unset($_SESSION["items"]); //unset item session, not required anymore.
                    unset($_SESSION["items_total"]); //unset items_total session, not required anymore.
                    header("location: ". $result->links[1]->href); //after success redirect user to approval URL 
                    exit();
                }

	}catch(PPConnectionException $ex) {
		echo parseApiError($ex->getData());
	} catch (Exception $ex) {
		echo $ex->getMessage();
	}
}



?>
