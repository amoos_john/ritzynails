<?php
include_once('config/connection.php');
include_once('config/model.php');
include_once('config/functions.php');
include_once('config/constants.php');

include_once __DIR__ . "/paypal/vendor/autoload.php"; //include PayPal SDK
include_once __DIR__ . "/functions.inc.php"; //our PayPal functions
$error='';
//After PayPal payment method confirmation, user is redirected back to this page with token and Payer ID ###
if(isset($_GET["token"]) && isset($_GET["PayerID"]) && isset($_SESSION["payment_id"]))
{
    
    try
    {
		$result = execute_payment($_SESSION["payment_id"], $_GET["PayerID"]);  //call execute payment function.

		if($result->state == "approved"){ //if state = approved continue..

//Rechecking the product price and currency details
                    
                     //unset payment_id, it is no longer needed 
			
			//get transaction details
			$transaction_id 		= $result->transactions[0]->related_resources[0]->sale->id;
			$transaction_time 		= $result->transactions[0]->related_resources[0]->sale->create_time;
			$transaction_currency 	= $result->transactions[0]->related_resources[0]->sale->amount->currency;
			$transaction_amount 	= $result->transactions[0]->related_resources[0]->sale->amount->total;
			$transaction_method 	= $result->payer->payment_method;
			$transaction_state 	= $result->transactions[0]->related_resources[0]->sale->state;
			
                    
    $sess_id=$_SESSION["cart"];
    $user_id=$_SESSION["user"];
    $mobile_fee=$_SESSION["mobile_fee"];
    $person=$_SESSION["person"];
    $parking=$_SESSION["park_fee"];
    $address='';
    $postal_code='';
    if(isset($_SESSION["address"]))
    {
        $address=$_SESSION["address"];
        $postal_code=$_SESSION["postal_code"];
        unset($_SESSION["address"]);
        unset($_SESSION["postal_code"]);
    }
    else
    {
        $profile=getProfile($user_id);
        if(count($profile)>0)
        {
            $row=fetch_assoc($profile);
            $address=$row["address"];
            $postal_code=$row["postal_code"];
        }
        
    }
    
    $token=$_GET["token"];
    $PayerID=$_GET["PayerID"];
    $payment_id=$_SESSION["payment_id"];
    
    $cDate=date("Y-m-d H:i:s");
    $addorder=array("user_id"=>$user_id,"person"=>$person,"grand_total"=>$transaction_amount,
    "parking"=>$parking,"mobile_fee"=>$mobile_fee,"address"=>$address,"postal_code"=>$postal_code,   
    "order_status"=>$transaction_state,"payment_status"=>$transaction_state,"created_date"=>$cDate);
    $query= insert('orders',$addorder);
    $order_id='';
    if($query)
    {
                
             $order_id=insert_id($query);
             
             $addpaypal=array("order_id"=>$order_id,"payment_id"=>$payment_id,
             "token"=>$token,"payerID"=>$PayerID,"created_date"=>$cDate);
             $query= insert('paypal',$addpaypal);
                
             unset($_SESSION["park_fee"]);
             unset($_SESSION["mobile_fee"]);
             unset($_SESSION["address"]);
             unset($_SESSION["grand_price"]);
             unset($_SESSION["payment_id"]);
             unset($_SESSION["person"]);
             
             $cartsers=ServicesCart($sess_id,''); 
             
             if($cartsers)
             {
                 while($ser = fetch_object($cartsers))
                 {
                     $addorderser=array("order_id"=>$order_id,"service_id"=>$ser->service_id,"price"=>$ser->price,
                      "persons"=>$ser->persons,"total_price"=>$ser->total_price,"created_date"=>$cDate);
                     $order_services = insert('order_services',$addorderser);
                     
                     
                     $getcolor = CartColor($ser->id,$sess_id);
                     
                     if(count($getcolor)>0)
                     {
                          $order_ser_id=insert_id($query);
                          while($row=fetch_object($getcolor))
                          {
                              $addcolors=array("order_service_id"=>$order_ser_id,"color_id"=>$row->color_id,"person"=>$row->person,"french_manicure"=>$row->french_manicure);
                              $order_color = insert('order_colors',$addcolors);
                          }
                         
                     }
                     
                 }
                 DeleteCart($sess_id);
             }
             
            $booking=Checkbooking($sess_id);
            
            if(count($booking)>0)
            { 
                $row=fetch_assoc($booking);
                
                $addbook=array("order_id"=>$order_id,"date"=>$row["date"],"start_time"=>$row["start_time"],"end_time"=>$row["end_time"],"status"=>'pending',"created_date"=>$cDate);
                $order_booking= insert('order_booking',$addbook);
              
                if($order_booking)
                {
                         $updatebook=DeleteBooking($sess_id);           
                }
                
             }
       
        $res=getProfile($user_id);     
        if($res)
        {
            $user=fetch_array($res);
            $to  = $user["email"]; // note the comma 
            $email=$account;
        // subject
            $subject = 'Order Confirmation Ritzy Nails';
            // message
            $body = '
            <html>
            <head>
              <title>Order Confirmation Ritzy Nails</title>
            </head>
            <body>
                <p>Dear '.$user["name"].' '.$user["last_name"].'</p>
                 <p>Thank you for online order. This the the confirmation and summary of you order, which is current being processed.
                 </p>
                 <p>For your convenience, you will able to track the status of you online orders and manage you account by visiting the "Order" section of our website.
                 </p>
                <p>Regards,
                </p>
                <p>
                Ritzy Nails.
                </p>
            </body>
            </html>
            ';


            // To send HTML mail, the Content-type header must be set
            $headers  = 'MIME-Version: 1.0' . "\r\n";
            $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";

            $headers .= 'From: '.$to.' <'.$email.'>' . "\r\n";
            $headers .= 'Cc: '.$account.'' . "\r\n";

            // Mail it
            if(mail($to, $subject, $body, $headers))
            {
                $_SESSION["results"] = '<div class="alert alert-success fade in alert-dismissable">
                             <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
                             <h4><i class="fa fa-check-circle"></i> Order has been submitted! </h4></div>';
            }
          
            /*else if($err===true)
            {
                    echo '<div class="alert alert-danger">'.$message.'</div>';
            }   */
        }
           
             
             
        unset($_SESSION["cart"]);     
            
    }
    
    else
    {
         $_SESSION["results"] =  '<div class="alert alert-danger fade in alert-dismissable" style="margin-top:18px;">
    <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
    <h4><i class="fa fa-times-circle"></i> Payment Failed! </h4></div>';
     $error=true;
    }
}
else
{
     $_SESSION["results"] =  '<div class="alert alert-danger fade in alert-dismissable" style="margin-top:18px;">
    <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
    <h4><i class="fa fa-times-circle"></i> Payment Failed! </h4></div>';
    $error=true;
     
}

header("location: ". RETURN_URL); //$_SESSION["results"] is set, redirect back to order_process.php
			exit();
		}
                
               catch(PPConnectionException $ex) {
		$ex->getData();
                $error=true;
	} catch (Exception $ex) {
		echo $ex->getMessage();
                $error=true;
	}
		
}
if(isset($_SESSION["results"]))
{
        header("location: dashboard.php");
        exit(); 
}
else
{
     header("Location: index.php");
     exit();
}
?>

