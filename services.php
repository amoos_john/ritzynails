<?php 
include_once('config/connection.php');
include_once('config/model.php');
include_once('config/functions.php');

if(isset($_GET["id"]))
{
     $cat_id= charEsc($_GET["id"]);
     if(!empty($cat_id))
     {
        $services = getServicesCat($cat_id); 
     }
     
}
else
{
    $services= getServices(); 
}

$pagetitle="Services";
include("header.php");
?>


	<section class="services-page">
		<div class="container">
			 <div class="hed">
				  <h2>Services<span></span></h2>
			 </div> 
                    <div id="message"></div>
			 
			<?php include("services-nav.php") ?>
			
				 
			 
			 <div class="services-list col-sm-9 girdbox-area ">
			 
                             <?php 
                             $count=0;
                             while($service = fetch_object($services))
                             { 
                             ?>
                             
                                <div class="services services-item clrlist" data-price="<?php echo $service->price; ?>">
					<ul>
						<li class="img"><span class="services__img"><img src="<?php echo $service->image; ?>" alt="" /></span></li>
						<li class="title">
						
						<div class="panel-group " id="accordion<?php echo $count; ?>">
					  
							  <div class="panel panel-default">
								<div class="panel-heading">
								  <h4 class="panel-title">
									<a data-toggle="collapse" data-parent="#accordion<?php echo $count; ?>" href="#collapseArr<?php echo $count; ?>">
									  <?php echo $service->service_name; ?>
									</a>
								  </h4>
								</div>
								<div id="collapseArr<?php echo $count; ?>" class="panel-collapse collapse">
								  <div class="panel-body">
									<?php echo $service->description;  ?>
								  </div>
								</div>
							  </div>
					  
						</div>
						
						
                                                </li>
						<li class="price">
							<table class="table">
								<tr><td>Time</td><td>Price</td></tr>
                                                                <tr><td><span class="services__time"><span class="hour"><?php echo ($service->hours!="")?$service->hours." hr":''; ?></span> <span class="mins"><?php echo ($service->minutes!="")?$service->minutes." mins":''; ?></span></span></td>
                                                                    <td><span class="services__price" id=""><?php echo PriceFormat($service->price); ?></span></td></tr>
							</table>
                                                        
						</li>
						<li class="cta">
							<div class="form-group">
                                                            <label>People</label>
								<select class="form-control persons"  id="sel-persons">
                                                                    <option value="1" selected>1</option>
									<option value="2">2</option>
                                                                        <option value="3">3</option>
									<option value="4">4</option>
								</select>
							</div>
                                              
                                                    <div class="form-group" id="">
                                                      <strong>Price </strong>$<span class="stotal" id="stotal-<?php echo $count; ?>"></span>
                                                    </div>
							<div class="form-group" id="tprice<?php echo $count; ?>">
                                                            <strong>Mobile Fee</strong> $<span id="fee<?php echo $count; ?>"></span><br/>
                                                            <span id="ddiscount<?php echo $count; ?>" style="display:none;"><strong>Discount </strong> <span id="sdiscount<?php echo $count; ?>"></span></span>
                                                            <strong>Total </strong><span id="totalprice<?php echo $count; ?>"></span><br/>
							</div>
                                                        <input type="hidden" id="item_id<?php echo $count; ?>" name="item_id" value="<?php echo $service->id; ?>"/>
                                                        <input type="hidden" id="person<?php echo $count; ?>" name="person" value="1"/>
                                                        <input type="hidden" id="discount<?php echo $count; ?>" name="discount"/>
                                                        <input type="hidden" id="totaltime<?php echo $count; ?>" name="totaltime" value="<?php echo $service->hours.":".$service->minutes; ?>"/>
                                                        <input type="hidden" id="gettotalprice<?php echo $count; ?>" name="total_price" value="<?php echo $service->price; ?>"/>
                                                        <button class="btn btn-book" id="btn-add-to-cart"onclick="cart(<?php echo $count; ?>)">Add to Cart</button>
                                                    
							</li>
                                                
					</ul>
				</div>
                             
                             <?php
                             $count++;
                             }
                             ?>
                                
				
			  
					<div class="btn-group mb30 text-right">
						<a href="appointment.php" class="btn btn-book btn-lg pul-rgt">Book Now</a>
					</div>
			 
			 
			 </div>
			 
		</div>
	</section>

	<script>

    var updateTotal = function() {
 
    var sumtotal;
    var sum = 0;
    //Add each product price to total
    $(".services").each(function(index) {
           
        //console.log(i);
        var price = $(this).data('price');
        var person = $('.persons :selected', this).val();
       
            
        var hour = $('.hour', this).text().split('hr');
        var mins = $('.mins', this).text().split('mins');
        var totaltime;
        var tohour = hour[0]*person;
         
        var minutes =  mins[0]*person;
        var tmins = minutes % 60;
        var hours = Math.floor(minutes / 60);
         //console.log(hours);
        var thour = parseInt(tohour) + hours;

        totaltime=thour+":"+tmins;
        
        //Total for one product
        var subtotal = price*person;
        //Round to 2 decimal places.
        subtotal = parseFloat(subtotal.toFixed(2)); 
        //Display subtotal in HTML element
        
        $('.stotal', this).html(subtotal);
        $('.totaltime', this).html(totaltime);
        
       
             $.ajax({
              url:'getdiscount.php',
              type:'GET',
              data:"person="+person+"&discount=1",
              dataType : 'json',
              success:function(data)
              {
                  var fee=parseFloat(data.person_price);
                  var feetot=parseFloat(data.discount_price);
                  var dis=parseInt(data.discount);
          
                  var totalprice=subtotal+fee;
                 
                  if(data.persons!='1')
                  {
                      $('#ddiscount'+index).show();
                      $('#sdiscount'+index).text(dis+"%");
                      $('#discount'+index).val(dis);
                  }
                  else
                  {
                      $('#ddiscount'+index).hide();
                      $('#sdiscount'+index).text('');
                      $('#discount'+index).val('');
                  }
                  $('#totaltime'+index).val(totaltime);
                  $('#person'+index).val(person);
                  $('#fee'+index).text(fee)
                  $('#totalprice'+index).text("$"+totalprice.toFixed(2));
                  $('#gettotalprice'+index).val(totalprice.toFixed(2));
                 
              }
             
             });     
      
       
    
    });
    // total
       $('.productTotal').each(function() {
        sum += Number($(this).html());
    }); 
    
    

    $('#sum').html(sum.toFixed(2));
};

//Update total when quantity changes
$(".services .persons").change(function() {
    updateTotal();
});

//Update totals when page first loads
updateTotal();
   $(".services").each(function(index) { 
        
        $('#ddiscount'+index).hide();
        $('#ttime'+index).hide(); 
     
    });  

// set this from local
    $('span.productTotal').each(function() {
        $(this).before("&euro;")
    }); 

// unit price
   $('.product p').each(function() {
       var $price = $(this).parents("div").data('price');
      $(this).before($price);
    }); 

	</script>
	
	
<?php include("footer.php"); ?>