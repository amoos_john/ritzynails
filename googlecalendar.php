<?php
require_once __DIR__ . '/googlecalendar/vendor/autoload.php';
session_start();
$redirect_uri='http://ecommercehouse.com/projects/ritzynails/admin/dashboard.php';
$client = new Google_Client();
$client->setApplicationName('Google_Calendar_API');
$client->setAuthConfig('googlecalendar/client_secret.json');
$client->setScopes(["https://www.googleapis.com/auth/calendar"]);
 $acctoken=$client->getAccessToken();
if (isset($_SESSION['access_token']) && $_SESSION['access_token']) {
  // Set the access token on the client.
  $client->setAccessToken($_SESSION['access_token']);
  
}

echo $acctoken;die();
$date=date("Y-m-d");
$cal = new Google_Service_Calendar($client);

    $event = new Google_Service_Calendar_Event(array(
      'summary' => $order["id"].' Ritzy Nail Order',
      'location' => $order["address"],
      'description' => 'Services Booking',
      'start' => array(
        'dateTime' => $book["date"].'T09:00:00-07:00',
        'timeZone' => 'America/Los_Angeles',
      ),
      'end' => array(
        'dateTime' => $date.'T17:00:00-07:00',
        'timeZone' => 'America/Los_Angeles',
      ),
      'recurrence' => array(
        'RRULE:FREQ=DAILY;COUNT=2'
      ),
      'attendees' => array(
        array('email' => 'lpage@example.com'),
        array('email' => 'sbrin@example.com'),
      ),
      'reminders' => array(
        'useDefault' => FALSE,
        'overrides' => array(
          array('method' => 'email', 'minutes' => 24 * 60),
          array('method' => 'popup', 'minutes' => 10),
        ),
      ),
    ));

    $calendarId = '7m27buurv0bph3bs6pt491640c@group.calendar.google.com';
    $event = $cal->events->insert($calendarId, $event);
    printf('Event created: %s\n', $event->htmlLink);

//$client->setRedirectUri($redirect_uri);
//$auth_url = $client->createAuthUrl();
//header('Location: ' . filter_var($auth_url, FILTER_SANITIZE_URL));
?>
