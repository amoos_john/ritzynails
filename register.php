<?php
include_once('config/connection.php');
include_once('config/model.php');
include_once('config/functions.php');
include_once('config/constants.php');

if(isset($_POST["register"]))
{
	
        $name = charEsc($_POST["full_name"]);
        $last_name = charEsc($_POST["last_name"]);
	$email = charEsc($_POST["email"]);
	$pass = charEsc($_POST["password"]);
        $confirm_pass = charEsc($_POST["confirm_password"]);
        $postal_code = charEsc($_POST["postal_code"]);
        $phone= charEsc($_POST["phone"]);
        $address = charEsc($_POST["address"]);
       
       
        /* Form Validation */
        $checkname=ValidateName($name);
        $checklastname=ValidateName($last_name);
        $checkemail=ValidateEmail($email);
        $checkpass=ValidatePassword($pass);
        $checkconfimpass=ValidatePassword($confirm_pass);
        $checkpostal=ValidatePostal($postal_code);
        $checkphone=ValidatePhone($phone);
        
       
        if(!empty($checkname))
        {
            $_SESSION["regerror"] = $checkname;	
            header("Location: login.php");
        }
        else if(!empty($checklastname))
        {
            $_SESSION["regerror"] = $checklastname;	
             header("Location: login.php");  
        }
	else if(!empty($checkemail))    
	{
            $_SESSION["regerror"] = $checkemail;	
            header("Location: login.php");
	}
	else if(!empty($checkpass))
	{
            $_SESSION["regerror"] = $checkpass;   
             header("Location: login.php");
	}
        else if(!empty($checkconfimpass))
	{
            $_SESSION["regerror"] = $checkconfimpass;    
             header("Location: login.php");
	}
        else if($pass!=$confirm_pass)
        {
             $_SESSION["regerror"] = "Your password and confirm password didn't matched!"; 
              header("Location: login.php");
        }
       
        else if(!empty($checkpostal))
	{
            $_SESSION["regerror"] = $checkpostal;    
             header("Location: login.php");
	}
        else if(!empty($checkphone))
	{
            $_SESSION["regerror"] = $checkphone;
             header("Location: login.php");
	}
        else if(!isset($_POST['g-recaptcha-response']) && empty($_POST['g-recaptcha-response']))
	{
            $_SESSION["regerror"] = "Captcha is empty!";
             header("Location: login.php");
	}
       
	else{
            
            $secret = $secretkey;
            $verifyResponse = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret='.$secret.'&response='.$_POST['g-recaptcha-response']);
            $responseData = json_decode($verifyResponse);
            if($responseData->success)
	    {	
		$sql= "select * from `users` where email='{$email}'";
		
		  $query = query($sql);
		  if(num_rows($query)==0)
		  {
                        $password= md5($pass);  
                          
                        $cDate=date("Y-m-d H:i:s");
                        $adduser=array("email"=>$email,"password"=>$password,"name"=>$name,"last_name"=>$last_name,"phone"=>$phone,"address"=>$address,"postal_code"=>$postal_code,"status"=>'active',"created_date"=>$cDate);
                        $query= insert('users',$adduser);
                        if($query)
                        {
                            $getuser= query("select * from `users` order by id desc limit 1");

                            $row =fetch_array($getuser);
			    $_SESSION["user"] = $row["id"];
			    $_SESSION["fullName"] = $row["name"]." ".$row["last_name"];
                            
                            
                            if(isset($_SESSION["cart"]))
                            {
                                header("Location: cart.php");
                            }
                            else
                            {
                                header("Location: dashboard.php");
                            }
                        }
                        else
                        {
                           $_SESSION["regerror"] = "User cannot add!";	
                           header("Location: login.php");
                        }
                          
                          
		  }
		  else
		  {
			  $_SESSION["regerror"] = "Email is already Exist!";	
			  header("Location: login.php");
		  }
            }
            else
            {
                 $_SESSION["regerror"] = "Captcha is Invalid!";	
		 header("Location: login.php");
            }
            
	}
        
        
  }
?>