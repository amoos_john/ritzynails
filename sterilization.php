<?php 
include_once('config/connection.php');
include_once('config/model.php');
include_once('config/functions.php');

$code="sterilization";
$result= getContent($code);
$content = fetch_array($result);

$title=$content["title"];
$body=$content["body"];
$banner=$content["image"];
$description=$content["metaDescription"];
$keywords=$content["keywords"];

$pagetitle="Sterilization";
include("header.php");
?>

<section class="inr-bnr-area ster-bnr clrlist" style="background-image: url(<?php echo $banner;?>);">
	<div class="container">
		<div class="inr-bnr-cont">
			
		</div>
	</div>
</section>

<section class="ster-cont-area">
	<div class="container">
		<div class="ster__cont__title col-sm-12">
			<h2><?php echo $title; ?></h2>
		</div>
		<div class="ster__cont__lft col-sm-7">
                    <?php echo $body; ?>
		</div>
		<div class="gall__slider__box ster__slider__box col-sm-5">
			<div id="carousel-example-generic" class="carousel slide slider--pauseplay" data-ride="carousel" data---interval="false">
			  <!-- Indicators -->
			  
			  <ol class="carousel-indicators thumbs">
				<li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
				<li data-target="#carousel-example-generic" data-slide-to="1"></li>
				<li data-target="#carousel-example-generic" data-slide-to="2"></li>
			  </ol>

			  <!-- Wrapper for slides -->
			  <div class="carousel-inner">

				<div class="item active">
				  <img src="images/ster-slide1.jpg" alt="...">
				</div>

				<div class="item">
				  <img src="images/ster-slide2.jpg" alt="...">
				</div>
				

				<div class="item">
				  <img src="images/ster-slide3.jpg" alt="...">
				</div>


			  </div>

			  <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
				<span class="fa fa-angle-left"></span>
			  </a>
			  <a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
				<span class="fa fa-angle-right"></span>
			  </a>
			  
			  
			</div>
		</div>	
	</div>
</section>


	
<?php include("footer.php"); ?>