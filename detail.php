<?php
extract($_POST);
include_once('config/connection.php');
include_once('config/model.php');


$colors=OrderColor($id);
if(count($colors)>0)
{
?>
<table class="table table-striped table-bordered order__table__inr">
				<thead>
					<tr>
						
						<th>Person</th>
						<th>Color Name</th>
                                                <th>French Manicure</th>
					</tr>
				</thead>
                            <tbody>
                                
                            <?php
                            while($color=fetch_object($colors))
                            {        
                            ?>
					<tr>
						<td><?php echo ($color->person)?$color->person:'';?></td>
						<td><?php echo $color->color_name;?></td>
                                                <td><?php echo ($color->french_manicure!=0)?'Yes':'No';?></td>
					</tr>
					
                           <?php
}?>
				
                           </tbody>
</table>
<?php
}
?>