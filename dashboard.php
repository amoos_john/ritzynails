<?php 
include_once('config/connection.php');
include_once('config/model.php');

if(!isset($_SESSION["user"]))
{
 
    echo "<script type='text/javascript'>window.location='index.php';</script>";
    exit;
}


$pagetitle="Dashboard";
include("header.php");

?>

<section class="dashboard-area">

	<div class="container">
		<div class="dboard__logo text-center col-sm-12">
			<div class="dboard__logo__img">
				<img src="images/logo.png" alt=" ">
			</div>
		</div>
		<div class="dboard__inr clrlist text-center col-sm-12">
                    <?php
                    if(isset($_SESSION["results"]))
                    {
                        echo $_SESSION["results"];
                        unset($_SESSION["results"]);
                    }
                    ?>
                    <h3>Welcome <?php echo $_SESSION["fullName"]; ?>!</h3>
			<ul>
				<li class="dboard__list">
					<a href="profile.php"><div class="dboard__list__circle"><i class="fa fa-user"></i></div></a>
					<a href="profile.php"><div class="dboard__list__link">Profile</div></a>
				</li>
				<li class="dboard__list">
					<a href="orders.php"><div class="dboard__list__circle"><i class="fa fa-shopping-cart"></i></div></a>
					<a href="orders.php"><div class="dboard__list__link">Orders</div></a>
				</li>
			</ul>
		</div>
	</div>

</section>


<?php include("footer.php"); ?>