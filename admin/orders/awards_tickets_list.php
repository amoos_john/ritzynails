<?php
$per_page=40;

$sql="SELECT o.id as order_id,b.firstName, b.lastName,c.paid_amount,c.created,c.unable_to_attend,c.unable_attend_name,c.fname,c.lname FROM `competitor_receiving_awards` AS a
INNER JOIN `competitors` AS b ON a.competitor_id = b.id
INNER JOIN `awards_transactions` as c on c.competitor_id=a.competitor_id
INNER JOIN `transactions` AS o ON o.id = c.order_id
where a.status=1 and o.deleted=0 ";
$subSql="group by o.id order by o.created desc";
if (isset($_GET["name"])) {
    $name = $_GET["name"];
    $mainSql=$sql . " and (b.firstName LIKE '%".$name."%' OR b.lastName LIKE '%" . $name . "%') ".$subSql;
    $result = query($mainSql);
} else {
    if (isset($_GET["page"])) {
        $page = $_GET["page"];
    } else {
        $page = 1;
    }
    $start_from = ($page - 1) * $per_page;
    $mainSql=$sql . $subSql;
    $result = query($mainSql." LIMIT $start_from, $per_page");
}
?>
<!--
<div class="page-title">
    <div class="title_left">
        <h3> <small> </small> </h3>
    </div>
    <div class="title_right">
        <form action="orders.php?type=awards_tickets" >
            <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                <div class="input-group">
                    <input required type="text" class="form-control" name="name" placeholder="Search">
                    <span class="input-group-btn">
                        <button class="btn btn-default" name="search" type="submit">Go!</button>
                    </span>
                </div>
            </div>
        </form>
    </div>
</div>
-->
<div class="clearfix"></div>
<div class="row">
    <div class="clearfix"></div>
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Competitor Orders</h2>

                <ul class="nav navbar-right panel_toolbox">
                    <li>
                        <select id="selectbox" name="" onchange="javascript:location.href = this.value;">
                            <option value="" selected> -- Export To Excel -- </option>
                            <option value="getExcel.php?report=all">ALL ORDERS Excel</option>
                            <option value="html2pdf.php">ALL ORDERS PDF</option>
                            <option value="getExcel.php?report=belts">BELTS</option>
                            <option value="getExcel.php?report=jackets">JACKETS</option>
                            <option value="getExcel.php?report=rings">Rings</option>
                        </select>
                    </li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <table class="table table-striped responsive-utilities jambo_table bulk_action">
                    <thead>
                        <tr class="headings">
                            <th class="column-title">#</th>
                            <th class="column-title">Competitor Name </th>
                            <th class="column-title">Paid Amount</th>
                            <th class="column-title">Payment Date</th>
                            <th class="column-title">Status</th>
                            <th class="column-title no-link last"><span class="nobr">Action</span></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        if (num_rows($result) > 0) {

                            if (isset($_POST["search"])) {
                                $i = 1;
                            } else {
                                $i = $per_page * $page - ($per_page - 1);
                            }
                            while ($row = fetch_object($result)) {
                                if ($row->unable_to_attend == 'Friend/Instructor Pi') {
                                    $status = 'Friend/Instructor Pickup';
                                } else if ($row->unable_to_attend == 'Ship My Awards') {
                                    $status = 'Ship Awards';
                                } else {
                                    $status = 'Self Pickup';
                                }
                                echo '<tr class="even pointer">
						  <td class=" ">' . $i . '</td>
						  <td class=" ">' . $row->firstName . " " . $row->lastName . '</td>
						  <td class=" ">' . $storeCurrency[2] . $row->paid_amount . '</td>
						  <td class=" ">' . date('d M Y', strtotime($row->created)) . '</td>
							<td class=" ">' . $status . '</td>';
                                echo '<td><a href="order.php?order_id=' . $row->order_id . '"><span class="label label-info">View</span></a> &nbsp';
                                echo '<a href="javascript:deleteId(' . $row->order_id . ');"><span class="label label-danger">Delete</span></a>';
                                echo '</td></tr>';
                                $i++;
                            }
                        }

//Now select all from table
                        $query = query($mainSql);

// Count the total records
                        $total_records = num_rows($query);

//Using ceil function to divide the total records on per page
                        $total_pages = ceil($total_records / $per_page);
                        ?>
                    </tbody>
                </table>
            </div>
            <!-- Pagination Start-->		
            <div class="dataTables_paginate paging_full_numbers" id="example_paginate">
                <a class="first paginate_button" href="orders.php?type=<?php echo $type; ?>&page=1" tabindex="0" id="example_first">First</a>
                <!-- <a class="previous paginate_button" tabindex="0" id="example_previous">Previous</a> -->
                <span>
                    <?php
                    for ($i = 1; $i <= $total_pages; $i++) {
                        echo '<a class="paginate_button" href="orders.php?type=' . $type . '&page=' . $i . '" tabindex="0">' . $i . '</a>';
                    };
                    ?>
                </span>
                <a class="last paginate_button" href="orders.php?type=<?php echo $type; ?>&page=<?php echo $total_pages; ?>" tabindex="0" id="example_last">Last</a> 
            </div>
        </div>
    </div>
</div>