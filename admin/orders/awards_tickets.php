<?php
$pageTitle = "Competitor Receiving Awards";

$sql = "SELECT b.firstName, b.lastName,c.paid_amount,c.created as created,c.unable_to_attend,c.unable_attend_name,c.fname,c.lname,c.email as orderEmail,c.food_allergies,c.our_staff,c.address1,c.address2,c.city,c.state,c.country,c.zip,a.competitor_id FROM `competitor_receiving_awards` AS a
INNER JOIN `competitors` AS b ON a.competitor_id = b.id
INNER JOIN `awards_transactions` as c on c.competitor_id=a.competitor_id
where c.order_id=" . $order_id . "  and a.status=1 and c.deleted=0 group by c.order_id order by c.created desc";
$comp_result = query($sql);
if (num_rows($comp_result) > 0) {
    $comp_row = fetch_array($comp_result);
    $fName = $comp_row['firstName'];
    $lName = $comp_row['lastName'];
    $email = $comp_row['email'];
    $cid = $comp_row['competitor_id'];
    $our_staff = $comp_row['our_staff'];
    $food_allergies = $comp_row['food_allergies'];
    if ($food_allergies == '') {
        $food_allergies = 'Not Defined';
    }
    if ($our_staff == '') {
        $our_staff = 'Not Defined';
    }



    $pickup_result = query("select unable_to_attend,unable_attend_name from `awards_transactions` where competitor_id=$cid order by id desc limit 1");
    if (num_rows($pickup_result) > 0) {
        $pickup_row = fetch_array($pickup_result);
        if ($pickup_row['unable_to_attend'] == 'Friend/Instructor Pi') {

            $friend_pickup = '<tr class="odd gradeX">
								<th>Friend/Instructor Pickup</th><td>Name : ' . $pickup_row['unable_attend_name'] . '</td>
							</tr>';
        } else {
            $friend_pickup = '';
        }
    } else {
        $friend_pickup = '';
    }
    $sql = "SELECT a.quantity,a.price as awardPrice,a.id, b.title,b.price,b.code,b.images FROM `competitor_receiving_awards` as a inner join competitor_awards as b on a.award_id=b.id where a.deleted=0 and a.competitor_id=$cid order by a.award_id";

    $summary_result = $conn->query($sql);
    ?>

    <style>
        .size_update{
            border:1px solid gray;
            margin-top:25px;
            margin-bottom:10px;
            padding:15px;
        }
    </style>
    <!-- page content -->

    <div class="page-title">
        <div class="title_left">
            <h3> Competitor Receiving Awards </h3>
        </div>
    </div>
    <div class="clearfix"></div>
    <div class="row">
        <div class="col-md-12">
            <div class="x_panel">
                <div class="x_content">
                    <div class="row">
                        <a href="orders.php?type=<?php echo $type;?>"><button class="btn btn-primary waves-effect waves-button waves-float pull-right">Go Back</button></a>
                        <a href="competitorView.php?id=<?php echo $cid; ?>"><button class="btn btn-primary">View Competitor</button></a>
                        <table class="table table-striped table-bordered table-hover" id="dataTables-example">

                            <tr class="odd gradeX">
                                <th>First Name</th><td><?php echo $fName; ?></td>
                            </tr>

                            <tr class="odd gradeX">
                                <th>Last Name</th><td><?php echo $lName; ?></td>
                            </tr>

                            <tr class="odd gradeX">
                                <th>Email Address</th><td><?php echo $email; ?></td>
                            </tr>
                            <tr class="odd gradeX">
                                <th>Order date</th><td><?php echo $comp_row['created']; ?></td>
                            </tr>

                            <tr class="odd gradeX">
                                <th>Food Allergies</th><td><?php echo $food_allergies; ?></td>
                            </tr>

                            <tr class="odd gradeX">
                                <th>Notes for our staff</th>
                                <td>
                                    <?php echo $our_staff; ?>

                                    <form method="post" action="update_award_values.php" id="update_notes" class="size_update" style="display:none;">
                                        <br>
                                        <input type="hidden" name="id" value="<?php echo $cid; ?>" /><input type="hidden" value="<?php echo $order_id; ?>" name="order_id">

                                        <label>Update Notes:</label>
                                        <input style="width:80%;" type="text" name="notes" value="<?php echo $our_staff; ?>" required />

                                        <br>	
                                        <button type="submit" class="btn btn-primary">Update</button>

                                    </form>
                                    <a class="pull-right" onclick="edit_notes()" >
                                        <span class="label label-info"> &nbsp <i class="fa fa-pencil"></i> &nbsp </span>
                                    </a>
                                </td>
                            </tr>
                            <?php echo $friend_pickup; ?>											

                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- /CONTENT MAIL -->
            </div>




            <div class="x_panel">
                <div class="x_title">
                    <h2>Transaction Summary</h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                        </li>
                        <li><a class="close-link"><i class="fa fa-close"></i></a>
                        </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <form method="post" action="resend_email.php" >
                        <input type="hidden" value="<?php echo $cid; ?>" name="id">
                        <input type="hidden" value="<?php echo $order_id; ?>" name="order_id">
                        <button type="submit" name="resend" class="btn btn-success pull-left">Resend E-Mail Confirmation</button>
                        <?php if (isset($_GET['response'])) { ?> 
                            <label id="msg" style="color:green;">Email Sent Successfully.</label>
                        <?php } ?>
                    </form>
                    <ul style="color:red; letter-spacing:1px; margin-top:10px;">
                        <li><b>Note : </b></li>
                        <li>Included Certificate - Additional Certificate(s) after the first included are $20 each.</li>
                        <li>Included Belt - Additional Belt(s) after the first included are $50 each. </li>
                        <li>Included Jacket - Additional Jacket(s) after the first included are $70 each. </li>
                    </ul>
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Title</th>
                                <th>Price</th>
                                <th>Quantity</th>
                                <th>Image</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>

                            <?php
                            if (num_rows($summary_result) > 0) {

                                $i = 1;
                                while ($row = fetch_array($summary_result)) {
                                    $btnEdit = '';
                                    if (!isset($row['awardPrice'])) {
                                            $price = 0;
                                        } else {
                                            $price = $row['awardPrice'];
                                        }
                                    ?>

                                    <tr>
                                        <td><?php echo $i; ?></td>
                                        <td><?php echo $row['title'] . " ( " . strtoupper(str_replace('_', ' ', $row['code'])); ?> )
                                            <?php
                                            // Selects Size if Exists
                                            $size_sql = "SELECT b.size,b.id,a.id as selected_id from selected_sizes as a inner join award_sizes as b on a.size_id=b.id where a.receiving_id=" . $row['id'];
                                            $size_result = $conn->query($size_sql);
                                            if (num_rows($size_result) > 0) {
                                                $dropdowns = '';
                                                while ($size_row = fetch_array($size_result)) {
                                                    echo "<p> Size : ( " . $size_row['size'] . " )</p>";

                                                    $dropdowns.='<input type="hidden" name="receiving_id[]" value="' . $size_row['selected_id'] . '" />';

                                                    if (is_numeric($size_row['size'])) {
                                                        $sql = "SELECT id,size from award_sizes where award_type='belt'";
                                                    } else {
                                                        $sql = "SELECT id,size from award_sizes where award_type!='belt'";
                                                    }
                                                    $size_update = $conn->query($sql);
                                                    $options = '';
                                                    while ($rows = $size_update->fetch_assoc()) {
                                                        $selected = '';
                                                        if ($rows['id'] == $size_row['id']) {
                                                            $selected = 'selected';
                                                        }
                                                        $options .="<option " . $selected . " value='" . $rows['id'] . "' >" . $rows['size'] . "</option>";
                                                    }



                                                    $dropdowns.='<select class="form-control" name="size[]">' . $options . '</select><br>';
                                                }
                                                ?>





                                                <?php
                                                $btnEdit = '<a onclick="edit(' . $row['id'] . ')" >
														<span class="label label-info"> &nbsp <i class="fa fa-pencil"></i> &nbsp </span>
													</a>';
                                            }


                                            // Selects Embroidery Name if Exists
                                            $name_sql = "SELECT id,name from embroidery_names where receiving_id=" . $row['id'];
                                            $name_result = $conn->query($name_sql);
                                            if (num_rows($name_result) > 0) {
                                                $inputs = '';
                                                while ($name_row = fetch_array($name_result)) {
                                                    echo "<p> Name On Embroidery  : ( " . $name_row['name'] . " )</p>";
                                                    $inputs.='<input type="hidden" name="receiving_id[]" value="' . $name_row['id'] . '" />';
                                                    $inputs.='<br><input type="text" name="name[]" value="' . $name_row['name'] . '" required /><br>';
                                                }
                                                ?>
                                                <form method="post" action="update_award_values.php" id="size_update_<?php echo $row['id']; ?>" class="size_update" style="display:none;">
                                                    <input type="hidden" name="id" value="<?php echo $id; ?>" />
                                                    <input type="hidden" name="order_id" value="<?php echo $order_id; ?>" />
                                                    <label>Update Name:</label>
                                                    <?php echo $inputs; ?>
                                                    <br>	
                                                    <button type="submit" class="btn btn-primary">Update</button>

                                                </form>
                                                <?php
                                                $btnEdit = '<a onclick="edit(' . $row['id'] . ')" >
														<span class="label label-info"> &nbsp <i class="fa fa-pencil"></i> &nbsp </span>
													</a>';
                                            }
                                            ?>
                                        </td>
                                        <td><?php echo $storeCurrency[2] . ' ' . $price; ?></td>
                                        <td><?php echo $row['quantity']; ?></td>
                                        <td><img src="../awards/<?php echo $row['images']; ?>" height="50" width="70" /></td>
                                        <td>
                                            <?php echo $btnEdit; ?>

                                            &nbsp  
                                            <a href="javascript:deleteId(<?php echo $row['id']; ?>);" >
                                                <span class="label label-danger"> &nbsp <i class="fa fa-times"> &nbsp </span></i>
                                            </a>
                                        </td>

                                    </tr>

                                    <?php
                                    $i++;
                                }
                                ?>
                                <tr>
                                    <td>#</td>
                                    <td>Competitor Ticket</td>
                                    <td>
                                        <?php
                                        $ticket_price_sql = query("SELECT ticket_price,paid_amount from `awards_transactions` where competitor_id=$cid");
                                        $ticket_price_row = fetch_array($ticket_price_sql);
                                        $paid_amount = $ticket_price_row['paid_amount'];
                                        $ticket_pirce=  get_ticket_price($row->created);
                                        echo $storeCurrency[2] . ' ' . $ticket_pirce;
                                        
                                        
                                        
                                        
                                        ?>
                                    </td>
                                    <td colspan="3">1</td>
                                </tr>
                                <?php
                                $family_ticket_sql = query("SELECT ticket_price,ticket_quantity from `competitor_tickets` where competitor_id=$cid order by id desc limit 1");
                                if (num_rows($family_ticket_sql) > 0) {
                                    $family_ticket_row = fetch_array($family_ticket_sql);
                                    $family_ticket_price = $family_ticket_row['ticket_price'];
                                    $quantity = $family_ticket_row['ticket_quantity'];
                                    ?>

                                    <tr>
                                        <td>#</td>
                                        <td>Family & Friends Tickets</td>
                                        <td><?php echo $storeCurrency[2] . ' ' . $family_ticket_price; ?></td>
                                        <td colspan="2" >
                                            <?php echo $quantity; ?>
                                            <form method="post" action="update_award_values.php" id="ticket_quantity" style="display:none;">
                                                <input type="hidden" name="id" value="<?php echo $cid; ?>" />
                                                <input type="hidden" name="order_id" value="<?php echo $order_id; ?>" />
                                                <input type="hidden" name="price" value="<?php echo $family_ticket_price; ?>" />
                                                <input type="hidden" name="previous_quantity" value="<?php echo $quantity; ?>" />

                                                <input name="ticket_quantity" style='width:50px;' min='1' type='number' value='<?php echo $quantity; ?>' required />
                                                <button type="submit" class="btn btn-primary">Update</button>

                                            </form>

                                        </td>
                                        <td>
                                            <a onclick="editTickets()" >
                                                <span class="label label-info"> &nbsp <i class="fa fa-pencil"></i> &nbsp </span>
                                            </a>
                                            &nbsp  
                                            <a href="javascript:deleteTickets(<?php echo $cid; ?>);" >
                                                <span class="label label-danger"> &nbsp <i class="fa fa-times"> &nbsp </span></i>
                                            </a>
                                        </td>
                                    </tr>

                                    <?php
                                }
                                ?>
                                <tr>
                                    <td scope="row" colspan="6"></td>
                                </tr>
                                <tr>                	
                                    <td>#</td>
                                    <td>
                                        
                                        <b>Grand Total</b>
                                        <!--<p>Want to change Grand Total Amount ? <a onclick="grand()"><b style="color:green;"> Click Here </b></a> </p>-->
                                    </td>
                                    <td colspan="3"><b><?php echo $storeCurrency[2] . ' ' . $orderTotal; ?></b>
                                        <br>
                                        <form method="post" action="update_award_values.php" id="grand_total" class="size_update" style="display:none;">
                                            <input type="hidden" name="id" value="<?php echo $cid; ?>" />
                                            <input type="hidden" name="order_id" value="<?php echo $order_id; ?>" />
                                           <?php echo $storeCurrency[2];?> <input type="text" name="grand" value="<?php echo $paid_amount; ?>" required />
                                            <button type="submit" class="btn btn-primary">Update</button>

                                        </form>

                                    </td>
                                    <td>
                                        <!--
                                        <a href="update_competitor_order.php?id=<?php echo $cid; ?>">
                                            <button class="btn btn-primary">Add More Items</button>
                                        </a>
                                        -->
                                    </td>
                                </tr>
                                <?php
                            }
                            ?>								


                        </tbody>
                    </table>

                </div>
            </div>

        </div>
    </div>

    <!-- /page content -->

    <div id="custom_notifications" class="custom-notifications dsp_none">
        <ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group"></ul>
        <div class="clearfix"></div>
        <div id="notif-group" class="tabbed_notifications"></div>
    </div>

    <script src="js/bootstrap.min.js"></script>
    <!-- chart js -->
    <script src="js/chartjs/chart.min.js"></script>
    <!-- bootstrap progress js -->
    <script src="js/progressbar/bootstrap-progressbar.min.js"></script>
    <script src="js/nicescroll/jquery.nicescroll.min.js"></script>
    <!-- icheck -->
    <script src="js/icheck/icheck.min.js"></script>
    <script src="js/custom.js"></script>

    <script>

                                        function deleteId(value) {
                                            var r = confirm("Sure to Remove this Item?");
                                            if (r == true) {
                                                window.location = 'update_competitor_order_form.php?id=<?php echo $id; ?>&order_id=<?php echo $order_id; ?>&delete=' + value;
                                            }

                                        }


                                        function deleteTickets() {
                                            var r = confirm("Sure to Remove Faimly & Friend Tickets ?");
                                            if (r == true) {
                                                window.location = 'update_competitor_order_form.php?deleteTicket=delete&id=<?php echo $id; ?>&order_id=<?php echo $order_id; ?>';
                                            }

                                        }


                                        function editTickets() {

                                            var form_id = $('#ticket_quantity');
                                            if (form_id.css('display') == 'none') {
                                                form_id.show();
                                            } else {
                                                form_id.hide();
                                            }


                                        }




                                        function edit(value) {

                                            var form_id = $('#size_update_' + value);
                                            if (form_id.css('display') == 'none') {
                                                form_id.show();
                                            } else {
                                                form_id.hide();
                                            }


                                        }

                                        function grand() {
                                            var grand_total = $('#grand_total');
                                            if (grand_total.css('display') == 'none') {
                                                grand_total.show();
                                            } else {
                                                grand_total.hide();
                                            }

                                        }



                                        function edit_notes() {

                                            var form_id = $('#update_notes');
                                            if (form_id.css('display') == 'none') {
                                                form_id.show();
                                            } else {
                                                form_id.hide();
                                            }


                                        }



                                        $(document).ready(function () {
                                            window.setTimeout('fadeout();', 2000);
                                        });

                                        function fadeout() {
                                            $('#msg').fadeOut('slow', function () {
                                                // Animation complete.
                                            });
                                        }
    </script>

    </body>

    </html>
    <?php
}
?>