<?php
$sql = "SELECT * FROM family_friends_tickets where order_id=$order_id";
$result = query($sql);
$row=fetch_object($result);
?>
<div class="x_content">
    <div class="row">
        <table class="table table-striped table-bordered table-hover" id="dataTables-example">
            <tbody><tr class="odd gradeX">
                    <th>ID</th><td><?php echo $row->order_id?></td>
                </tr>

                <tr class="odd gradeX">
                    <th>First Name</th><td><?php echo $row->fname?></td>
                </tr>

                <tr class="odd gradeX">
                    <th>Last Name</th><td><?php echo $row->lname?></td>
                </tr>

                <tr class="odd gradeX">
                    <th>Email</th><td><?php echo $row->email?></td>
                </tr>

                <tr class="odd gradeX">
                    <th>Home Phone</th><td><?php echo $row->homePhone?></td>
                </tr>

                <tr class="odd gradeX">
                    <th>Cell Phone</th><td><?php echo $row->cellPhone?></td>
                </tr>

                <tr class="odd gradeX">
                    <th>Address 1</th><td><?php echo $row->address1?></td>
                </tr><tr class="odd gradeX">
                    <th>Address 2 (Optional)</th><td><?php echo $row->address2?></td>
                </tr>

                <tr class="odd gradeX">
                    <th>City</th><td><?php echo $row->city?></td>
                </tr>

                <tr class="odd gradeX">
                    <th>State</th><td><?php echo $row->state?></td>
                </tr>

                <tr class="odd gradeX">
                    <th>Country</th><td><?php echo $row->country?></td>
                </tr>

                <tr class="odd gradeX">
                    <th>Zip</th><td><?php echo $row->zip?></td>
                </tr><tr class="odd gradeX">
                    <th>Food Allergies (Optional)</th><td><?php echo $row->foodAllergies?></td>
                </tr><tr class="odd gradeX">
                    <th>Our Staff (Optional)</th><td><?php echo $row->ourStaff?></td>
                </tr>
                <tr class="odd gradeX">
                    <th>Tickets Quantity</th><td><?php echo $row->ticketQuantity?></td>
                </tr>
                <tr class="odd gradeX">
                    <th>Total Amount</th><td><?php echo $storeCurrency[2] . ' ' .$row->amount?></td>
                </tr>

                                           </tbody>
        </table>
    </div>
</div>