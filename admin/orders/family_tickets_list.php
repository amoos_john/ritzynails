<?php
$per_page = 20;

$sql = "select o.id as order_id,f.fname as fname,f.lname as lname,f.amount as amount ,f.created as created,f.ticketQuantity from transactions o inner join `family_friends_tickets` f on f.order_id=o.id where o.deleted=0 ";
$subSql = "order by o.id desc";

if (isset($_POST["search"])) {
    $name = $_POST["name"];
    $mainSql = $sql . " AND f.fname like '" . $name . "%' || f.lname like '" . $name . "%' ";
    $result = query($mainSql . $subSql);
} else {
    if (isset($_GET["page"])) {
        $page = $_GET["page"];
    } else {
        $page = 1;
    }
    $start_from = ($page - 1) * $per_page;
    $mainSql = $sql . $subSql;
    $result = query($mainSql." LIMIT $start_from, $per_page");
}
if (isset($_POST["search"])) {
    $i = 1;
} else {
    $i = $per_page * $page - ($per_page - 1);
}
// echo $mainSql;
?>
<!-- page content -->
<!-- 
<div class="page-title">
    <div class="title_left">
        <h3> <small> </small> </h3>
    </div>
    <div class="title_right">
        <form action="familyTicket.php?asdasd">
            <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                <div class="input-group">
                    <input type="text" class="form-control" name="name" placeholder="Search">
                    <span class="input-group-btn">
                        <button class="btn btn-default" name="search" type="submit">Go!</button>
                    </span>
                </div>
            </div>
        </form>
    </div>
</div>
-->
<div class="clearfix"></div>
<div class="row">
    <div class="clearfix"></div>
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Family & Friend Tickets Table</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <table class="table table-striped responsive-utilities jambo_table bulk_action">
                    <thead>
                        <tr class="headings">
                            <th class="column-title">#</th>
                            <th class="column-title">First Name </th>
                            <th class="column-title">Last Name </th>
                            <th class="column-title">Total Amount</th>
                            <th class="column-title">Payment Date </th>
                            <th class="column-title no-link last"><span class="nobr">Action</span></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        while ($row = fetch_object($result)) {

                            echo '<tr class="even pointer">
			<td class=" ">' . $i . '</td>
 			  <td class=" ">' . $row->fname . '</td>
 			  <td class=" ">' . $row->lname . '</td>
 			  <td class=" ">$ ' . $row->amount . '</td>
 			  <td class=" ">' . date('d M Y', strtotime($row->created)) . '</td>';

                            echo '<td><a href="order.php?order_id=' . $row->order_id . '"><span class="label label-info">View</span></a> &nbsp';
                            echo '<a href="javascript:deleteId(' . $row->order_id . ');"><span class="label label-danger">Delete</span></a>';
                            echo '</td></tr>';
                            $i++;
                        }
                        ?>
                    </tbody>
                </table>
            </div>
            <!-- Pagination Start-->		
            <div class="dataTables_paginate paging_full_numbers" id="example_paginate">
                <a class="first paginate_button" href="orders.php?type=<?php echo $type; ?>&page=1" tabindex="0" id="example_first">First</a>
                <!-- <a class="previous paginate_button" tabindex="0" id="example_previous">Previous</a> -->
                <span>
                    <?php
                        $query = query($mainSql);
                        $total_records = num_rows($query);
                        $total_pages = ceil($total_records / $per_page);
                    for ($i = 1; $i <= $total_pages; $i++) {
                        echo '<a class="paginate_button" href="orders.php?type=' . $type . '&page=' . $i . '" tabindex="0">' . $i . '</a>';
                    };
                    ?>
                </span>
                <a class="last paginate_button" href="orders.php?type=<?php echo $type; ?>&page=<?php echo $total_pages; ?>" tabindex="0" id="example_last">Last</a> 
            </div>
            <!-- Pagination End-->
        </div>
    </div>
</div>