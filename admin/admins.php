<?php 
	$pageTitle = "Admins"; 
	include('header.php'); 
?>
    <!-- page content -->
    <div class="right_col" role="main">
    	<div class="">
			<div class="page-title">
            	<div class="title_left">
                	<h3> <small> Admins </small> </h3>
                </div>
				
             </div>
             <div class="clearfix"></div>
	             <div class="row">
                    <div class="clearfix"></div>
						 <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="x_panel">
                                <div class="x_title">
                                    <h2>Admins Table</h2>
                                    <ul class="nav navbar-right panel_toolbox">
                                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                                        <li><a class="close-link"><i class="fa fa-close"></i></a></li>
                                    </ul>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="x_content">
						          <?php if($_SESSION["role"] == 1){ ?>
                                    <a href="addadmin.php">
                                    	<button class="btn btn-primary waves-effect waves-button waves-float pull-right">Add Admin</button>
                                    </a>
								  <?php } ?>
                                  <table class="table table-striped responsive-utilities jambo_table bulk_action">
                                  	<thead>
                                    	<tr class="headings">
                                        	<th class="column-title">Image </th>
											<th class="column-title">Name </th>
                                            <th class="column-title">Email </th>
											<th class="column-title">User Name </th>
											<th class="column-title">Status </th>
											<th class="column-title">Created Date </th>
											<th class="column-title">Last Modified</th>
											<th class="column-title">Role </th>
											<?php if($_SESSION["role"] == 1){ ?>
											 <th class="column-title no-link last"><span class="nobr">Action</span></th>
											<?php } ?>
		                                </tr>
        		                      </thead>
						              <tbody>
                        		        <?php include('admindata.php'); ?>
                                      </tbody>
									</table>
                                </div>
				<!-- Pagination Start-->		
				<div class="dataTables_paginate paging_full_numbers" id="example_paginate">
				       <a class="first paginate_button" href="admins.php?page=1" tabindex="0" id="example_first">First</a>
					  <!-- <a class="previous paginate_button" tabindex="0" id="example_previous">Previous</a> -->
					 <span>
						<?php for ($i=1; $i<=$total_pages; $i++) {
                            //echo “<a href=’pagination.php?page=”.$i.”‘>”.$i.”</a> “;
                            echo '<a class="paginate_button" href="admins.php?page='.$i.'" tabindex="0">'.$i.'</a>';
                             };
                        ?>
                          <!-- <a class="paginate_button" tabindex="0">1</a>
                           <a class="paginate_button" tabindex="0">2</a>
                           <a class="paginate_active" tabindex="0">3</a>
                           <a class="paginate_button" tabindex="0">4</a> -->
					 </span>
					 
					<!--  <a class="next paginate_button" tabindex="0" id="example_next">Next</a>-->
					  <a class="last paginate_button" href="admins.php?page=<?php echo $total_pages; ?>" tabindex="0" id="example_last">Last</a> 
			    </div>
				<!-- Pagination End-->
             </div>
           </div>
         </div>
       </div>
      <!-- footer content -->
      <footer>
	      <div class="">
           	<p class="pull-right">
            	<span><?php echo $adminTitle["copyright"]; ?></span> 
            </p>
          </div>
          <div class="clearfix"></div>
      </footer>
      <!-- /footer content -->
    </div>
    <!-- /page content -->
  </div>
</div>

    <div id="custom_notifications" class="custom-notifications dsp_none">
        <ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group">
        </ul>
        <div class="clearfix"></div>
        <div id="notif-group" class="tabbed_notifications"></div>
    </div>

    <script src="js/bootstrap.min.js"></script>
    <!-- chart js -->
    <script src="js/chartjs/chart.min.js"></script>
    <!-- bootstrap progress js -->
    <script src="js/progressbar/bootstrap-progressbar.min.js"></script>
    <script src="js/nicescroll/jquery.nicescroll.min.js"></script>
    <!-- icheck -->
    <script src="js/icheck/icheck.min.js"></script>
    <script src="js/custom.js"></script>
	
	<script>
	 function deactivateAdmin(value){
		  var r = confirm("Sure to deactive this admin?");
			if (r == true) {
				window.location ='adminaction.php?deactivateId='+value;
			} 
			else 
			{
			}
	  }
	  
	  
	function activateAdmin(value){
		  var r = confirm("Sure to Active this admin?");
			if (r == true) {
				window.location ='adminaction.php?activateId='+value;
			} 
			else 
			{
			}
	  }
	  
	  
	   function deleteId(value){
		  var r = confirm("Sure to Delete this admin?");
			if (r == true) {
				window.location ='adminaction.php?delete='+value;
			} 
			else 
			{   
			}
	  }
	  
	</script>

</body>

</html>