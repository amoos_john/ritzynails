<?php 
        include_once('../config/connection.php');
        include_once('sessions.php');
        include_once('../config/model.php');
        include_once('../config/functions.php');
	include_once('../config/constants.php');
	
        
	$mDate=date("Y-m-d H:i:s");
        if(isset($_GET["active"]))
	{
	    $id =  charEsc($_GET["active"]);
            $query=query("UPDATE `orders` SET `order_status`='success',`modified_date`='{$mDate}' WHERE `id`='{$id}'");
            $sql=query("UPDATE `order_booking` SET `status`='booked',`modified_date`='{$mDate}' WHERE `order_id`='{$id}'");
            if($query)
            {
                $result=getOrderId($id);
                if(count($result)>0)
                {
                      $order=fetch_array($result);

                }
                $books=OrderBook($id);
                if(count($books)>0)
                {
                      $book=fetch_array($books);

                }
                
                $users=getProfile($order["user_id"]);
                $user=fetch_array($users); 
                $from=$account;
                $to=$user["email"];
                
                $subject = 'Ritzy Nails Order Confirmation';
            // message
                $body = '
                <html>
                <head>
                  <title>Ritzy Nails Order Confirmation</title>
                  
                </head>
                <body>
                    <p>Thank you for order in Ritzy Nails.Kindly see your Order and booking details.</p>
                <table>
                    <thead>
                    <tr><th colspan="2">Order Details</th>
                    </tr>
                    </thead>

                    <tbody>
                    <tr>
                        <td>Order ID</td>
                        <td>'.$order["id"].'</td>
                    </tr>
                    <tr>
                        <td>Payment ID</td>
                        <td>'.$order["payment_id"].'</td>
                    </tr>
                     <tr>
                        <td>Order Status</td>
                        <td>'.$order["order_status"].'</td>
                    </tr>
                    <tr>
                        <td>Order Date</td>
                        <td>'.DateFormat($order["created_date"]).'</td>
                    </tr>
                   
                    <tr id="addfee">
                        <td>Parking fee</td>
                        <td>$'.$order["parking"].'</td>
                    </tr>
                    <tr class="total-area-btns">
                         <td>Order Total</td>
                        <td>$'.$order["grand_total"].'</td>   
                    </tr>

                </tbody>
                </table>
                    <table>
                    <thead>
                    <tr><th colspan="2">Booking Detail</th>
                    </tr>
                    </thead>

                    <tbody>
                   
                    <tr>
                        <td>Date</td>
                        <td>'.DateFormat($book["date"]).'</td>
                    </tr>
                     <tr>
                        <td>Start Time</td>
                        <td>'.$book["start_time"].'</td>
                    </tr>
                    <tr>
                        <td>End Time</td>
                        <td>'.$book["end_time"].'</td>
                    </tr>
                    <tr>
                        <td>Booking Status</td>
                        <td>'.$book["status"].'</td>
                    </tr>
                    <tr>
                        <td>Address</td>
                        <td>'.$order["address"].'</td>
                    </tr>
                    
                   
                </tbody>
                </table>
                </body>
                </html>';
            
            // To send HTML mail, the Content-type header must be set
            $headers  = 'MIME-Version: 1.0' . "\r\n";
            $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";

            $headers .= 'From: '.$to.' <'.$from.'>' . "\r\n";
            $headers .= 'Cc: '.$account.'' . "\r\n";

            if(mail($to, $subject, $body, $headers))
            {
                $_SESSION['success']="Order updated successfully!";
            }
            else
            {
               $_SESSION['success']="Order email not send!";
            }
                
                
            }
            else
            {
                $_SESSION['error']="Order cannot update!";
            }
            //header('location: orders.php');
	}   
        if(isset($_GET["deactive"]))
	{
	    $id =  charEsc($_GET["deactive"]);
            $query=query("UPDATE `orders` SET `order_status`='pending',`modified_date`='{$mDate}' WHERE `id`='{$id}'");
            $sql=query("UPDATE `order_booking` SET `status`='pending',`modified_date`='{$mDate}' WHERE `order_id`='{$id}'");
            if($query)
            {
                $result=getOrderId($id);
                if(count($result)>0)
                {
                      $order=fetch_array($result);

                }
                $users=getProfile($order["user_id"]);
                $user=fetch_array($users); 
                $from=$account;
                $to=$user["email"];
                
                $subject = 'Ritzy Nails Order Confirmation';
            // message
                $body = '
                <html>
                <head>
                  <title>Ritzy Nails Order Confirmation</title>
                  
                </head>
                <body>
                    <p>Your Order has been canceled kindly contact with Ritzy nails emails. info@ritzynails.com</p>
                
                </body>
                </html>';
            
            // To send HTML mail, the Content-type header must be set
            $headers  = 'MIME-Version: 1.0' . "\r\n";
            $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";

            $headers .= 'From: '.$to.' <'.$from.'>' . "\r\n";
            $headers .= 'Cc: '.$account.'' . "\r\n";

            if(mail($to, $subject, $body, $headers))
            {
                $_SESSION['success']="Order updated successfully!";
            }
            else
            {
               $_SESSION['success']="Order email not send!";
            }
            }
            else
            {
                $_SESSION['error']="Order cannot update!";
            }
           //header('location: orders.php');       
	}  
$pageTitle = "View Orders"; 
include('header.php');         
?>
   <!-- page content -->
   <div class="right_col" role="main">
     <div class="">
        <div class="page-title">
            <div class="title_left">
               <h3> <?php echo $pageTitle ?></h3>
            </div>

           </div>
					
           <div class="clearfix"></div>
 
           <div class="row">
           <div class="clearfix"></div>
			 <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                 
                     <div class="x_content">
                         	<?php if(isset($_SESSION["error"])){ ?> 
				<div class="x_content bs-example-popovers">
					<div class="alert alert-danger alert-dismissible fade in" role="alert">
						<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
							<?php echo "<strong>Error ! </strong>"." ".$_SESSION["error"]; unset($_SESSION["error"]); ?>
					</div>
 				</div>
							<?php  unset($_SESSION["error"]);} ?>
									
				<?php if(isset($_SESSION["success"])){ ?> 
					<div class="x_content bs-example-popovers">
						<div class="alert alert-success alert-dismissible fade in" role="alert">
							<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
							<?php echo "<strong>Congrats ! </strong>"." ".$_SESSION["success"]; unset($_SESSION["success"]); ?>
						</div>
					</div>
				<?php
                                unset($_SESSION["success"]);
                                } ?>
                         <div class="table-responsive"> 
		          <table class="table table-striped responsive-utilities jambo_table bulk_action" id="datatable">
                           <thead>
                             <tr>
				<th>Order ID</th>
				<th>Payment ID</th>
                                <th>Email</th>
				<th>Order Date</th>						
                                <th>Order Total</th>
				<th>Order Status</th>
                                <th>Details</th>
				
				<!--<th class="column-title no-link last"><span class="nobr">Action</span></th>
                                 -->   
                             </tr>
                          </thead>
                          <tbody>
                             <?php
                         
                        //Selecting the data from table 
                        $result = query("SELECT ord.*,pay.order_id,pay.payment_id FROM orders ord
                        INNER JOIN  paypal pay ON ord.id=pay.order_id  order by ord.id DESC");
                        while($row = fetch_object($result)) 
                        {
                           $users=getProfile($row->user_id);
                           $user=fetch_array($users);      
                        ?>
                              <tr >
                                <td><?php echo $row->id; ?></td>
                                <td><?php echo $row->payment_id; ?></td>
                                <td><?php echo $user["email"]; ?></td>

				<td><?php echo DateFormat($row->created_date); ?></td>
                                <td><?php echo PriceFormat($row->grand_total); ?></td>

                                  <td><?php
                                  if($row->order_status=='success')
                                  {
                                      ?>
                                      <a href="#" data-href="?deactive=<?php echo $row->id; ?>" data-target="#confirm-delete" class="btn btn-success btn-xs"  data-toggle="modal">
                                      <?php echo $row->order_status; ?></a>
                            
                                  <?php   
                                  }
                                  else
                                  {
                                   ?>
                                       <a href="#" data-href="?active=<?php echo $row->id; ?>" data-target="#confirm-active" class="btn btn-danger btn-xs"  data-toggle="modal">
                                       <?php echo $row->order_status; ?></a>
                                    <?php   
                                  }?>
                                  </td>
                                  <td>
				<a href="order-detail.php?id=<?php echo $row->id; ?>" class="btn btn-primary btn-xs">View</a>
                                </td>
                              </tr>    
                              
                              
                        <?php
                        }
                        ?>
                       
                          </tbody>
                        </table>
                         </div>
                                
        <div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
        <div class="modal-dialog">
            <div class="modal-content">
            
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title" id="myModalLabel">Confirm Deactive</h4>
                </div>
            
                <div class="modal-body">
                    <p>Are You Sure You want to pending this order?</p>
                </div>
                
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <a class="btn btn-danger btn-ok">Pending</a>
                </div>
            </div>
        </div>
    </div>
                         
    <div class="modal fade" id="confirm-active" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
        <div class="modal-dialog">
            <div class="modal-content">
            
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title" id="myModalLabel">Confirm active</h4>
                </div>
            
                <div class="modal-body">
                    <p>Are You Sure You want to success this order?</p>
                </div>
                
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <a class="btn btn-success btn-ok">Success</a>
                </div>
            </div>
        </div>
    </div>
                     </div>
			
		  </div>
		</div>
	 </div>
   </div>

    <!-- footer content -->
    <footer>
      <div class="">
        <p class="pull-right">
           <span><?php echo $adminTitle["copyright"]; ?></span> 
        </p>
      </div>
      <div class="clearfix"></div>
    </footer>
    <!-- /footer content -->
  </div>
  <!-- /page content -->
 </div>
</div>

    <div id="custom_notifications" class="custom-notifications dsp_none">
        <ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group">
        </ul>
        <div class="clearfix"></div>
        <div id="notif-group" class="tabbed_notifications"></div>
    </div>

    <script src="js/bootstrap.min.js"></script>
    <!-- chart js -->
    <script src="js/chartjs/chart.min.js"></script>
    <!-- bootstrap progress js -->
    <script src="js/progressbar/bootstrap-progressbar.min.js"></script>
    <script src="js/nicescroll/jquery.nicescroll.min.js"></script>
    <!-- icheck -->
    <script src="js/icheck/icheck.min.js"></script>
    <script src="js/custom.js"></script>
    
     <!-- Datatables -->
  <script src="js/jquery.dataTables.min.js"></script>

	
<script>
    $(document).ready(function(){
    $('#datatable').DataTable();
});
	  
	</script>
<script>
        $('#confirm-delete').on('show.bs.modal', function(e) {
            $(this).find('.btn-ok').attr('href', $(e.relatedTarget).data('href'));
            
        });
         $('#confirm-active').on('show.bs.modal', function(e) {
            $(this).find('.btn-ok').attr('href', $(e.relatedTarget).data('href'));
            
        });
</script>


</body>

</html>s