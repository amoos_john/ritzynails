<?php
include('config/connection.php');
include('config/constants.php');
include('config/session.php');
include('config/model.php');
include('commons/head.php');
include('commons/header.php');
?>
<div class="row">
    <div class="col-lg-6 col-lg-offset-3 col-md-6 col-md-offset-3 fom-standalone">
        <div class="alert-placeholder"></div>
        <div class="panel panel-success">
            <div class="panel-body">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="text-center"><h2><b>Change Password</b></h2></div>
                        <form id="password-form" role="form" autocomplete="off">
                            <div id="error_password" style="display: none;" class="alert alert-danger alert-dismissible">
                                <ul id="error_password_message">

                                </ul>

                            </div>
                            <div id="success_password_message" style="display: none;" class="alert alert-success alert-dismissible">

                            </div>
                            <div class="form-group">
                                <label for="current_password">Current Password</label>
                                <input type="password" name="password" id="password" tabindex="1" class="form-control" placeholder="Current Password" autocomplete="off" required>
                            </div>
                            <div class="form-group">
                                <label for="current_password">New Password</label>
                                <input type="password" name="new_password" id="new_password" tabindex="1" class="form-control" placeholder="New Password" autocomplete="off" required>
                            </div>
                            <div class="form-group">
                                <label for="label_confirm_password">Confirm Password</label>
                                <input type="password" name="confirm_password" id="confirm_password" tabindex="1" class="form-control" placeholder="Confirm Password" value="" autocomplete="off" required>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="text-center col-lg-6 col-sm-offset-3">
                                        <input type="submit" name="password-submit" id="password-submit" tabindex="2" class="form-control btn btn-success" value="Change Password">
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php include('commons/venue.php'); ?>
<script>
    $('#password-form').submit(function (event) {
        $("#error_password").hide();
        $("#success_password_message").hide();
        event.preventDefault();
        var $form = $("#password-form");
        $("#error_password").hide();

        var $inputs = $form.find("input, select, button, textarea");

        var form = $("#password-form").serialize();
        $inputs.prop("disabled", true);
        request = $.ajax({
            dataType: "json",
            url: "actions/change_password.php",
            type: "post",
            data: form
        });

        request.done(function (response, textStatus, jqXHR) {
            
            if (response.error) {
                $("#error_password").show();
                $("#error_password_message").html(response.message);
            } else {
                $("#success_password_message").html(response.message).show();
                //window.top.location = "register.php";
            }
        });

        request.always(function () {
            $('#password-form').closest('form').find("input[type=text]").val("");
            $inputs.prop("disabled", false);
        });

        return false;
    });
</script>
<?php include('commons/footer.php'); ?>