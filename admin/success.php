<?php
include('config/connection.php');
include('config/constants.php');
include('config/model.php');
include('config/messages.php');
if(isset($_SESSION['user_id'])){
	header("location:index.php#register");
	die;
}
include('commons/head.php');
include('commons/header.php');

?>
           <div class="row">
		   <?php
		   if(isset($messages[$_GET['message']])){
		    ?>
                <div class="container">
					<div class="text-center"><h2><b><?php echo $messages[$_GET['message']];?></b></h2></div>
                </div>
			<?php
		   }
			?>
			<div class="col-lg-6 col-lg-offset-3 col-md-6 col-md-offset-3 fom-standalone">
			<div class="panel panel-success">
			<div class="panel-body">
			<div class="text-center"><h2><b>Login</b></h2></div>
			<?php 
			$display=1;
			include('commons/login.php'); 
			?>
			</div>
</div></div>
</div>
<br clear="all"/>
<?php include('commons/venue.php'); ?>
<?php include('commons/footer.php'); ?>	
