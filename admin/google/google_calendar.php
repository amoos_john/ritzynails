<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$calendarpath="https://www.google.com/calendar/embed?src=e6fcs3joc3vs7gldhahbogm7oo@group.calendar.google.com&ctz=Asia/Karachi";
$newcss="google_calendar.css";
$defaultview=($_GET["v"]) ? $_GET["v"] : "month";
// import the contents of the Google Calendar page into a string
$contents = file_get_contents($calendarpath);
// add secure Google address to root relative links
$contents = str_replace('<link type="text/css" rel="stylesheet" href="', '<link type="text/css" rel="stylesheet" href="https://www.google.com/calendar/', $contents );
$contents = str_replace('<script type="text/javascript" src="', '<script type="text/javascript" src="https://www.google.com/calendar/'  , $contents );
// inject css file reference
$contents = str_replace('<script>function _onload()', '<link rel="stylesheet" type="text/css" href="'.$newcss.'" /><script>function _onload()', $contents );
// update settings found in javascript _onload() function
$contents = str_replace('"view":"month"', '"view":"'.$defaultview.'"', $contents);
$contents = str_replace('"showCalendarMenu":true', '"showCalendarMenu":false', $contents);
if($defaultview == "month") $contents = str_replace('"showDateMarker":true', '"showDateMarker":false', $contents);
if($defaultview != "month") $contents = str_replace('"showTabs":true', '"showTabs":false', $contents);
echo $contents;


?>