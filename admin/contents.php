<?php
include_once('../config/connection.php');

if (!isset($_SESSION["admin"])) {

    echo "<script type='text/javascript'>window.location='login/';</script>";
    exit();
}
include_once('../config/constants.php');
include_once('../config/model.php');
include_once('../config/functions.php');

if (isset($_GET["delete"])) {
    $id = charEsc($_GET["delete"]);
    
    $mDate=date("Y-m-d H:i:s");
    $query=query("UPDATE `contents` SET `status`='0',`modified_date`='{$mDate}' WHERE `id`='{$id}'");
    
    $_SESSION["success"] = "Content Deleted successfully.";
    redirectUrl();
    
    /*$delrow = select("select image from contents where id = '$id'");

    if (count($delrow) > 0) {
        unlink("../" . $delrow[0]["image"]);
        query("delete from contents where id = '$id'");
        $_SESSION["success"] = "Content Deleted successfully.";
        echo '<script>window.location = "' . $_SERVER["PHP_SELF"] . '";</script>';
    } else {
        query("delete from contents where id = '$id'");
        $_SESSION["success"] = "Content Deleted successfully.";
        echo '<script>window.location = "' . $_SERVER["PHP_SELF"] . '";</script>';
    }*/
}
if (isset($_GET["add"])) {
   $id = charEsc($_GET["add"]);
    
   $result = query("SELECT * FROM `contents` where id='{$id}'");
   $content = fetch_array($result);

   $title = $content["title"];
   $code = $content["code"];
   
   $menu = query("SELECT sort_id FROM `menus` order by sort_id DESC limit 1");
   $row = fetch_array($menu);
   
   $sort_id = $row["sort_id"]+1;
    
   $ins=array("menu_name" => $title,"url" => $code,"content_id" =>$id,
        "sort_id" => $sort_id);
   $query = insert("menus",$ins);
    
   $_SESSION["success"] = "Menu Added successfully.";
   redirectUrl();
    
    
}

$pageTitle = "View Content";
include('header.php');
?>
<!-- page content -->
<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h3> <small> <?php echo $pageTitle; ?> </small> </h3>
            </div>

        </div>

        <div class="clearfix"></div>

        <div class="row">
            <div class="clearfix"></div>
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">

                    <div class="x_content">
<?php if (isset($_SESSION["error"])) { ?> 
                            <div class="x_content bs-example-popovers">
                                <div class="alert alert-danger alert-dismissible fade in" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
    <?php echo "<strong>Error ! </strong>" . " " . $_SESSION["error"];
    unset($_SESSION["error"]); ?>
                                </div>
                            </div>
<?php } ?>

                                <?php if (isset($_SESSION["success"])) { ?> 
                            <div class="x_content bs-example-popovers">
                                <div class="alert alert-success alert-dismissible fade in" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                            <?php echo "<strong>Congrats ! </strong>" . " " . $_SESSION["success"];
                            unset($_SESSION["success"]); ?>
                                </div>
                            </div>
                                <?php } ?>
                        <a href="addcontents.php"><button class="btn btn-primary waves-effect waves-button waves-float pull-right">Add Content</button></a>
                        <div class="table-responsive"> 
                            <table class="table table-striped responsive-utilities jambo_table bulk_action" id="datatable">
                                <thead>
                                    <tr>
                                        <th>Title </th>
                                        <th>Code </th>
                                        <th>Content Type </th>
                                        <th class="column-title no-link last"><span class="nobr">Action</span></th>
                                    </tr>
                                </thead>
                                <tbody>
    <?php
    //Selecting the data from table 
    $result = query("SELECT * FROM `contents` where status=1 order by id desc");
    while ($row = fetch_object($result)) 
    {
         $type_name="";
         if (array_key_exists($row->type, $content_types)) {
            $type_name = $content_types[$row->type];
         }
        ?> 
                <tr>
                    <td><?php echo $row->title; ?></td>
                    <td><?php echo $row->code; ?></td>
                    <td><?php echo $type_name; ?></td>
                    <td>
                        <?php 
                        if($row->type=='page')
                        {
                            $menu = query("SELECT * FROM `menus` where content_id='{$row->id}'"); 
                              
                            if(num_rows($menu)>0)
                            {
                                ?>
                                <a  class="btn btn-success btn-sm" >
                                <i class="fa fa-check"></i> Menu Exist</a>
                                <?php
                            }
                            else
                            {
                         ?>
                            <a href="?add=<?php echo $row->id; ?>"  class="btn btn-success btn-sm" title="Add in Menu">
                             <i class="fa fa-plus"></i> Add in Menu</a>
                        <?php
                            }
                        }
                        ?>
                        <a href="addcontents.php?editId=<?php echo $row->id; ?>" class="btn btn-info btn-sm"><i class="fa fa-pencil"></i> Edit</a>
                        <?php 
                        if($row->id!=1 && $row->code!='block1')
                        {?>
                        <a href="#" data-href="?delete=<?php echo $row->id; ?>" data-target="#confirm-delete" class="btn btn-danger btn-sm " title="Remove" data-toggle="modal">
                            <i class="fa fa-trash"></i> Delete</a>
                        <?php
                        }
                       
                        ?>
                    </td>
                </tr>    

   <?php
    }
    ?>

                                </tbody>
                            </table>
                        </div>

                        <div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                            <div class="modal-dialog">
                                <div class="modal-content">

                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                        <h4 class="modal-title" id="myModalLabel">Confirm Delete</h4>
                                    </div>

                                    <div class="modal-body">
                                        <p>Are You Sure You want to Delete?</p>
                                    </div>

                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                        <a class="btn btn-danger btn-ok">Delete</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>

    <!-- footer content -->
    <footer>
        <div class="">
            <p class="pull-right">
                <span><?php echo $adminTitle["copyright"]; ?></span> 
            </p>
        </div>
        <div class="clearfix"></div>
    </footer>
    <!-- /footer content -->
</div>
<!-- /page content -->
</div>
</div>

<div id="custom_notifications" class="custom-notifications dsp_none">
    <ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group">
    </ul>
    <div class="clearfix"></div>
    <div id="notif-group" class="tabbed_notifications"></div>
</div>

<script src="js/bootstrap.min.js"></script>
<!-- chart js -->
<script src="js/chartjs/chart.min.js"></script>
<!-- bootstrap progress js -->
<script src="js/progressbar/bootstrap-progressbar.min.js"></script>
<script src="js/nicescroll/jquery.nicescroll.min.js"></script>
<!-- icheck -->
<script src="js/icheck/icheck.min.js"></script>
<script src="js/custom.js"></script>

<!-- Datatables -->
<script src="js/jquery.dataTables.min.js"></script>


<script>
    $(document).ready(function () {
        $('#datatable').DataTable();
    });


</script>
<script>
    $('#confirm-delete').on('show.bs.modal', function (e) {
        $(this).find('.btn-ok').attr('href', $(e.relatedTarget).data('href'));

    });
</script>


</body>

</html>