<?php
include_once('../config/connection.php');
if(!isset($_SESSION["admin"])){
        
	 echo "<script type='text/javascript'>window.location='login/';</script>";
	exit();
}

include_once('../config/model.php');
  
 if(isset($_GET["editId"]))
  {
	$id = charEsc($_GET["editId"]);
        if($id!=null)
        {
            $result = query("select * from services where id={$id}");
            if(num_rows($result) > 0)
            {
                    $row = fetch_array($result);
                    $get = "editId";
                    $ser_name = $row['service_name'];
                    $price = $row['price'];
                    $old_price = $row['old_price'];
                    $description = $row['description'];
                    $short_desc = $row['short_desc'];
                    $image = $row['image'];
                    $status=($row['status']==1)?'selected':'';
                    $status2=($row['status']==0)?'selected':'';
                    

            }
        }
        else {
            
            header("Location: products.php");

        }
  }
  if(isset($_POST["submit"])) 
  {
        $service_name = charEsc($_POST["service_name"]);
        $price = charEsc($_POST["price"]);
        $old_price = charEsc($_POST["old_price"]);
	$description = $_POST["description"];
        $short_desc = charEsc($_POST["short_desc"]);
	$image = $_FILES["image"]["name"];
        $status=charEsc($_POST["status"]);
      
	if($service_name == "")
	{
            $_SESSION["error"] = "Product Name is  required.";	
	}
        else if($price == "")
	{
            $_SESSION["error"] = "Price is  required.";	
	}
	else if($image == "")
	{
            $_SESSION["error"] = "Image is  required";	
	}
        else
        {
            $imageFileType = pathinfo($image,PATHINFO_EXTENSION);
            
            if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg" && $imageFileType != "gif" ) 
            {
		$_SESSION["error"] = "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";	
            } 
            else
            {
                $cDate = date('Y-m-d H:i:s');
                
                $newimage = uploadImage($_FILES["image"],"../images/products/",'product');
               
                $ins=array("service_name" => $service_name,
               "type" => "products","price" => $price,"old_price" => $old_price,  
               "image" => 'images/products/'.$newimage,"description" => $description,
               "short_desc" => $short_desc,"status" => $status,"created_date" => $cDate);
                $query = insert("services",$ins);
                if(!$query)
		{
                    $_SESSION["error"] = "Product Failed to Added.";	
		}
		else
		{
                   $_SESSION["success"] = "Product Added Successfully.";
                   echo "<script type='text/javascript'>window.location ='products.php' </script>";

		}
                
                
            }
        }
}

  if(isset($_POST["update"])) 
  {
        $service_name = charEsc($_POST["service_name"]);
        $price = charEsc($_POST["price"]);
        $old_price = charEsc($_POST["old_price"]);
	$description = $_POST["description"];
        $short_desc = charEsc($_POST["short_desc"]);
	$image = $_FILES["image"]["name"];
        $id =  charEsc($_POST["editId"]);
        $status=charEsc($_POST["status"]);
        
        if($service_name == "")
	{
            $_SESSION["error"] = "Product Name is  required.";	
	}
        else if($price == "")
	{
            $_SESSION["error"] = "Price is required.";	
	}
	
	else
        {
           $mDate = date('Y-m-d H:i:s');
           if($image != "")
            {
                $imageFileType = pathinfo($image,PATHINFO_EXTENSION);
            
                if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg" && $imageFileType != "gif" ) 
                {
                    $_SESSION["error"] = "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";	
                } 
                else
                {
                    $row = select("select image from `services` where id={$id}"); 
                    $dbImage = $row[0]["image"];
                    
                    $deleteFile1 = "../".$dbImage;
                    
                    if (file_exists($deleteFile1)) { unlink($deleteFile1); }
                    
                    $newimage = uploadImage($_FILES["image"],"../images/products/",'product');
                    
                    $imagename='images/products/'.$newimage;
                }
            }
            else
            {
                $imagename=$_POST['getImage'];
            }
             
                
            if(!isset($_SESSION["error"]))
            {  
                $sql="UPDATE `services` SET `service_name`='{$service_name}',`status`='{$status}',`old_price`='{$old_price}',`price`='{$price}',`image`='{$imagename}',`description`='{$description}',`short_desc`='{$short_desc}',`modified_date`='{$mDate}' WHERE `id`={$id}";
               
                $query=query($sql);        
                         
                if(!$query)
		{
                   //echo "<script>window.location ='addproducts.php?editId='+'$id' </script>";
                    $_SESSION["error"] = "Product Failed to Update.";	
		}
		else
		{
                   // echo "<script>window.location ='addproducts.php?editId='+'$id' </script>";
                    $_SESSION["success"] = "Product Updated Successfully.";
                    echo "<script type='text/javascript'>window.location ='products.php' </script>";
		}
            }
            
           
        }
  }
   
  $pageTitle = "Add Products"; 
  include('header.php'); 
 
?>
<style type="text/css">

#imagePreview {
    background-image:url('<?php echo ($image)?'../'.$image:''; ?>');
    width: 50%;
    height: 250px;
    background-size: 300px 250px;
    background-repeat: no-repeat;
    margin: 0 auto;
    border: 1px solid #DDE2E8;
     display: table;
}


</style>
<script>

var loadImageFile = (function () {
    if (window.FileReader) {
        var    oPreviewImg = null, oFReader = new window.FileReader(),
            rFilter = /^(?:image\/bmp|image\/cis\-cod|image\/gif|image\/ief|image\/jpeg|image\/jpeg|image\/jpeg|image\/pipeg|image\/png|image\/svg\+xml|image\/tiff|image\/x\-cmu\-raster|image\/x\-cmx|image\/x\-icon|image\/x\-portable\-anymap|image\/x\-portable\-bitmap|image\/x\-portable\-graymap|image\/x\-portable\-pixmap|image\/x\-rgb|image\/x\-xbitmap|image\/x\-xpixmap|image\/x\-xwindowdump)$/i;

        oFReader.onload = function (oFREvent) {
            if (!oPreviewImg) {
                var newPreview = document.getElementById("imagePreview");
                oPreviewImg = new Image();
                oPreviewImg.style.width = (newPreview.offsetWidth).toString() + "px";
                oPreviewImg.style.height = (newPreview.offsetHeight).toString() + "px";
                newPreview.appendChild(oPreviewImg);
            }
            oPreviewImg.src = oFREvent.target.result;
        };

        return function () {
            var aFiles = document.getElementById("image").files;
            if (aFiles.length === 0) { return; }
            if (!rFilter.test(aFiles[0].type)) { alert("You must select a valid image file !"); return; }
            oFReader.readAsDataURL(aFiles[0]);
        }

    }

})();
</script>
<script src="ckeditor.js"></script>
<script src="js/sample.js"></script>
<link rel="stylesheet" href="toolbarconfigurator/lib/codemirror/neo.css">
<style>
textarea {
	width: 100% !important;
}
</style>
	<!-- page content -->
		<div class="right_col" role="main">
			<div class="">
 				<div class="page-title">
					<div class="title_left">
						<h3> Products </h3>
					</div>
  
      
				</div> <!--Page Title Div end here -->
			</div> 
			<div class="clearfix"></div>

			<div class="row">
				<div class="col-md-12 col-sm-12 col-xs-12">
					<div class="x_panel">
						
				<div class="x_content">
				<br />
                                <form method="post" action="addproducts.php" id="add-services" data-parsley-validate class="form-horizontal form-label-left" enctype="multipart/form-data" >
				
				<?php if(isset($_SESSION["error"])){ ?> 
				<div class="x_content bs-example-popovers">
					<div class="alert alert-danger alert-dismissible fade in" role="alert">
						<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
							<?php echo "<strong>Error ! </strong>"." ".$_SESSION["error"]; unset($_SESSION["error"]); ?>
					</div>
 				</div>
							<?php } ?>
									
				<?php if(isset($_SESSION["success"])){ ?> 
					<div class="x_content bs-example-popovers">
						<div class="alert alert-success alert-dismissible fade in" role="alert">
							<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
							<?php echo "<strong>Congrats ! </strong>"." ".$_SESSION["success"]; unset($_SESSION["success"]); ?>
						</div>
					</div>
				<?php } ?>
								
		<div class="form-group">
		  <label class="control-label col-md-2 col-sm-2 col-xs-12" for="service_name">Name <span class="required">*</span></label>
			<div class="col-md-6 col-sm-6 col-xs-12">
		   	 <input type="text" id="service_name" value="<?php echo $ser_name; ?>" required name="service_name" class="form-control col-md-7 col-xs-12">
		 	 <input type="hidden" name="<?php echo $get; ?>" value="<?php echo $id; ?>" class="form-control col-md-7 col-xs-12">
			<input type="hidden" name="getImage" value="<?php echo $image?>" class="form-control col-md-7 col-xs-12">   
                        </div>
		</div>
                 <div class="form-group">
                    <label class="control-label col-md-2 col-sm-2 col-xs-12" for="price">Price <span class="required">*</span></label>
                                  <div class="col-md-6 col-sm-6 col-xs-12">
                                      <input type="text" id="price" value="<?php echo $price; ?>"  name="price" class="form-control col-md-7 col-xs-12" required>
                                </div>
                </div>
                 
               <div class="form-group">
                              <label class="control-label col-md-2 col-sm-2 col-xs-12" for="price">Old Price </label>
                                  <div class="col-md-6 col-sm-6 col-xs-12">
                                      <input type="text" id="price" value="<?php echo $old_price; ?>"  name="old_price" class="form-control col-md-7 col-xs-12" >
                                </div>
                </div>
                <div class="form-group">
                                <label class="control-label col-md-2 col-sm-2 col-xs-12" for="price">Status <span class="required">*</span></label>
                                  <div class="col-md-6 col-sm-6 col-xs-12">
                                      <select class="form-control" name="status">
                                      <option value="1" <?php echo $status; ?>>Active</option>
                                      <option value="0" <?php echo $status2; ?>>Deactive</option>
                                     </select>
                                </div>
                </div>
                 <div class="form-group">
                              <label class="control-label col-md-2 col-sm-2 col-xs-12" for="price">Short Description</label>
                                  <div class="col-md-6 col-sm-6 col-xs-12">
                                      <textarea id="short_desc"  name="short_desc" class="form-control" ><?php echo $short_desc; ?></textarea>
                                </div>
                </div>                       
                		   
		<div class="form-group">
			<label for="middle-name" class="control-label col-md-2 col-sm-2 col-xs-12">Image<span class="required">*</span></label>
				<div class="col-md-6 col-sm-6 col-xs-12">
					<input type="file" onchange="loadImageFile();" name="image" id="image" class="form-control col-md-7 col-xs-12" >
				</div>
		</div>
                                
               <div class="form-group">
                   <label for="middle-name" class="control-label col-md-2 col-sm-2 col-xs-12">&nbsp;</label>

                        <div id="imagePreview" class="form-control col-md-7 col-xs-12" ></div>
                       
                </div>   
                <div class="form-group">
			<label class="control-label col-md-2 col-sm-2 col-xs-12" for="description">Description</label>
                   <div class="col-md-8 col-sm-8 col-xs-12"> 
                  <!-- ck Editor Start-->
                  <div class="adjoined-bottom">
                    <div class="grid-container">
                      <div class="grid-width-100">
                              <textarea id="editor" name="description" ><?php echo $description; ?></textarea>
			</div>
                    </div>
                  </div>
                  <!-- ck Editor End--> 
                  
                </div>
              </div>
		<div class="ln_solid"></div>
			<div class="form-group">
				<div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                    <?php if(isset($_GET['editId'])) { ?> 
                                    	  <button type="submit" name="update" class="btn btn-success">Update</button>
			
                                    <?php
                                    }  
                                    else
                                    {  ?>
                                    	<button type="submit" name="submit" class="btn btn-success">Save</button>

                                  <?php 
                                    }
                                    ?>
                                        <a href="products.php" class="btn btn-danger">Cancel</a>
				</div>
			</div>
  		</form>
		</div>
	   </div>
      </div><!-- class x_panel end here-->
	 </div>
    </div><!--Row end Here-->
	
    <!-- footer content -->
	<footer>
		<div class="">
			<p class="pull-right">
				<span><?php echo $adminTitle["copyright"]; ?></span> 
			</p>
		</div>
		<div class="clearfix"></div>
	</footer>
	<!-- /footer content -->
  
	</div>
	<!-- /page content -->
	<!-- </div> </div> extra divs--> 
	<div id="custom_notifications" class="custom-notifications dsp_none">
		<ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group"></ul>
		<div class="clearfix"></div>
		<div id="notif-group" class="tabbed_notifications"></div>
	</div>
  
	<script src="js/bootstrap.min.js"></script> 

        <!-- chart js --> 
        <script src="js/chartjs/chart.min.js"></script> 
        <!-- bootstrap progress js --> 
        <script src="js/progressbar/bootstrap-progressbar.min.js"></script> 
        <script src="js/nicescroll/jquery.nicescroll.min.js"></script> 
        <!-- icheck --> 
        <script src="js/icheck/icheck.min.js"></script> 
        <script src="js/custom.js"></script> 
       
        <script>
            initSample();
        </script>
        <script>
          $(document).ready(function() {
    $("#price").keydown(function (e) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
             // Allow: Ctrl+A, Command+A
            (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) || 
             // Allow: home, end, left, right, down, up
            (e.keyCode >= 35 && e.keyCode <= 40)) {
                 // let it happen, don't do anything
                 return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });
});

        </script>
        
     
 </body>
</html>