<?php 
include_once('../../config/connection.php');
if(isset($_SESSION["admin"]))
{
        
	 echo "<script type='text/javascript'>window.location='../dashboard.php';</script>";
	exit();
}

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Admin Login</title>

    <!-- Bootstrap core CSS -->
    <link href="../css/bootstrap.min.css" rel="stylesheet">
    <link href="../fonts/css/font-awesome.min.css" rel="stylesheet">
    <link href="../css/animate.min.css" rel="stylesheet">

    <!-- Custom styling plus plugins -->
    <link href="../css/custom.css" rel="stylesheet">
    <link href="../css/icheck/flat/green.css" rel="stylesheet">

    <script src="../js/jquery.min.js"></script>

    <!--[if lt IE 9]>
        <script src="../assets/js/ie8-responsive-file-warning.js"></script>
        <![endif]-->

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

</head>

<body style="background:#F7F7F7;">
  <div class="">
  	<a class="hiddenanchor" id="toregister"></a>
    <a class="hiddenanchor" id="tologin"></a>
    	<div id="wrapper">
        	<div id="login" class="animate form">
            	<section class="login_content">
                	<form method="post" action="logincheck.php">
                    	<h1>Admin Ritzy Nails</h1>
                        <p>Login to start your session</p>
                        <div class="x_content bs-example-popovers">
			<?php if(isset($_SESSION["error"])){ 
			echo '<div  class="alert alert-danger alert-dismissible fade in" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                        '.$_SESSION["error"].'<br></div>'; 
			unset($_SESSION["error"]);} 
			?>
			</div>
			<div class="item form-group bad">
                            <input type="email" class="form-control" placeholder="Email" name="email" required />
			</div>
                        <div class="item form-group bad">
                            <input type="password" class="form-control" placeholder="Password" name="pass" required />
                        </div>
                        <div>
                            <button type="submit" name="login" class="btn btn-primary btn-block btn-lg submit">Login</button>
                        </div>
                         <div class="">
                            <a href="<?php echo $siteUrl; ?>" >Back to main website</a>
                        </div>
                        
                        <div class="clearfix"></div>
                        <div class="separator"></div>
                        <div class="clearfix"></div>
                            <br />
                        <div>
                    </form>
                    <!-- form -->
                </section>
                <!-- content -->
            </div>
        </div>
    </div>

	
<script src="http://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script>
    $(document).ready(function(){   
        window.setTimeout('fadeout();', 2000);
    });

    function fadeout(){
        $('#fade').fadeOut('slow', function() {
           // Animation complete.
        });
    }

</script>
</body>

</html>