<?php
$pageTitle = "Orders Detail";
include_once('../config/connection.php');
include_once('../config/model.php');
include_once('../config/constants.php');
include_once('../config/functions.php');
include('header.php');
$order_id = $_GET['order_id'];
$sqlOrder="select id as order_id,total,type from transactions where id=$order_id;";
$resultOrder = query($sqlOrder);
$rowOrder = fetch_object($resultOrder);
$orderTotal=$rowOrder->total;
$type=$rowOrder->type;
?>
<div class="right_col" role="main">
    <?php
    include("orders/".$type.".php");
    ?>
    <footer>
        <div class="">
            <p class="pull-right">
                <span><?php echo $adminTitle["copyright"]; ?></span> 
            </p>
        </div>
        <div class="clearfix"></div>
    </footer>
</div>
<div id="custom_notifications" class="custom-notifications dsp_none">
    <ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group">
    </ul>
    <div class="clearfix"></div>
    <div id="notif-group" class="tabbed_notifications"></div>
</div>
<script src="js/bootstrap.min.js"></script>
<!-- chart js -->
<script src="js/chartjs/chart.min.js"></script>
<!-- bootstrap progress js -->
<script src="js/progressbar/bootstrap-progressbar.min.js"></script>
<script src="js/nicescroll/jquery.nicescroll.min.js"></script>
<!-- icheck -->
<script src="js/icheck/icheck.min.js"></script>
<script src="js/custom.js"></script>
</body>
</html>