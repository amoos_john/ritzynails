<?php $pageTitle = "Inbox"; include('header.php'); include_once('../config/connection.php');
include_once('../config/model.php');
include_once('../config/constants.php');


if(isset($_GET['id']))
{
	$id = $_GET['id'];
	$result = query("select * from contact where deleted=0");
	$row = fetch_array($result);
	$name = $row["fullName"];
	$email = $row["email"];
	$message = $row["message"];
	$mdate = $row["emailDate"];
}

?>


<!-- page content -->
<div class="right_col" role="main">
	<div class="">
		<div class="page-title">
        	<div class="title_left">
            	<h3>View</h3>
            </div>
        </div>
        <div class="clearfix"></div>

        <div class="row">
        	<div class="col-md-12">
            	<div class="x_panel">
                	<div class="x_content">
 	                	<div class="row">
							<table class="table table-striped table-bordered table-hover" id="dataTables-example">
								<?php
									$id= $_GET['id'];
									$sql = "SELECT * FROM contact where id=$id";
									$result = $conn->query($sql);
                    
									if ($result->num_rows > 0) {
									// output data of each row
										while($row = $result->fetch_assoc()) {
										echo '<tr class="odd gradeX">
												<th>ID</th><td>'. $row["id"].'</td>
												</tr>
												
											  <tr class="odd gradeX">
												<th>Name</th><td>'. $row["fullName"].'</td>
											  </tr>
											
											  <tr class="odd gradeX">
												<th>Contact Number</th><td>'. $row["contactNumber"].'</td>
											  </tr>
											
											  <tr class="odd gradeX">
												 <th>Email</th><td>'. $row["email"].'</td>
											 </tr>
											 
											 <tr class="odd gradeX">								
												<th>Message</th><td>'. $row["message"].'</td>
											</tr>
											
											<tr class="odd gradeX">
												<th>Date</th><td>'. $row["emailDate"].'</td>
											</tr>';
										}
									}
									else {
										echo "0 results";
									}
								?>
								</tbody>
							</table>
							<a href="contact.php"><button class="btn btn-primary waves-effect waves-button waves-float pull-right">Go Back</button></a>
						</div>
					</div>
       <!-- /CONTENT MAIL -->
				</div>
			</div>
		</div>
  </div>
<!-- footer content -->
<footer>
	<div class=""></div>
	<div class="clearfix"></div>
</footer>
<!-- /footer content -->

    <div id="custom_notifications" class="custom-notifications dsp_none">
        <ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group">
        </ul>
        <div class="clearfix"></div>
        <div id="notif-group" class="tabbed_notifications"></div>
    </div>

    <script src="js/bootstrap.min.js"></script>
    <!-- chart js -->
    <script src="js/chartjs/chart.min.js"></script>
    <!-- bootstrap progress js -->
    <script src="js/progressbar/bootstrap-progressbar.min.js"></script>
    <script src="js/nicescroll/jquery.nicescroll.min.js"></script>
    <!-- icheck -->
    <script src="js/icheck/icheck.min.js"></script>

    <script src="js/custom.js"></script>
	
	<script>
		function idelete(value){
			var r = confirm("Sure to delete this message?");
			if (r == true) {
				window.location ='contact.php?delete='+value;
			}
			else {}
		}
	</script>

</body>

</html>