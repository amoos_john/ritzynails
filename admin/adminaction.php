<?php session_start();

include_once('../config/connection.php');
include_once('../config/model.php');


if(isset($_GET["editId"]))
{
	$id = $_GET["editId"];
	$sql = query("select * from admin where id={$id}");
	$row = fetch_array($sql);
	$_SESSION["editId"] = $row['id'];
	$_SESSION["editfName"] = $row['fName'];
	$_SESSION["editlName"] = $row['lName'];
	$_SESSION["editEmail"] = $row['email'];
	$_SESSION["editUserName"] = $row['userName'];
	$_SESSION["editPassword"] = $row['password'];
	$_SESSION["editRole"] = $row['roleId'];
	 header("Location: addadmin.php");
	
	
	
}



if(isset($_GET["delete"]))
{
	$id = $_GET["delete"];
	$query = query("update admin set deleted=1 where id={$id}");
	 header("Location: admins.php");
	
}


if(isset($_GET["deactivateId"]))
{
	$id = $_GET["deactivateId"];
	$query = query("update admin set status=0 where id={$id}");
	 header("Location: admins.php");
	
}

if(isset($_GET["activateId"]))
{
	$id = $_GET["activateId"];
	$query = query("update admin set status=1 where id={$id}");
	 header("Location: admins.php");
	
}

else
{
	
	echo "No direct access allowed.";
}



?>