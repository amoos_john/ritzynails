<?php
// Connection 
include_once('../config/connection.php');
include_once('../config/model.php');
include_once('../config/constants.php');

	
$filename = "Orders.xls"; // File Name
$type = $_GET['report'];
$title='a';


if($type == 'belts'){
	
$filename = "Belt Orders.xls"; // File Name	
$title = 'Belt Size';

$user_query = query("SELECT c.firstName, c.lastName, b.title,
UPPER(REPLACE(b.code, '_', ' ')) as Type,b.price,  d.name,(select size from award_sizes where id=e.size_id) as size,f.our_staff
FROM `competitor_receiving_awards` AS a
INNER JOIN competitor_awards AS b ON a.award_id = b.id
INNER JOIN competitors AS c ON a.competitor_id = c.id
Left JOIN embroidery_names AS d ON a.id = d.receiving_id
Left JOIN selected_sizes AS e ON a.id = e.receiving_id
Left JOIN awards_transactions AS f ON a.competitor_id = f.competitor_id
WHERE a.deleted =0 and b.type in ('belt','belt_emb') order by c.firstName");	
}

if($type == 'jackets'){
	
$filename = "Jacket Orders.xls"; // File Name	
$title = 'Jacket Size';

$user_query = query("SELECT c.firstName, c.lastName, b.title,
UPPER(REPLACE(b.code, '_', ' ')) as Type,b.price,  d.name,(select size from award_sizes where id=e.size_id) as size,f.our_staff
FROM `competitor_receiving_awards` AS a
INNER JOIN competitor_awards AS b ON a.award_id = b.id
INNER JOIN competitors AS c ON a.competitor_id = c.id
Left JOIN embroidery_names AS d ON a.id = d.receiving_id
Left JOIN selected_sizes AS e ON a.id = e.receiving_id
Left JOIN awards_transactions AS f ON a.competitor_id = f.competitor_id
WHERE a.deleted =0 and b.title in ('Jacket','Jacket Embroidery') order by c.firstName");	
}

$th = array('First Name'=>'1','Last Name'=>'2','Award Title'=>'3','Award Type'=>'4','Price'=>'5','Embroidery Name'=>'6',$title=>'7','Notes for our staff'=>'8', 'Status'=>'9');


if($type == 'all'){
    
$filename = "All Orders.xls"; // File Name  
$title= 'Jacket / Belt Size';

/*
$user_query = query("SELECT c.firstName, c.lastName, b.title,
UPPER(REPLACE(b.code, '_', ' ')) as Type,b.price,  d.name,(select size from award_sizes where id=e.size_id) as size,f.our_staff
FROM `competitor_receiving_awards` AS a
INNER JOIN competitor_awards AS b ON a.award_id = b.id
INNER JOIN competitors AS c ON a.competitor_id = c.id
Left JOIN embroidery_names AS d ON a.id = d.receiving_id
Left JOIN selected_sizes AS e ON a.id = e.receiving_id
Left JOIN awards_transactions AS f ON a.competitor_id = f.competitor_id
WHERE a.deleted =0 order by c.firstName");  
}
*/

/*
$user_query = query("SELECT c.firstName, c.lastName, 
GROUP_CONCAT(b.title, '(', UPPER(REPLACE(b.code, '_', ' ')),')') AS 'Type',
 GROUP_CONCAT(b.price), GROUP_CONCAT(d.name),GROUP_CONCAT((SELECT size FROM award_sizes WHERE id=e.size_id)) AS Size, f.our_staff , f.unable_to_attend, f.unable_attend_name 
 FROM `competitor_receiving_awards` AS a 
 LEFT JOIN competitor_awards AS b ON a.award_id = b.id 
 LEFT JOIN competitors AS c ON a.competitor_id = c.id 
 LEFT JOIN embroidery_names AS d ON a.id = d.receiving_id 
 LEFT JOIN selected_sizes AS e ON a.id = e.receiving_id 
 LEFT JOIN awards_transactions AS f ON a.competitor_id = f.competitor_id 
 WHERE a.deleted =0 GROUP BY c.firstName ORDER BY c.firstName
 ");
$th = array('First Name'=>'1','Last Name'=>'2',' Award ( Type ) '=>'3','Price'=>'4','Embroidery Name'=>'5','Size'=>'6', 'Note for Staff'=>'7', 'Status'=>'8', 'Who Will pick awards'=>'9');
}
*/


$user_query = query("SELECT c.firstName, c.lastName, 
GROUP_CONCAT(b.label) AS 'Title',
GROUP_CONCAT((SELECT size_label FROM award_sizes WHERE id=e.size_id)) AS Size,
GROUP_CONCAT(b.price), GROUP_CONCAT(d.name), f.our_staff , f.unable_to_attend, f.unable_attend_name , f.food_allergies 
 FROM `competitor_receiving_awards` AS a 
 LEFT JOIN competitor_awards AS b ON a.award_id = b.id 
 LEFT JOIN competitors AS c ON a.competitor_id = c.id 
 LEFT JOIN embroidery_names AS d ON a.id = d.receiving_id 
 LEFT JOIN selected_sizes AS e ON a.id = e.receiving_id 
 LEFT JOIN awards_transactions AS f ON a.competitor_id = f.competitor_id 
 WHERE a.deleted =0 GROUP BY c.firstName ORDER BY c.firstName
 ");
$th = array('First Name'=>'1','Last Name'=>'2',' Award ( Type ) '=>'3','Size'=>'4','Price'=>'5','Embroidery Name'=>'6', 'Note for Staff'=>'7', 'Status'=>'8', 'Who Will pick awards'=>'9', 'Food Allergies'=>'10');
}






if($type == 'rings'){
	
$filename = "Ring Orders.xls"; // File Name	
$title = 'Rings Size';

$user_query = query("SELECT c.firstName, c.lastName, b.title,
UPPER(REPLACE(b.code, '_', ' ')) as Type,b.price
FROM `competitor_receiving_awards` AS a
INNER JOIN competitor_awards AS b ON a.award_id = b.id
INNER JOIN competitors AS c ON a.competitor_id = c.id
Left JOIN embroidery_names AS d ON a.id = d.receiving_id
Left JOIN selected_sizes AS e ON a.id = e.receiving_id
WHERE a.deleted =0 and b.type='ring' order by c.firstName");	

$th = array('First Name'=>'1','Last Name'=>'2','Award Title'=>'3','Award Type'=>'4','Price'=>'5');

}


// Download file
header("Content-Disposition: attachment; filename=\"$filename\"");
header("Content-Type: application/vnd.ms-excel");

// Write data to file
$flag = false;
while ($row = fetch_assoc($user_query)) {
    if (!$flag) {
        // display field/column names as first row
   
        echo implode("\t", array_keys($th)) . "\r\n";
        $flag = true;
    }
    echo implode("\t", array_values($row)) . "\r\n";
}

?>