<?php 
        include_once('../config/connection.php');
        include_once('sessions.php');
        include_once('../config/model.php');
	include_once('../config/constants.php');
	
        
	$mDate=date("Y-m-d H:i:s");
        if(isset($_GET["active"]))
	{
	    $id =  charEsc($_GET["active"]);
            $query=query("UPDATE `users` SET `status`='active',`modified_date`='{$mDate}' WHERE `id`='{$id}'");
            if($query)
            {
                $_SESSION['success']="Profile updated successfully!";
            }
            else
            {
                $_SESSION['error']="Profile cannot update!";
            }
            header('location: users.php');
	}   
        if(isset($_GET["deactive"]))
	{
	    $id =  charEsc($_GET["deactive"]);
            $query=query("UPDATE `users` SET `status`='deactive',`modified_date`='{$mDate}' WHERE `id`='{$id}'");
            if($query)
            {
                $_SESSION['success']="Profile updated successfully!";
            }
            else
            {
                $_SESSION['error']="Profile cannot update!";
            }
             header('location: users.php');       
	}  
$pageTitle = "View Displacement Time"; 
include('header.php');         
?>
	<link rel="stylesheet" href="../css/fullcalendar.css">

   <!-- page content -->
   <div class="right_col" role="main">
     <div class="">
        <div class="page-title">
            <div class="title_left">
               <h3><?php echo $pageTitle ?> </h3>
            </div>

           </div>
					
           <div class="clearfix"></div>
 
           <div class="row">
           <div class="clearfix"></div>
		<div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                
                     <div class="x_content">
                        
                        <!--<iframe src="https://calendar.google.com/calendar/embed?src=7m27buurv0bph3bs6pt491640c%40group.calendar.google.com&ctz=Asia/Karachi" style="border: 0" width="800" height="600" frameborder="0" scrollinga="no"></iframe>        
               -->
                <div id='calendar'></div>
                     </div>
			
		  </div>
		</div>
	 </div>
   </div>

<div id="fullCalModal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span> <span class="sr-only">close</span></button>
                <h4 id="modalTitle" class="modal-title"></h4>
            </div>
            <div id="modalBody" class="modal-body">
                 Start: <span id="startTime"></span><br/>
                 End: <span id="endTime"></span><br/>
                 Description: <span id="summary"></span><br/>
                 Address: <span id="address"></span><br/>
                 
            </div>
            <div class="modal-footer">
               <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
               <a id="eventUrl"  class="btn btn-primary" >Order detail</a>
            </div>
        </div>
    </div>
</div>
    <!-- footer content -->
    <footer>
      <div class="">
        <p class="pull-right">
           <span><?php echo $adminTitle["copyright"]; ?></span> 
        </p>
      </div>
      <div class="clearfix"></div>
    </footer>
    <!-- /footer content -->
  </div>
  <!-- /page content -->
 </div>
</div>

    <div id="custom_notifications" class="custom-notifications dsp_none">
        <ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group">
        </ul>
        <div class="clearfix"></div>
        <div id="notif-group" class="tabbed_notifications"></div>
    </div>

    <script src="js/bootstrap.min.js"></script>
    <!-- chart js -->
    <script src="js/chartjs/chart.min.js"></script>
    <!-- bootstrap progress js -->
    <script src="js/progressbar/bootstrap-progressbar.min.js"></script>
    <script src="js/nicescroll/jquery.nicescroll.min.js"></script>
    <!-- icheck -->
    <script src="js/icheck/icheck.min.js"></script>
    <script src="js/custom.js"></script>
    
     <!-- Datatables -->
  <script src="js/jquery.dataTables.min.js"></script>

	

<script src='../js/moment.min.js'></script>
<script src='../js/fullcalendar.min.js'></script>
<script src='../js/gcal.js'></script>
<script src='../js/gcal.min.js'></script>
<script type="text/javascript">
    $(document).ready(function() {
    var date = new Date();
    var d = date.getDate();
    var m = date.getMonth();
    var y = date.getFullYear();

$('#calendar').fullCalendar({
    
    
  header: {
      left: 'prev,next today',
      center: 'title',
      right: 'month,agendaWeek,agendaDay'
    },
    editable: false,
    selectable: true,
    selectHelper: false,
googleCalendarApiKey: 'AIzaSyAlVvOfCGHMPfd6alfeimkF3WqR49T6P_Y',
events: 
{
googleCalendarId: '7m27buurv0bph3bs6pt491640c@group.calendar.google.com',
className: 'gcal-event',
allDay: true,

}
,
    eventClick: function(calEvent, jsEvent, view) {
            var id=calEvent.title.split(" ");
           
            $('#modalTitle').html(calEvent.title);
            $('#summary').html(calEvent.description);
            $('#address').html(calEvent.location);
            $("#startTime").html(moment(calEvent.start).format('MMM Do h:mm A'));
            $("#endTime").html(moment(calEvent.end).format('MMM Do h:mm A'));
            $('#eventUrl').attr('href','order-detail.php?id='+id[0]);
            $('#fullCalModal').modal();
        
        //alert('Event: ' + calEvent.description);
       // alert('Location: ' + calEvent.location);
       
        // change the border color just for fun
        //$(this).css('border-color', 'red');
         if (calEvent.url) {
            //window.open(calEvent.url);
            return false;
        }

    }
  
});




});





</script>

</body>

</html>