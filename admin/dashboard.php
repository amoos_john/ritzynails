<?php
include_once('../config/connection.php');
include_once('sessions.php');
include_once('../config/model.php');
include_once('../config/functions.php');
include_once('../config/constants.php');

if(isset($_GET['code']))
{    
    require_once '../googlecalendar/vendor/autoload.php';
    $client = new Google_Client();
    $client->setApplicationName('Google_Calendar_API');
    $client->setAuthConfig('client_secret.json');
    $client->addScope("https://www.googleapis.com/auth/calendar");

    $accessToken = $client->fetchAccessTokenWithAuthCode($_GET['code']);

if ($accessToken){

    $id=$_SESSION["order_id"];
    $result=getOrderId($id);
  
       if(count($result)>0)
       {
             $order=fetch_array($result);
            
       }
       $items=OrderItems($id,'');
       $books=OrderBook($id);
       if(count($books)>0)
       {
             $book=fetch_array($books);
            
       }
       $servicearray=array();
       while($item = fetch_object($items))
       {
           if($item->type=='services')
           {
                $servicearray[]=$item->service_name;
           }  
       }
       $services=implode(',',$servicearray);
       //echo $services;
    
   $cal = new Google_Service_Calendar($client);
   
   $starDateTime = new DateTime($book["date"].$book["start_time"]);
   $startDate = $starDateTime->format(DateTime::ISO8601);
   
   $endDateTime = new DateTime($book["date"].$book["end_time"]);
   $endDate = $endDateTime->format(DateTime::ISO8601);
   
   
   
   
   //print_r($startDate);
  //print_r($endDate);
  // die();
   //America/Toronto
    $event = new Google_Service_Calendar_Event(array(
      'summary' => $order["id"].' Ritzy Nail Order',
      'location' => $order["address"],
      'description' => $services,
      'start' => array(
        'dateTime' => $startDate,
        'timeZone' => 'Asia/Karachi',
      ),
      'end' => array(
        'dateTime' => $endDate,
        'timeZone' => 'Asia/Karachi',
      ),
      'recurrence' => array(
        'RRULE:FREQ=DAILY;COUNT=2'
      ),
      'attendees' => array(
        array('email' => 'amoos.golpik@gmail.com')    
      ),
      'reminders' => array(
        'useDefault' => FALSE,
        'overrides' => array(
          array('method' => 'email', 'minutes' => 24 * 60),
          array('method' => 'popup', 'minutes' => 10),
        ),
      ),
    ));

    $calendarId = '7m27buurv0bph3bs6pt491640c@group.calendar.google.com';
    $event = $cal->events->insert($calendarId, $event);
    
    if($event)
    {
        UpdateOrderBook($id,$event->htmlLink);
    
        unset($_SESSION["order_id"]);
        $_SESSION["success"]='Event created: '.$event->htmlLink ;
    }
    

    }

}

$pageTitle = "Welcome Admin";

include('header.php');

$subSqL = "";
$year =date('Y');
if (isset($_GET['year'])) {
    $year = $_GET['year'];
}


$query=query("SELECT count(*) as totalusers FROM users ");
$row=fetch_array($query);

$totalusers=($row["totalusers"])?$row["totalusers"]:0;

$sql=query("SELECT SUM(grand_total) as grand_totals FROM orders where order_status='success'");
$rw1=fetch_array($sql);

$totalrevenue=($rw1["grand_totals"])?PriceFormat($rw1["grand_totals"]):0;




$sql1=query("SELECT COUNT(*) as total_orders FROM orders");
$rw2=fetch_array($sql1);

$totalorders=($rw2["total_orders"])?$rw2["total_orders"]:0;
?>




<!-- page content -->
<div class="right_col" role="main">


    <div class="row top_tiles">
         <?php 
                            if(isset($_SESSION['success']))
                            {
                                ?>
                            <div class="alert alert-success fade in alert-dismissable" style="margin-top:18px;">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
                            <i class="fa fa-tick-circle"></i> <?php echo $_SESSION['success']; ?> </div>
                                <?php
                               unset($_SESSION['success']);      
                                
                            }
                         ?>
        <div class="animated flipInY col-lg-4 col-md-4 col-sm-6 col-xs-12">
            <div class="tile-stats">
                <div class="icon"><i class="fa fa-users"></i>
                </div>
                <div class="count"><?php echo $totalusers; ?></div>

                <h3>Total Users</h3>
            </div>
        </div>
         <div class="animated flipInY col-lg-4 col-md-4 col-sm-6 col-xs-12">
            <div class="tile-stats">
                <div class="icon"><i class="fa fa-shopping-cart"></i>
                </div>
                <div class="count"><?php echo $totalorders; ?></div>
                <h3>Total Orders </h3>
            </div>
        </div>
        <div class="animated flipInY col-lg-4 col-md-4 col-sm-6 col-xs-12">
            <div class="tile-stats">
                <div class="icon"><i class="fa fa-money"></i>
                </div>
                <div class="count"><?php echo $totalrevenue; ?></div>
                <h3>Total Sales </h3>
            </div>
        </div>
     
     
    </div>

    
        <div class="row ">
            
   <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Latest Users </h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      
                      <li><a ><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content" style="display: block;">

                    <table class="table table-striped">
                      <thead>
                        <tr>
                          <th>Name</th>
                          <th>Email</th>
                          <th>Status</th>
                        </tr>
                      </thead>
                      <tbody>
                          <?php
                          $result = query("SELECT * FROM `users` order by id desc limit 5");
                        while($row = fetch_object($result)) 
                        {     
                        ?>
                              <tr >
                                  <td><?php echo $row->name; ?></td>
                                  <td><?php echo $row->email; ?></td>
                                
                                  <td><?php
                                  if($row->status=='active')
                                  {
                                      ?>
                                      <a href="#" data-href="?deactive=<?php echo $row->id; ?>" data-target="#confirm-delete" class="btn btn-success btn-xs"  data-toggle="modal">
                                      <?php echo $row->status; ?></a>
                            
                                  <?php   
                                  }
                                  else
                                  {
                                   ?>
                                       <a href="#" data-href="?active=<?php echo $row->id; ?>" data-target="#confirm-active" class="btn btn-danger btn-xs"  data-toggle="modal">
                                       <?php echo $row->status; ?></a>
                                    <?php   
                                  }?>
                                  </td>
                                
                              </tr>    
                              
                              
                        <?php
                        }
                        ?>
                       
                      </tbody>
                    </table>
                      <div class="text-center"><h4><a href="users.php">View all users</a></h4></div>

                  </div>
                </div>
              </div>



<div class="col-md-6 col-sm-6 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Latest Orders </h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      
                      <li><a ><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content" style="display: block;">

                    <table class="table table-striped">
                      <thead>
                        <tr>
                          <th>ID</th>
                          <th>Email</th>
                          <th>Amount</th>
                          <th>Details</th>
                        </tr>
                      </thead>
                      <tbody>
                          <?php
                        $result = query("SELECT ord.*,pay.order_id,pay.payment_id FROM orders ord
                        INNER JOIN  paypal pay ON ord.id=pay.order_id  order by ord.id DESC limit 5");
                        while($row = fetch_object($result)) 
                        {   
                            $users=getProfile($row->user_id);
                            $user=fetch_array($users);  
                        ?>
                              <tr>
                                  <td><?php echo $row->id; ?></td>
                                  <td><?php echo $user["email"]; ?></td>
                                  <td><?php echo PriceFormat($row->grand_total); ?></td>

                                  <td>	<a href="order-detail.php?id=<?php echo $row->id; ?>" class="btn btn-primary btn-xs">View</a>

                                  </td>
                                
                              </tr>    
                              
                              
                        <?php
                        }
                        ?>
                       
                      </tbody>
                    </table>
                  <div class="text-center"><h4><a href="orders.php">View all orders</a></h4></div>

                  </div>
                </div>
              </div>
             </div>
    <!-- footer content -->
    <footer>
        <div class="">
            <p class="pull-right">
                <span><?php echo $adminTitle["copyright"]; ?></span> 
            </p>
        </div>
        <div class="clearfix"></div>
    </footer>
    <!-- /footer content -->

</div>


<script src="js/bootstrap.min.js"></script>
<script src="js/nicescroll/jquery.nicescroll.min.js"></script>

<!-- chart js -->
<script src="js/chartjs/chart.min.js"></script>
<!-- bootstrap progress js -->
<script src="js/progressbar/bootstrap-progressbar.min.js"></script>
<!-- icheck -->
<script src="js/icheck/icheck.min.js"></script>
<!-- daterangepicker -->
<script type="text/javascript" src="js/moment.min2.js"></script>
<script type="text/javascript" src="js/datepicker/daterangepicker.js"></script>
<!-- sparkline -->
<script src="js/sparkline/jquery.sparkline.min.js"></script>

<script src="js/custom.js"></script>

<!-- flot js -->
<!--[if lte IE 8]><script type="text/javascript" src="js/excanvas.min.js"></script><![endif]-->
<script type="text/javascript" src="js/flot/jquery.flot.js"></script>
<script type="text/javascript" src="js/flot/jquery.flot.pie.js"></script>
<script type="text/javascript" src="js/flot/jquery.flot.orderBars.js"></script>
<script type="text/javascript" src="js/flot/jquery.flot.time.min.js"></script>
<script type="text/javascript" src="js/flot/date.js"></script>
<script type="text/javascript" src="js/flot/jquery.flot.spline.js"></script>
<script type="text/javascript" src="js/flot/jquery.flot.stack.js"></script>
<script type="text/javascript" src="js/flot/curvedLines.js"></script>
<script type="text/javascript" src="js/flot/jquery.flot.resize.js"></script>

<!-- flot -->
<script type="text/javascript">
    $('#year').change(function () {
        var year = $('#year').val();
        window.top.location = "dashboard.php?year=" + year;
    });



    //define chart clolors ( you maybe add more colors if you want or flot will add it automatic )
    var chartColours = ['#96CA59', '#3F97EB', '#72c380', '#6f7a8a', '#f7cb38', '#5a8022', '#2c7282'];

    //generate random number for charts
    randNum = function () {
        return (Math.floor(Math.random() * (1 + 40 - 20))) + 20;
    }

    $(function () {
        var d1 = [];
        //var d2 = [];

        //here we generate data for chart
        for (var i = 0; i < 30; i++) {
            d1.push([new Date(Date.today().add(i).days()).getTime(), randNum() + i + i + 10]);
            //    d2.push([new Date(Date.today().add(i).days()).getTime(), randNum()]);
        }

        var chartMinDate = d1[0][0]; //first day
        var chartMaxDate = d1[20][0]; //last day

        var tickSize = [1, "day"];
        var tformat = "%d/%m/%y";

        //graph options
        var options = {
            grid: {
                show: true,
                aboveData: true,
                color: "#3f3f3f",
                labelMargin: 10,
                axisMargin: 0,
                borderWidth: 0,
                borderColor: null,
                minBorderMargin: 5,
                clickable: true,
                hoverable: true,
                autoHighlight: true,
                mouseActiveRadius: 100
            },
            series: {
                lines: {
                    show: true,
                    fill: true,
                    lineWidth: 2,
                    steps: false
                },
                points: {
                    show: true,
                    radius: 4.5,
                    symbol: "circle",
                    lineWidth: 3.0
                }
            },
            legend: {
                position: "ne",
                margin: [0, -25],
                noColumns: 0,
                labelBoxBorderColor: null,
                labelFormatter: function (label, series) {
                    // just add some space to labes
                    return label + '&nbsp;&nbsp;';
                },
                width: 40,
                height: 1
            },
            colors: chartColours,
            shadowSize: 0,
            tooltip: true, //activate tooltip
            tooltipOpts: {
                content: "%s: %y.0",
                xDateFormat: "%d/%m",
                shifts: {
                    x: -30,
                    y: -50
                },
                defaultTheme: false
            },
            yaxis: {
                min: 0
            },
            xaxis: {
                mode: "time",
                minTickSize: tickSize,
                timeformat: tformat,
                min: chartMinDate,
                max: chartMaxDate
            }
        };
        var plot = $.plot($("#placeholder33x"), [{
                label: "Email Sent",
                data: d1,
                lines: {
                    fillColor: "rgba(150, 202, 89, 0.12)"
                }, //#96CA59 rgba(150, 202, 89, 0.42)
                points: {
                    fillColor: "#fff"
                }
            }], options);
    });
</script>
<!-- /flot -->
<!--  -->
<script>
    $('document').ready(function () {
        $(".sparkline_one").sparkline([2, 4, 3, 4, 5, 4, 5, 4, 3, 4, 5, 6, 4, 5, 6, 3, 5, 4, 5, 4, 5, 4, 3, 4, 5, 6, 7, 5, 4, 3, 5, 16], {
            type: 'bar',
            height: '125',
            barWidth: 13,
            colorMap: {
                '7': '#a1a1a1'
            },
            barSpacing: 2,
            barColor: '#26B99A'
        });

        $(".sparkline11").sparkline([2, 4, 3, 4, 5, 4, 5, 4, 3, 4, 6, 2, 4, 3, 4, 5, 4, 5, 4, 3], {
            type: 'bar',
            height: '40',
            barWidth: 8,
            colorMap: {
                '7': '#a1a1a1'
            },
            barSpacing: 2,
            barColor: '#26B99A'
        });

        $(".sparkline22").sparkline([2, 4, 3, 4, 7, 5, 4, 3, 5, 6, 2, 4, 3, 4, 5, 4, 5, 4, 3, 4, 6], {
            type: 'line',
            height: '40',
            width: '200',
            lineColor: '#26B99A',
            fillColor: '#ffffff',
            lineWidth: 3,
            spotColor: '#34495E',
            minSpotColor: '#34495E'
        });

        var doughnutData = [
            {
                value: 30,
                color: "#455C73"
            },
            {
                value: 30,
                color: "#9B59B6"
            },
            {
                value: 60,
                color: "#BDC3C7"
            },
            {
                value: 100,
                color: "#26B99A"
            },
            {
                value: 120,
                color: "#3498DB"
            }
        ];
        var myDoughnut = new Chart(document.getElementById("canvas1i").getContext("2d")).Doughnut(doughnutData);
        var myDoughnut = new Chart(document.getElementById("canvas1i2").getContext("2d")).Doughnut(doughnutData);
        var myDoughnut = new Chart(document.getElementById("canvas1i3").getContext("2d")).Doughnut(doughnutData);
    });
</script>
<!-- -->
<!-- datepicker -->
<script type="text/javascript">
    $(document).ready(function () {

        var cb = function (start, end, label) {
            console.log(start.toISOString(), end.toISOString(), label);
            $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
            //alert("Callback has fired: [" + start.format('MMMM D, YYYY') + " to " + end.format('MMMM D, YYYY') + ", label = " + label + "]");
        }

        var optionSet1 = {
            startDate: moment().subtract(29, 'days'),
            endDate: moment(),
            minDate: '01/01/2012',
            maxDate: '12/31/2015',
            dateLimit: {
                days: 60
            },
            showDropdowns: true,
            showWeekNumbers: true,
            timePicker: false,
            timePickerIncrement: 1,
            timePicker12Hour: true,
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
            opens: 'left',
            buttonClasses: ['btn btn-default'],
            applyClass: 'btn-small btn-primary',
            cancelClass: 'btn-small',
            format: 'MM/DD/YYYY',
            separator: ' to ',
            locale: {
                applyLabel: 'Submit',
                cancelLabel: 'Clear',
                fromLabel: 'From',
                toLabel: 'To',
                customRangeLabel: 'Custom',
                daysOfWeek: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
                monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
                firstDay: 1
            }
        };
        $('#reportrange span').html(moment().subtract(29, 'days').format('MMMM D, YYYY') + ' - ' + moment().format('MMMM D, YYYY'));
        $('#reportrange').daterangepicker(optionSet1, cb);
        $('#reportrange').on('show.daterangepicker', function () {
            console.log("show event fired");
        });
        $('#reportrange').on('hide.daterangepicker', function () {
            console.log("hide event fired");
        });
        $('#reportrange').on('apply.daterangepicker', function (ev, picker) {
            console.log("apply event fired, start/end dates are " + picker.startDate.format('MMMM D, YYYY') + " to " + picker.endDate.format('MMMM D, YYYY'));
        });
        $('#reportrange').on('cancel.daterangepicker', function (ev, picker) {
            console.log("cancel event fired");
        });
        $('#options1').click(function () {
            $('#reportrange').data('daterangepicker').setOptions(optionSet1, cb);
        });
        $('#options2').click(function () {
            $('#reportrange').data('daterangepicker').setOptions(optionSet2, cb);
        });
        $('#destroy').click(function () {
            $('#reportrange').data('daterangepicker').remove();
        });
    });
</script>
<!-- /datepicker -->
</body>

</html>