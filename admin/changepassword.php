<?php 
include_once('../config/connection.php');
if(!isset($_SESSION["admin"])){
        
	 echo "<script type='text/javascript'>window.location='login/';</script>";
	exit();
}
include_once('../config/model.php');
include_once('../config/functions.php');
include_once('../config/constants.php');
 
$id=charEsc($_SESSION["admin"]);
$sql= "select * from `admin` where id='{$id}'";
$result = query($sql);
$user=fetch_array($result);

if(isset($_POST["update"]))
{
    $old_pass = charEsc($_POST["old_pass"]);
    $pass = charEsc($_POST["password"]);
    $confirm_pass = charEsc($_POST["confirm_password"]);
    $name = charEsc($_POST["name"]);
    $email = charEsc($_POST["email"]);
    
            $checkold=ValidatePassword($old_pass);
            $checkpass=ValidatePassword($pass);
            $checkconfimpass=ValidatePassword($confirm_pass);
            $checkemail=ValidateEmail($email);
            $checkname=ValidateName($name);
            
            if(!empty($checkname))
            {
                $_SESSION["error"] = $checkname;	
              
            }
            else if(!empty($checkemail))    
            {
                $_SESSION["error"] = $checkemail;	
           
            }
            else if(!empty($checkold))
            {
                $_SESSION["error"] = $checkold;   

            }
            else if(!empty($checkpass))
            {
                $_SESSION["error"] = $checkpass;   

            }
            else if(!empty($checkpass))
            {
                $_SESSION["error"] = $checkpass;   

            }
            else if(!empty($checkconfimpass))
            {
                $_SESSION["error"] = $checkconfimpass;    
                
            }
            else if($pass!=$confirm_pass)
            {
                 $_SESSION["error"] = "Your password and confirm password didn't matched!"; 
                 
            }
            else
            {
                $old_pass= md5($old_pass);
                
                if($old_pass==$user["password"])
                {
                    $password= md5($pass);
                    $mDate=date("Y-m-d H:i:s");
                    $passquery=query("UPDATE `admin` SET `name`='{$name}',
                    `email`='{$email}',`password`='{$password}',
                    `modified_date`='{$mDate}' WHERE `id`='{$id}'");
                    if($passquery)
                    {
                        $_SESSION['name']=$name;
                        $_SESSION['success']="Profile updated successfully!";
                    }
                    else
                    {
                        $_SESSION['error']="Profile cannot update!";
                    }
                }
                else {
                    
                    $_SESSION['error']="Old password is not correct!";
                }
                
            }
}

$pageTitle = "Change Password"; 
include('header.php'); ?>

            <!-- page content -->
            <div class="right_col" role="main">
                <div class="">

                    <div class="page-title">
                        <div class="title_left">
                            <h3>
                     
                        Change Password
                   
                </h3>
                        </div>

                        
					
                    </div>
					
                    <div class="clearfix"></div>


                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="x_panel">
                              
                                <div class="x_content">
                                    <br />
                                    <form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left"  method="post" action="">

									
									<?php if(isset($_SESSION["error"])){ ?> 
									
									
									<div class="x_content bs-example-popovers">

                               
										<div class="alert alert-danger alert-dismissible fade in" role="alert">
										<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
										</button>
									<?php echo "<strong>Error ! </strong>"." ".$_SESSION["error"]; unset($_SESSION["error"]); ?>
										</div>

									</div>
									<?php } ?>
									
									
									<?php if(isset($_SESSION["success"])){ ?> 
									
									
									<div class="x_content bs-example-popovers">

                               
										<div class="alert alert-success alert-dismissible fade in" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                                    </button>
                                    <?php echo "<strong>Congrats ! </strong>"." ".$_SESSION["success"]; unset($_SESSION["success"]); ?>
                                </div>

									</div>
									<?php } ?>
									
									
									
									
                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Full Name <span class="required">*</span>
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <input type="text"  id="first-name" required="required" value="<?php if(isset($user['name'])){ echo $user['name']; }?>" name="name" class="form-control col-md-7 col-xs-12">
											
                                            </div>
                                        </div>
                                       
                                        <div class="form-group">
                                            <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Email<span class="required">*</span></label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <input id="middle-name" required="required"  value="<?php if(isset($user['email'])){ echo $user['email']; }?>" class="form-control col-md-7 col-xs-12" type="email" name="email">
                                            </div>
                                        </div>
										
									    
										 <div class="form-group">
                                            <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Old Password<span class="required">*</span></label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <input  class="form-control col-md-7 col-xs-12" required="required" type="password" name="old_pass" required>
                                            </div>
                                        </div>
										
										
										
										<div class="form-group">
                                            <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">New Password<span class="required">*</span></label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <input  class="form-control col-md-7 col-xs-12" required="required" type="password" name="password" required>
                                            </div>
                                        </div>
										
										<div class="form-group">
                                            <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Re-Enter New Password<span class="required">*</span></label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <input  class="form-control col-md-7 col-xs-12" required="required" type="password" name="confirm_password" required>
                                            </div>
                                        </div>
                                       
									   
                                        
									   
									   
                                        <div class="ln_solid"></div>
                                        <div class="form-group">
                                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                             
                                                <button type="submit" name="update" class="btn btn-success">Update Password</button>
                                            </div>
                                        </div>

                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>

                  

                </div>

                <!-- footer content -->
                <footer>
                    <div class="">
                        <p class="pull-right">
                                 <span><?php echo $adminTitle["copyright"]; ?></span> 
                        </p>
                    </div>
                    <div class="clearfix"></div>
                </footer>
                <!-- /footer content -->

            </div>
            <!-- /page content -->
        </div>

    </div>

    <div id="custom_notifications" class="custom-notifications dsp_none">
        <ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group">
        </ul>
        <div class="clearfix"></div>
        <div id="notif-group" class="tabbed_notifications"></div>
    </div>

    <script src="js/bootstrap.min.js"></script>

    <!-- chart js -->
    <script src="js/chartjs/chart.min.js"></script>
    <!-- bootstrap progress js -->
    <script src="js/progressbar/bootstrap-progressbar.min.js"></script>
    <script src="js/nicescroll/jquery.nicescroll.min.js"></script>
    <!-- icheck -->
    <script src="js/icheck/icheck.min.js"></script>

    <script src="js/custom.js"></script>
	
	<script>
    $(document).ready(function(){   
        window.setTimeout('fadeout();', 2000);
    });

    function fadeout(){
        $('#fade').fadeOut('slow', function() {
           // Animation complete.
        });
    }

    </script>

</body>

</html>