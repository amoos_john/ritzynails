<?php 
        include_once('../config/connection.php');
        include_once('sessions.php');
        include_once('../config/model.php');
        include_once('../config/functions.php');
	include_once('../config/constants.php');
        
if(isset($_GET["booking"]))        
{
    require_once '../googlecalendar/vendor/autoload.php';
    

    $redirect_uri='http://ecommercehouse.com/projects/ritzynails/admin/dashboard.php';
    $client = new Google_Client();
    $client->setApplicationName('Google_Calendar_API');
    $client->setAuthConfig('client_secret.json');
    $client->setScopes(["https://www.googleapis.com/auth/calendar"]);
   
    $_SESSION["order_id"]=$_GET["booking"];
     $client->setRedirectUri($redirect_uri);
    $auth_url = $client->createAuthUrl();
    header('Location: ' . filter_var($auth_url, FILTER_SANITIZE_URL));
}
        
	
if(isset($_GET["id"]))
{
       $id=charEsc($_GET["id"]); 
       $result=getOrderId($id);
       if(count($result)>0)
       {
             $order=fetch_array($result);
            
       }
       $items=OrderItems($id,'');
       $books=OrderBook($id);
       if(count($books)>0)
       {
             $book=fetch_array($books);
            
       }
}
else
{
     echo "<script type='text/javascript'>window.location='dashboard.php';</script>";
}
       
	
$pageTitle = "Order Information"; 
include('header.php');         
?>
   <!-- page content -->
   <div class="right_col" role="main">
     <div class="">
        <div class="page-title">
            <div class="title_left">
               <h3> <?php echo $pageTitle ?></h3>
            </div>

           </div>
					
           <div class="clearfix"></div>
 
           <div class="row">
           <div class="clearfix"></div>
		<div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                 
                     <div class="x_content">
                         	<?php if(isset($_SESSION["error"])){ ?> 
				<div class="x_content bs-example-popovers">
					<div class="alert alert-danger alert-dismissible fade in" role="alert">
						<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
							<?php echo "<strong>Error ! </strong>"." ".$_SESSION["error"]; unset($_SESSION["error"]); ?>
					</div>
 				</div>
							<?php  unset($_SESSION["error"]);} ?>
									
				<?php if(isset($_SESSION["success"])){ ?> 
					<div class="x_content bs-example-popovers">
						<div class="alert alert-success alert-dismissible fade in" role="alert">
							<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
							<?php echo "<strong>Congrats ! </strong>"." ".$_SESSION["success"]; unset($_SESSION["success"]); ?>
						</div>
					</div>
				<?php
                                unset($_SESSION["success"]);
                                } ?>
                           <div class="col-sm-12">
                                <?php
                                  if($order["order_status"]=='success')
                                  {
                                      ?>
                                      <a href="#" data-href="orders.php?deactive=<?php echo  $order["id"]; ?>" data-target="#confirm-delete" class="btn btn-success"  data-toggle="modal">
                                      <?php echo $order["order_status"]; ?></a>
                            
                                  <?php   
                                  }
                                  else
                                  {
                                   ?>
                                       <a href="#" data-href="orders.php?active=<?php echo  $order["id"]; ?>" data-target="#confirm-active" class="btn btn-danger"  data-toggle="modal">
                                       <?php echo $order["order_status"]; ?></a>
                                    <?php   
                                  }?>  
                              <a href="#userModal" data-toggle="modal" class="btn btn-info userdet" >User Detail</a>
                              <div id="userModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content" >
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                            <h4 class="modal-title" id="myModalLabel">User Details</h4>
                        </div>
                        <div class="modal-body" id="modaluserContent" style="display:none;">
                            <table class="table table-striped" >
                           <tbody>
                                <?php
                               $result = query("SELECT * FROM `users` where id=".$order["user_id"]."");
                               $row = fetch_array($result);
                               ?>
                             <tr>
				<th>Name </th>
                                <td><?php echo $row["name"]; ?></td>
                             </tr> 
                             <tr>
                                <th>Last Name </th>
                                <td><?php echo $row["name"]; ?></td>

                             </tr>
                              <tr>
                                <th>Email</th>
                                <td><?php echo $row["email"]; ?></td>

                              </tr>
                               <tr>
                                <th>Phone No.</th>
                                <td><?php echo $row["phone"]; ?></td>

                                </tr>
                                 <tr>
                                <th>Postal Code</th>
                                <td><?php echo $row["postal_code"]; ?></td>

                                </tr>
                                 <tr>
                                <th>Address </th>
                                <td><?php echo $row["address"]; ?></td>

                                </tr>
                                 <tr>
				<th>Status </th>
                                 <td><?php
                                  if($row["status"]=='active')
                                  {
                                      ?>
                                      <span class="btn btn-success btn-xs">
                                      <?php echo $row["status"]; ?></span>
                            
                                  <?php   
                                  }
                                  else
                                  {
                                   ?>
                                       <span class="btn btn-danger btn-xs">
                                       <?php echo $row["status"]; ?></span>
                                    <?php   
                                  }?>
                                  </td>
				  
                             </tr>
                          </tbody>
                    
                           
                            </table>
                        </div>
                                        <div class="modal-footer">
                               <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                        </div>
                                    </div>
                                </div>
                            </div>	
                              <a href="orders.php" class="btn btn-primary" >Back</a>  
                           </div>
                          <div class="table-total-area col-sm-6">
                          
	   <table class="table  table-bordered">
                    <thead>
                    <tr><th colspan="2">Order Detail</th>
                    </tr>
                    </thead>

                    <tbody>
                    <tr>
                        <td>Order ID</td>
                        <td><?php echo $order["id"]; ?></td>
                    </tr>
                    <tr>
                        <td>Payment ID</td>
                        <td><?php echo $order["payment_id"]; ?></td>
                    </tr>
                     <tr>
                        <td>Order Status</td>
                        <td><?php echo $order["order_status"]; ?></td>
                    </tr>
                    <tr>
                        <td>Order Date</td>
                        <td><?php echo DateFormat($order["created_date"]); ?></td>
                    </tr>
                   <tr>
                        <td>Mobile fee</td>
                        <td><?php echo ($order["mobile_fee"]!=0)?PriceFormat($order["mobile_fee"]):''; ?></td>
                    </tr>
                   
                    <tr>
                        <td>Parking fee</td>
                        <td><?php echo ($order["parking"]!='')?PriceFormat($order["parking"]):'$0.00'; ?></td>
                    </tr>
                    <tr class="total-area-btns">
                         <td>Order Total</td>
                        <td><?php echo PriceFormat($order["grand_total"]); ?></td>   
                    </tr>

                </tbody>
                </table>
            </div>  
            <div class="table-total-area col-sm-6">
                <table class="table  table-bordered">
                    <thead>
                    <tr><th colspan="2">Booking Detail</th>
                    </tr>
                    </thead>

                    <tbody>
                   
                    <tr>
                        <td>Date</td>
                        <td><?php echo ($books)?DateFormat($book["date"]):''; ?></td>
                    </tr>
                     <tr>
                        <td>Start Time</td>
                        <td><?php echo ($books)?$book["start_time"]:''; ?></td>
                    </tr>
                    <tr>
                        <td>End Time</td>
                        <td><?php echo ($books)?$book["end_time"]:''; ?></td>
                    </tr>
                    <tr>
                        <td>Booking Status</td>
                        <td><?php echo ($books)?$book["status"]:''; ?></td>
                    </tr>
                    <tr>
                        <td>Postal Code</td>
                        <td><?php echo ($books)?$order["postal_code"]:''; ?></td>
                    </tr>
                    <tr>
                        <td>Address</td>
                        <td><?php echo ($books)?$order["address"]:''; ?></td>
                    </tr>
                    <?php
                    if($books)
                    {
                        if($book["event_id"]=='')
                        {
                        ?>
                    
                     <tr>
                         <td colspan="2"><a href="order-detail.php?booking=<?php echo $order["id"]; ?>">Add Booking in google calendar</a></td>
                         
                       
                    </tr>
                    <?php
                        }
                    } ?>
                </tbody>
                </table>
            </div>
                    <div class="clearfix"></div>        
			
		   <div class="table-area  col-sm-12">
                    <table class="table cart-item-table table-bordered table-valign">
                        <thead>
                        <tr>    
                               <th>Persons</th>
                                <th>Image</th>
                                <th>Service / Product</th>
                                <th>Price</th> 
                                <th>Color</th>
                        </tr>
                        </thead> 
                        <tbody>
                            <?php 
                                for($i=1;$i<=$order["person"];$i++)
                                {
                                $service_image='';
                                $service_name='';
                                $service_price='';
                                $service_color=array();
                                $items=OrderItems($id,$i);
                                while($item = fetch_object($items))
                                {
                                    
                                    $service_image.='<span class="pic"><img src="../'.$item->image.'"  /></span><br/>';
                                    $service_name.='<span class="txt-price">'.$item->service_name."</span><br/>";
                                    $service_price.='<span class="txt-price">'.PriceFormat($item->total_price)."</span><br/>";
                                   if($item->type=='services') {
                                    
                                    $colors=OrderColor($item->id);
                                    if(count($colors)>0)
                                    {
                                      $color=fetch_array($colors);
                                      while($color=fetch_object($colors))
                                      {
                                          $service_color[]=$color->color_name.'<br/>';
                                      }
                                    }
                                    
                                    } 
                                }
                                   
                                ?>
                               
                            <tr>
				<td>Person <?php echo $i; ?></td>
                                <td class="cart__table__image">
                                    <?php echo $service_image; ?>
                                </td>
                                <td><?php echo $service_name; ?></td>
                                <td><?php echo $service_price; ?></td>
                                <td><?php 
                                if(count($service_color)>0)
                                {
                                    foreach($service_color as $sercolor)
                                    {
                                        echo $sercolor;
                                    }
                                }
                                 ?></td>
                              

                            </tr>
                            <?php 
                                }
                               
                            ?>
                            
                        
                        </tbody>
                    </table>
                                
      <div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
        <div class="modal-dialog">
            <div class="modal-content">
            
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title" id="myModalLabel">Confirm Deactive</h4>
                </div>
            
                <div class="modal-body">
                    <p>Are You Sure You want to pending this order?</p>
                </div>
                
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <a class="btn btn-danger btn-ok">Pending</a>
                </div>
            </div>
        </div>
    </div>
                         
    <div class="modal fade" id="confirm-active" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
        <div class="modal-dialog">
            <div class="modal-content">
            
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title" id="myModalLabel">Confirm active</h4>
                </div>
            
                <div class="modal-body">
                    <p>Are You Sure You want to success this order?</p>
                </div>
                
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <a class="btn btn-success btn-ok">Success</a>
                </div>
            </div>
        </div>
    </div>
                     </div>
			
		  </div>
		</div>
	 </div>
   </div>

    <!-- footer content -->
    <footer>
      <div class="">
        <p class="pull-right">
           <span><?php echo $adminTitle["copyright"]; ?></span> 
        </p>
      </div>
      <div class="clearfix"></div>
    </footer>
    <!-- /footer content -->
  </div>
  <!-- /page content -->
 </div>
</div>

    <div id="custom_notifications" class="custom-notifications dsp_none">
        <ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group">
        </ul>
        <div class="clearfix"></div>
        <div id="notif-group" class="tabbed_notifications"></div>
    </div>

    <script src="js/bootstrap.min.js"></script>
    <!-- chart js -->
    <script src="js/chartjs/chart.min.js"></script>
    <!-- bootstrap progress js -->
    <script src="js/progressbar/bootstrap-progressbar.min.js"></script>
    <script src="js/nicescroll/jquery.nicescroll.min.js"></script>
    <!-- icheck -->
    <script src="js/icheck/icheck.min.js"></script>
    <script src="js/custom.js"></script>
    
     <!-- Datatables -->
  <script src="js/jquery.dataTables.min.js"></script>


<script>
        $('#confirm-delete').on('show.bs.modal', function(e) {
            $(this).find('.btn-ok').attr('href', $(e.relatedTarget).data('href'));
            
        });
         $('#confirm-active').on('show.bs.modal', function(e) {
            $(this).find('.btn-ok').attr('href', $(e.relatedTarget).data('href'));
            
        });
</script>

<div id="myModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content" >
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title" id="myModalLabel">Colors</h4>
</div>
<div class="modal-body" id="modalContent" style="display:none;">
 
</div>
                <div class="modal-footer">
       <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
</div>
            </div>
        </div>
</div>	
<script>
$(".cdetail[data-toggle=modal]").click(function()  {
var essay_id = $(this).attr('id');
        $.ajax({
            cache: false,
            type: 'POST',
            url: 'detail.php',
            data: 'id='+essay_id,
            success: function(data) 
            {
                $('#myModal').show();
                $('#modalContent').show().html(data);
            }
        });
    });
    
    $(".userdet[data-toggle=modal]").click(function()  {

                $('#userModal').show();
                $('#modaluserContent').show();
            
       
    });


</script>
</body>

</html>