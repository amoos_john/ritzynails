<?php
include_once('../config/connection.php');
 if(!isset($_SESSION["admin"])){
        
	 echo "<script type='text/javascript'>window.location='login/';</script>";
	exit();
    }
include_once('../config/model.php');
include_once('../config/constants.php');

if(isset($_GET["editId"]))
{
	$id = charEsc($_GET["editId"]);
        if($id!=null)
        {
            $result = query("select * from contents where id={$id}");
            if(num_rows($result) > 0)
            {
                    $row = fetch_array($result);
                    $get = "editId";
                    $title = $row['title'];
                    $code = $row['code'];
                    $type = $row['type'];
                    $subject = $row['subject'];
                    $description = $row['metaDescription'];
                    $keywords = $row['keywords'];
                    $body = $row['body'];
                    $image = $row['image'];
            }    
        }
         else {
            
          echo "<script type='text/javascript'>window.location='contents.php';</script>";


        }
}

else if(isset($_POST["update"]))
{
        
	$id = $_POST["editId"];
	$get = "editId";
	$title = charEsc($_POST["title"]);
	$code = charEsc($_POST["code"]);
	$type = charEsc($_POST["type"]);
	$subject = charEsc($_POST["subject"]);
	$description = charEsc($_POST["description"]);
	$keywords = charEsc($_POST["keywords"]);
	$body = $_POST["body"];
	$image = $_FILES["image"]["name"];

	if($title == "")
	{
	$_SESSION["error"] = "Title is required.";	
	}
	
	else if($code == "")
	{
	$_SESSION["error"] = "Code is required.";	
	}
        else if($type == "")
	{
            $_SESSION["error"] = "Type is required";	
	}

	else if($body == "")
	{
	 $_SESSION["error"] = "Body is required";	
	}
	else
        {
            $mDate = date('Y-m-d H:i:s');
            if($image != "")
            {
                $imageFileType = pathinfo($image,PATHINFO_EXTENSION);
            
                if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg" && $imageFileType != "gif" ) 
                {
                    $_SESSION["error"] = "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";	
                } 
                else
                {
                    $row = select("select image from `content` where id={$id}"); 
                    if(count($row)>0)
                    {
                        $dbImage = $row[0]["image"];
                    
                        $deleteFile1 = "../".$dbImage;

                        if (file_exists($deleteFile1)) { unlink($deleteFile1); }
                    }
                    
                    $newimage = uploadImage($_FILES["image"],"../images/content/",$type);
                    $imagename='images/content/'.$newimage;
                }
            }
            else
            {
                $imagename=$_POST['getImage'];
            }
            if(!isset($_SESSION["error"])){
							
						//	$img ='image_' . date('Y-m-d-H-i-s') . '_' . uniqid() . '.jpg';
		$cDate = date('Y-m-d-H-i-s');
		$query = query("update `contents` set `title`='{$title}',`image`='{$imagename}',`code`='{$code}',`type`='{$type}',`subject`='{$subject}' ,`body`='{$body}'
                ,`modified_date`='{$mDate}',`metaDescription`='{$description}',`keywords`='{$keywords}' where id={$id}"); 
					
            if(!$query)
            {
								  //trigger_error('Invalid query: ' . mysql_error());
                   $_SESSION["error"] = "Content Updation Failed.";	
                   echo "<script type='text/javascript'>window.location ='addcontents.php?editId='+'$id' </script>";

            }
          else
          {
                  $_SESSION["success"] = "Content Updated Successfully.";
                  echo "<script type='text/javascript'>window.location ='contents.php' </script>";

          }
        }

        }
        
}

else if(isset($_POST["submit"])){
	
        
	$title = charEsc($_POST["title"]);
	//$get = "addId";
	//$id = "";
	$code = charEsc($_POST["code"]);
	$type = charEsc($_POST["type"]);
	$subject = charEsc($_POST["subject"]);
	$description = charEsc($_POST["description"]);
	$keywords = charEsc($_POST["keywords"]);
	$body = addslashes($_POST["body"]);
	$image = $_FILES["image"]["name"];

	
	if($title == "")
	{
            $_SESSION["error"] = "Title is  required.";	
	}	
	
	else if($code == "")
	{
            $_SESSION["error"] = "Code is  required.";	
	}
        else if($type == "")
	{
            $_SESSION["error"] = "Type is  required";	
	}
	
	else if($body == "")
	{
            $_SESSION["error"] = "Body is  required";	
	}

	else{
         if($image!="") 
         {  
          $imageFileType = pathinfo($image,PATHINFO_EXTENSION);

          if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
                        && $imageFileType != "gif" ) {
              $_SESSION["error"] = "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";	
          }   
           
        $newimage = uploadImage($_FILES["image"],"../images/content/",$type);
        $imagenew='images/content/'.$newimage;
         }
         else
         {
             $imagenew="";
         }
         $cDate = date('Y-m-d-H-i-s');
        //,"status" => $status,
         $ins=array("title" => $title,"code" => $code,"type" =>$type,
        "subject" => $subject,"body" => $body,"metaDescription" => $description,
        "keywords" => $keywords,"image" => $imagenew,"status" => 1,
        "created_date" => $cDate);
         $query = insert("contents",$ins);

          
          if(!$query)
          {
                 // trigger_error('Invalid query: ' . mysql_error());
                   $_SESSION["error"] = "Content Failed to Added.";

          }
          else
          {
                  $_SESSION["success"] = "Content Added Successfully.";    
                  echo "<script type='text/javascript'>window.location ='contents.php' </script>";

          }	
                
	 }
}

else{
	
	$id = "";
	$get = "addId";
	$title = "";
	$code = "";
	$type = "";
	$subject = "";
	$description = "";
	$keywords = "";
	$body = "";
	
}



$pageTitle = "Add Content";
include('header.php'); 

?>
<style type="text/css">

#imagePreview {
    background-image:url('<?php echo ($image)?'../'.$image:''; ?>');
    width: 50%;
   height: 200px;
    background-size: 100% 200px;
    background-repeat: no-repeat;
    margin: 0 auto;
    border: 1px solid #DDE2E8;
     display: table;
     margin-left: 10px;
}


</style>
<script>

var loadImageFile = (function () {
    if (window.FileReader) {
        var    oPreviewImg = null, oFReader = new window.FileReader(),
            rFilter = /^(?:image\/bmp|image\/cis\-cod|image\/gif|image\/ief|image\/jpeg|image\/jpeg|image\/jpeg|image\/pipeg|image\/png|image\/svg\+xml|image\/tiff|image\/x\-cmu\-raster|image\/x\-cmx|image\/x\-icon|image\/x\-portable\-anymap|image\/x\-portable\-bitmap|image\/x\-portable\-graymap|image\/x\-portable\-pixmap|image\/x\-rgb|image\/x\-xbitmap|image\/x\-xpixmap|image\/x\-xwindowdump)$/i;

        oFReader.onload = function (oFREvent) {
            if (!oPreviewImg) {
                var newPreview = document.getElementById("imagePreview");
                $("#imagePreview").css("background-image","");
                oPreviewImg = new Image();
                oPreviewImg.style.width = (newPreview.offsetWidth).toString() + "px";
                oPreviewImg.style.height = (newPreview.offsetHeight).toString() + "px";
                newPreview.appendChild(oPreviewImg);
            }
            oPreviewImg.src = oFREvent.target.result;
        };

        return function () {
            var aFiles = document.getElementById("image").files;
            if (aFiles.length === 0) { return; }
            if (!rFilter.test(aFiles[0].type)) { alert("You must select a valid image file !"); return; }
            oFReader.readAsDataURL(aFiles[0]);
        }

    }

})();
</script>
<script src="ckeditor.js"></script>
<script src="js/sample.js"></script>
<!--<link rel="stylesheet" href="toolbarconfigurator/lib/codemirror/neo.css">-->
<style>
textarea {
	width: 100% !important;
}
.textarea {
	height:150px !important;
        resize:none;
}
</style>

	<!-- page content -->
		<div class="right_col" role="main">
			<div class="">
 				<div class="page-title">
					<div class="title_left">
						<h3> <?php echo $pageTitle; ?> </h3>
					</div>
  
      
				</div> <!--Page Title Div end here -->
   
    <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
          <div class="x_content">
            <form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left"  method="post" action="addcontents.php" enctype="multipart/form-data">
              <?php if(isset($_SESSION["error"])){ ?>
              <div class="x_content bs-example-popovers">
                <div class="alert alert-danger alert-dismissible fade in" role="alert">
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span> </button>
                  <?php echo "<strong>Error ! </strong>"." ".$_SESSION["error"]; unset($_SESSION["error"]); ?> </div>
              </div>
              <?php } ?>
             
              <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Title <span class="required">*</span> </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <input type="text"  value="<?php echo $title; ?>" required name="title" class="form-control col-md-7 col-xs-12">
                  <input type="hidden" name="<?php echo $get; ?>" value="<?php echo $id; ?>" />
                  <input type="hidden" name="getImage" value="<?php echo $image; ?>" />   

                </div>
              </div>
              
              <div class="form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Code <span class="required">*</span>
              </label>
                  <div class="col-md-6 col-sm-6 col-xs-12">
                  <input type="text" value="<?php echo $code; ?>" required name="code" id="code" pattern="^\S+$" class="form-control col-md-7 col-xs-12" />
                  <em>Note: (Don't use space and use lower case alphabets)</em>
                  </div>
              </div>
              <div class="form-group">
              <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Content Type*</label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                
                <select class="form-control" name="type" id="type" required>
                                  <option value="">Select Type</option>
                                  <?php
                             
                                  foreach($content_types as $key=>$content_type) 
                                  {    
                                           $selected=($type==$key)?'selected':'';
                                  ?>
                                  <option value="<?php echo $key; ?>" <?php echo $selected ?>><?php echo $content_type; ?></option>
                                  
                                  <?php
                                  }
                                  ?>
                              </select>
                    
                    
                </div>
              </div>
			  <div class="form-group">
              <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Subject</label>
                 <div class="col-md-6 col-sm-6 col-xs-12">
                 <input id="middle-name" value="<?php echo $subject; ?>" class="form-control col-md-7 col-xs-12" type="text" name="subject">
                 </div>
             </div>
            <div class="form-group">
            <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Keywords</label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                <input id="middle-name" value="<?php echo $keywords; ?>" class="form-control col-md-7 col-xs-12" type="text" name="keywords">
                </div>
            </div>
										
	  <div class="form-group">
            <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Meta Description</label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <textarea class="form-control col-md-7 col-xs-12 textarea" name="description"><?php echo $description; ?></textarea>   
                </div>
            </div>
										
	  

              <div class="form-group">
                <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Image</label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <input type="file" onchange="loadImageFile();"  name="image" id="image" class="form-control col-md-7 col-xs-12" >
                </div>
              </div> 
               <div class="form-group">
                 <label for="middle-name" class="control-label col-md-2 col-sm-2 col-xs-12">&nbsp;&nbsp;</label>

                  <div id="imagePreview" class="form-control col-md-7 col-xs-12" ></div>
                       
              </div>
										
              <div class="form-group">
                <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Body</label>
                <div class="col-md-10 col-sm-10 col-xs-12"> 
                  <!-- ck Editor Start-->
                  <div class="adjoined-bottom">
                    <div class="grid-container">
                      <div class="grid-width-100">
                        <textarea id="editor" name="body">
							 <?php echo $body; ?>
			</textarea>
                      </div>
                    </div>
                  </div>
                  <!-- ck Editor End--> 
                  
                </div>
              </div>
              <div class="ln_solid"></div>
              <div class="form-group">
				<div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                    <?php if(isset($_GET['editId'])) { ?> 
                                    	  <button type="submit" name="update" class="btn btn-success">Update</button>
			
                                    <?php
                                    }  
                                    else
                                    {  ?>
                                    	<button type="submit" name="submit" class="btn btn-success">Save</button>

                                  <?php 
                                    }
                                    ?>
                                        <a href="contents.php" class="btn btn-danger">Cancel</a>
				</div>
			</div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
  
  <!-- footer content -->
  <footer>
    <div class="">
      <p class="pull-right"> <span><?php echo $adminTitle["copyright"]; ?></span> </p>
    </div>
    <div class="clearfix"></div>
  </footer>
  <!-- /footer content --> 
  
</div>
<!-- /page content -->
</div>
</div>
<div id="custom_notifications" class="custom-notifications dsp_none">
  <ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group">
  </ul>
  <div class="clearfix"></div>
  <div id="notif-group" class="tabbed_notifications"></div>
</div>
<script src="js/bootstrap.min.js"></script> 

<!-- chart js --> 
<script src="js/chartjs/chart.min.js"></script> 
<!-- bootstrap progress js --> 
<script src="js/progressbar/bootstrap-progressbar.min.js"></script> 
<script src="js/nicescroll/jquery.nicescroll.min.js"></script> 
<!-- icheck --> 
<script src="js/icheck/icheck.min.js"></script> 
<script src="js/custom.js"></script> 
<script>
    $(document).ready(function(){   
        window.setTimeout('fadeout();', 2000);
    });

    function fadeout(){
        $('#fade').fadeOut('slow', function() {
           // Animation complete.
        });
    }
$(document).ready( function() {
$("#code").keypress( function(e) {
if (e.which==32){
return false;
}
});
});

    </script> 
<script>
	initSample();
</script>
</body>
</html>