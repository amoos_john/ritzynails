<?php 
        include_once('../config/connection.php');
        include_once('sessions.php');
        include_once('../config/model.php');
	include_once('../config/constants.php');
	
        
	$mDate=date("Y-m-d H:i:s");
        if(isset($_GET["active"]))
	{
	    $id =  charEsc($_GET["active"]);
            $query=query("UPDATE `users` SET `status`='active',`modified_date`='{$mDate}' WHERE `id`='{$id}'");
            if($query)
            {
                $_SESSION['success']="Profile updated successfully!";
            }
            else
            {
                $_SESSION['error']="Profile cannot update!";
            }
            header('location: users.php');
	}   
        if(isset($_GET["deactive"]))
	{
	    $id =  charEsc($_GET["deactive"]);
            $query=query("UPDATE `users` SET `status`='deactive',`modified_date`='{$mDate}' WHERE `id`='{$id}'");
            if($query)
            {
                $_SESSION['success']="Profile updated successfully!";
            }
            else
            {
                $_SESSION['error']="Profile cannot update!";
            }
             header('location: users.php');       
	}  
$pageTitle = "View Users"; 
include('header.php');         
?>
   <!-- page content -->
   <div class="right_col" role="main">
     <div class="">
        <div class="page-title">
            <div class="title_left">
               <h3><?php echo $pageTitle ?> </h3>
            </div>

           </div>
					
           <div class="clearfix"></div>
 
           <div class="row">
           <div class="clearfix"></div>
			 <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                
                     <div class="x_content">
                         	<?php if(isset($_SESSION["error"])){ ?> 
				<div class="x_content bs-example-popovers">
					<div class="alert alert-danger alert-dismissible fade in" role="alert">
						<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
							<?php echo "<strong>Error ! </strong>"." ".$_SESSION["error"]; unset($_SESSION["error"]); ?>
					</div>
 				</div>
							<?php  unset($_SESSION["error"]);} ?>
									
				<?php if(isset($_SESSION["success"])){ ?> 
					<div class="x_content bs-example-popovers">
						<div class="alert alert-success alert-dismissible fade in" role="alert">
							<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
							<?php echo "<strong>Congrats ! </strong>"." ".$_SESSION["success"]; unset($_SESSION["success"]); ?>
						</div>
					</div>
				<?php
                                unset($_SESSION["success"]);
                                } ?>
                          <div class="table-responsive"> 
			            <table class="table table-striped responsive-utilities jambo_table bulk_action" id="datatable">
                           <thead>
                             <tr>
				<th>Name </th>
                                <th>Last Name </th>
                                <th>Email</th>
                                <th>Phone No.</th>
                                <th>Postal Code</th>
                                <th>Address </th>
                                <th>Registed Date</th>
				<th>Status </th>
                                
				<!--<th class="column-title no-link last"><span class="nobr">Action</span></th>
                                 -->   
                             </tr>
                          </thead>
                          <tbody>
                             <?php
                         
                        //Selecting the data from table 
                        $result = query("SELECT * FROM `users` order by id desc");
                        while($row = fetch_object($result)) 
                        {     
                        ?>
                              <tr >
                                  <td><?php echo $row->name; ?></td>
                                  <td><?php echo $row->last_name; ?></td>
                                  <td><?php echo $row->email; ?></td>
                                  <td><?php echo $row->phone; ?></td>
                                  <td><?php echo $row->postal_code; ?></td>
                                  <td><?php echo $row->address; ?></td>
                                  <td><?php echo $row->created_date; ?></td>
                                  <td><?php
                                  if($row->status=='active')
                                  {
                                      ?>
                                      <a href="#" data-href="?deactive=<?php echo $row->id; ?>" data-target="#confirm-delete" class="btn btn-success btn-xs"  data-toggle="modal">
                                      <?php echo $row->status; ?></a>
                            
                                  <?php   
                                  }
                                  else
                                  {
                                   ?>
                                       <a href="#" data-href="?active=<?php echo $row->id; ?>" data-target="#confirm-active" class="btn btn-danger btn-xs"  data-toggle="modal">
                                       <?php echo $row->status; ?></a>
                                    <?php   
                                  }?>
                                  </td>
                                  <!--<td><a href="?editId=<?php echo $row->id; ?>" class="btn btn-xs btn-info"><span>Edit</span></a>
                                   </td>-->
                              </tr>    
                              
                              
                        <?php
                        }
                        ?>
                       
                          </tbody>
                        </table>
                          </div>
                                
        <div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
        <div class="modal-dialog">
            <div class="modal-content">
            
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title" id="myModalLabel">Confirm Deactive</h4>
                </div>
            
                <div class="modal-body">
                    <p>Are You Sure You want to Deactive this user?</p>
                </div>
                
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <a class="btn btn-danger btn-ok">Deactive</a>
                </div>
            </div>
        </div>
    </div>
                         
    <div class="modal fade" id="confirm-active" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
        <div class="modal-dialog">
            <div class="modal-content">
            
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title" id="myModalLabel">Confirm active</h4>
                </div>
            
                <div class="modal-body">
                    <p>Are You Sure You want to active this user?</p>
                </div>
                
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <a class="btn btn-success btn-ok">Active</a>
                </div>
            </div>
        </div>
    </div>
                     </div>
			
		  </div>
		</div>
	 </div>
   </div>

    <!-- footer content -->
    <footer>
      <div class="">
        <p class="pull-right">
           <span><?php echo $adminTitle["copyright"]; ?></span> 
        </p>
      </div>
      <div class="clearfix"></div>
    </footer>
    <!-- /footer content -->
  </div>
  <!-- /page content -->
 </div>
</div>

    <div id="custom_notifications" class="custom-notifications dsp_none">
        <ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group">
        </ul>
        <div class="clearfix"></div>
        <div id="notif-group" class="tabbed_notifications"></div>
    </div>

    <script src="js/bootstrap.min.js"></script>
    <!-- chart js -->
    <script src="js/chartjs/chart.min.js"></script>
    <!-- bootstrap progress js -->
    <script src="js/progressbar/bootstrap-progressbar.min.js"></script>
    <script src="js/nicescroll/jquery.nicescroll.min.js"></script>
    <!-- icheck -->
    <script src="js/icheck/icheck.min.js"></script>
    <script src="js/custom.js"></script>
    
     <!-- Datatables -->
  <script src="js/jquery.dataTables.min.js"></script>

	
<script>
    $(document).ready(function(){
    $('#datatable').DataTable();
});
	  
	</script>
<script>
        $('#confirm-delete').on('show.bs.modal', function(e) {
            $(this).find('.btn-ok').attr('href', $(e.relatedTarget).data('href'));
            
        });
         $('#confirm-active').on('show.bs.modal', function(e) {
            $(this).find('.btn-ok').attr('href', $(e.relatedTarget).data('href'));
            
        });
</script>


</body>

</html>