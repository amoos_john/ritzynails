  <?php 
include_once('../config/connection.php');
  if(!isset($_SESSION["admin"])){
        
	 echo "<script type='text/javascript'>window.location='login/';</script>";
	exit();
}
  include_once('../config/model.php');
  $pageTitle = "Add Color Selector"; 
  include('header.php'); 
  

  
 if(isset($_GET["editId"]))
  {
	$id = charEsc($_GET["editId"]);
        if($id!=null)
        {
            $result = query("select * from color_selector where id={$id}");
            if(num_rows($result) > 0)
            {
                    $row = fetch_array($result);
                    $get = "editId";
                    $col_name = $row['color_name'];
                    $brand_id = $row['brand_id'];
                    $collection =  $row["collection"];
                    $image = $row['image'];

            }
        }
        else {
            
            header("Location: colourselector.php");

        }
  }
  if(isset($_POST["submit"])) 
  {
        $color_name = charEsc($_POST["color_name"]);
        $brand_id = charEsc($_POST["brand_id"]);
        $collection = charEsc($_POST["collection"]);
	$image = $_FILES["image"]["name"];
      
	if($color_name == "")
	{
            $_SESSION["error"] = "Colour Name is  required.";	
	}
        else if($brand_id == "")
	{
            $_SESSION["error"] = "Brand Name is  required.";	
	}
	else if($image == "")
	{
            $_SESSION["error"] = "Image is  required";	
	}
        else
        {
            $imageFileType = pathinfo($image,PATHINFO_EXTENSION);
            
            if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg" && $imageFileType != "gif" ) 
            {
		$_SESSION["error"] = "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";	
            } 
            else
            {
                $cDate = date('Y-m-d H:i:s');
                
                $colorid= str_replace(" ", "-", strtolower($color_name));
                
                $newimage = uploadColImage($_FILES["image"],"../images/colors/",$colorid);
                
                $color_sort = query("SELECT sort_id FROM `color_selector` order by sort_id DESC limit 1");
                $row = fetch_array($color_sort);

                $sort_id = $row["sort_id"]+1;
               
                $ins=array("color_name" => $color_name,"brand_id" => $brand_id,
               "image" => 'images/colors/'.$newimage," collection" => $collection,
                "color_id" => $colorid,"sort_id" => $sort_id,"created_date" => $cDate);
                $query = insert("color_selector",$ins);
                if(!$query)
		{
                    $_SESSION["error"] = "Color Failed to Added.";	
		}
		else
		{
                    $_SESSION["success"] = "Color Added Successfully.";
                    echo "<script type='text/javascript'>window.location ='colourselector.php' </script>";
		}
                
                
            }
        }
}

  if(isset($_POST["update"])) 
  {
        $color_name = charEsc($_POST["color_name"]);
        $brand_id = charEsc($_POST["brand_id"]);
        $collection = charEsc($_POST["collection"]);
	$image = $_FILES["image"]["name"];
        $id =  charEsc($_POST["editId"]);
       
      
	if($color_name == "")
	{
            $_SESSION["error"] = "Color Name is  required.";	
	}
        else if($brand_id == "")
	{
            $_SESSION["error"] = "Brand Name is  required.";	
	}
	
	
	else
        {
           $mDate = date('Y-m-d H:i:s');
           $colorid= str_replace(" ", "-", strtolower($color_name));

           if($image != "")
            {
                $imageFileType = pathinfo($image,PATHINFO_EXTENSION);
            
                if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg" && $imageFileType != "gif" ) 
                {
                    $_SESSION["error"] = "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";	
                } 
                else
                {
                    
                    $row = select("select image from `color_selector` where id={$id}"); 
                    $dbImage = $row[0]["image"];
                    
                    $deleteFile1 = "../".$dbImage;
                    
                    if (file_exists($deleteFile1)) { unlink($deleteFile1); }
                    
                    $newimage = uploadColImage($_FILES["image"],"../images/colors/",$colorid);
                    
                    $imagename='images/colors/'.$newimage;
                }
            }
            else
            {
                $imagename=$_POST['getImage'];
            }
             
                
            if(!isset($_SESSION["error"]))
            {  
                
                $query=query("UPDATE `color_selector` SET `color_name`='{$color_name}',`brand_id`='{$brand_id}',
               `image`='{$imagename}',`collection`='{$collection}',`color_id`='{$colorid}',`modified_date`='{$mDate}' WHERE id={$id}");        
                         
                if(!$query)
		{
                    $_SESSION["error"] = "Color Failed to Update.";	
                    echo "<script>window.location ='addcolourselector.php?editId='+'$id' </script>";

		}
		else
		{
                    $_SESSION["success"] = "Color Updated Successfully.";
                     echo "<script type='text/javascript'>window.location ='colourselector.php' </script>";

		}
            }
            
           
        }
  }
   
 $collections=array("winter", "spring", "summer", "fall"); 
  
 $brands=query("Select * from brands order by brand_name ASC");
 
?>
<style type="text/css">

#imagePreview {
    background-image:url(<?php echo ($image)?'../'.$image:''; ?>);
    width: 415px;
    height: 200px;
    background-size: cover;
    background-repeat: no-repeat;
    margin: 0px;
    border: 1px solid #DDE2E8;
    display: table;
}


</style>
<script>

var loadImageFile = (function () {
    if (window.FileReader) {
        var    oPreviewImg = null, oFReader = new window.FileReader(),
            rFilter = /^(?:image\/bmp|image\/cis\-cod|image\/gif|image\/ief|image\/jpeg|image\/jpeg|image\/jpeg|image\/pipeg|image\/png|image\/svg\+xml|image\/tiff|image\/x\-cmu\-raster|image\/x\-cmx|image\/x\-icon|image\/x\-portable\-anymap|image\/x\-portable\-bitmap|image\/x\-portable\-graymap|image\/x\-portable\-pixmap|image\/x\-rgb|image\/x\-xbitmap|image\/x\-xpixmap|image\/x\-xwindowdump)$/i;

        oFReader.onload = function (oFREvent) {
            if (!oPreviewImg) {
                var newPreview = document.getElementById("imagePreview");
                oPreviewImg = new Image();
                oPreviewImg.style.width = (newPreview.offsetWidth).toString() + "px";
                oPreviewImg.style.height = (newPreview.offsetHeight).toString() + "px";
                newPreview.appendChild(oPreviewImg);
            }
            oPreviewImg.src = oFREvent.target.result;
        };

        return function () {
            var aFiles = document.getElementById("image").files;
            if (aFiles.length === 0) { return; }
            if (!rFilter.test(aFiles[0].type)) { alert("You must select a valid image file !"); return; }
            oFReader.readAsDataURL(aFiles[0]);
        }

    }

})();
</script>

<style>
textarea {
	width: 100% !important;
}
</style>
	<!-- page content -->
		<div class="right_col" role="main">
			<div class="">
 				<div class="page-title">
					<div class="title_left">
						<h3>Add Color Selector </h3>
					</div>
  
      
				</div> <!--Page Title Div end here -->
			</div> 
			<div class="clearfix"></div>

			<div class="row">
				<div class="col-md-12 col-sm-12 col-xs-12">
					<div class="x_panel">
						
				<div class="x_content">
				<br />
                                <form method="post" action="addcolourselector.php" id="add-services" data-parsley-validate class="form-horizontal form-label-left" enctype="multipart/form-data" >
				
				<?php if(isset($_SESSION["error"])){ ?> 
				<div class="x_content bs-example-popovers">
					<div class="alert alert-danger alert-dismissible fade in" role="alert">
						<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
							<?php echo "<strong>Error ! </strong>"." ".$_SESSION["error"]; unset($_SESSION["error"]); ?>
					</div>
 				</div>
							<?php } ?>
									
				<?php if(isset($_SESSION["success"])){ ?> 
					<div class="x_content bs-example-popovers">
						<div class="alert alert-success alert-dismissible fade in" role="alert">
							<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
							<?php echo "<strong>Congrats ! </strong>"." ".$_SESSION["success"]; unset($_SESSION["success"]); ?>
						</div>
					</div>
				<?php } ?>
								
		<div class="form-group">
		  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="color_name">Name <span class="required">*</span></label>
			<div class="col-md-6 col-sm-6 col-xs-12">
		   	 <input type="text" id="color_name" value="<?php echo $col_name; ?>" required name="color_name" class="form-control col-md-7 col-xs-12">
		 	 <input type="hidden" name="<?php echo $get; ?>" value="<?php echo $id; ?>" class="form-control col-md-7 col-xs-12">
			<input type="hidden" name="getImage" value="<?php echo $image?>" class="form-control col-md-7 col-xs-12">   
                        </div>
		</div>
                  
               <div class="form-group">
			<label class="control-label col-md-3 col-sm-3 col-xs-12" for="services">Brand <span class="required">*</span></label>
			  <div class="col-md-6 col-sm-6 col-xs-12">
                              <select class="form-control" name="brand_id" required>
                                  <option value="">Select Brand</option>
                                  <?php
                             
                                  while($row = fetch_object($brands)) 
                                  {  
                                           $selected=($brand_id==$row->id)?'selected':'';
                                  ?>
                                  <option value="<?php echo $row->id; ?>" <?php echo $selected ?>><?php echo $row->brand_name; ?></option>
                                  
                                  <?php
                                  }
                                  ?>
                              </select>
			</div>
		</div>
                                    
                
		
                <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="price">Collection </label>
                                  <div class="col-md-6 col-sm-6 col-xs-12">
                                   <select class="form-control" name="collection" >
                                  <option value="">Select Collection</option>
                                  <?php
                             
                                  foreach($collections as $col) 
                                  {  
                                           $selected=($collection==$col)?'selected':'';
                                  ?>
                                  <option value="<?php echo $col; ?>" <?php echo $selected ?>><?php echo $col; ?></option>
                                  
                                  <?php
                                  }
                                  ?>
                              </select>
                                       
                                </div>
                </div>
                
               
                 			   
		<div class="form-group">
			<label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Image<span class="required">*</span></label>
				<div class="col-md-6 col-sm-6 col-xs-12">
					<input type="file" onchange="loadImageFile();" name="image" id="image" class="form-control col-md-7 col-xs-12" >
				</div>
		</div>
               <div class="form-group">
                   <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">&nbsp;</label>

                        <div id="imagePreview" class="form-control col-md-7 col-xs-12" ></div>
                       
                </div>   
              
		<div class="ln_solid"></div>
			<div class="form-group">
				<div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                    <?php if(isset($_GET['editId'])) { ?> 
                                    	  <button type="submit" name="update" class="btn btn-success">Update</button>
			
                                    <?php
                                    }  
                                    else
                                    {  ?>
                                    	<button type="submit" name="submit" class="btn btn-success">Save</button>

                                  <?php 
                                    }
                                    ?>
                                        <a href="colourselector.php" class="btn btn-danger">Cancel</a>
				</div>
			</div>
  		</form>
		</div>
	   </div>
      </div><!-- class x_panel end here-->
	 </div>
    </div><!--Row end Here-->
	
    <!-- footer content -->
	<footer>
		<div class="">
			<p class="pull-right">
				<span><?php echo $adminTitle["copyright"]; ?></span> 
			</p>
		</div>
		<div class="clearfix"></div>
	</footer>
	<!-- /footer content -->
  
	</div>
	<!-- /page content -->
	<!-- </div> </div> extra divs--> 
	<div id="custom_notifications" class="custom-notifications dsp_none">
		<ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group"></ul>
		<div class="clearfix"></div>
		<div id="notif-group" class="tabbed_notifications"></div>
	</div>
  
	<script src="js/bootstrap.min.js"></script> 

        <!-- chart js --> 
        <script src="js/chartjs/chart.min.js"></script> 
        <!-- bootstrap progress js --> 
        <script src="js/progressbar/bootstrap-progressbar.min.js"></script> 
        <script src="js/nicescroll/jquery.nicescroll.min.js"></script> 
        <!-- icheck --> 
        <script src="js/icheck/icheck.min.js"></script> 
        <script src="js/custom.js"></script> 
      
 </body>
</html>