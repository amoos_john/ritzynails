<?php
include_once('../config/connection.php');

if (!isset($_SESSION["admin"])) {

    echo "<script type='text/javascript'>window.location='login/';</script>";
    exit();
}
include_once('../config/constants.php');
include_once('../config/model.php');
include_once('../config/functions.php');

if (isset($_GET["delete"])) {
    $id = charEsc($_GET["delete"]);
    
    query("delete from menus where id = '$id'");
    
    $_SESSION["success"] = "Menu Deleted successfully.";
    redirectUrl();
    
    /*$delrow = select("select image from contents where id = '$id'");

    if (count($delrow) > 0) {
        unlink("../" . $delrow[0]["image"]);
        query("delete from contents where id = '$id'");
        $_SESSION["success"] = "Content Deleted successfully.";
        echo '<script>window.location = "' . $_SERVER["PHP_SELF"] . '";</script>';
    } else {
        query("delete from contents where id = '$id'");
        $_SESSION["success"] = "Content Deleted successfully.";
        echo '<script>window.location = "' . $_SERVER["PHP_SELF"] . '";</script>';
    }*/
}
if (isset($_POST["update"])) {
    $id = charEsc($_POST["id"]);
    $menu_name = charEsc($_POST["menu_name"]);
    $url = charEsc($_POST["url"]);
    
    $query=query("UPDATE `menus` SET `menu_name`='{$menu_name}',`url`='{$url}' WHERE `id`='{$id}'");

    
    $_SESSION["success"] = "Menu updated successfully.";
    
    //redirectUrl();
    
}

$pageTitle = "Color Swatch";
include('header.php');
?>
<style type="text/css">
 
.ui-sortable {
    margin-top: 0;
    margin-bottom: 10px;
    margin-left: 0 !important;
    padding: 0 !important;
}    
#contentWrap {
	width: 800px;
	margin: 0 auto;
	height: auto;
	overflow: hidden;
}


#contentLeft {
	display: block;
        position: relative;
        width: 100%;
        overflow: hidden;
        height: auto;   
}

#contentLeft li {
	list-style: none;
        margin: 0;
        padding: 10px;
        color: #000;
        font-weight: bold;
        font-size: 12px;
        display: inline-block;
        text-align: center;
        width: 140px;
        cursor: -webkit-grab;
}

    </style>
<!-- page content -->
<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h3> <?php echo $pageTitle; ?></h3>
            </div>

        </div>

        <div class="clearfix"></div>

        <div class="row">
            <div class="clearfix"></div>
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">

                    <div class="x_content">
                    <?php if (isset($_SESSION["error"])) { ?> 
                                                <div class="x_content bs-example-popovers">
                                                    <div class="alert alert-danger alert-dismissible fade in" role="alert">
                                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                        <?php echo "<strong>Error ! </strong>" . " " . $_SESSION["error"];
                        unset($_SESSION["error"]); ?>
                                                    </div>
                                                </div>
                    <?php } ?>

                           <?php if (isset($_SESSION["success"])) { ?> 
                            <div class="x_content bs-example-popovers">
                                <div class="alert alert-success alert-dismissible fade in" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                            <?php echo "<strong>Congrats ! </strong>" . " " . $_SESSION["success"];
                            unset($_SESSION["success"]); ?>
                                </div>
                            </div>
                                <?php } ?>
                        <div class="table-responsive"> 
                            <div id="">

                        <p>Drag each color into the order you prefer</p>

                    <div id="contentLeft">
                    <ul>
                        <?php
                        $result = query("SELECT * FROM `color_selector` order by sort_id ASC");
                        while ($row = fetch_object($result)) 
                        {

                        ?>
                            <li id="recordsArray_<?php echo $row->id; ?>">
                                <img src="../<?php echo $row->image; ?>" style="border-radius: 100%;" width="90px" height="90px" />
                                <p><?php echo $row->color_name; ?></p>

                            </li>
                        
                            
                        <?php 
                         }
                      
                        ?>
                    </ul>
                    </div>



                            </div>
                        </div>

                        <div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                            <div class="modal-dialog">
                                <div class="modal-content">

                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                        <h4 class="modal-title" id="myModalLabel">Confirm Delete</h4>
                                    </div>

                                    <div class="modal-body">
                                        <p>Are You Sure You want to Delete?</p>
                                    </div>

                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                        <a class="btn btn-danger btn-ok">Delete</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>

    <!-- footer content -->
    <footer>
        <div class="">
            <p class="pull-right">
                <span><?php echo $adminTitle["copyright"]; ?></span> 
            </p>
        </div>
        <div class="clearfix"></div>
    </footer>
    <!-- /footer content -->
</div>
<!-- /page content -->
</div>
</div>



<script src="js/bootstrap.min.js"></script>

<!-- chart js -->
<script src="js/chartjs/chart.min.js"></script>
<!-- bootstrap progress js -->
<script src="js/progressbar/bootstrap-progressbar.min.js"></script>
<script src="js/nicescroll/jquery.nicescroll.min.js"></script>
<!-- icheck -->
<script src="js/icheck/icheck.min.js"></script>
<script src="js/custom.js"></script>


<script type="text/javascript" src="js/jquery-1.3.2.min.js"></script>
<script type="text/javascript" src="js/jquery-ui-1.7.1.custom.min.js"></script>

<script>
   
$(document).ready(function(){ 
						   
	$(function() {
		$("#contentLeft ul").sortable({ opacity: 0.6, cursor: 'move', update: function() {
			var order = $(this).sortable("serialize") + '&action=updateRecordsListings'; 
			$.post("colour_location.php", order, function(theResponse){
				$("#contentRight").html(theResponse);
			}); 															 
		}								  
		});
	});
        

});	

function deleteId(value) {
        var r = confirm("Are You Sure You want to Delete?");
        if (r == true) {
            window.location = '?delete=' + value;
        } else
        {
        }
    }
   </script>  


</body>

</html>