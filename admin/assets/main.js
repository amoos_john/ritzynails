function check(val)
{

        // Adding the form-invalid-data class will show
        // the error message and the red x for that field
        var errorField = $('.form-input-name-row');
		
		if(val.length < 3)
		{
		    errorField.removeClass('form-valid-data');
			errorField.addClass('form-invalid-data');
			
		}
		if(val.length >= 3)
		{
		    errorField.removeClass('form-invalid-data');
			errorField.addClass('form-valid-data');
		}
	};
	
	
	
	
function university(uni)
{

        // Adding the form-invalid-data class will show
        // the error message and the red x for that field
        var errorUni = $('.form-input-uni-row');
		
		if(uni.length < 2)
		{
		    errorUni.removeClass('form-valid-data');
			errorUni.addClass('form-invalid-data');
			
		}
		if(uni.length >= 2)
		{
		    errorUni.removeClass('form-invalid-data');
			errorUni.addClass('form-valid-data');
		}
	};
	
	
	
	
	
function degreeCheck(deg)
{

        // Adding the form-invalid-data class will show
        // the error message and the red x for that field
        var errorDeg = $('.form-input-degree-row');
		
		if(deg.length < 2)
		{
		    errorDeg.removeClass('form-valid-data');
			errorDeg.addClass('form-invalid-data');
			
		}
		if(deg.length >= 2)
		{
		    errorDeg.removeClass('form-invalid-data');
			errorDeg.addClass('form-valid-data');
		}
	};
	
	
	
	
	
function validateEmail(email) {

var errorEmail = $('.form-input-email-row');

  var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
  if(emailReg.test(email)) { 
  
  errorEmail.removeClass('form-invalid-data');
  errorEmail.addClass('form-valid-data');
  
  }
  
  if(!emailReg.test(email)) { 
  
  errorEmail.removeClass('form-valid-data');
  errorEmail.addClass('form-invalid-data');
  
  }
};



function phoneCheck(phone)
{

        // Adding the form-invalid-data class will show
        // the error message and the red x for that field
        var errorPhone = $('.form-input-phone-row');
		
		
		if(phone.length < 9 || !$.isNumeric(phone))
		{
		    errorPhone.removeClass('form-valid-data');
			errorPhone.addClass('form-invalid-data');
			
		}
		if(phone.length >= 9 && $.isNumeric(phone))
		{
		    errorPhone.removeClass('form-invalid-data');
			errorPhone.addClass('form-valid-data');
		}
	};
	
	

	
	

function messageCheck(message)
{

        // Adding the form-invalid-data class will show
        // the error message and the red x for that field
        var errorMessage = $('.form-input-message-row');
		
		
		if(message.length < 19)
		{
		    errorMessage.removeClass('form-valid-data');
			errorMessage.addClass('form-invalid-data');
			
		}
		if(message.length >= 19)
		{
		    errorMessage.removeClass('form-invalid-data');
			errorMessage.addClass('form-valid-data');
		}
	};





function imageCheck(image)
{	
	 var errorImage = $('.form-input-image-row');
	var ext = $('#img').val().split('.').pop().toLowerCase();
	//var ext = image.val().split('.').pop().toLowerCase();
if($.inArray(ext, ['gif','png','jpg','jpeg']) == -1) {
             errorImage.removeClass('form-valid-data');
		     errorImage.addClass('form-invalid-data');
}
else{

			errorImage.removeClass('form-invalid-data');
			errorImage.addClass('form-valid-data');
}


}


 $(document).on("submit", "#myform", function (e) {
        //var result = confirm("Are you sure you want to log this fault?");
        //if cancel is cliked
		
		var submitError = $('.form-input-image-row');
		submitError.find('.form-invalid-data-info').show();		 
       
	   if (!$("#nameRow").hasClass("form-valid-data")) {
            submitError.find('.form-invalid-data-info').text('Please enter your Name');
			  
			return false;   
              		 
        }
		
		if (!$("#uniRow").hasClass("form-valid-data")) {
		 submitError.find('.form-invalid-data-info').text('Please enter your University/Institution');
             return false;    
			 
        }
		
		if (!$("#degreeRow").hasClass("form-valid-data")) {
		submitError.find('.form-invalid-data-info').text('Please enter your Degree/Diploma');
             return false;      
			  
        }
		
		if (!$("#emailRow").hasClass("form-valid-data")) {
		submitError.find('.form-invalid-data-info').text('Please enter a valid Email');
             return false;      
		
        }
		
		if (!$("#phoneRow").hasClass("form-valid-data")) {
		submitError.find('.form-invalid-data-info').text('Please enter a valid Phone Number');
             return false;      
			  	
        }
		
		if (!$("#messageRow").hasClass("form-valid-data")) {
		submitError.find('.form-invalid-data-info').text('Message must have more then 20 characters.');
             return false;      
			  
        }
		
		if (!$("#imageRow").hasClass("form-valid-data")) {
		submitError.find('.form-invalid-data-info').text('Please upload a Image File');
             return false;      
			 
        }
		
		
        //if ok is cliked, form will be submitted
    });
