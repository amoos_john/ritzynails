<!DOCTYPE html>
<html lang="en">

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <!-- Meta, title, CSS, favicons, etc. -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title><?php echo $pageTitle; ?></title>

        <!-- Bootstrap core CSS -->
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link href="css/font-awesome.min.css" rel="stylesheet">
        <link href="css/animate.min.css" rel="stylesheet">

        <!-- Custom styling plus plugins -->
        <link href="css/custom.css" rel="stylesheet">
        <link href="css/icheck/flat/green.css" rel="stylesheet">

        <script src="js/jquery.min.js"></script>
        <!-- Datatable -->
        <link href="css/jquery.dataTables.min.css" rel="stylesheet">

        <!--[if lt IE 9]>
            <script src="../assets/js/ie8-responsive-file-warning.js"></script>
            <![endif]-->
        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
              <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
              <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
            <![endif]-->
        <style>
            .form-horizontal .control-label {
    text-align: left;
    width:15%;
}
.table-responsive {
    overflow-x: inherit;
    min-height: 0.01%;
}
        </style>
    </head>

    <body class="nav-md">
        <div class="container body">
            <div class="main_container">
                <div class="col-md-3 left_col">
                    <div class="left_col scroll-view">
                        <div class="navbar nav_title" style="border: 0;">
                            <a href="dashboard.php" class="site_title"><span>Ritzy Nails</span></a>
                        </div>
                        <div class="clearfix"></div>
                        <!-- menu prile quick info -->
                        <div class="profile">
                            <div class="profile_pic">
                                <img src="medium/<?php echo $_SESSION["image"]; ?>" alt="..." class="img-circle profile_img">
                            </div>
                            <div class="profile_info">
                                <span>Welcome,</span>
                                <h2><?php echo $_SESSION["name"]; ?></h2>
                            </div>
                        </div>
                        <!-- /menu prile quick info -->
                        <br />
                        <!-- sidebar menu -->
                        <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
                            <div class="menu_section">
                                <h3>Main Navigation</h3>
                                <ul class="nav side-menu">
                                    <li><a href="dashboard.php"><i class="fa fa-dashboard"></i> Dashboard</a> </li>
                                      
                                    <li><a><i class="fa  fa-file"></i> Content Block <span class="fa fa-chevron-down"></span></a>
                                        <ul class="nav child_menu" style="display: none">
                                            <li><a href="contents.php">View Content Block</a></li>
                                            <li><a href="addcontents.php">Add Content Block</a></li>
                                        </ul>
                                    </li>
                                    <li><a href="menus.php"><i class="fa fa-briefcase"></i> Menus</a> </li>
                                    <li><a><i class="fa  fa-tag"></i> Categories <span class="fa fa-chevron-down"></span></a>
                                        <ul class="nav child_menu" style="display: none">
                                            <li><a href="categories.php">View Categories</a></li>
                                            <li><a href="addcategories.php">Add Category</a></li>
                                        </ul>
                                    </li>
                                    <li><a><i class="fa  fa-tags"></i> Services <span class="fa fa-chevron-down"></span></a>
                                        <ul class="nav child_menu" style="display: none">
                                            <li><a href="services.php">View Services</a></li>
                                            <li><a href="addservices.php">Add Service</a></li>
                                        </ul>
                                    </li>
                                    <li><a><i class="fa fa-shopping-bag"></i> Products <span class="fa fa-chevron-down"></span></a>
                                        <ul class="nav child_menu" style="display: none">
                                            <li><a href="products.php">View Products</a></li>
                                            <li><a href="addproducts.php">Add Product</a></li>
                                        </ul>
                                    </li>
                                    <li><a><i class="fa  fa-pencil-square"></i> Color Selector <span class="fa fa-chevron-down"></span></a>
                                        <ul class="nav child_menu" style="display: none">
                                            <li><a href="colourselector.php">View Color Selector</a></li>
                                            <li><a href="addcolourselector.php">Add Color Selector</a></li>
                                            <li><a href="colourswatch.php">Color Swatch</a></li>
                                        </ul>
                                    </li>
                                    <li><a href="displacement.php"><i class="fa  fa-calendar"></i> Displacement Time </a></li>
                                 
                                    
                                    <li><a href="users.php"><i class="fa  fa-users"></i> Users</a> </li>
                                   
                                    <li><a href="orders.php"><i class="fa fa-shopping-cart"></i> Orders</a> </li> 
                                    
                                </ul>
                            </div>
                        

                        </div>
                     
                    </div>
                </div>

                <!-- top navigation -->
                <div class="top_nav">

                    <div class="nav_menu">
                        <nav class="" role="navigation">
                            <div class="nav toggle">
                                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                            </div>

                            <ul class="nav navbar-nav navbar-right">
                                <li class="">
                                    <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                        <img src="small/<?php echo $_SESSION["image"]; ?>" alt=""><?php echo $_SESSION["name"]; ?>
                                        <span class=" fa fa-angle-down"></span>
                                    </a>
                                    <ul class="dropdown-menu dropdown-usermenu animated fadeInDown pull-right">
                                        <!--<li><a href="profile.php"> Profile</a></li>-->
                                        <li>
                                            <a href="changepassword.php">
                                                <span>Change Password</span>
                                            </a>
                                        </li>

                                        <li><a href="logout.php"><i class="fa fa-sign-out pull-right"></i> Log Out</a></li>
                                    </ul>
                                </li>

                            </ul>
                        </nav>
                    </div>

                </div>
                <!-- /top navigation -->