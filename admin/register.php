<?php
include_once('config/connection.php');
include_once('config/constants.php');
include_once('config/model.php');
include_once('config/functions.php');
include_once('commons/head.php');
include_once('commons/header.php');
?>
<section id="register" class="mypage" >
            

    <div class="hussainbtns">
        <div class="dwn-area top-area col-sm-12">
            <a href="index.php#information"><img src="images/up-arr.png" alt="" /><h4>Information</h4></a>
        </div>
    </div>
    <?php
    if (!isset($_SESSION['user_id'])) {
        ?>

        <div class="container">
                <h1>Login</h1>
            <div class="row">
                <div class="col-md-6 col-md-offset-3">
                    <div class="panel panel-login">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-6">
                                    <a href="#" class="active" id="login-form-link">Login</a>
                                </div>
                                <div class="col-xs-6">
                                    <a href="#" id="register-form-link">Register</a>
                                </div>
                            </div>
                            <hr>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-12">
								
									<?php
										$display=1;
										include_once("commons/login.php");
									?>
									<?php
										unset($display);
										include_once("commons/register.php");
									?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php
    } else {
        include_once('register_tickets.php');
    }
    ?>
</section>
<script>

    $(function () {

        $('#login-form-link').click(function (e) {
            $("#login-form").delay(100).fadeIn(100);
            $("#register-form").fadeOut(100);
            $('#register-form-link').removeClass('active');
            $(this).addClass('active');
            e.preventDefault();
        });
        $('#register-form-link').click(function (e) {
            $("#register-form").delay(100).fadeIn(100);
            $("#login-form").fadeOut(100);
            $('#login-form-link').removeClass('active');
            $(this).addClass('active');
            e.preventDefault();
        });

    });

</script>
<?php // include_once('commons/footer.php'); ?>	
