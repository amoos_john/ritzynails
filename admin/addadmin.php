<?php 
$pageTitle = "Add Admin"; 
include('header.php'); 

include_once('../config/connection.php');
include_once('../config/model.php');
include( 'imageresize.php');

$sizes = array(100 => 100, 150 => 150, 250 => 250);

if(isset($_GET["editId"]))
{
	$id = $_GET["editId"];
	$result = query("select * from admin where id={$id}");
	if(num_rows($result) > 0)
	{
		$row = fetch_array($result);
		$get = "editId";
		$fName = $row['fName'];
		$lName = $row['lName'];
		$email = $row['email'];
		$userName = $row['userName'];
		$password = $row['password'];
		$roleId = $row['roleId'];
	}
}
else if(isset($_POST["editId"]))
{
	$get = "editId";
	$id = $_POST["editId"];
	$fName = $_POST["fname"];
	$lName = $_POST["lname"];
	$email = $_POST["email"];
	$userName = $_POST["userName"];
	$password = $_POST["pass"];
	$roleId = $_POST["role"];
	$image = $_FILES["image"]["name"];
	
	if($fName == "")
	{
	$_SESSION["error"] = "First Name is  required.";	
	}
	
	else if($lName == "")
	{
	$_SESSION["error"] = "Last Name is  required.";	
	}
	
	else if($email == "")
	{
	$_SESSION["error"] = "Email is  required.";	
	}
	
	else if($userName == "")
	{
	$_SESSION["error"] = "User Name is  required.";	
	}
	
	else if($password == "")
	{
	 $_SESSION["error"] = "Password is  required";	
	}
	
	else{

		$checkEmail = query("select * from `admin` where `email`='{$email}' and id!={$id}");
		       if(num_rows($checkEmail) > 0){
				     $_SESSION["error"] = "Email Already Exists.";	
			       }
				
		$checkUserName = query("select * from `admin` where `userName`='{$userName}' and id!={$id}");
		       if(num_rows($checkUserName) > 0){
				     $_SESSION["error"] = "User Name Already Exists.";	
			       }
		         else{
					 $cDate = date('Y-m-d-H-i-s');
					 
					 if($image != "")
					 {
						 $imageFileType = pathinfo($image,PATHINFO_EXTENSION);
						
						if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
						&& $imageFileType != "gif" ) {
							$_SESSION["error"] = "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";	
						} 
						else
						{
							 $getImage = query("select image from `admin` where id={$id}"); 
							 $row = fetch_array($getImage);
							 $dbImage = $row["image"];
							//	$img ='image_' . date('Y-m-d-H-i-s') . '_' . uniqid() . '.jpg';
							$deleteFile = "uploads/".$dbImage;
							$deleteFile1 = "medium/".$dbImage;
							$deleteFile2 = "small/".$dbImage;
							 if (file_exists($deleteFile)) { unlink($deleteFile); }
							 if (file_exists($deleteFile1)) { unlink($deleteFile1); }
							 if (file_exists($deleteFile2)) { unlink($deleteFile2); }
							 
							//move_uploaded_file($_FILES['image']['tmp_name'], 'uploads/'.$dbImage);
	                          
							 $i  = 1;
							 foreach ($sizes as $w => $h) {
								   
								   if($i == 1){ $folder = "small/"; }
								   if($i == 2){ $folder = "medium/"; }
								   if($i == 3){ $folder = "uploads/"; }
								   
								   $files[] = resize($w, $h,$folder,$dbImage);
									$i++;
							  }
						}
					  }
		             
					if(!isset($_SESSION["error"]))
					  {
						$query = query("update `admin` set `fName`='{$fName}', `lName`='{$lName}', `email`='{$email}',
						`userName`='{$userName}', `password`='{$password}', `modifiedDate`='{$cDate}', `roleId`={$roleId} where id={$id}"); 
							  if(!$query)
							  {
								   $_SESSION["error"] = "Updation Failed.";	
							  }
							  else
							  {
								  $_SESSION["success"] = "Admin Updated Successfully.";	
							  }
						}
				 }
        }
}
else if(isset($_POST["addId"])){
	
	$id = "";
	$get = "addId";
	$fName = $_POST["fname"];
	$lName = $_POST["lname"];
	$email = $_POST["email"];
	$userName = $_POST["userName"];
	$password = $_POST["pass"];
	$roleId = $_POST["role"];
	$roleId = $_POST["role"];
	$target_dir = "uploads/";
	$image = $_FILES["image"]["name"];

	if($fName == "")
	{
	$_SESSION["error"] = "First Name is  required.";	
	}
	
	else if($lName == "")
	{
	$_SESSION["error"] = "Last Name is  required.";	
	}
	
	else if($email == "")
	{
	$_SESSION["error"] = "Email is  required.";	
	}
	
	else if($userName == "")
	{
	$_SESSION["error"] = "User Name is  required.";	
	}
	
	else if($password == "")
	{
	 $_SESSION["error"] = "Password is  required";	
	}
	
	else if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
     $_SESSION["error"] = "Invalid email format";
   }
	
	else if($image == "")
	{
	 $_SESSION["error"] = "Image is  required";	
	}
	
	else{
		
		$checkEmail = query("select * from `admin` where `email`='{$email}'");
		       if(num_rows($checkEmail) > 0){
				     $_SESSION["error"] = "Email Already Exists.";	
			       }
				else{
					$checkUserName = query("select * from `admin` where `userName`='{$userName}'");
				       if(num_rows($checkUserName) > 0){
					     $_SESSION["error"] = "User Name Already Exists.";	
				       }
					else{
						$imageFileType = pathinfo($image,PATHINFO_EXTENSION);
						
						if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
						&& $imageFileType != "gif" ) {
							$_SESSION["error"] = "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";	
						} 
						
						else{
						//	$img ='image_' . date('Y-m-d-H-i-s') . '_' . uniqid() . '.jpg';
						$cDate = date('Y-m-d-H-i-s');
						$img ='image_' . date('Y-m-d-H-i-s') . '_' . uniqid() . '.jpg';
						// move_uploaded_file($_FILES['image']['tmp_name'], 'uploads/'.$img);
						$i  = 1;
						 foreach ($sizes as $w => $h) {
							   if($i == 1){ $folder = "small/"; }
							   if($i == 2){ $folder = "medium/"; }
							   if($i == 3){ $folder = "uploads/"; }
							   $files[] = resize($w, $h,$folder,$img);
								$i++;
							  }
						
							 $query = query("insert into `admin`(`fName`,`lName`,`email`,`userName`,`password`,`image`,`status`,`createdDate`,`deleted`,`roleId`) 
				 values('{$fName}','{$lName}','{$email}','{$userName}','{$password}','{$img}',1,'{$cDate}',0,{$roleId})");
							  
							  if(!$query)
							  {
								 // trigger_error('Invalid query: ' . mysql_error());
								   $_SESSION["error"] = "Registeration Failed.";	
							  }
							  else
							  {
								  $_SESSION["success"] = "Admin Registered Successfully.";	
							  }
		  
				          }
						}
					}
        }
}

else{
		$id = "";
		$get = "addId";
		$fName = "";
		$lName = "";
		$email = "";
		$userName = "";
		$password = "";
		$roleId = 1;
}



?>
        <!-- page content -->
        <div class="right_col" role="main">
        	<div class="">
            	<div class="page-title">
                	<div class="title_left">
                    	<h3> <small> Add Admins </small> </h3>
                    </div>
			    <!--<div class="title_right">
                       <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                         <div class="input-group">
							<form method="post" action="table.php">
                               <input type="text" class="form-control" name="name" placeholder="Search for...">
                                    <span class="input-group-btn">
			                            <button class="btn btn-default" name="search" type="submit">Go!</button>
									</span>
	                        </form>
                          </div>
                       </div>
                     </div>-->
                 </div> 
			</div>
			<div class="clearfix"></div>
		        <div class="row">
               		<div class="col-md-12 col-sm-12 col-xs-12">
                    	<div class="x_panel">
                        	<div class="x_title">
                            	<h2>Form </h2>
                                	<ul class="nav navbar-right panel_toolbox">
                                    	<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                                        <li><a class="close-link"><i class="fa fa-close"></i></a></li>
                                    </ul>
                                    <div class="clearfix"></div>
                                </div>
                         <div class="x_content">
                         <br />
                          <form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left"  method="post" action="addadmin.php" enctype="multipart/form-data">
							<?php if(isset($_SESSION["error"])){ ?> 
								<div class="x_content bs-example-popovers">
									<div class="alert alert-danger alert-dismissible fade in" role="alert">
										<button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        	<span aria-hidden="true">×</span>
										</button>
											<?php echo "<strong>Error ! </strong>"." ".$_SESSION["error"]; unset($_SESSION["error"]); ?>
									</div>
								</div>
							<?php } ?>
                            <?php if(isset($_SESSION["success"])){ ?> 
									<div class="x_content bs-example-popovers">
										<div class="alert alert-success alert-dismissible fade in" role="alert">
                                    		<button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    			<span aria-hidden="true">×</span>
                                    		</button>
                                    		<?php 
												echo "<strong>Congrats ! </strong>"." ".$_SESSION["success"]; 
													unset($_SESSION["success"]); 
											?>
                                		  </div>
										</div>
							<?php } ?>
			<div class="form-group">
               	<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">First Name 
                   	<span class="required">*</span>
                </label>
            	<div class="col-md-6 col-sm-6 col-xs-12">
                   <input type="text" id="first-name" value="<?php echo $fName; ?>" required name="fname" class="form-control col-md-7 col-xs-12">
				   <input type="hidden" name="<?php echo $get; ?>" value="<?php echo $id; ?>" class="form-control col-md-7 col-xs-12">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Last Name <span class="required">*</span></label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <input type="text" id="last-name" value="<?php echo $lName; ?>" required name="lname" class="form-control col-md-7 col-xs-12">
                </div>
            </div>
            <div class="form-group">
                <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Email<span class="required">*</span></label>
                <div class="col-md-6 col-sm-6 col-xs-12">
            	     <input id="middle-name" value="<?php echo $email; ?>" class="form-control col-md-7 col-xs-12" type="email" name="email" required>
                </div>
            </div>
			<div class="form-group">
               <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">User Name<span class="required">*</span></label>
               <div class="col-md-6 col-sm-6 col-xs-12">
                   <input id="middle-name" value="<?php echo $userName; ?>" class="form-control col-md-7 col-xs-12" type="text" name="userName" required>
               </div>
            </div>
                                        
			<div class="form-group">
              <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Password<span class="required">*</span></label>
              <div class="col-md-6 col-sm-6 col-xs-12">
                 <input id="middle-name" value="<?php echo $password; ?>" class="form-control col-md-7 col-xs-12" type="password" name="pass" required>
              </div>
            </div>
			<div class="form-group">
              <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Role</label>
              <div class="col-md-6 col-sm-6 col-xs-12">
                  <p style="margin-top:7px;">
                      &nbsp &nbsp Super Admin:
                      <input type="radio" <?php if($roleId == 1){ echo 'checked=""'; } ?> class="flat" name="role" id="genderM" value="1" required /> &nbsp &nbsp Admin:
                      <input type="radio" <?php if($roleId == 2){ echo 'checked=""'; } ?> class="flat" name="role" id="genderF" value="2" />
                  </p>
              </div>
            </div>
		   <div class="form-group">
                <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Image<span class="required">*</span></label>
                       <div class="col-md-6 col-sm-6 col-xs-12">
                             <input type="file" name="image" id="middle-name" class="form-control col-md-7 col-xs-12" >
                       </div>
           </div>
									   
           <div class="ln_solid"></div>
                <div class="form-group">
                     <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                         <button type="submit" name="submit" class="btn btn-success">Submit</button>
                      </div>
                </div>
        </form>
       </div>
      </div>
     </div>
    </div>
   </div>
   <!-- footer content -->
   <footer>
      <div class="">
         <p class="pull-right">
            <span><?php echo $adminTitle["copyright"]; ?></span> 
         </p>
      </div>
      <div class="clearfix"></div>
   </footer>
   <!-- /footer content -->
  </div>
 <!-- /page content -->
 </div>
</div>

    <div id="custom_notifications" class="custom-notifications dsp_none">
        <ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group">
        </ul>
        <div class="clearfix"></div>
        <div id="notif-group" class="tabbed_notifications"></div>
    </div>

    <script src="js/bootstrap.min.js"></script>
    <!-- chart js -->
    <script src="js/chartjs/chart.min.js"></script>
    <!-- bootstrap progress js -->
    <script src="js/progressbar/bootstrap-progressbar.min.js"></script>
    <script src="js/nicescroll/jquery.nicescroll.min.js"></script>
    <!-- icheck -->
    <script src="js/icheck/icheck.min.js"></script>
    <script src="js/custom.js"></script>
	

</body>
</html>