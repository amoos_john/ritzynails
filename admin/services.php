<?php 
        include_once('../config/connection.php');
        include_once('sessions.php');
        include_once('../config/model.php');
	include_once('../config/constants.php');
        include_once('../config/functions.php');
	
	
        if(isset($_GET["delete"]))
	{
		$id =  charEsc($_GET["delete"]);
		$delrow = select("select image from services where id = '$id' and type='services'");
               
                if(unlink("../".$delrow[0]["image"])) {
                query("delete from services where id = '$id' and type='services'");
                $_SESSION["success"] = "Service Deleted successfully.";
                echo '<script>window.location = "'.$_SERVER["PHP_SELF"].'";</script>';
                } else {
               query("delete from services where id = '$id' and type='services'");
          
                $_SESSION["success"] = "Service Deleted successfully.";
                echo '<script>window.location = "'.$_SERVER["PHP_SELF"].'";</script>';
                }
                
	}   
        
        $pageTitle = "Services"; 
	include('header.php'); 
        
?>
   <!-- page content -->
   <div class="right_col" role="main">
     <div class="">
        <div class="page-title">
            <div class="title_left">
               <h3> View Services </h3>
            </div>

           </div>
					
           <div class="clearfix"></div>
 
           <div class="row">
           <div class="clearfix"></div>
			 <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                 
                     <div class="x_content">
                         	<?php if(isset($_SESSION["error"])){ ?> 
				<div class="x_content bs-example-popovers">
					<div class="alert alert-danger alert-dismissible fade in" role="alert">
						<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
							<?php echo "<strong>Error ! </strong>"." ".$_SESSION["error"]; unset($_SESSION["error"]); ?>
					</div>
 				</div>
							<?php  unset($_SESSION["error"]);} ?>
									
				<?php if(isset($_SESSION["success"])){ ?> 
					<div class="x_content bs-example-popovers">
						<div class="alert alert-success alert-dismissible fade in" role="alert">
							<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
							<?php echo "<strong>Congrats ! </strong>"." ".$_SESSION["success"]; unset($_SESSION["success"]); ?>
						</div>
					</div>
				<?php
                                unset($_SESSION["success"]);
                                } ?>
				<a href="addservices.php"><button class="btn btn-primary waves-effect waves-button waves-float pull-right">Add Service</button></a>
			    <div class="table-responsive"> 
                                <table class="table table-striped responsive-utilities jambo_table bulk_action" id="datatable">
                           <thead>
                             <tr>
                                <th>Image </th>
				<th >Name </th>
                                <th >Category Name </th>
                                <!--<th >Brand</th>-->
                                <th>Duration </th>
                                <th >Price </th>
				<th >Status </th>
				<th class="column-title no-link last"><span class="nobr">Action</span></th>
                            </tr>
                          </thead>
                          <tbody>
                             <?php
                         
                        //Selecting the data from table 
                        $result = query("SELECT ser.*,cat.category_name,bran.brand_name FROM `services` ser
                        INNER JOIN  categories cat ON ser.category_id=cat.id 
                        LEFT JOIN  brands  bran ON ser.brand_id=bran.id where ser.type='services' order by ser.id desc");
                        while($row = fetch_object($result)) 
                        {     
                        ?>
                              <tr >
                                  <td><img src="../<?php echo $row->image; ?>" style="border-radius:3px;" width="100" height="100" /></td>
                                  <td><?php echo $row->service_name; ?></td>
                                  <td><?php echo $row->category_name; ?></td>
                                  <!--<td><?php echo $row->brand_name; ?></td>-->
                                  <td><?php echo ($row->hours!="")?$row->hours." hr":''; ?> <?php echo ($row->minutes!="")?$row->minutes." min":''; ?> </td>
                                  <td><?php echo PriceFormat($row->price); ?></td>
                                  <td><?php echo ($row->status==1)?'Active':'Deactive'; ?></td>
                                  <td><a href="addservices.php?editId=<?php echo $row->id; ?>" class="btn btn-info btn-sm"><i class="fa fa-pencil"></i> Edit</a>
                                   <a href="#" data-href="?delete=<?php echo $row->id; ?>" data-target="#confirm-delete" class="btn btn-danger btn-sm " title="Remove" data-toggle="modal">
                                  <i class="fa fa-trash"></i> Delete</a></td>
                              </tr>    
                              
                              
                        <?php
                        }
                        ?>
                       
                          </tbody>
                        </table>
                        </div>        
        <div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
        <div class="modal-dialog">
            <div class="modal-content">
            
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title" id="myModalLabel">Confirm Delete</h4>
                </div>
            
                <div class="modal-body">
                    <p>Are You Sure You want to Delete?</p>
                </div>
                
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <a class="btn btn-danger btn-ok">Delete</a>
                </div>
            </div>
        </div>
    </div>
                     </div>
			
		  </div>
		</div>
	 </div>
   </div>

    <!-- footer content -->
    <footer>
      <div class="">
        <p class="pull-right">
           <span><?php echo $adminTitle["copyright"]; ?></span> 
        </p>
      </div>
      <div class="clearfix"></div>
    </footer>
    <!-- /footer content -->
  </div>
  <!-- /page content -->
 </div>
</div>

    <div id="custom_notifications" class="custom-notifications dsp_none">
        <ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group">
        </ul>
        <div class="clearfix"></div>
        <div id="notif-group" class="tabbed_notifications"></div>
    </div>

    <script src="js/bootstrap.min.js"></script>
    <!-- chart js -->
    <script src="js/chartjs/chart.min.js"></script>
    <!-- bootstrap progress js -->
    <script src="js/progressbar/bootstrap-progressbar.min.js"></script>
    <script src="js/nicescroll/jquery.nicescroll.min.js"></script>
    <!-- icheck -->
    <script src="js/icheck/icheck.min.js"></script>
    <script src="js/custom.js"></script>
    
     <!-- Datatables -->
  <script src="js/jquery.dataTables.min.js"></script>

	
<script>
    $(document).ready(function(){
    $('#datatable').DataTable();
});
	  
	</script>
<script>
        $('#confirm-delete').on('show.bs.modal', function(e) {
            $(this).find('.btn-ok').attr('href', $(e.relatedTarget).data('href'));
            
        });
</script>


</body>

</html>