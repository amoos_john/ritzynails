<?php 
	$pageTitle = "Contact"; 
	include('header.php'); 
?>
    <!-- page content -->
    <div class="right_col" role="main">
       <div class="">
           <div class="page-title">
               <div class="title_left">
                   <h3> <small> Contact Us </small> </h3>
               </div>
       
				<div class="title_right">
					<form method="post" action="contact.php">
                        <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                           <div class="input-group">
							   <input type="text" class="form-control" name="name" placeholder="Search Name...">
                                 <span class="input-group-btn">
                       				<button class="btn btn-default" name="search" type="submit">Go!</button>
                       			 </span>
                            </div>
                         </div>
					</form>
                  </div>
             </div>
					
             <div class="clearfix"></div>

             <div class="row">
             <div class="clearfix"></div>
 				<div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                             <h2>Contact Us Messages</h2>
                                 <ul class="nav navbar-right panel_toolbox">
                                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                                    <li><a class="close-link"><i class="fa fa-close"></i></a></li>
                                 </ul>
                         	<div class="clearfix"></div>
                         </div>
					     <div class="x_content">
					         <table class="table table-striped responsive-utilities jambo_table bulk_action">
                             	<thead>
                                	<tr class="headings">
                                    	<th class="column-title">#</th>
										<th class="column-title">Name </th>
                                        <th class="column-title">Contact Number </th>
										<th class="column-title"> Email</th>
										<th class="column-title">Message </th>
										<th class="column-title">Date</th>
										<th class="column-title no-link last"><span class="nobr">Action</span></th>
                                	</tr>
                            	</thead>
	                            <tbody>
	                                <?php include('contactdata.php'); ?>
                                </tbody>

                              </table>
                          </div>
						  <!--
						  <div class="pull-left">
							<button type="button" onclick="getExcel();" class="btn btn-primary">Export To Excel</button>	
						  </div>
						-->	
                          <!-- Pagination Start-->		
                          <div class="dataTables_paginate paging_full_numbers" id="example_paginate">
				       		<a class="first paginate_button" href="contact.php?page=1" tabindex="0" id="example_first">First</a>
					  		<!-- <a class="previous paginate_button" tabindex="0" id="example_previous">Previous</a> -->
					 			<span>
									<?php for ($i=1; $i<=$total_pages; $i++) {
										//echo “<a href=’pagination.php?page=”.$i.”‘>”.$i.”</a> “;
										echo '<a class="paginate_button" href="contact.php?page='.$i.'" tabindex="0">'.$i.'</a>';
									   };
 									?>
                                      <!-- <a class="paginate_button" tabindex="0">1</a>
                                       <a class="paginate_button" tabindex="0">2</a>
                                       <a class="paginate_active" tabindex="0">3</a>
                                       <a class="paginate_button" tabindex="0">4</a> -->
								 </span>
							<!--  <a class="next paginate_button" tabindex="0" id="example_next">Next</a>-->
					  <a class="last paginate_button" href="contact.php?page=<?php echo $total_pages; ?>" tabindex="0" id="example_last">Last</a> 
						    </div>
							<!-- Pagination End-->
                       </div>
                   </div>
               </div>
            </div>
            <!-- footer content -->
            <footer>
            	<div class="">
                	<p class="pull-right">
                    	<span><?php echo $adminTitle["copyright"]; ?></span> 
                    </p>
                </div>
                <div class="clearfix"></div>
            </footer>
            <!-- /footer content -->
        </div>
        <!-- /page content -->
      </div>
    </div>

    <div id="custom_notifications" class="custom-notifications dsp_none">
        <ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group">
        </ul>
        <div class="clearfix"></div>
        <div id="notif-group" class="tabbed_notifications"></div>
    </div>

    <script src="js/bootstrap.min.js"></script>
   <!-- chart js -->
    <script src="js/chartjs/chart.min.js"></script>
    <!-- bootstrap progress js -->
    <script src="js/progressbar/bootstrap-progressbar.min.js"></script>
    <script src="js/nicescroll/jquery.nicescroll.min.js"></script>
    <!-- icheck -->
    <script src="js/icheck/icheck.min.js"></script>
    <script src="js/custom.js"></script>
	
	<script>
	 function idelete(value){
		  var r = confirm("Are you sure it's completed ?");
			if (r == true) {
				window.location ='contact.php?complete='+value;
			} 
			else 
			{    
			}
	  }
	  
	  function getExcel(){
		  window.location ='getExcel.php?contact=contact';	  
	  }
	  
	</script>

</body>

</html>