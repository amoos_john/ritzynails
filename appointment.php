<?php 
include_once('config/connection.php');
include_once('config/model.php');
include_once('config/functions.php');

if(isset($_SESSION["cart"]))
{
  
   unset($_SESSION["review"]); 
   $sess_id=$_SESSION["cart"];
   $cartservices=ServicesCartBooking($sess_id,''); 
   $booking=Checkbooking($sess_id);
   $bookdate='';
   $booktime='';
   
   if($booking)
   {
       $row=fetch_array($booking);
       $bookdate=$row["date"];
       $booktime=$row["start_time"];
   }
   
   
   if(!$cartservices)
   {
       echo "<script type='text/javascript'>window.location='index.php';</script>";
       exit();
   }
}
else
{
    echo "<script type='text/javascript'>window.location='index.php';</script>"; 
    exit();
}

if(isset($_POST["btn-next"]))
{
    $date= charEsc($_POST["bookdate"]);
    $time= charEsc($_POST["booktime"]);
    $sess_id=$_SESSION["cart"];
  
    if(!empty($date) && !empty($time))
    {
        $validedate=validateDate($date);
        if($validedate)
        {
            $cartservices=ServicesCartBooking($sess_id,''); 
            $seconds = 0;
      
          
            while($cart = fetch_object($cartservices))
            {   
                
                list($hour,$minute) = explode(':', $cart->total_time);
                $seconds += $hour*3600;
                $seconds += $minute*60;
                $seconds += $second;
            }
           
           
            $start_date = new DateTime($time);
            $since_start = $start_date->diff(new DateTime('09:00 pm'));
            
            $hours = floor($seconds / 3600);
            $seconds -= $hours * 3600;
            $minutes  = floor($seconds / 60);
            $seconds -= $minutes * 60;
            $thour=$since_start->h;
            $tmin=$since_start->i;
           
            if($hours>$thour)
            {
                 $_SESSION['error']="Total Time Surpasses available time select new date/time!";
               
                 header("location: appointment.php");
                 exit();    
            }   
          
            $end_time = strtotime('+'.$hours.' hour +'.$minutes.' minutes', strtotime($time));
            
            $end_time=date("h:i a",$end_time);
     
          
            $checkbook=Checkbooking($sess_id);
            if(count($checkbook)>0)
            {
                $mDate=date("Y-m-d H:i:s");
                $query=query("UPDATE `booking` SET `date`='{$date}',`start_time`='{$time}',`end_time`='{$end_time}',`modified_date`='{$mDate}'
                WHERE  `session_id`='{$sess_id}'");
                if($query)
                {
                   echo "<script type='text/javascript'>window.location='colors.php';</script>"; 
                }
            }
            else
            {
                $cDate=date("Y-m-d H:i:s");
                $insert=array("session_id"=>$sess_id,"date"=>$date,"start_time"=>$time,"end_time"=>$end_time,"created_date"=>$cDate);
                $query=insert("booking",$insert);
                if($query)
                {
                    //$_SESSION['error']="Completed!";
                    echo "<script type='text/javascript'>window.location='colors.php';</script>"; 

                }
            }
           
        }
        else
        {
            $_SESSION['error']="Date is Invalid!";
        }
    }
    else
    {
            $_SESSION['error']="Date / Time is empty!";
    }
    
}   

if(isset($_POST["btn-back"]))
{
    $_SESSION["review"]=1;
    echo "<script type='text/javascript'>window.location='booknow.php';</script>"; 
}
$pagetitle="Booking";    
include("header.php"); 

?>

 <style>
td.fc-past {
    opacity: 0.2;
    background: #fff !important;
}
     td.fc-other-month {
      opacity: 1 !important;
}

.btn-time
{
    cursor:pointer;
}
.timeslots__content ul li.current{
	border: 2px solid #FFCC00;
	color:#FFCC00;
}
.timeslots__content ul li.booked{
	border: 2px solid #FFCC00;
	color:#FFCC00;
}
.table.datetime>tbody>tr>td {
    width: 120px;
    padding: 8px 10px;
    display: inline-block;
}
.fc-toolbar .fc-state-active, .fc-toolbar .ui-state-active {
    z-index: 1;
}
.fc-time-grid-container {
    display: none;
}
.fc-state-active {
    background: #4cae4c !important;
}
.fc-highlight-skeleton {
    z-index: 3;
    cursor: pointer;
}
td#date {
    font-weight: 900;
}
td#time {
    font-weight: 900;
}

</style>
	<section class="services-page">
		<div class="container">
			 <div class="hed">
				  <h2>Schedule Online <span></span></h2>
			 </div> 
			 <?php 
                            if(isset($_SESSION['error']))
                            {
                                ?>
    <div class="alert alert-danger fade in alert-dismissable" style="margin-top:18px;">
    <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
    <?php echo $_SESSION['error']; ?> </div>
                                <?php
                                 unset($_SESSION['error']);
                            }
                         ?>
			 
			 <div class="services-list col-sm-9 girdbox-area ">
			
				
				 <div class="calendar-area">
				 <div id='calendar'></div>
				 </div>
				 
				 <div class="timeslot-area">
                                     <div id="timeslots">
                                       
                                     </div>
				 </div>
				
			 </div>
                   
			 
			 <div class="services-list col-sm-3">
                        <div class="selected-item-detail panel panel-default">
				  
                        <div class="panel-heading"><h4>Selected Date/Time</h4></div>
                        <form action="" method="post">
                        <?php    
                        
                        ?>
                        <input type="hidden" name="bookdate" id="bookdate" value="<?php echo (!empty($bookdate))?$bookdate:''; ?>"/>
                        <input type="hidden" name="booktime" id="booktime" value="<?php echo (!empty($booktime))?$booktime:''; ?>"/>
				  <table class="table datetime">
					<tbody>
						<tr>
                                                    <td>Date:</td><td id="date"><?php echo (!empty($bookdate))?DateFormat($bookdate):''; ?></td>
						</tr>
						<tr>
							<td>Time:</td><td id="time"><?php echo (!empty($booktime))?$booktime:''; ?></td>
						</tr>
                                                <tr>
						<td><div class="mb10">
                                                        <button type="submit" id="btn-next" name="btn-next" class="btn btn-book btn-lg" <?php echo (empty($bookdate) && empty($bookdate))?'disabled':''; ?>>Next</button>
                                                    
                                                </div>
                                                <div class="mb10">
                                                <form action="" method="post">
                                                    <button type="submit" name="btn-back" class="btn btn-book btn-lg">Review Services</button>
   
                                                    <!--<a href="booknow.php" class="btn btn-book btn-lg">Review Services</a>-->
                                                </form>   
                                                </div>
                                                </td>
						</tr>
						
					</tbody>
				  </table>
                        </form>
                    </div>
			 
				<!--
				<?php 
                                /*if(count($cartservices)>0)
                                {
                                    
                                while($cartservice = fetch_object($cartservices))
                                {
                                ?>
                                
				<div class="selected-item-detail panel panel-default">
				  
				  <div class="panel-heading"><h4><?php echo $cartservice->service_name; ?></h4></div>

				  <table class="table">
					<tbody>
						<tr>
							<td>Service Time:</td><td><?php echo ($cartservice->hours!="")?$cartservice->hours." hr":''; ?> <?php echo ($cartservice->minutes!="")?$cartservice->minutes." mins":''; ?></td>
						</tr>
						<tr>
							<td>Persons:</td><td><?php echo $cartservice->persons; ?></td>
						</tr>
                                                <tr>
							<td>Price:</td><td>$ <?php echo $cartservice->price; ?></td>
						</tr>
						<tr>
							<td>Total Price:</td><td>$ <?php echo $cartservice->total_price; ?></td>
						</tr>
					</tbody>
				  </table>
				</div>
                             <?php
                                }
                             }*/
                                ?>
				-->
			 
			 </div>
			 
			 
			 
		</div>
	</section>
	 

<script src='js/moment.min.js'></script>
<script src='js/fullcalendar.min.js'></script>	
<script type="text/javascript">
    $(document).ready(function() {
    var date = new Date();
    var d = date.getDate();
    var m = date.getMonth();
    var y = date.getFullYear();

$('#calendar').fullCalendar({
    
    
  header: {
      left: 'prev,next today',
      center: 'title',
      right: 'month,agendaWeek,agendaDay'
    },
    editable: false,
    selectable: true,
    selectHelper: false,
    displayEventTime: false,
     dayClick: function(date, jsEvent, view) {
         
            $(".fc-highlight").removeClass("fc-highlight");
            $(".fc-state-highlight").removeClass("fc-state-highlight");
            
            $(this).addClass("fc-highlight");
            
            var selectionStart = moment(date).format('MM-DD-YYYY'); 
            var today = moment().format('MM-DD-YYYY'); // passing moment nothing defaults to today
            
            if (selectionStart < today) { 
                $('#calendar').fullCalendar('unselect');
            }
            else {
               
                $("#btn-next").css('opacity','0.3');
                $("#btn-next").attr('disabled',true);
                $("#time").text('');
                $("#booktime").val('');
                var ddate=date.format('YYYY-MM-DD');
                $("#date").text(date.format('LL'));
                $("#bookdate").val(ddate);
               
                gettimeslot(ddate);
                 
            } 
   

    }
  
});




});



function gettimeslot(date)
{ 
   
    $.ajax({
            type:'post',
            url:'timeslots.php',
            data:{
              date:date,  
              showtime:'showtime'
            },
            success:function(response) {
               
              $('#timeslots').stop(true).slideUp(100); 
              $('#timeslots').html(response).stop(true).slideDown(100);
            
            }
          });
}




</script>

    <?php include("footer.php"); ?>