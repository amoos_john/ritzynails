<?php
extract($_POST);
include_once('config/connection.php');
include_once('config/model.php');
include_once('config/functions.php');

if(isset($showcart))
{
        $session_id=$_SESSION['cart'];
       
        if(isset($session_id))
        {
            $query= ServicesCart($session_id,'');
            if(count($query)>0)
            {
                  echo '<tr>
                            <th>Image</th>
                            <th>Name</th>
                            <th>Person</th>
                            <th>Price</th>
                            <th>&nbsp;</th>
                        </tr>';
                while($row=fetch_object($query))
                {
                    echo '<tr>
                            <td class="cart__prod__img"><img src="'.$row->image.'" alt="'.$row->service_name.'" /></td>
                            <td class="text-left">'.$row->service_name.'</td>
                            <td>'.$row->persons.'</td>    
                            <td>'.PriceFormat($row->total_price).'</td>
                        </tr>';
                } 
            }
            else
            {
                echo "No Items to Show";
            }
             exit();
        }

}
if(isset($itemid))
{
    $itemid= charEsc($itemid);
    $delete=RemoveItem($itemid);
    if($delete==TRUE)
    {
        $_SESSION["success"] = '<div class="alert alert-success fade in alert-dismissable">
                <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
                <i class="fa fa-check-circle"></i> Item Remove Successfully!</div>';
    }
        
}
if(isset($showsumm))
{
    $person=$_SESSION["person"];
    $sess_id=$_SESSION["cart"];
    ?>
<style>
table.table.sub tbody>tr>td {
    border: 0 !important;
    padding: 2px !important;
}
table.table.sub>thead>tr>th:nth-child(1), table.table.sub>tbody>tr>td:nth-child(1) {
    width: 40%;
}

table.table.sub>thead>tr>th:nth-child(3), table.table.sub>tbody>tr>td:nth-child(3) {
    width: 40%;
}


table.table.sub>thead>tr>th:nth-child(2), table.table.sub>tbody>tr>td:nth-child(2) {
    width: 20%;
}
td.p0 {
    padding: 0 !important;
}

td table {
    margin: 0 !important;
}
table.table.sub thead {
      border: 0 !important;
}
table.table.sub thead>tr>th {
    border: 0 !important;
}   
table.table.sub tbody>tr {
    border-bottom: 1px solid #000 !important;
}
table.table.subcol tbody>tr {
    border-bottom:0 !important;
}
table.table.sub tbody>tr:last-child{
     border-bottom:0 !important;
}

</style>

       <table class="table">
         <thead>         
        <tr>
           
               <th> Person</th>
               <td class="p0"> 
                   <table class="table sub">
                       <thead>
            <tr>
             
                <th  for="service">Service / Product</th>
                <th  for="">Price</th>
                <?php if(isset($colors))
                {
                ?>
                <th  for="">Selected Colors</th>
                <?php
                }
                else {
                    ?>
                <th for="">&nbsp;</th>
                <?php } ?>
                
            </tr>
        </thead>
              </table>
               </td>
       </tr>
       </thead>
           
		<tbody>
                 <?php
                $price_total=0; 
                for($i=1;$i<=$person;$i++)
                {
                    $service_name='';
                    $service_price='';
                    $delete='';
                    $color_name='';
                    $query=ServicesCart($sess_id,$i);
                ?>  
                <tr>
                    <td  for="person">Person <?php echo $i; ?> </td>
                    <td for="service"> 
                <table class="table sub">
                      
                <?php    
                    while($row=fetch_object($query))
                    {
                        ?>
                         <tr>
                          <td  for="service"><?php echo $row->service_name; ?></td>
                          <td  for="price"><?php echo PriceFormat($row->total_price); ?></td>
                          <td>
                            
                         <?php
                         //$service_name.=''.$row->service_name.'';
                         //$service_price.=PriceFormat($row->total_price)."";
                         $price_total=$price_total+$row->total_price;
                         
                         if(isset($colors))
                         {
                             $colors=CheckColor($row->id,$sess_id,$i);
                             if(count($colors)>0)
                             { 
                                ?> 
                                 <table class="table subcol">   
                                <?php     
                                while($color=fetch_object($colors))
                                {
                                    echo '<tr><td>'.$color->color_name.'</td>
                                    <td class="pull-right"><button type="submit" onclick="deleteColor('.$color->id.')" class="btn btn-danger btn-xs"><i class="fa fa-times"></i></a></button></td></tr>';
                                }
                                ?>
                                 </table>      
                             <?php      
                             }
                             
                         }
                         else
                         {
                           echo '<button type="submit" onclick="deleteItem('.$row->id.')" class="btn btn-danger btn-xs"><i class="fa fa-times"></i></a></button><br/>';

                         }
                         ?>
                              
                          </td> 
                         </tr>
                          <?php     
                    }
                  
                ?>
                    </table> 
                </td> 		  
          
                
               
             </tr>
          				
            <?php
                }
               
                $discount=getDiscount($person);
                $subtotal=$discount[0]["discount_price"]+$price_total+6.00;
                $per_discount=getDiscount(1);
               
            ?>  
             <span id="subtotal" style="display:none;"><?php echo $subtotal; ?></span>

            <tr>
            	 <td></td>
			<td  for="mobilefee">
                         <table class="table sub">
                         <tr>   
                             <td>Mobile fee: <?php echo $per_discount[0]["discount_price"]."$ (".$per_discount[0]["persons"].")"; ?>  one person,
                                 <br/> add 5$ for each additional person.</td>  
                             <td><?php echo PriceFormat($discount[0]["discount_price"]); ?></td> 
                             <td></td>
                         </tr>
                         </table>    
                          </td>
                        
            </tr>			
	  <tr>
            	 <td></td>
			<td  for="parking">
                       <table class="table sub">
                         <tr>   
                             <td><label>
                                     <input type="checkbox" name="park" id="park"/>
				     <strong>Free parking is available nearby</strong>
				</label></td> 
                              <td  for="price" id="parkfee">$6.00</td>
                              <td></td>
                         </tr>
                         </table>      
				
			</td>
				
                               
            </tr>
            <tr>
            	 <td></td>
                 <td  for="subtotal">
                       <table class="table sub">
                         <tr>   
                             <td><strong>SubTotal</strong>
				</td> 
                                <td><strong>&dollar;<span id="total"><?php echo number_format($subtotal,2); ?></span></strong></td>
                              <td></td>
                         </tr>
                         </table>      
				
		</td>
                 
				
                                 

           </tr>	

	 </tbody>
  </table>
<script>
$( document ).ready(function() {  
 $(function(){    
    var test = localStorage.input === 'true'? true: false;
    $('input').prop('checked', test || false);
    
    var subtotal=$("#total").text().split("$");
    var fee=parseFloat(6.00);
   
    
    if(test==true)
    {
        var total= subtotal - fee;
        var grandtotal=total.toFixed(2);
        $('#parkfee').text('$0.00');
        $('#park_fee').val('');
        $('#total').text(grandtotal);
        $('#grand_total').val(grandtotal);
       
    }
    else
    {
        $('#park_fee').val('6.00');
        $('#parkfee').text('$6.00');
        $('#grand_total').val(subtotal);
       
    }   
    
 });    
    
$('#park').on('change', function() {
    
    
    var subtotal=$("#subtotal").text().split("$");
    var fee=parseFloat(6.00);
    localStorage.input = $(this).is(':checked');
    
    
    if(this.checked)
    {
        var total= parseFloat(subtotal) - fee;
        var grandtotal=total.toFixed(2);
        $('#parkfee').text('$0.00');
        $('#park_fee').val('');
        $('#total').text(grandtotal);
        $('#grand_total').val(grandtotal);
     
    
    }
    else
    {
        var totalprice= parseFloat(subtotal);
        var grandtotal=totalprice.toFixed(2);
        $('#parkfee').text('$6.00');
        $('#park_fee').val('6.00');
        $('#total').text(grandtotal);
        $('#grand_total').val(grandtotal);

    }
 
});    
 
 
}); 



 </script>
    
    <?php
}
    
?>
