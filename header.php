<!DOCTYPE html>
<html lang="en" class="broken-image-checker">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge"> 
    <meta name="keywords" content="<?php echo (isset($keywords))?$keywords:'Ritzy Nails'; ?>">
    <meta name="description" content="<?php echo (isset($description))?$description:'Ritzy Nails'; ?>">
    <!--iPhone from zooming form issue (maximum-scale=1, user-scalable=0)-->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0"/>
    <title><?php echo isset($pagetitle)?$pagetitle:'Home'; ?></title>


	<link rel="icon" type="image/png" href="images/favicon.png">
	
    <!-- Bootstrap -->
        <link href="css/bootstrap.min.css" rel="stylesheet">
	<link rel="stylesheet" href="css/fullcalendar.css">
	<link rel="stylesheet" href="style.css">
	<link rel="stylesheet" href="css/colorized.css">
	<link rel="stylesheet" href="css/animate.css">
	<!--<link rel="stylesheet" href="css/slidenav0.css">-->
	<link rel="stylesheet" href="css/font-awesome.min.css">
	
	<!-- jQuery -->
	<!--[if (!IE)|(gt IE 8)]><!-->
	  <script src="js/jquery-2.2.4.min.js"></script>
	<!--<![endif]-->


	<!--[if lte IE 8]>
	  <script src="js/jquery1.9.1.min.js"></script>
	<![endif]-->
	
	<!--browser selector-->
	<script src="js/css_browser_selector.js" type="text/javascript"></script>
	


    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="js/html5shiv.min.js"></script>
      <script src="js/respond.min.js"></script>
    <![endif]-->
    <style>    
.top-area {
    min-width: 250px;

}
.goog-te-banner-frame.skiptranslate {
    display: none !important;
}

a.goog-logo-link {
    display: none !important;
}
body {
    top: 0 !important;
}
.goog-te-gadget img {
    display: none !important;
}
.goog-te-gadget-simple {
    display: none !important;
}
</style>

<script>
/*preloader*/
jQuery(window).on('load', function() {
  jQuery('#preloader__status').fadeOut();
  jQuery('#preloader').delay(350).fadeOut('slow'); 
  jQuery('body').delay(350).css({'overflow':'visible'});
})

/*./preloader*/
</script>
    
  </head>
  <body class="transition nav-plusminus slide-navbar slide-navbar--right">
 <div id="preloader">
   <div id="preloader__status">
  <i class="fa fa-spinner fa-spin fa-2x"></i>
   </div>
 </div>
<header>

	<section class="hdr-area hdr-nav  cross-toggle bg-white" data-navitemlimit="0">
		<div class="container">
		
			<div class="top-area clrlist  pul-rgt">
				<div class="lang-area login-area clrlist">
					 <ul>
                                                 <?php 
                                                if(isset($_SESSION['user']))
                                                {
                                                    ?>
                                                <li><a href="dashboard.php"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                                                <li><a href="logout.php"><i class="fa fa-sign-in"></i> Logout</a></li>

                                                    <?php
                                                }
                                                else
                                                {
                                                     ?>
                                                     <li><a href="login.php"><i class="fa fa-user"></i> Login</a></li>
                                                    <li><a href="login.php"><i class="fa fa-sign-in"></i> Signup</a></li>

                                                    <?php
                                                }
                                                 ?>
						 
					 </ul>
				</div>

                            
                            <div class="lang-area clrlist">
                                      <div id="google_translate_element"></div>
        
					<ul class="translation-links">
						 <li><a href="#" class="english" data-lang="English">eng</a></li>
						 <li><a href="#"  data-lang="French">fr</a></li> <!--class="french" -->
					</ul>
                                
                                 <script type="text/javascript">
                                function googleTranslateElementInit() {
                                  new google.translate.TranslateElement({pageLanguage: 'en', includedLanguages: 'en,fr', layout: google.translate.TranslateElement.InlineLayout.SIMPLE}, 'google_translate_element');
                                }
                                </script>
                            
                                <script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
				<!-- Flag click handler -->
                                <script type="text/javascript">
                                    $('.translation-links a').click(function() {
                                      var lang = $(this).data('lang');
                                      var $frame = $('.goog-te-menu-frame:first');
                                      if (!$frame.size()) {
                                        alert("Error: Could not find Google translate frame.");
                                        return false;
                                      }
                                      $frame.contents().find('.goog-te-menu2-item span.text:contains('+lang+')').get(0).click();
                                      return false;
                                    });
                                </script>
                            </div>
			</div>
			
			<div class="cart-area clrlist  pull-right">
				<ul>
					<li class="dropdown last">
                                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" onclick="show_cart()">
						<img src="images/cart.png" alt="">
						<span class="badge" id="total_items"></span>
						</a>
							<ul class="dropdown-menu overload">
								<li>
									  <table class="table" id="mycart">
                                                                         
									  </table>
									<div class="btns cart__checkout__btns">
										<a class="btn btn-primary" href="cart.php"><i class="fa fa-shopping-cart"></i>View Cart</a>
										<a class="btn btn-warning" href="cart.php"><i class="fa fa-share"></i>Checkout</a>
									</div>
									
									<div class="clearfix"></div>
								</li>
							</ul>
					</li>	
				</ul>						
			</div>
			
			
			
			<nav class="navbar navbar-default" role="navigation" id="slide-nav">
			  <div class="container-fluid">
				<!-- Brand and toggle get grouped for better mobile display -->
				<div class="navbar-header">
				  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				  </button>
					<a class="navbar-brand" href="./"><img src="images/logo.png" alt="logo" class="broken-image"/></a>
				</div>

				<!-- Collect the nav links, forms, and other content for toggling -->
				<div id="slidemenu">
					<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
					  <ul class="nav navbar-nav navbar-main pull-right">
                                                <?php 
                                                $menus = getMenus();
                                                while ($row = fetch_object($menus)) 
                                                {
                                                ?>
                                                    <li><a href="<?php echo ($row->url!="")?$row->url:$row->code; ?>"><?php echo $row->menu_name; ?></a></li>
                                                <?php
                                                }
                                                ?>
                                                
					  </ul>
					
					</div><!-- /.navbar-collapse -->
				</div>
			  </div><!-- /.container-fluid -->
			</nav>
		</div>
	</section>
</header>

<main id="page-content">
