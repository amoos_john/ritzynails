<?php 
include_once('config/connection.php');
include_once('config/model.php');
include_once('config/functions.php');
include_once('config/emails.php');
include_once('config/constants.php');

if(isset($_POST["forgot"]))
{
    $email = charEsc($_POST["email"]);
    $checkemail=ValidateEmail($email);
    
    if(!empty($checkemail))    
    {
            $_SESSION["error"] = $checkemail;	          
    }
    else
    {
        $sql= "select * from `users` where email='{$email}' and status='active'";
	$query = query($sql);
	if(num_rows($query) > 0)
	{
            $token = bin2hex(openssl_random_pseudo_bytes(16));
            $cDate=date("Y-m-d H:i:s");
            $addtoken=array("token"=>$token,"email"=>$email,"created_date"=>$cDate);
            $querytoken = insert('reset_password',$addtoken);
            $sendmail=ForgotPassword($email,$account,$siteUrl,$token);
            if($sendmail && $querytoken)
            {
                $_SESSION["success"] = "Reset Password Email Successfully Sent!";
            }
            else
            {
                $_SESSION["error"] = "Reset Password cannot sent!";
            }
       
        }
        else
        {
             $_SESSION["error"] = "Email is Invalid!";	          
        }
    }
}

$pagetitle="Forgot Password";
include("header.php");
?>


	<section class="services-page">
		<div class="container">
			 <div class="hed">
				  <h2>Forgot Password <span></span></h2>
			 </div> 
			 
		
			<div class="fom fom-login col-sm-6 center">
                         <?php 
                            if(isset($_SESSION['success']))
                            {
                                ?>
                            <div class="alert alert-success fade in alert-dismissable" style="margin-top:18px;">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
                            <i class="fa fa-check-circle"></i> <?php echo $_SESSION['success']; ?> </div>
                                <?php
                               unset($_SESSION['success']);      
                                
                            }
                         ?>
                             <?php 
                            if(isset($_SESSION['error']))
                            {
                                ?>
                            <div class="alert alert-danger fade in alert-dismissable" style="margin-top:18px;">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
                            <i class="fa fa-times-circle"></i> <?php echo $_SESSION['error']; ?> </div>
                                <?php
                               unset($_SESSION['error']);      
                                
                            }
                         ?>
                            <form method="post" action="" name="forgot-form" id="forgot-form">
				<div class="form-group">
					<label>Email<em>*</em></label>
                                        <input type="email" name="email"  id="user_email" class="form-control" placeholder="Email" maxlength="60" required/>
				</div>
				
				<div class="form-group">
				
                                        <button type="submit" class="btn btn-book btn-lg " name="forgot" id="forget">Forgot</button>
				</div>
                            </form>    
			</div>
			
			
			 
		</div>
	</section>
	 

	
<?php include("footer.php"); ?>