<?php 
include_once('config/connection.php');
include_once('config/model.php');
include_once('config/functions.php');
include_once('config/constants.php');

if(isset($_SESSION["cart"]))
{
    $sess_id=$_SESSION["cart"];
    $carts=ServicesCart($sess_id,'');
    //$checkouts=ServicesCart($sess_id);
    //$ssubtotal=CartTotal($sess_id);
    //$ssubtotal=number_format($ssubtotal,2);
    //$subtotal=PriceFormat($ssubtotal);
    $checkbook=Checkbooking($sess_id);
    
    $person=$_SESSION["person"];
    
    if(isset($_POST["remove_cart"]))
    {
       $result=RemoveCart($sess_id);
       if($result)
       {
           unset($_SESSION["cart"]);
           unset($_SESSION["person"]);
           echo "<script type='text/javascript'>window.location='cart.php';</script>";

       }
    }
    
    
}

$pagetitle="Shopping Cart";
include("header.php"); 
?>
<style>
   form {
    display: inline;
}
</style>
<style>
table.table.sub tbody>tr>td {
    border: 0 !important;
    padding: 2px !important;
}
table.table.sub>thead>tr>th:nth-child(1), table.table.sub>tbody>tr>td:nth-child(1) {
    width: 10%;
}

table.table.sub>thead>tr>th:nth-child(2), table.table.sub>tbody>tr>td:nth-child(2) {
    width:30%;
}
table.table.sub>thead>tr>th:nth-child(3), table.table.sub>tbody>tr>td:nth-child(3) {
    width: 10%;
}


table.table.sub>thead>tr>th:nth-child(4), table.table.sub>tbody>tr>td:nth-child(4) {
    width: 30%;
}
table.table.sub>thead>tr>th:nth-child(5), table.table.sub>tbody>tr>td:nth-child(5) {
    width: 30%;
}
td.p0 {
    padding: 0 !important;
}

td table {
    margin: 0 !important;
}
table.table.sub thead {
      border: 0 !important;
}
table.table.sub thead>tr>th {
    border: 0 !important;
}   
table.table.sub tbody>tr {
    border-bottom: 1px solid #ddd !important;
}
table.table.subcol tbody>tr {
    border-bottom:0 !important;
}
table.table.sub tbody>tr:last-child{
     border-bottom:0 !important;
}
.fom label em {
    color: #d00;
    font-size: 12px;
    font-style: normal;
}
</style>

	<section class="services-page">
		<div class="container">
			 <div class="hed">
				  <h2>Shopping Cart<span></span></h2>
			 </div> 
			 
		<?php
            
                if(count($carts)>0)
                {
                     if(count($checkbook)>0)
                     {
              ?>
                    
			<div class="cart-page-area">
			
			<div class="table-area  col-sm-12">
                    <div id="message"></div>
                    <?php 
                       if(isset($_SESSION['error']))
                        {
                            ?>
                            <div class="alert alert-danger fade in alert-dismissable" style="margin-top:18px;">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
                            <i class="fa fa-times-circle"></i> <?php echo $_SESSION['error']; ?> </div>
                                <?php
                               unset($_SESSION['error']);      
                                
                        }
                         ?>
                    <table class="table cart-item-table table-bordered table-valign">
                        <thead>
                        <tr>
                                <th>Persons</th>
                                <td  class="p0">
                                <table class="table sub">
                                <thead>
                                <tr>    
                                <th>Image</th>
                                <th>Service / Product</th>
                                <th>Price</th>
                                <th>Selected Colors</th>
                                <th class="text-center">Action</th>
                                 </tr>
                                </thead>
                                </table>
                                </td>
                                
                        </tr>
                        </thead>
                          
                        <tbody>
                            <?php
                            for($i=1;$i<=$person;$i++)
                            {
                                $service_image='';
                                $service_name='';
                                $service_price='';
                                $delete='';
                                $color_name='';
                                $carts=ServicesCart($sess_id,$i);
                                  ?>  
                            <tr>
                                <td  for="person">Person <?php echo $i; ?> </td>
                                <td for="service"> 
                            <table class="table sub">

                            <?php
                                while($cart = fetch_object($carts))
                                {
                                     ?>
                                <tr>
                                 <td for="image"><span class="pic"><img src="<?php echo $cart->image; ?>"  alt=""/></span></td>   
                                 <td  for="service"><?php echo $cart->service_name; ?></td>
                                 <td  for="price"><span class="txt-price"><?php echo PriceFormat($cart->total_price); ?></span></td>
                                 <td>
                                  <?php
                                   $colors=CheckColor($cart->id,$sess_id,$i);
                                    if(count($colors)>0)
                                    {
                                         ?> 
                                    <table class="table subcol">   
                                   <?php     
                                       while($color=fetch_object($colors))
                                       {
                                           echo '<tr><td>'.$color->color_name.'</td>
                                           <td class="pull-right"><button type="submit" onclick="colorItem('.$color->id.')" class="btn btn-danger btn-xs"><i class="fa fa-times"></i></a></button></td></tr>';
                                       }
                                  ?>
                                 </table>
                                
                                <?php  

                                    }
                                    
                                    $service_image.='<span class="pic"><img src="'.$cart->image.'"  /></span><br/>';
                                    $service_name.=$cart->service_name."<br/>";
                                    $service_price.='<span class="txt-price">'.PriceFormat($cart->total_price)."</span><br/>";
                                    
                                    $delete.='<button type="submit" onclick="cartItem('.$cart->id.')" class="btn btn-danger btn-sm"><i class="fa fa-times"></i></a></button><br/>';
                                    $price_total=$price_total+$cart->total_price;
                                ?>
                              
                         </td>
                         <td class="text-center"><button type="submit" onclick="cartItem(<?php echo $cart->id; ?>)" class="btn btn-danger btn-sm"><i class="fa fa-times"></i></a></button></td>
                         </tr>
                          <?php  
                                    
                                    
                             }
                                   
                                ?>
                           </table> 
                        </td> 		  
          
                
               
             </tr>
                            <?php 
                            }
                             $discount=getDiscount($person);
                             $discount_price=$discount[0]["discount_price"];
                             $subtotal=$discount[0]["discount_price"]+$price_total+6.00;
                             $per_discount=getDiscount(1);
                           ?>
                                         <span id="subtotal" style="display:none;"><?php echo $subtotal; ?></span>

             <tr>
            	 <td></td>
			<td  for="mobilefee">
                         <table class="table sub">
                         <tr>   
                             <td>&nbsp;</td>
                             <td>Mobile fee: <?php echo $per_discount[0]["discount_price"]."$ - ".$per_discount[0]["persons"]; ?>  one person,   
                                 <br/>add 5$ for each additional person.</td>
                           
                             <td><?php echo PriceFormat($discount[0]["discount_price"]); ?></td> 
                             <td>&nbsp;</td>
                             <td>&nbsp;</td>
                         </tr>
                         </table>    
                          </td>
                        
            </tr>			
	 <tr>
            	 <td></td>
			<td  for="parking">
                       <table class="table sub">
                         <tr> 
                             <td>&nbsp;</td>
                             <td><label>
                                     <input type="checkbox" name="park" id="park" >
					<strong>Free parking is available nearby</strong>
				</label></td> 
                              
                              <td  for="price" id="parkfee">$6.00</td>
                              <td>&nbsp;</td>
                              <td>&nbsp;</td>
                         </tr>
                         </table>      
				
			</td>
				
                               
            </tr>
           
                        
                         </tbody>
                    </table>
            </div>
                            
			<div class="clearfix"></div>
			
		<div class="col-sm-6 fom fom-login checkbox mb20">
               <?php 
                if(isset($_SESSION['user']))
                {
                 ?>
                <form method="post" action="order_process.php" target="_blank" name="checkout-form" id="checkout-form">     
                <?php
                }              
                 ?>        
                <label class="m10"><input type="radio" class="addr" value="default"  name="addr" checked="checked"> <strong>Default address</strong></label>
                <label class="m10"><input type="radio" class="addr" value="addit" name="addr"/> <strong>Additional address</strong></label>
                   
                <div id="additional"></div>

                </div>
			
				
		<div class="col-sm-6 table-total-area pul-rgt">
                <label>
                      <input type="checkbox" value="1" id="terms" name="terms" required />
                      <a href="#termsModal" data-toggle="modal" class="termsdet"> I accept the terms and conditions</a>
		</label>   
                <table class="table table-bordered">
                    <thead>
                    <tr><th colspan="2">CART TOTAL</th>
                    </tr>
                    </thead>

                    <tbody>
                    
                    <tr>
                        <th>Subtotal</th>
                        <td><strong>&dollar;<span id="total"><?php echo $subtotal; ?></span></strong></td>
                    </tr>
                    <tr class="total-area-btns">
                        <td colspan="2" for="">
                            
                            <?php 
                               if(isset($_SESSION['user']))
                                {
                                ?>
                                        <input type="hidden" name="park_fee" id="park_fee"  />
                                        <input type="hidden" name="mobile_fee" id="mobile_fee"  value="<?php echo $discount_price;?>"/>
                                        <input type="hidden" name="grand_total" id="grand_total" value="" />
                                        <button type="submit" name="checkout" id="btn-cont" class="btn btn-primary" disabled><i class="fa fa-share"></i> Check Out</button>
                    </form>
                                    
                                <?php
                                }
                                else
                                {
                                ?>
                                <a href="login.php" class="btn btn-primary"><i class="fa fa-share"></i> Check Out</a>

                                <?php
                                }
                            ?>
                         <form method="post" action="" name="remove-form" id="remove-form">
                            <button type="submit" name="remove_cart" class="btn  btn-danger"><i class="fa fa-times"></i> Remove Cart</button>
                         </form>   
                        </td>    
                    </tr>

                </tbody>
                </table>
            </div>
                        <div class="clearfix"></div>
			
			
			
			</div>
		<?php
                    }
                else
                {
                ?>
                     <div class="alert alert-success fade in alert-dismissable">
    <h4 class="text-center"><i class="fa fa-calendar"></i> Kindly first <strong><a href="appointment.php">schedule an appointment!</a></strong></h4></div>

		<?php
                }
             }   
              else
                {
                ?>
      <div class="alert alert-success fade in alert-dismissable">
    <h4 class="text-center"><i class="fa fa-check-circle"></i> Your Shopping Cart is empty!</strong></h4></div>
		<?php
                }
                ?>
			 
		</div>
            
   <div id="termsModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content" >
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title" id="myModalLabel">Terms and Conditions</h4>
</div>
<div class="modal-body" id="modalContent" >
    <div class="col-md-12">
        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. 
            Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, 
            when an unknown printer took a galley of type and scrambled it to make a type specimen book. 
            It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. 
            It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
        </p>
        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. 
            Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, 
            when an unknown printer took a galley of type and scrambled it to make a type specimen book. 
            It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. 
            It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
        </p>
        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. 
            Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, 
            when an unknown printer took a galley of type and scrambled it to make a type specimen book. 
            It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. 
            It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
        </p>
    </div>
</div>
 <div class="modal-footer">
       <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
</div>
            </div>
        </div>
</div>	    
            
            
	</section>
	 
<script>
   function changebutton()
    {
        var person='<?php echo ($person)?$person:''; ?>';
         $.ajax({
            type:'post',
            url:'getdiscount.php',
            data:{
              person:person,
              getperson:1
            },
          success:function(html){
                    if(html!=0)
                    {
                        //$('#btn-cont').removeAttr("disabled");
                        $('#btn-cont1').removeAttr("disabled");
                    }  
                },
                error: function(errormessage) {
                      //you would not show the real error to the user - this is just to see if everything is working
                    
                    alert("Error"+errormessage);
                }
      });
    }
    changebutton();

   // $('#addfee').hide();
 $(document).ready(function(){

    $(function(){    
    var test = localStorage.input === 'true'? true: false;
    $('#park').prop('checked', test || false);
    
    var subtotal=$("#subtotal").text().split("$");
    var fee=parseFloat(6.00);
   
    
    if(test==true)
    {
        var total= subtotal - fee;
        var grandtotal=total.toFixed(2);
        $('#parkfee').text('$0.00');
        $('#park_fee').val('');
        $('#total').text(grandtotal);
        $('#grand_total').val(grandtotal);
        
       
    }
    else
    {
        $('#park_fee').val('6.00');
        $('#parkfee').text('$6.00');
        $('#grand_total').val(subtotal);  
    }   
    
 });
 
    
    
});   
    
   
</script>	
	
<?php include("footer.php"); ?>