<?php
$emailTemplates=array(
    'confirm_email'=>array(
        "subject"=>"Verify Email Address | NASKA Banquet",
        "body"=>'<table><tr><td colspan=2><p>Hi <span>%%NAME%%!</span><p>Please confirm your email address by clicking the following link.</p><p><a class="btn" href="%%LINK%%" style="border-radius: 4px;background-color: #39c;color: #fff;display: inline-block;padding: 5px 10px;text-decoration: none;">Click here</a></p></td></tr><tr><td width="80px"><a href="#"><img src="http://ecommercehouse.com/projects/naskabanquet/images/logo.png" alt="" style="width: 90px;margin: 0;"></a></td><td><p>Respectfully yours,<br>The NASKA Banquet Team</p></td></tr></table>',
        "footer"=>""
        ),
    'recover'=>array(
        "subject"=>"Password Reset | NASKA Banquet",
        "body"=>'<table><tr><td colspan=2><p>Hi <span>%%NAME%%!</span><p>Your reset password link is given below.</p><p><a class="btn" href="%%LINK%%" style="border-radius: 4px;background-color: #39c;color: #fff;display: inline-block;padding: 5px 10px;text-decoration: none;">Click here</a></p></td></tr><tr><td width="80px"><a href="#"><img src="http://ecommercehouse.com/projects/naskabanquet/images/logo.png" alt="" style="width: 90px;margin: 0;"></a></td><td><p>Respectfully yours,<br>The NASKA Banquet Team</p></td></tr></table>',
        "footer"=>""
        ) 
);
function ForgotPassword($to,$from,$siteUrl,$token)
{
    
     $subject = 'Ritzy Nails Forgot Password';
            // message
            $body = '
            <html>
            <head>
              <title>Ritzy Nails Forgot Password</title>
            </head>
            <body>
                <p>Kindly click on this link to reset your password.</p>
                 <p><a href="'.$siteUrl.'/reset-password.php?token='.$token.'">Reset Password</a></p>
                <p>Regards,</p>
                <p> Ritzy Nails. </p>  
            </body>
            </html>';
            
            // To send HTML mail, the Content-type header must be set
            $headers  = 'MIME-Version: 1.0' . "\r\n";
            $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";

            $headers .= 'From: '.$to.' <'.$from.'>' . "\r\n";
            $headers .= 'Cc: '.$from.'' . "\r\n";

            if(mail($to, $subject, $body, $headers))
            {
                return true;
            }
            else
            {
                return false;
            }
}
?>
