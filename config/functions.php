<?php
function to_prety_url($str){
	if($str !== mb_convert_encoding( mb_convert_encoding($str, 'UTF-32', 'UTF-8'), 'UTF-8', 'UTF-32') )
		$str = mb_convert_encoding($str, 'UTF-8', mb_detect_encoding($str));
	$str = htmlentities($str, ENT_NOQUOTES, 'UTF-8');
	$str = preg_replace('`&([a-z]{1,2})(acute|uml|circ|grave|ring|cedil|slash|tilde|caron|lig);`i', '\1', $str);
	$str = html_entity_decode($str, ENT_NOQUOTES, 'UTF-8');
	$str = preg_replace(array('`[^a-z0-9]`i','`[-]+`'), '-', $str);
	$str = strtolower( trim($str, '-') );
	return $str;
}
function url() {
    $url = $_SERVER['HTTP_REFERER'];
    $escaped_url = htmlspecialchars($url, ENT_QUOTES, 'UTF-8');
    $escaped_url = str_replace(basename($_SERVER["SCRIPT_FILENAME"]), "", $escaped_url);
    return $escaped_url;
}

function clean_input($input) {
    $input = trim($input);
    $input = strip_tags($input);
    return $input = htmlspecialchars($input);
}

function set_email_template($contentModel,
        $replaces) {
    $data['title'] = $contentModel['title'];
    $data['subject'] = $contentModel['subject'];
    $data['body'] = $contentModel['body'];
    $data['footer'] = $contentModel['footer'];

    foreach ($replaces as $key => $replace) {
        $data['body'] = str_replace("%%" . $key . "%%", $replace, $data['body']);
    }

    return $data;
}

function generate_random_string($length = 10) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}


function validateDate($date)
{
    $d = DateTime::createFromFormat('Y-m-d', $date);
    return $d && $d->format('Y-m-d') == $date;
}


function ValidateText($text)
{
    $text = strip_tags($text);
    if (!preg_match("/^[a-zA-Z ]*$/",$text)) 
    {
        $message="Only letters and white space allowed!";
        return $message;
    }
}
function ValidateEmail($email)
{
     $email = filter_var($email, FILTER_SANITIZE_EMAIL);
     
     if(empty($email))
     {
         $emailErr = "Your Email is required!";
     }
    else if(strlen($email)<6)
    {
        $emailErr="You can enter name only minimum 6 characters in email!";
    }
    else if(strlen($email)>60)
    {
        $emailErr="You can enter name only maximum 60 characters in email!!";
    }
    else if (!preg_match("/^[_A-Za-z0-9-\+]+(\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\.[A-Za-z0-9]+)*(\.[A-Za-z]{2,})$/",$email)) 
    {
       $emailErr="Your Email is Invalid!";
    }
     else if(filter_var($email, FILTER_VALIDATE_EMAIL)===false)
     {
         $emailErr = "Your Email is Invalid!";
     }
    
     return $emailErr;
     
}
function ValidatePassword($password)
{
    
    if(empty($password)) {
        $cpasswordErr = "Your Password is required!";
    }
    else if (strlen($password) < '6') {
        $passwordErr = "Your Password Must Contain At Least 6 Characters!";
    }
    /*elseif(!preg_match("#[0-9]+#",$password)) {
        $passwordErr = "Your Password Must Contain At Least 1 Number!";
    }
    elseif(!preg_match("#[A-Z]+#",$password)) {
        $passwordErr = "Your Password Must Contain At Least 1 Capital Letter!";
    }
    elseif(!preg_match("#[a-z]+#",$password)) {
        $passwordErr = "Your Password Must Contain At Least 1 Lowercase Letter!";
    }*/ 
   
    return  $passwordErr;
}

function ValidateName($name)
{
    $name = strip_tags($name);
    if(empty($name))
    {
        $message="Please enter a  name!";
    }
    else if(strlen($name)<2)
    {
        $message="You can enter name only minimum 2 characters in name!";
    }
    else if(strlen($name)>60)
    {
        $message="You can enter name only maximum 60 characters in name!";
    }
    else if (!preg_match("/^[a-zA-Z ]*$/",$name)) 
    {
        $message="Only letters and white space allowed in name!";
        
    }
    return $message;
}
function ValidatePostal($code)
{
    $code = strip_tags($code);
    if(empty($code))
    {
        $message="Please enter a valid postal code!";
    }
    else if(strlen($code)<2)
    {
        $message="You can enter postal code only minimum 2 characters in postal code!";
    }
    else if(strlen($code)>10)
    {
        $message="You can enter postal code only maximum 10 characters in postal code!";
    }
    else if (!preg_match("/([A-Za-z0-9]+)/",$code)) 
    {
        $message="Only letters and numbers allowed in postal code!";
        
    }
    return $message;
}

function ValidatePhone($phone)
{
    $phone = strip_tags($phone);
    if(empty($phone))
    {
        $message="Please enter a phone no.!";
    }
    else if(strlen($phone)<2)
    {
        $message="You can enter phone no only minimum 2 characters in phone no.!";
    }
    else if(strlen($phone)>15)
    {
        $message="You can enter phone no only maximum 10 characters in phone no.!";
    }
    else if (!preg_match("/([0-9]*$)/",$code)) 
    {
        $message="Only numbers allowed in phone no. !";
        
    }
    return $message;
}
function PriceFormat($price)
{
    $price=number_format($price,2);
    $rprice="&dollar;".$price;
    return $rprice;
}
function DateFormat($date)
{
    $date=date("F j, Y",strtotime($date));
    return $date;
}
function TotalTime($seconds)
{
    $hours = floor($seconds / 3600);
    $seconds -= $hours * 3600;
    $minutes  = floor($seconds / 60);
    $seconds -= $minutes * 60;
    
    return sprintf('%02d:%02d', $hours, $minutes);
}
function redirectUrl()
{
    echo '<script>window.location = "' . $_SERVER["PHP_SELF"] . '";</script>';
}
?>