<?php

function d($arr,
        $stop = false) {

    echo "<pre>";
    print_r($arr);
    echo "<pre>";

    if ($stop) {
        die("end");
    }
}

function fetch_object($result) {
    $rows = mysqli_fetch_object($result);
    return $rows;
}

function num_rows($result) {
    $rows = mysqli_num_rows($result);
    return $rows;
}

function fetch_array($result) {
    $rows = mysqli_fetch_array($result);
    return $rows;
}

function query($result) {
    global $conn;
    $rows = mysqli_query($conn, $result);
    return $rows;
}

function insert_id($result) {
    global $conn;
    $id = mysqli_insert_id($conn);
    return $id;
}

function fetch_assoc($result) {
    $rows = mysqli_fetch_assoc($result);
    return $rows;
}
function charEsc($str)
{
        global $conn;
	$str = mysqli_real_escape_string($conn,htmlentities(trim($str),ENT_QUOTES));
	return $str;
}
function insert($table,$values)
{
	global $conn;
	$arrKeys = array_keys($values);
	$arrVals = array_values($values);
	$sql = sprintf('insert into %s (%s) values("%s")',$table,implode(',',$arrKeys), implode('","',$arrVals));
	
        $res = mysqli_query($conn,$sql);
        //mysqli_error($conn)
	if(!$res) {
		return FALSE;
	} else {
		return TRUE;
	}
}

function uploadImage($file,$dir,$cat) {
        $ext= pathinfo($file["name"],PATHINFO_EXTENSION);
	$uniq = $cat."_".uniqid().".".$ext;
	while(file_exists($dirnew = $dir . $uniq)) {
		$uniq++;
	}
	
	if(!getimagesize($file['tmp_name'])) {
		return false;
	} else {
		if(move_uploaded_file($file["tmp_name"], $dirnew)) {
			return $uniq;
		} else {
			return false;
		}
	}
}
function uploadColImage($file,$dir,$col) {
        $ext= pathinfo($file["name"],PATHINFO_EXTENSION);
	$uniq = $col.".".$ext;
	while(file_exists($dirnew = $dir . $uniq)) {
		$uniq++;
	}
	
	if(!getimagesize($file['tmp_name'])) {
		return false;
	} else {
		if(move_uploaded_file($file["tmp_name"], $dirnew)) {
			return $uniq;
		} else {
			return false;
		}
	}
}
function select($sql)
{
	global $conn;
	$res = mysqli_query($conn,$sql);
	if(!$res) {
		return FALSE;
	} else {
		$rowarr = array();
		$i = 0;
		while($row = mysqli_fetch_assoc($res))
		{
			$rowarr[$i] = $row;
			$i++;
		}
		return $rowarr;
	}
}


function sendemail($email,
        $subject,
        $msg,
        $from = array(),
        $bcc_address = "") {
    $mandrill = new Mandrill('jiidardfZPw6Ikl_9bJZSA');

    try {

        $message = array(
            'html' => $msg,
            'text' => strip_tags($msg),
            'subject' => $subject,
            'to' => array(
                array(
                    'email' => $email,
                    'type' => 'to'
                )
            ),
        );

        if (empty($from)) {
            $message['from_email'] = "";
            $message['name'] = "";
        } else {
            $message['from_email'] = $from['email'];
            $message['name'] = $from['name'];
        }

        if ($bcc_address != "") {
            $message['bcc_address'] = $bcc_address;
        }


        $async = false;
        $result = $mandrill->messages->send($message, $async);
    } catch (Mandrill_Error $e) {
        echo 'A mandrill error occurred: ' . get_class($e) . ' - ' . $e->getMessage();
        throw $e;
    }
}
/* Front End Functions */
function getCategories()
{
    $result= query("SELECT * FROM categories ORDER BY sort_order ASC");
   
    if(num_rows($result)>0)
    {        
        return $result;        
    }   
}
function getServices()
{
    $result= query("SELECT * FROM services where status=1 ORDER BY id DESC");
   
    if(num_rows($result)>0)
    {        
        return $result;        
    }   
}
function getServicesCat($id)
{
    //where type='services'
    $result= query("SELECT * FROM services  where category_id='{$id}' and status=1 ORDER BY id DESC");
   
    if(num_rows($result)>0)
    {        
        return $result;        
    } 
}
function getServicesId($id)
{
    $result= query("SELECT * FROM services  where id='{$id}' and status=1");
   
    if(num_rows($result)>0)
    {        
        return $result;        
    } 
}
/* Cart Functions */
function getDiscount($person)
{
    $query=select("SELECT * FROM discount where persons='{$person}'");
    return $query;
}
function ServicesCart($session_id,$person)
{
    $sql=($person)?" AND persons='{$person}'":'';
    $result= query("SELECT ca.*,ser.service_name,ser.price,ser.hours,ser.minutes,ser.image,ser.brand_id  FROM `cart` ca
    INNER JOIN  services ser ON ser.id=ca.service_id WHERE session_id='{$session_id}' $sql order by ser.id ASC");
   
    if(count($result)>0)
    {        
        return $result;        
    } 
}
function CheckPerson($session_id,$ser_id,$person)
{
    
    $result= query("SELECT * FROM `cart` WHERE session_id='{$session_id}' AND service_id='{$ser_id}' AND persons='{$person}'");
   
    if(num_rows($result)>0)
    {        
        return $result;        
    } 
}

function ServicesCartBooking($session_id,$person)
{
    $sql=($person)?" AND persons='{$person}'":'';
    $result= query("SELECT ca.*,ser.service_name,ser.price,ser.hours,ser.minutes,ser.image,ser.brand_id  FROM `cart` ca
    INNER JOIN  services ser ON ser.id=ca.service_id WHERE session_id='{$session_id}' AND  type='services' $sql order by ser.id ASC");
   
    if(num_rows($result)>0)
    {        
        return $result;        
    } 
}

function CheckService($id,$session_id)
{
    $result= query("SELECT * FROM cart WHERE service_id='{$id}' and session_id='{$session_id}'");
           
    return $result;        
    
}
function getCart($id)
{
    $result= query("SELECT * FROM cart WHERE id='{$id}'");
    if(num_rows($result)>0)
    {
        return $result;
    }
}

function RemoveItem($id)
{
    $result= query("DELETE FROM cart WHERE id='{$id}'");
    $color= query("DELETE FROM color_cart WHERE cart_id='{$id}'");
    
    if($result)
    {
        return TRUE;
    }
}
function RemoveCart($sess_id)
{
    $cart= query("DELETE FROM cart WHERE session_id='{$sess_id}'");
    $booking= query("DELETE FROM booking WHERE session_id='{$sess_id}'");
    $color= query("DELETE FROM color_cart WHERE session_id='{$sess_id}'");
    
    if($cart)
    {
        return TRUE;
    }
}
function CartTotal($sess_id)
{
    $result= query("SELECT SUM(total_price) AS sub_total FROM cart WHERE session_id='{$sess_id}'");
    if(num_rows($result)>0)
    {
        $row=fetch_array($result);
        $subtotal=$row["sub_total"];
        return $subtotal;
    }
}
function DeleteCart($sess_id)
{
    $cart= query("DELETE FROM cart WHERE session_id='{$sess_id}'");
    $color= query("DELETE FROM color_cart WHERE session_id='{$sess_id}'");
    
    if($cart)
    {
        return TRUE;
    }
}
function DeleteCartPerson($sess_id,$person)
{
    $cart= query("DELETE FROM cart WHERE session_id='{$sess_id}' AND persons='{$person}'");
    $color= query("DELETE FROM color_cart WHERE session_id='{$sess_id}' AND person='{$person}'");
    
    if($cart)
    {
        return TRUE;
    }
}
function UpdateCart($sess_id)
{
    $result= query("UPDATE `booking` SET `status`='booking' WHERE `session_id`='{$sess_id}'");

}
/* Booking Functions */
function Checkbooking($sess_id)
{
    $result= query("SELECT * FROM booking WHERE session_id='{$sess_id}'");
    if(num_rows($result)>0)
    {
        return $result;
    }
}
function CheckTime($date)
{
    $result= query("SELECT * FROM order_booking WHERE date='{$date}' and status='booked'");
           
    if(num_rows($result)>0)
    {        
        return $result;
    }  
}

function DeleteBooking($sess_id)
{
    $result= query("DELETE FROM booking WHERE session_id='{$sess_id}'");
    if($result)
    {
        return TRUE;
    } 
}
function UpdateBooking($id)
{
    $mDate=date("Y-m-d H:i:s");
    $result= query("UPDATE `booking` SET `session_id`='', `status`='booked',`modified_date`='{$mDate}' WHERE `id`='{$id}'");
    if($result)
    {
        return TRUE;
    } 
}
function UpdateOrderBook($order_id,$event_id)
{
    $mDate=date("Y-m-d H:i:s");
    $result= query("UPDATE `order_booking` SET `event_id`='{$event_id}',`modified_date`='{$mDate}' WHERE `order_id`='{$order_id}'");
    if($result)
    {
        return TRUE;
    } 
}

/* Color Functions */
function CheckColor($id,$session_id,$person)
{
    $result= query("SELECT ord.*,col.color_name,col.color_id FROM color_cart ord  INNER JOIN color_selector col ON ord.color_id=col.color_id 
    WHERE cart_id='{$id}' and session_id='{$session_id}' and person='{$person}'");
           
    return $result;   
}
function CartColor($id,$session_id)
{
    $result= query("SELECT * FROM color_cart WHERE cart_id='{$id}' and session_id='{$session_id}'");
           
    return $result;   
}
function OrderColor($id)
{
    $result= query("SELECT ord.*,col.color_name,col.color_id FROM order_colors ord
    INNER JOIN color_selector col ON ord.color_id=col.color_id  WHERE order_service_id='{$id}'");
           
    return $result;   
}
function RemoveColor($id)
{
    $color= query("DELETE FROM color_cart WHERE id='{$id}'");
    
    if($color)
    {
        return TRUE;
    }
}
function CheckColorid($colorid,$session_id,$person)
{
    $result= query("SELECT * FROM color_cart WHERE color_id='{$colorid}' and person='{$person}' and session_id='{$session_id}'");
           
    return $result;   
}
function ServicesColor($session_id,$person)
{
    $result= query("SELECT ca.*,ser.service_name,ser.price,ser.hours,ser.minutes,ser.image,ser.brand_id,ser.color_select  FROM `cart` ca
    INNER JOIN  services ser ON ser.id=ca.service_id WHERE session_id='{$session_id}'  AND persons='{$person}'
    AND type='services' AND color_select='1' order by ser.id ASC"); 
   
    if(num_rows($result)>0)
    {        
        return $result;        
    } 
}
function ServicesCartId($cart_id)
{
    $result= query("SELECT ca.*,ser.category_id  FROM `cart` ca
    INNER JOIN  services ser ON ser.id=ca.service_id WHERE cart_id='{$cart_id}'");
   
    if(count($result)>0)
    {        
        return $result;        
    } 
}

/* Products */
function getProducts()
{
   $result= query("SELECT * FROM services where type='products' and status=1 ORDER BY id DESC");
   
    if(num_rows($result)>0)
    {        
        return $result;        
    }  
}
function getProductbyid($id)
{
   $result= query("SELECT * FROM services where id='{$id}' and type='products' and status=1");
   
    if(num_rows($result)>0)
    {        
        return $result;        
    }  
}
/* Profile */
function getProfile($id)
{
    $result= query("SELECT * FROM users where id='{$id}'");
   
    if(num_rows($result)>0)
    {        
        return $result;        
    }  
}
/*Orders*/
function getOrders($user_id)
{
   $result= query("SELECT ord.*,pay.order_id,pay.payment_id FROM orders ord
   INNER JOIN  paypal pay ON ord.id=pay.order_id  WHERE ord.user_id='{$user_id}' order by ord.id DESC");
           
    return $result;
}
function getOrderId($id)
{
   $result= query("SELECT ord.*,pay.order_id,pay.payment_id FROM orders ord
   INNER JOIN  paypal pay ON ord.id=pay.order_id  WHERE ord.id='{$id}'");
           
    return $result;
}
function OrderItems($id,$person)
{
    $sql=($person)?" AND ord.persons='{$person}'":'';
    $result= query("SELECT ord.*,ser.service_name,ser.image,ser.type  FROM `order_services` ord
    INNER JOIN services ser ON ord.service_id=ser.id WHERE ord.order_id='{$id}' $sql");
   
    if(num_rows($result)>0)
    {        
        return $result;        
    } 
       
}
function OrderBook($id)
{
    $result= query("SELECT * FROM `order_booking` WHERE order_id='{$id}'");
   
    if(num_rows($result)>0)
    {        
        return $result;        
    } 
   
    
}
function getContent($code)
{
   $result= query("SELECT * FROM contents where code='{$code}' and type='page' and status=1");
   
    if(num_rows($result)>0)
    {        
        return $result;        
    }  
}
function getSlider()
{
   $result= query("SELECT * FROM contents where type='slider' and code='slider' and status=1 order by id desc limit 10");
  
    if(num_rows($result)>0)
    {        
        return $result;        
    }  
}
function getGalleryImages()
{
   $result= query("SELECT * FROM contents where type='slider' and code='gallery' and status=1 order by id desc limit 10");
  
    if(num_rows($result)>0)
    {        
        return $result;        
    }  
}
function getBlock($code)
{
   $result= query("SELECT * FROM contents where code='{$code}' and type='block' and status=1");
  
    if(num_rows($result)>0)
    {        
        return $result;        
    }  
}
function getTestimonials()
{
   $result= query("SELECT * FROM contents where type='test' and status=1 order by id desc limit 10");
  
    if(num_rows($result)>0)
    {        
        return $result;        
    }  
}
function getFooter($code)
{
   $result= query("SELECT * FROM contents where type='footer' and code='{$code}' and status=1");
  
    if(num_rows($result)>0)
    {        
        return $result;        
    }  
}
function getMenus()
{
   $result= query("SELECT men.*,con.code FROM `menus` men
    LEFT JOIN contents con ON men.content_id=con.id order by men.sort_id ASC limit 7");
   
    if(num_rows($result)>0)
    {        
        return $result;        
    }  
}
function getPhotos($code)
{
   $result= query("SELECT * FROM contents where code='{$code}' and type='block' and status=1 limit 10");

   
    if(num_rows($result)>0)
    {        
        return $result;        
    }  
}

?>
