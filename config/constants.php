<?php
date_default_timezone_set("America/Chicago");
define('currency_code', 'USD');
define('currency_symbol', '$');
define('currency_title', 'US Dollar');

$siteAdminUsersRole = array(
    1 => 'Super Admin',
    2 => 'Admin'
);

$storeCurrency = array(
    1 => 'CAD',
    2 => '$',
    3 => 'CAD Dollar'
);

$siteAdminStatus = array(
    0 => 'Inactive',
    1 => 'Active'
);

$imageCategory = array(
    1 => 'Footer Images',
    2 => 'Gallery'
);

$webData = array(
    'copyright' => 'Copyright © 2017, AKA Warrior Cup All Rights Reserved.',
    'tournament' => '844.AKA.WCUP',
    'phone' => '844.252.9287',
    'registration' => '855.969.4MAT',
    'regPhone' => '855.969.4628',
    'hotelMain' => '312-565-1234',
    'hotelReservation' => '888-421-1442',
);

$adminTitle = array(
    'copyright' => 'Powered By <a href="http://www.golpik.com" target="_blank">Golpik</a> Ritzy Nails © '.date('Y'),
    'name' => 'Ritzy Nails');

$regEmail = array(
    'email' => 'info@ritzynails.com',
    'contact_subject' => 'Contact form submitted NASKA');

$content_types = array('page'=>'Page',
            'slider'=>'Slider',
            'block'=>'Block',
            'test'=>'Testimonial',
            'footer'=>'Footer'
    ); 

$competitorCounty = array(
    1 => 'United State',
    2 => 'Canada'
);



$currentTime = date("Y-m-d H:i:s");
$correctMessage = "By clicking correct you are confirming that your name is spelled correctly and that the displayed ratings are accurate per Mr. Carnahan's FINAL release of the 2016 NASKA Ratings.";

/*Paypal Integration*/
//Set useful variables for paypal form
$paypalURL = 'https://www.sandbox.paypal.com/cgi-bin/merchantpaymentweb'; //Test PayPal API URL
$paypalID = 'amoos.golpik-facilitator@gmail.com'; //Business Email
$paypalPass = 'DK7DZ7Z3L9RC3R3F'; 
$paypalSign = 'AFcWxV21C7fd0v3bYYYRCpSSRl31ABJd3G0clpl7cqkIHfgTXHytQCJu'; 
$paypalSuccess = 'success.php'; //Success
$paypalCancel = 'cancel.php'; //Cancel
$secretkey="6Le_ixQUAAAAAI5uQP9_rMY-APsX56Tc2qV9MDTl";

$account="amoos.golpik@gmail.com";

//define('CLIENT_ID', 'AamSLlxcvd1orEcUyZ9qup4-CzfDrdK1AeJ8eQIrAd38Rq7ewD47RW5uo-iU240AMJsg6eEBazhkNC7Q'); //your PayPal client ID
//define('CLIENT_SECRET', 'EOWGTizj-0YfsRRs81mChLZkiaQw48qdqcEXv9MPRJ3v8yfML0IS5uIEIOX4FywNtJKAgggCq3q3f_5C'); //PayPal Secret
define('CLIENT_ID', 'Aag7PucHYtXvTp9VmCyqmu3gfU4XrTSdHKuyegxlRjMiu1ZQ9umyOEI0oviapMhEiW7tw5nbSkNXcUJe'); //your PayPal client ID
define('CLIENT_SECRET', 'ELIfjhxSD1IBaQvWvyHzLAtTbP3EowNVUyJJiDiIG29XCYGHUl5U9Q3KHO-7EcMz8a1FO9JPDk6Jpki-'); //PayPal Secret
define('RETURN_URL', 'http://ecommercehouse.com/projects/ritzynails/success.php'); //return URL where PayPal redirects user
define('CANCEL_URL', 'http://ecommercehouse.com/projects/ritzynails/index.php'); //cancel URL
define('PP_CURRENCY', 'CAD'); //Currency code
define('PP_CONFIG_PATH', 'sdk_config.ini'); //PayPal config path (sdk_config.ini)
?>
