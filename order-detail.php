<?php 
include_once('config/connection.php');
include_once('config/session.php');
include_once('config/model.php');
include_once('config/functions.php');
include_once('config/constants.php');

if(isset($_GET["id"]))
{
       $id=charEsc($_GET["id"]); 
       $result=getOrderId($id);
       if(count($result)>0)
       {
             $order=fetch_array($result);
            
       }
       $items=OrderItems($id,'');
       $books=OrderBook($id);
       if(count($books)>0)
       {
             $book=fetch_array($books);
            
       }
}
else
{
     echo "<script type='text/javascript'>window.location='index.php';</script>";
}

$pagetitle="Order Information";
include("header.php"); 
?>

	<section class="services-page">
		<div class="container">
			 <div class="hed">
				  <h2>Order Information<span></span></h2>
			 </div> 
			<?php
            
                if(count($items)>0)
                {
                     
              ?>
                    
		<div class="cart-area">
                            
              <div class="table-total-area col-sm-6">
                <table class="table  table-bordered">
                    <thead>
                    <tr><th colspan="2">Order Detail</th>
                    </tr>
                    </thead>

                    <tbody>
                    <tr>
                        <td>Order ID</td>
                        <td><?php echo $order["id"]; ?></td>
                    </tr>
                    <tr>
                        <td>Payment ID</td>
                        <td><?php echo $order["payment_id"]; ?></td>
                    </tr>
                     <tr>
                        <td>Order Status</td>
                        <td><?php echo $order["order_status"]; ?></td>
                    </tr>
                    <tr>
                        <td>Order Date</td>
                        <td><?php echo DateFormat($order["created_date"]); ?></td>
                    </tr>
                    <tr>
                        <td>Mobile fee</td>
                        <td><?php echo ($order["mobile_fee"]!=0)?PriceFormat($order["mobile_fee"]):''; ?></td>
                    </tr>
                   
                    <tr>
                        <td>Parking fee</td>
                        <td><?php echo ($order["parking"]!='')?PriceFormat($order["parking"]):'$0.00'; ?></td>
                    </tr>
                    
                    <tr class="total-area-btns">
                         <td>Order Total</td>
                        <td><?php echo PriceFormat($order["grand_total"]); ?></td>   
                    </tr>

                </tbody>
                </table>
            </div>  
            <div class="table-total-area col-sm-6">
                <table class="table  table-bordered">
                    <thead>
                    <tr>
                        <th colspan="2">Booking Detail</th>
                    </tr>
                    </thead>

                    <tbody>
                   
                    <tr>
                        <td>Date</td>
                        <td><?php echo DateFormat($book["date"]); ?></td>
                    </tr>
                     <tr>
                        <td>Start Time</td>
                        <td><?php echo $book["start_time"]; ?></td>
                    </tr>
                    <tr>
                        <td>End Time</td>
                        <td><?php echo $book["end_time"]; ?></td>
                    </tr>
                    <tr>
                        <td>Booking Status</td>
                        <td><?php echo $book["status"]; ?></td>
                    </tr>
                    <tr>
                        <td>Postal Code</td>
                        <td><?php echo $order["postal_code"]; ?></td>
                    </tr>
                    <tr>
                        <td>Address</td>
                        <td><?php echo $order["address"]; ?></td>
                    </tr>
                    
                   
                </tbody>
                </table>
            </div>
                    <div class="clearfix"></div>        
			
		   <div class="table-area  col-sm-12">
                    <table class="table cart-item-table table-bordered table-valign">
                        <thead>
                       <tr>
                                <th>Persons</th>
                                <th>Image</th>
                                <th>Service / Product</th>
                                <th>Price</th> 
                                <th>Color</th>
                        </tr>
                        </thead> 
                        <tbody>
                            <?php 
                                for($i=1;$i<=$order["person"];$i++)
                                {
                                $service_image='';
                                $service_name='';
                                $service_price='';
                                $service_color=array();
                                $items=OrderItems($id,$i);
                                while($item = fetch_object($items))
                                {
                                    
                                    $service_image.='<span class="pic"><img src="'.$item->image.'"  /></span><br/>';
                                    $service_name.=$item->service_name."<br/>";
                                    $service_price.='<span class="txt-price">'.PriceFormat($item->total_price)."</span><br/>";
                                   if($item->type=='services') {
                                    
                                    $colors=OrderColor($item->id);
                                    if(count($colors)>0)
                                    {
                                      while($color=fetch_object($colors))
                                      {
                                          $service_color[]=$color->color_name.'<br/>';
                                      }
                                      
                                    }
                                    
                                    } 
                                }
                                   
                                ?>
                               
                            <tr>
				<td>Person <?php echo $i; ?></td>
                                <td class="cart__table__image">
                                    <?php echo $service_image; ?>
                                </td>
                                <td><?php echo $service_name; ?></td>
                                <td><?php echo $service_price; ?></td>
                                <td><?php 
                                if(count($service_color)>0)
                                {
                                    foreach($service_color as $sercolor)
                                    {
                                        echo $sercolor;
                                    }
                                }
                                 ?>
                                </td>
                              

                            </tr>
                            <?php 
                                }
                               
                            ?>
                            
                        
                        </tbody>
                    </table>
              
            </div>
                            
			<div class="clearfix"></div>
	
			</div>
		<?php
                    }
               
              else
                {
                ?>
      <div class="alert alert-success fade in alert-dismissable">
    <h4 class="text-center"><i class="fa fa-check-circle"></i> Order information not found!</strong></h4></div>
		<?php
                }
                ?>
 
					 
		</div>
	</section>
	
<div id="myModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content" >
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title" id="myModalLabel">Colors</h4>
</div>
<div class="modal-body" id="modalContent" style="display:none;">
 
</div>
 <div class="modal-footer">
       <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
</div>
            </div>
        </div>
</div>	
<script>
$(".cdetail[data-toggle=modal]").click(function()  {
var essay_id = $(this).attr('id');
        $.ajax({
            cache: false,
            type: 'POST',
            url: 'detail.php',
            data: 'id='+essay_id,
            success: function(data) 
            {
                $('#myModal').show();
                $('#modalContent').show().html(data);
            }
        });
    });

</script>	
<?php include("footer.php"); ?>