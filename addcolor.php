<?php
extract($_POST);
include_once('config/connection.php');
include_once('config/model.php');

if(isset($addcolor))
{
    $cart_id= charEsc($cartid);
    $person= charEsc($persons);
    $color_id= charEsc($colorid);
    $french_manicure= charEsc($french_manicure);
    $session_id=$_SESSION["cart"];

    
    if(!empty($cart_id) && !empty($person) && !empty($color_id) && !empty($session_id))
    {
      
        $result= CheckColor($cart_id,$session_id,$person);
       
        $count=1;
        if(num_rows($result)>0)
        {      
            /*$mDate=date("Y-m-d H:i:s");
            $query=query("UPDATE `color_cart` SET `color_id`='{$color_id}',`french_manicure`='{$french_manicure}',`modified_date`='{$mDate}'
            WHERE `cart_id`='{$cart_id}' and `session_id`='{$session_id}' and `person`='{$person}'");
            if($query)
            {
                echo '<div class="alert alert-success fade in alert-dismissable">
    <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
     <i class="fa fa-check-circle"></i><strong> person color updated to successfully!</strong></div>';

            }
            else
            {
                echo '<div class="alert alert-danger fade in alert-dismissable" >
    <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
      <i class="fa fa-times-circle"></i> <strong> person color cannot update!</strong></div>';
            } */
            
            while($row=fetch_object($result))
            {
                $count++;
            }
            
        } 
        
    
        $category=query("SELECT ca.*,ser.category_id  FROM `cart` ca
        INNER JOIN  services ser ON ser.id=ca.service_id WHERE ca.id='{$cart_id}'");
        
        $counter=3;
        if(num_rows($category)>0)
        {
             $row_category= fetch_array($category);
        
            if($row_category['category_id']==3)
            {
                $counter=6;
            }
            
        }
       
       
        
        if($count<=$counter) 
        {
            $result= CheckColorid($color_id,$session_id,$person);
           
            if(num_rows($result)>0)
            {
                 echo '<div class="alert alert-warning fade in alert-dismissable">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
                     <i class="fa fa-info-circle"></i> <strong> Color already added!</strong></div>';
            }
            else
            {
                $cDate=date("Y-m-d H:i:s");
                $addcolor=array("session_id"=>$session_id,"cart_id"=>$cart_id,"color_id"=>$color_id,"person"=>$person,"french_manicure"=>$french_manicure,"created_date"=>$cDate);
                $query= insert('color_cart',$addcolor);
                if($query)
                {

                     echo '<div class="alert alert-success fade in alert-dismissable">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
                    <i class="fa fa-check-circle"></i><strong> Person color added to successfully!</strong></div>';


                }
                else
                {
                    echo '<div class="alert alert-danger fade in alert-dismissable">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
                     <i class="fa fa-times-circle"></i> <strong>Person color cannot added!</strong></div>';

                }
            }
           

        }
        else
        {
              echo '<div class="alert alert-warning fade in alert-dismissable">
                <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
                 <i class="fa fa-info-circle"></i> <strong>Color can be added '.$counter.' for this service!</strong></div>';
        }
        
       
    }
    else
    {
          echo '<div class="alert alert-danger fade in alert-dismissable" >
                <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
                 <i class="fa fa-times-circle"></i> Color,person or service is empty</div>';
    }
    
    
    exit(); 
}

if(isset($delcolor))
{
    $id= charEsc($id);
    if(!empty($id))
    {
        $res=RemoveColor($id);
        if($res)
        {
             echo '<div class="alert alert-success fade in alert-dismissable">
                <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
                 <i class="fa fa-check-circle"></i> <strong>Color has been deleted!</strong></div>';
        }
    }
    else
    {
         echo '<div class="alert alert-danger fade in alert-dismissable" >
                <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
                 <i class="fa fa-times-circle"></i> Color is empty</div>';
    }
}    
?>