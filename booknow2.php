<?php 
include_once('config/connection.php');
include_once('config/model.php');
include_once('config/functions.php');

if(isset($_GET["id"]))
{
     $cat_id= charEsc($_GET["id"]);
     if(!empty($cat_id))
     {
        $services = getServicesCat($cat_id); 
     }
     
}
else
{
    $services= getServices(); 
}

$pagetitle="Services";
include("header.php");
?>

<section class="services-page booknow-page">
		<div class="container">
			 <div class="hed">
				  <h2>Book Now<span></span></h2>
			 </div> 
            
			
		    <div class="booktab-area">
			    
				<div class="form-group col-sm-6 col-sm-offset-3 p0">
					<div class="col-sm-3"><label>Select Persons</label></div>
					<div class="col-sm-6">
					<select class="form-control persons" id="sel-persons">
							<option value="1" selected="">1</option>
							<option value="2">2</option>
							<option value="3">3</option>
							<option value="4">4</option>
					</select>
					</div>
				</div>	

        <div class="booktable-area table-responsive col-sm-8 col-sm-offset-2 ">
<table class="table">
        <thead>
            <tr>
                <th  for="person">Person</th>
                <th  for="service">Service / Product</th>
                <th  for="">Price</th>
            </tr>
        </thead>
		<tbody>
			<tr>
            	<td  for="person">Person 1</td>
				<td  for="service">service 1 <br> service 2 <br> service 3</td>
				<td  for="price">$xx</td>
            </tr>
			<tr>
            	<td  for="person">Person 2</td>
				<td  for="service">service 6</td>
				<td  for="price">$xx</td>
            </tr>
			<tr>
            	<td  for="person">Person 3</td>
				<td  for="service">service 4 <br> service 5</td>
				<td  for="price">$xx</td>
            </tr>
			<tr>
            	<td  for="person">Person 4</td>
				<td  for="service"></td>
				<td  for="price">$xx</td>
            </tr>	
			<tr>
            	<td  for="person"></td>
				<td  for="service">Mobile Free 4 x YY <br> (zz% discount)</td>
				<td  for="price">$xx</td>
            </tr>			
			<tr>
            	<td  for="person"></td>
				<td  for="service">
				<label>
				    <input type="checkbox" name="parking" id="parking">
					<strong>Free parking is available nearby</strong>
				</label>
				</td>
				<td  for="price">$6.00</td>
            </tr>
			<tr>
            	<td  for="person"></td>
				<td  for="service">SubTotal</td>
				<td  for="price">$xx</td>
            </tr>				
		


		</tbody>
  </table>		     
        </div> 		
			        
        <div class="booktab-nav col-sm-12 p0">    
                <!-- Nav tabs -->
				<ul class="nav nav-tabs">
				  <li class="active"><a href="#client1" data-toggle="tab">Client1</a></li>
				  <li><a href="#client2" data-toggle="tab">Client2</a></li>
				  <li><a href="#client3" data-toggle="tab">Client3</a></li>
				  <li><a href="#client4" data-toggle="tab">Client4</a></li>
				</ul>

				<!-- Tab panes -->
				<div class="tab-content">
				  <div class="tab-pane active" id="client1">
				        <div class="services services-item">
							<ul>
							  <li class="img"><span class="services__img"><img src="images/services/service_58773e99cd453.png" alt=""></span></li>
							  <li class="title">

								<div class="panel-group " id="accordion1">

								  <div class="panel panel-default">
									<div class="panel-heading">
									  <h4 class="panel-title">
											<a data-toggle="collapse" data-parent="#accordion1" href="#collapseArr1">
												Pédicure Française Shellac	
											</a>
										</h4>
									</div>
									<div id="collapseArr1" class="panel-collapse collapse">
									  <div class="panel-body">
										<p>Le limage des ongles, le soin des cuticules, le retrait des callosités, un massage avec une lotion aux extraits de chèvrefeuille et de pamplemousse rose, le soin à l’huile de jojoba, la pose du vernis à ongles Shellac de la couleur de votre
										  choix et une manucure à la française.</p>

										<p><strong>SHELLAC FRENCH PEDICURE </strong></p>

										<p>The nail shaping, cuticle care, removal of calluses, a massage with lotion with extracts of honeysuckle and pink grapefruit, care with jojoba oil, laying the Shellac polish color of your choice and a French manicure.</p>
									  </div>
									</div>
								  </div>

								</div>


							  </li>
							  <li class="price">
								<table class="table">
								  <tbody>
									<tr>
									  <td>Time</td>
									  <td>Price</td>
									</tr>
									<tr>
									  <td><span class="services__time"><span class="hour">1 hr</span> <span class="mins">20 mins</span></span>
									  </td>
									  <td><span class="services__price" id="">$60.00</span></td>
									</tr>
								  </tbody>
								</table>

							  </li>
							  <li class="cta">


                                  
								<input type="hidden" id="item_id0" name="item_id" value="4">
								<input type="hidden" id="person0" name="person" value="1">
								<input type="hidden" id="discount0" name="discount" value="">
								<input type="hidden" id="totaltime0" name="totaltime" value="1:20">
								<input type="hidden" id="gettotalprice0" name="total_price" value="79.95">
								<button class="btn btn-book" id="btn-add-to-cart" onclick="cart(0)">Add to Cart</button>

							  </li>

							</ul>						
                        </div>	
				        <div class="services services-item">
							<ul>
							  <li class="img"><span class="services__img"><img src="images/services/service_58773e99cd453.png" alt=""></span></li>
							  <li class="title">

								<div class="panel-group " id="accordion2">

								  <div class="panel panel-default">
									<div class="panel-heading">
									  <h4 class="panel-title">
											<a data-toggle="collapse" data-parent="#accordion2" href="#collapseArr2">
												Pédicure Française Shellac	
											</a>
										</h4>
									</div>
									<div id="collapseArr2" class="panel-collapse collapse">
									  <div class="panel-body">
										<p>Le limage des ongles, le soin des cuticules, le retrait des callosités, un massage avec une lotion aux extraits de chèvrefeuille et de pamplemousse rose, le soin à l’huile de jojoba, la pose du vernis à ongles Shellac de la couleur de votre
										  choix et une manucure à la française.</p>

										<p><strong>SHELLAC FRENCH PEDICURE </strong></p>

										<p>The nail shaping, cuticle care, removal of calluses, a massage with lotion with extracts of honeysuckle and pink grapefruit, care with jojoba oil, laying the Shellac polish color of your choice and a French manicure.</p>
									  </div>
									</div>
								  </div>

								</div>


							  </li>
							  <li class="price">
								<table class="table">
								  <tbody>
									<tr>
									  <td>Time</td>
									  <td>Price</td>
									</tr>
									<tr>
									  <td><span class="services__time"><span class="hour">1 hr</span> <span class="mins">20 mins</span></span>
									  </td>
									  <td><span class="services__price" id="">$60.00</span></td>
									</tr>
								  </tbody>
								</table>

							  </li>
							  <li class="cta">


                                  
								<input type="hidden" id="item_id0" name="item_id" value="4">
								<input type="hidden" id="person0" name="person" value="1">
								<input type="hidden" id="discount0" name="discount" value="">
								<input type="hidden" id="totaltime0" name="totaltime" value="1:20">
								<input type="hidden" id="gettotalprice0" name="total_price" value="79.95">
								<button class="btn btn-book" id="btn-add-to-cart" onclick="cart(0)">Add to Cart</button>

							  </li>

							</ul>						
                        </div>							
				  </div>
				  <div class="tab-pane" id="client2">
				        <div class="services services-item">
							<ul>
							  <li class="img"><span class="services__img"><img src="images/services/service_58774113baacc.png" alt=""></span></li>
							  <li class="title">

								<div class="panel-group " id="accordion5">

								  <div class="panel panel-default">
									<div class="panel-heading">
									  <h4 class="panel-title">
											<a data-toggle="collapse" data-parent="#accordion5" href="#collapseArr5">
												Pédicure Française Shellac	
											</a>
										</h4>
									</div>
									<div id="collapseArr5" class="panel-collapse collapse">
									  <div class="panel-body">
										<p>Le limage des ongles, le soin des cuticules, le retrait des callosités, un massage avec une lotion aux extraits de chèvrefeuille et de pamplemousse rose, le soin à l’huile de jojoba, la pose du vernis à ongles Shellac de la couleur de votre
										  choix et une manucure à la française.</p>

										<p><strong>SHELLAC FRENCH PEDICURE </strong></p>

										<p>The nail shaping, cuticle care, removal of calluses, a massage with lotion with extracts of honeysuckle and pink grapefruit, care with jojoba oil, laying the Shellac polish color of your choice and a French manicure.</p>
									  </div>
									</div>
								  </div>

								</div>


							  </li>
							  <li class="price">
								<table class="table">
								  <tbody>
									<tr>
									  <td>Time</td>
									  <td>Price</td>
									</tr>
									<tr>
									  <td><span class="services__time"><span class="hour">1 hr</span> <span class="mins">20 mins</span></span>
									  </td>
									  <td><span class="services__price" id="">$60.00</span></td>
									</tr>
								  </tbody>
								</table>

							  </li>
							  <li class="cta">


                                  
								<input type="hidden" id="item_id0" name="item_id" value="4">
								<input type="hidden" id="person0" name="person" value="1">
								<input type="hidden" id="discount0" name="discount" value="">
								<input type="hidden" id="totaltime0" name="totaltime" value="1:20">
								<input type="hidden" id="gettotalprice0" name="total_price" value="79.95">
								<button class="btn btn-book" id="btn-add-to-cart" onclick="cart(0)">Add to Cart</button>

							  </li>

							</ul>						
                        </div>					  
				  
				  </div>
				  <div class="tab-pane" id="client3">
				        <div class="services services-item">
							<ul>
							  <li class="img"><span class="services__img"><img src="images/services/services_58787ed88dcac.jpg" alt=""></span></li>
							  <li class="title">

								<div class="panel-group " id="accordion3">

								  <div class="panel panel-default">
									<div class="panel-heading">
									  <h4 class="panel-title">
											<a data-toggle="collapse" data-parent="#accordion3" href="#collapseArr3">
												Pédicure Française Shellac	
											</a>
										</h4>
									</div>
									<div id="collapseArr3" class="panel-collapse collapse">
									  <div class="panel-body">
										<p>Le limage des ongles, le soin des cuticules, le retrait des callosités, un massage avec une lotion aux extraits de chèvrefeuille et de pamplemousse rose, le soin à l’huile de jojoba, la pose du vernis à ongles Shellac de la couleur de votre
										  choix et une manucure à la française.</p>

										<p><strong>SHELLAC FRENCH PEDICURE </strong></p>

										<p>The nail shaping, cuticle care, removal of calluses, a massage with lotion with extracts of honeysuckle and pink grapefruit, care with jojoba oil, laying the Shellac polish color of your choice and a French manicure.</p>
									  </div>
									</div>
								  </div>

								</div>


							  </li>
							  <li class="price">
								<table class="table">
								  <tbody>
									<tr>
									  <td>Time</td>
									  <td>Price</td>
									</tr>
									<tr>
									  <td><span class="services__time"><span class="hour">1 hr</span> <span class="mins">20 mins</span></span>
									  </td>
									  <td><span class="services__price" id="">$60.00</span></td>
									</tr>
								  </tbody>
								</table>

							  </li>
							  <li class="cta">


                                  
								<input type="hidden" id="item_id0" name="item_id" value="4">
								<input type="hidden" id="person0" name="person" value="1">
								<input type="hidden" id="discount0" name="discount" value="">
								<input type="hidden" id="totaltime0" name="totaltime" value="1:20">
								<input type="hidden" id="gettotalprice0" name="total_price" value="79.95">
								<button class="btn btn-book" id="btn-add-to-cart" onclick="cart(0)">Add to Cart</button>

							  </li>

							</ul>						
                        </div>					  
				  </div>
				  <div class="tab-pane" id="client4">
				        <div class="services services-item">
							<ul>
							  <li class="img"><span class="services__img"><img src="images/services/service_587744acc0f6a.jpg" alt=""></span></li>
							  <li class="title">

								<div class="panel-group " id="accordion4">

								  <div class="panel panel-default">
									<div class="panel-heading">
									  <h4 class="panel-title">
											<a data-toggle="collapse" data-parent="#accordion4" href="#collapseArr4">
												Pédicure Française Shellac	
											</a>
										</h4>
									</div>
									<div id="collapseArr4" class="panel-collapse collapse">
									  <div class="panel-body">
										<p>Le limage des ongles, le soin des cuticules, le retrait des callosités, un massage avec une lotion aux extraits de chèvrefeuille et de pamplemousse rose, le soin à l’huile de jojoba, la pose du vernis à ongles Shellac de la couleur de votre
										  choix et une manucure à la française.</p>

										<p><strong>SHELLAC FRENCH PEDICURE </strong></p>

										<p>The nail shaping, cuticle care, removal of calluses, a massage with lotion with extracts of honeysuckle and pink grapefruit, care with jojoba oil, laying the Shellac polish color of your choice and a French manicure.</p>
									  </div>
									</div>
								  </div>

								</div>


							  </li>
							  <li class="price">
								<table class="table">
								  <tbody>
									<tr>
									  <td>Time</td>
									  <td>Price</td>
									</tr>
									<tr>
									  <td><span class="services__time"><span class="hour">1 hr</span> <span class="mins">20 mins</span></span>
									  </td>
									  <td><span class="services__price" id="">$60.00</span></td>
									</tr>
								  </tbody>
								</table>

							  </li>
							  <li class="cta">


                                  
								<input type="hidden" id="item_id0" name="item_id" value="4">
								<input type="hidden" id="person0" name="person" value="1">
								<input type="hidden" id="discount0" name="discount" value="">
								<input type="hidden" id="totaltime0" name="totaltime" value="1:20">
								<input type="hidden" id="gettotalprice0" name="total_price" value="79.95">
								<button class="btn btn-book" id="btn-add-to-cart" onclick="cart(0)">Add to Cart</button>

							  </li>

							</ul>						
                        </div>					  
				  </div>
				</div>	
				
            </div>			
		</div>	 
        
			 

                             
                                                          
                        
                             
                                                          
               
                             
                                                          

                             
                                                          
      
                             
                                                          
      
                             
                                                          

                             
                                                          

                             
                                                             
				    <div class="clearfix"></div>
			  
					<div class="btn-group mb30 pul-rgt">
						<a href="appointment.php" class="btn btn-book btn-lg pul-rgt">Continue</a>
					</div>
			 
			 
			 </div>
			 
		</div>
	</section>


	<script>

    var updateTotal = function() {
 
    var sumtotal;
    var sum = 0;
    //Add each product price to total
    $(".services").each(function(index) {
           
        //console.log(i);
        var price = $(this).data('price');
        var person = $('.persons :selected', this).val();
       
            
        var hour = $('.hour', this).text().split('hr');
        var mins = $('.mins', this).text().split('mins');
        var totaltime;
        var tohour = hour[0]*person;
         
        var minutes =  mins[0]*person;
        var tmins = minutes % 60;
        var hours = Math.floor(minutes / 60);
         //console.log(hours);
        var thour = parseInt(tohour) + hours;

        totaltime=thour+":"+tmins;
        
        //Total for one product
        var subtotal = price*person;
        //Round to 2 decimal places.
        subtotal = parseFloat(subtotal.toFixed(2)); 
        //Display subtotal in HTML element
        
        $('.stotal', this).html(subtotal);
        $('.totaltime', this).html(totaltime);
        
       
             $.ajax({
              url:'getdiscount.php',
              type:'GET',
              data:"person="+person+"&discount=1",
              dataType : 'json',
              success:function(data)
              {
                  var fee=parseFloat(data.person_price);
                  var feetot=parseFloat(data.discount_price);
                  var dis=parseInt(data.discount);
          
                  var totalprice=subtotal+fee;
                 
                  if(data.persons!='1')
                  {
                      $('#ddiscount'+index).show();
                      $('#sdiscount'+index).text(dis+"%");
                      $('#discount'+index).val(dis);
                  }
                  else
                  {
                      $('#ddiscount'+index).hide();
                      $('#sdiscount'+index).text('');
                      $('#discount'+index).val('');
                  }
                  $('#totaltime'+index).val(totaltime);
                  $('#person'+index).val(person);
                  $('#fee'+index).text(fee)
                  $('#totalprice'+index).text("$"+totalprice.toFixed(2));
                  $('#gettotalprice'+index).val(totalprice.toFixed(2));
                 
              }
             
             });     
      
       
    
    });
    // total
       $('.productTotal').each(function() {
        sum += Number($(this).html());
    }); 
    
    

    $('#sum').html(sum.toFixed(2));
};

//Update total when quantity changes
$(".services .persons").change(function() {
    updateTotal();
});

//Update totals when page first loads
updateTotal();
   $(".services").each(function(index) { 
        
        $('#ddiscount'+index).hide();
        $('#ttime'+index).hide(); 
     
    });  

// set this from local
    $('span.productTotal').each(function() {
        $(this).before("&euro;")
    }); 

// unit price
   $('.product p').each(function() {
       var $price = $(this).parents("div").data('price');
      $(this).before($price);
    }); 

	</script>
	
	
<?php include("footer.php"); ?>