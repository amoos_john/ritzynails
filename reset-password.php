<?php 
include_once('config/connection.php');
include_once('config/model.php');
include_once('config/functions.php');
include_once('config/constants.php');

if(!isset($_GET["token"]))
{
    echo "<script type='text/javascript'>window.location='index.php';</script>";
    exit();
}

if(isset($_POST["reset"]))
{
    $token=$_GET["token"];
    $pass = charEsc($_POST["password"]);
    $confirm_pass = charEsc($_POST["confirm_password"]);
    
    $sql= "select * from `reset_password` where token='{$token}'";
    $query = query($sql);
    if(num_rows($query) > 0)
    {
            $checkpass=ValidatePassword($pass);
            $checkconfimpass=ValidatePassword($confirm_pass);
            $row= fetch_array($query);
            $email=$row["email"];
            if(!empty($checkpass))
            {
                $_SESSION["error"] = $checkpass;   

            }
            else if(!empty($checkconfimpass))
            {
                $_SESSION["error"] = $checkconfimpass;    
                
            }
            else if($pass!=$confirm_pass)
            {
                 $_SESSION["error"] = "Your password and confirm password didn't matched!"; 
                 
            }
            else
            {
                $password= md5($pass);
                $mDate=date("Y-m-d H:i:s");
                $passquery=query("UPDATE `users` SET `password`='{$password}',
                `modified_date`='{$mDate}' WHERE `email`='{$email}'");
                if($passquery)
                {
                    query("Delete FROM reset_password WHERE `token`='{$token}'");
                    $_SESSION['success']="Password updated successfully!";
                }
                else
                {
                    $_SESSION['error']="Password cannot update!";
                }
            }
         
    }
    else
    {
         $_SESSION["error"] = "Email not found!";
    }
   
}

$pagetitle="Reset Password";
include("header.php");
?>


	<section class="services-page">
		<div class="container">
			 <div class="hed">
				  <h2>Reset Password <span></span></h2>
			 </div> 
			 
		
			<div class="fom fom-login col-sm-6 center">
                         <?php 
                            if(isset($_SESSION['success']))
                            {
                                ?>
                            <div class="alert alert-success fade in alert-dismissable" style="margin-top:18px;">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
                            <i class="fa fa-check-circle"></i> <?php echo $_SESSION['success']; ?> </div>
                                <?php
                               unset($_SESSION['success']);      
                                
                            }
                         ?>
                             <?php 
                            if(isset($_SESSION['error']))
                            {
                                ?>
                            <div class="alert alert-danger fade in alert-dismissable" style="margin-top:18px;">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
                            <i class="fa fa-times-circle"></i> <?php echo $_SESSION['error']; ?> </div>
                                <?php
                               unset($_SESSION['error']);      
                                
                            }
                         ?>
                            <form method="post" action="" name="forgot-form" id="forgot-form">
				<div class="form-group">
					<label>Password<em>*</em></label>
					<input type="password" name="password"  id="password" class="form-control"  placeholder="Password" maxlength="60" required/>
				</div>
				<div class="form-group">
					<label>Confirm Password<em>*</em></label>
					<input type="password" name="confirm_password"  id="confirm_password" class="form-control"  placeholder="Confirm Password" maxlength="60" required/>
				</div>
				
				<div class="form-group">
				
                                        <button type="submit" class="btn btn-book btn-lg " name="reset" id="reset">Reset</button>
				</div>
                            </form>    
			</div>
			
			
			 
		</div>
	</section>
	 

	
<?php include("footer.php"); ?>